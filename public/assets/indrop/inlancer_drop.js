/************************* Drag and drop ***************** */
    let dropAreas = document.getElementsByClassName("drop-area");
    
    var i;
    for (i = 0; i < dropAreas.length; i++) { 
      dropArea = dropAreas[i];
      /*Prevent default drag behaviors*/
      ;['dragenter', 'dragover', 'dragleave', 'drop'].forEach(eventName => {
        dropArea.addEventListener(eventName, preventDefaults, false) ;  
        document.body.addEventListener(eventName, preventDefaults, false);
      })

      /*Highlight drop area when item is dragged over it*/
      ;['dragenter', 'dragover'].forEach(eventName => {
        dropArea.addEventListener(eventName, highlight, false);  
      })

      ;['dragleave', 'drop'].forEach(eventName => {
        dropArea.addEventListener(eventName, unhighlight, false);
      })
 
      /*Handle dropped files*/
      dropArea.addEventListener('drop', handleDrop, false) 
    }
    
 
    function preventDefaults (e) {
      e.preventDefault();
      e.stopPropagation();
    }

    function highlight(e) {
      dropArea.classList.add('highlight');
    }

    function unhighlight(e) {
      dropArea.classList.remove('active');
      dropArea.classList.remove('highlight');
    }

    function handleDrop(e) {
      var dt = e.dataTransfer;
      var files = dt.files; 
      handleFiles(files,e.currentTarget.id,e.currentTarget.getAttribute('data-throwback'));  
    }
 
    function handleFiles(files,file_id,thFn) { 
      files = [...files];

      for(let file of files){
          previewFile(file,file_id);
          doUpload(file,file_id,thFn); 
      }

    }

    function previewFile(file,file_id) {
      if($('#imgPreview'+file_id).find('img').length <= 0){
        $('#imgPreview'+file_id).empty();
      }
      if($('#'+file_id).find('input').attr('multiple') != 'multiple'){
        $('#imgPreview'+file_id).empty();
      }
      
      if(file.type.split('/')[0]=='image') {
        var img = ' <img class="previewImage" src="'+URL.createObjectURL(file)+'" style="max-width: 100%;">'; 
      } else {
        var img= '<svg style="height: 130px;  object-fit: contain; margin: auto;" enable-background="new 0 0 600 600" inkscape:version="0.92.3 (2405546, 2018-03-11)" sodipodi:docname="general.svg" version="1.1" viewBox="0 0 600 600" xml:space="preserve" xmlns="http://www.w3.org/2000/svg" xmlns:inkscape="http://www.inkscape.org/namespaces/inkscape" xmlns:sodipodi="http://sodipodi.sourceforge.net/DTD/sodipodi-0.dtd"><sodipodi:namedview bordercolor="#666666" borderopacity="1" gridtolerance="10" guidetolerance="10" inkscape:current-layer="Layer_1" inkscape:cx="256" inkscape:cy="256" inkscape:pageopacity="0" inkscape:pageshadow="2" inkscape:window-height="667" inkscape:window-maximized="1" inkscape:window-width="1366" inkscape:window-x="0" inkscape:window-y="27" inkscape:zoom="0.4609375" objecttolerance="10" pagecolor="#ffffff" showgrid="false"></sodipodi:namedview><polygon transform="matrix(.93509 0 0 .93509 16.617 16.617)" points="337.21 4 441.88 108.67 441.88 508 63.24 508 63.24 4" fill="#fff" stroke-width="1.0694"></polygon><path d="m330.38 24.098 95.693 95.693v368.11h-346.58v-463.8h250.89m3.097-7.4807h-261.47v478.77h361.54v-378.69z" fill="#ccc" inkscape:connector-curvature="0"></path><g stroke-width="1.0694"><polygon transform="matrix(.93509 0 0 .93509 16.617 16.617)" points="338.84 0 445.88 107.02 338.84 107.02" fill="#999"></polygon><polygon transform="matrix(.93509 0 0 .93509 16.617 16.617)" points="22.04 293.01 489.96 293.01 407.53 422.9 22.04 422.9" fill="#333"></polygon><text x="235" y="375" text-anchor="middle" fill="#fff" font-size="70" style="font-weight: 600;">'+file.name.split('.').pop().toUpperCase()+'</text><polygon transform="matrix(.93509 0 0 .93509 16.617 16.617)" points="22.04 422.9 59.24 460.08 59.24 422.9" fill="#999"></polygon></g></svg><div>'+file.name.split('.').slice(0, -1).join('.').substring(0,15)+'.'+file.name.split('.').pop()+'</div>'
      } 
      var div = '<div class="col-5 text-center d-block mx-auto"><div style="padding: 5px;margin-bottom: 8px;">'+img+'</div><div class="progress progress-primary progress-da mb-3"><div class="progress-bar" id="progress'+file_id+'" role="progressbar" style="width: 0%" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div> </div></div>';
      
      $('#imgPreview'+file_id).append(div);   
    }

    function doUpload(file,file_id,thFn){  
      // console.log('fileID:'  +file_id); 
      // console.log(file);
      // console.log(thFn); 

      var iat = $('input[name="iat'+file_id+'"]').val(); 

      var fd = new FormData();    
      
      if (typeof(iat)!=='undefined' && iat.length!=0) { 
        fd.append('image_alt_tag',iat); 
      }else{
        fd.append('image_alt_tag',file.name); 
      }  
      if (typeof(file)!=='undefined' && file.length!=0) {
        fd.append('image_file', file,file.name); 
        fd.append('_token', csrf_token); 
         /* // console.log(fd.get('image_file'));*/
        $.ajax({
          xhr: function() { 
            var xhr = new window.XMLHttpRequest();
            xhr.upload.addEventListener("progress", function(evt) { 
              if (evt.lengthComputable) { 
                var percentComplete = (evt.loaded / evt.total) * 100; 
                $('[role="progressbar"]').attr('aria-valuenow',Math.round(percentComplete));
                $('[role="progressbar"]').css('width',Math.round(percentComplete)+'%');
              } 
            }, false); 
            return xhr; 
          },
          type      :   "POST",  
          data      :   fd, 
          contentType: false,      
          processData: false,
          url       :   APPLICATION_URL+'/common-image-upload',
          success   :   function(response)  {   
            /*$('body').css('pointer-events','none'); */
            console.log(response);   
            window[thFn](file_id,response);
            setTimeout(function(){
                if($('#imgPreviewi1').find('.previewImage').length == $('[data-image="i1"]').val().split(',').filter(x => x).length ){
                  $('[role="progressbar"]').css('background-color','#04a61f');
                  $('.drop-area').css({
                    border: '2px dashed #04a61f',
                    background: '#04a61f80'
                  });
                  audio_src = 'public/assets/success.mp3';
                  var audio = new Audio(audio_src);
                  audio.loop = false;
                  audio.play();
                  Swal.fire({
                    type: 'success',
                    title: 'Image Uploaded Successfully',
                    showConfirmButton: false,
                    timer: 1500
                  });
                }
            },1000);
            var json_data = response;
            if (json_data.status=="500"){ 
              Swal.fire({
                  type: 'warning',
                  title: 'Oops',
                  text: json_data.message,
                  showConfirmButton: false,
                  timer: 2000,
              });
            }  
          }, 
        });  
      }else{
        alert('Please Select valid file to upload');
      }  
    } 

    let uploadProgress = [];
    let progressBar = document.getElementById('progress-bar'); 
    function updateProgress(fileNumber, percent) {
      uploadProgress[fileNumber] = percent;
      let total = uploadProgress.reduce((tot, curr) => tot + curr, 0) / uploadProgress.length;
      console.debug('update', fileNumber, percent, total);
      progressBar.value = total;
    }

$('form').on('reset', function(e){
  $('#imgPreviewi1').html('<h3 style="color: #445dbe;font-size: 18px;line-height: 100px;">Click "Here" or drop your image here<span style="color:red">*</span></h3>');
  // $($('.drop-area').find('img')[0]).parent().parent().html('<h3 style="color: #445dbe;font-size: 18px;line-height: 100px;">Click "Here" or drop your image here<span style="color:red">*</span></h3>');
});