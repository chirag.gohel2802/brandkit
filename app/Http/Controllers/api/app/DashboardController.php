<?php

namespace App\Http\Controllers\api\app;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController; 
use Illuminate\Http\Request;
use Redirect;
 
use Illuminate\Support\Str;
/*Security & Session*/
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Session; 
 
/*Validation*/ 
use Illuminate\Support\Facades\Validator;
 
/*Loading Models Here*/  
use App\Models\api\UserModel; 
use App\Models\api\DashboardModel; 
use App\Models\Common; 

class DashboardController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    public function __construct(){ 
        $this->common_model = New Common; 
        $this->user_model   = New UserModel; 
        $this->dashboard_model   = New DashboardModel; 

        $this->table_name   = 'users';
    }
/*Dashboard*/
    public function index(Request $request)
        {   
            $fields=array(
                "user_token", 
            );  
            $data=array(); 
            foreach ($fields as $field) 
            {
                $data[$field]=$request->input($field);
            } 
             
            $rules = [  
                'user_token'    => ['required','string','max:255']  
            ];
            
            $messages = [
                
            ]; 
            $validator = Validator::make($request->all(), $rules, $messages);

            if($validator->fails()){
                return response()->json([
                    'message' =>\Arr::flatten($validator->errors()->toArray())[0],
                    'success' =>0,
                ], 200); 
            }
            $verifyToken    =  $this->user_model->authUser($data); 
            if(!empty($verifyToken)) { 
                $bannerList             = $this->dashboard_model->getBannerList();

                $data['dateCurrent']  = date('Y-m-d');   
                $data['endDate']      = date('Y-m-d',strtotime("+10 day", strtotime(date('Y-m-d'))));   
                $upcomingFestivalList = $this->dashboard_model->getUpcomingList($data);
                $dashboardList        = $this->dashboard_model->getDashboardList();
                
                $arr                   =   array();
                $array['success']      =   1;          
                $array['message']      =   __('auth.dashboard');  
                $array['data']         =   [
                                            'banner_list'       =>$bannerList['banner_list'],
                                            'dashboardList'     =>$dashboardList['dashboard_list']
                                            ];  
                return response()->json($array, 200);   
            }else{     
                $arr                    =   array();
                $array['success']       =   2;         
                $array['message']       =   __('auth.login_failed');   
                return response()->json($array, 200);   
            } 
        }
/*Custom Dashboard*/
    public function customDashboardList(Request $request)
        {
            $fields=array(
                "user_token", 
            );  
            $data=array(); 
            foreach ($fields as $field) 
            {
                $data[$field]=$request->input($field);
            } 
             
            $rules = [  
                'user_token'    => ['required','string','max:255']  
            ];
            
            $messages = [
                
            ]; 
            $validator = Validator::make($request->all(), $rules, $messages);

            if($validator->fails()){
                return response()->json([
                    'message' =>\Arr::flatten($validator->errors()->toArray())[0],
                    'success' =>0,
                ], 200); 
            }
            $verifyToken    =  $this->user_model->authUser($data); 
            if(!empty($verifyToken)) { 
                $dashboardList        = $this->dashboard_model->getCustomDashboardList();
                $arr                   =   array();
                $array['success']      =   1;          
                $array['message']      =   __('Custom Dashboard List fetched');  
                $array['data']         =   ['customDashboardList'=>$dashboardList['custom_dashboard_list'] ];  
                return response()->json($array, 200);   
            }else{     
                $arr                    =   array();
                $array['success']       =   2;         
                $array['message']       =   __('auth.login_failed');   
                return response()->json($array, 200);   
            }
        }
    public function help_support(Request $request)
        {
            $fields=array(
                "user_token", 
            );  
            $data=array(); 
            foreach ($fields as $field) 
            {
                $data[$field]=$request->input($field);
            } 
             
            $rules = [  
                'user_token'    => ['required','string','max:255']  
            ];
            
            $messages = []; 
            $validator = Validator::make($request->all(), $rules, $messages);

            if($validator->fails()){
                return response()->json([
                    'message' =>\Arr::flatten($validator->errors()->toArray())[0],
                    'success' =>0,
                ], 200); 
            }
            $verifyToken    =  $this->user_model->authUser($data); 
            if(!empty($verifyToken)) { 
                $fields=['phone_no1','email1','whatsapp_contact'];
                    foreach ($fields as $key => $value) {
                        $data[$value]  = $this->dashboard_model->getHelpSupportList($value); 
                    }
                    unset($data['user_token']);
                $arr                   =   array();
                $array['success']      =   1;          
                $array['message']      =   __('auth.help_fetched');  
                $array['data']         =   ['help_support'=>$data];  
                return response()->json($array, 200);   
            }else{     
                $arr                    =   array();
                $array['success']       =   2;         
                $array['message']       =   __('auth.login_failed');   
                return response()->json($array, 200);   
            }
        }
    public function general(Request $request)
        {
            $fields=array(
                "user_token", 
            );  
            $data=array(); 
            foreach ($fields as $field) 
            {
                $data[$field]=$request->input($field);
            } 
             
            $rules = [  
                'user_token'    => ['required','string','max:255']  
            ];
            
            $messages = []; 
            $validator = Validator::make($request->all(), $rules, $messages);

            if($validator->fails()){
                return response()->json([
                    'message' =>\Arr::flatten($validator->errors()->toArray())[0],
                    'success' =>0,
                ], 200); 
            }
            $verifyToken    =  $this->user_model->authUser($data); 
            if(!empty($verifyToken)) { 
                
                $data['general'] = [
                        'terms-conditions'=> url('/terms-conditions'), 
                        'privacy-policy'=> url('/privacy-policy'),
                      ];
                $how_it_work  = $this->dashboard_model->getHelpSupportList('how_it_work')[0]->setting_value; 
                if(!empty($how_it_work)){
                    $data['general']['how-it-work'] =$how_it_work;
                }

                $arr                   =   array();
                $array['success']      =   1;          
                $array['message']      =   __('auth.help_fetched');  
                $array['data']         =   ['general'=>$data['general']];  
                return response()->json($array, 200);   
            }else{     
                $arr                    =   array();
                $array['success']       =   2;         
                $array['message']       =   __('auth.login_failed');   
                return response()->json($array, 200);   
            }
        }    
    /*public function banners(Request $request)
        {   
            $fields=array(
                "user_token", 
            );  
            $data=array(); 
            foreach ($fields as $field) 
            {
                $data[$field]=$request->input($field);
            } 
             
            $rules = [  
                'user_token'    => ['required','string','max:255']  
            ];
            
            $messages = []; 
            $validator = Validator::make($request->all(), $rules, $messages);

            if($validator->fails()){
                return response()->json([
                    'message' =>\Arr::flatten($validator->errors()->toArray())[0],
                    'success' =>0,
                ], 200); 
            }
            $verifyToken    =  $this->user_model->authUser($data); 
            if(!empty($verifyToken)) { 
                //$data_where=$verifyToken['user_id'];
                $bannerList = $this->dashboard_model->getBannerList();

                $arr                    =   array();
                $array['success']       =   1;          
                $array['message']       =   __('Banner list has been fetched.');  
                $array['data']          =   $bannerList;  
                return response()->json($array, 200);   
            }else{     
                $arr                    =   array();
                $array['success']       =   2;         
                $array['message']       =   __('auth.login_failed');   
                return response()->json($array, 200);   
            } 
        }*/
   /* public function upcoming_festival(Request $request)
        {   
            $fields=array(
                "user_token",
                "post_type" 
            );  
            $data=array(); 
            foreach ($fields as $field) 
            {
                $data[$field]=$request->input($field);
            } 
             
            $rules = [  
                'user_token'    => ['required','string','max:255'],  
                "post_type"     => ['string','max:255','nullable'], 
            ];
            
            $messages = []; 
            $validator = Validator::make($request->all(), $rules, $messages);

            if($validator->fails()){
                return response()->json([
                    'message' =>\Arr::flatten($validator->errors()->toArray())[0],
                    'success' =>0,
                ], 200); 
            }
            $verifyToken    =  $this->user_model->authUser($data); 
            if(!empty($verifyToken)) { 
                $data['dateCurrent']  = date('Y-m-d');   
                $data['endDate']      = date('Y-m-d',strtotime("+10 day", strtotime(date('Y-m-d'))));   
                
                $upcomingFestivalList = $this->dashboard_model->getUpcomingList($data);
                $arr                    =   array();
                $array['success']       =   1;          
                $array['message']       =   __('Upcoming Festival list has been fetched.');  
                $array['data']          =   $upcomingFestivalList;  
                return response()->json($array, 200);

            }else{     
                $arr                    =   array();
                $array['success']       =   2;         
                $array['message']       =   __('auth.login_failed');   
                return response()->json($array, 200);   
            } 
        }*/
    /*public function business_category(Request $request)
        {   
            $fields=array(
                "user_token",
                "category_type" 
            );  
            $data=array(); 
            foreach ($fields as $field) 
            {
                $data[$field]=$request->input($field);
            } 
             
            $rules = [  
                'user_token'        => ['required','string','max:255'],  
                "category_type"     => ['integer','nullable'], 
            ];
            
            $messages = []; 
            $validator = Validator::make($request->all(), $rules, $messages);

            if($validator->fails()){
                return response()->json([
                    'message' =>\Arr::flatten($validator->errors()->toArray())[0],
                    'success' =>0,
                ], 200); 
            }
            $verifyToken    =  $this->user_model->authUser($data); 
            if(!empty($verifyToken)) { 
                $data['category_parent_id'] = 1;
                $categoryList = $this->dashboard_model->getCategoryList($data);
                $arr                    =   array();
                $array['success']       =   1;          
                $array['message']       =   __('Business Category list has been fetched.');  
                $array['data']          =   $categoryList;  
                return response()->json($array, 200);

            }else{     
                $arr                    =   array();
                $array['success']       =   2;         
                $array['message']       =   __('auth.login_failed');   
                return response()->json($array, 200);   
            } 
        }*/
    /*public function general_category(Request $request)
        {   
            $fields=array(
                "user_token",
                "category_type" 
            );  
            $data=array(); 
            foreach ($fields as $field) 
            {
                $data[$field]=$request->input($field);
            } 
             
            $rules = [  
                'user_token'        => ['required','string','max:255'],  
                "category_type"     => ['integer','nullable'], 
            ];
            
            $messages = []; 
            $validator = Validator::make($request->all(), $rules, $messages);

            if($validator->fails()){
                return response()->json([
                    'message' =>\Arr::flatten($validator->errors()->toArray())[0],
                    'success' =>0,
                ], 200); 
            }
            $verifyToken    =  $this->user_model->authUser($data); 
            if(!empty($verifyToken)) { 
                $data['category_parent_id'] = 2;
                $categoryList = $this->dashboard_model->getCategoryList($data);
                $arr                    =   array();
                $array['success']       =   1;          
                $array['message']       =   __('General Category list has been fetched.');  
                $array['data']          =   $categoryList;  
                return response()->json($array, 200);

            }else{     
                $arr                    =   array();
                $array['success']       =   2;         
                $array['message']       =   __('auth.login_failed');   
                return response()->json($array, 200);   
            } 
        }
*/
}
