<?php

namespace App\Http\Controllers\api\app;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController; 
use Illuminate\Http\Request;
use Redirect;

use Illuminate\Support\Str; 
/*Security & Session*/
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash; 
use Session;

/*Validation*/ 
use Illuminate\Support\Facades\Validator;
 
/*Loading Models Here*/  
use App\Models\api\UserModel; 
use App\Models\Common; 
Use Image;


class UserController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    public function __construct(){ 
        $this->common_model = New Common; 
        $this->user_model   = New UserModel; 
        $this->table_name   = 'users';
    }
 /*Send & Verify OTP*/
    public function sendOtp(Request $request)
        {
            $common_model   =   new Common();
            $fields=array(
                "user_phone_no",
            );
            /*getting post data in data variable*/
            $data=array();  
            foreach($fields as $field)  {
                $data[$field]=$request->input($field);
            }  

            $rules = [  
                'user_phone_no'             => ['required','string','min:10','max:12'],
            ];
            
            $messages = [
                'user_phone_no.required'  => __("auth.phone_required")
            ]; 
            $validator = Validator::make($request->all(), $rules, $messages);

            if($validator->fails()){
                return response()->json([
                    'message' =>\Arr::flatten($validator->errors()->toArray())[0],
                    'success' =>0,
                ], 200); 
            } 
            $user_data=$this->user_model->getUserData($data);   
            $otp =  rand(10000,99999);
            if (!empty($user_data)) {      
                $otpUpdate=[
                    'user_otp'          =>$otp,  
                ];  
                $common_model->set_fields($otpUpdate);
                $common_model->update_my_data('users',['user_phone_no'=>$data['user_phone_no']]); 
                $return_data            =   ['user_otp'=>$otp];
                $arr                    =   array();
                $array['success']       =   1;          
                $array['message']       =   __('auth.otp_success');  
                $array['data']          =   $return_data;  
                return response()->json($array, 200);  
                
            }else{    
                $userSave=[
                    'user_phone_no'         =>$data['user_phone_no'], 
                    'user_otp'              =>$otp,  
                    'user_status'           =>1,  
                    /* If New Registration Then Status will be 2, once profile has been update status will be 1 (Active) */
                ];  
                $common_model->set_fields($userSave); 
                $userId = $common_model->save_my_data('users');
                
                $return_data            =   ['user_otp'=>$otp];
                $arr                    =   array();
                $array['success']       =   1;          
                $array['message']       =   __('auth.otp_success');  
                $array['data']          =   $return_data;  
                return response()->json($array, 200);  
            } 
        }
    public function verifyOtp(Request $request)
        {
            $common_model   =   new Common();
            $fields=array(
                "user_phone_no", 
                "user_otp", 
                "is_verified", 
                "device",
                "user_fcm_token", 
            );  
            /*getting post data in data variable*/
            $data=array();  
            foreach($fields as $field)  {
                $data[$field]=$request->input($field);
            }  

            $rules = [  
                'user_phone_no'       => ['required','string','min:10','max:12'],
                // 'user_otp'            => ['required','string'],
                'device'              => ['required'],
            ];
            
            $messages = [
                'user_phone_no.required'  => __("auth.phone_required"),
                // 'user_otp.required'  => __("auth.otp_required"),
            ]; 
            $validator = Validator::make($request->all(), $rules, $messages);

            if($validator->fails()){
                return response()->json([
                    'message' =>\Arr::flatten($validator->errors()->toArray())[0],
                    'success' =>0,
                ], 200); 
            }
            $user_data=$this->user_model->getUserData($data); 
            $otp =  rand(10000,99999);
            // dd($user_data);
            if (!empty($user_data) &&  ( ( empty($data['user_otp']) && $data['is_verified'] == 1) || $data['user_otp']==$user_data['user_otp'])) {
                $userRegistered = 1;
                if ($user_data['user_status']==2) { 
                    $userRegistered = 0;
                }

                $session_expiry_timestamp = date('Y-m-d H:i:s',strtotime("+90 day", strtotime(date('Y-m-d H:i:s'))));   
                $session_token      = md5(time().rand(10000,99999));     
                
                $returnData=[ 
                    'user_token'        =>  $session_token /*1-Registered, 0-Not Registered*/, 
                    'user_name'         =>  $user_data['user_name'],
                    'user_phone_no'     =>  $user_data['user_phone_no'], 
                    'user_country_id'   =>  $user_data['user_country_id'], 
                    'user_state_id'     =>  $user_data['user_state_id'], 
                    'user_city_id'      =>  $user_data['user_city_id'], 
                    'user_registered'   =>  $userRegistered /*1-Registered, 0-Not Registered*/,
                    'user_fcm_token'    =>$data['user_fcm_token'],
                ]; 
                   
                $save_session   =   [
                    'session_expiry_timestamp'      =>$session_expiry_timestamp,
                    'session_fcm_token'             =>$data['user_fcm_token'],
                    'session_token'                 =>$session_token,
                    'session_user_id'               =>$user_data['user_id'],    
                    'session_user_ip_address'       =>$request->ip(),  
                    'session_fcm_token'             =>$data['user_fcm_token'],
                ]; 

                $common_model->set_fields($save_session);
                $session_id =$common_model->save_my_data('user_login_sessions');

                if ($session_id>0) { 
                    $userUpdate=[
                        'user_otp'          =>  $otp,  
                        'user_session_id'   =>  $session_id,
                        'user_fcm_token'    =>  $data['user_fcm_token']
                    ];  
                    $common_model->set_fields($userUpdate);
                    $common_model->update_my_data('users',['user_id'  => $user_data['user_id']]);  

                    $arr                    =   array();
                    $array['success']       =   1;          
                    $array['message']       =   __('auth.login_success');
                    $array['data']          =   $returnData;  
                    return response()->json($array, 200);  
                }else{
                    $arr                    =   array();
                    $array['success']       =   0;          
                    $array['message']       =   __('auth.login_failed');   
                    return response()->json($array, 200);  
                } 
                
            }else{     
                $arr                    =   array();
                $array['success']       =   0;          
                $array['message']       =    __('auth.otp_valid');   
                return response()->json($array, 200);  
            } 
        }    
    public function signOut(Request $request)
        {   
            $fields=array(
                "user_token", 
            );  
            $data=array(); 
            foreach ($fields as $field) 
            {
                $data[$field]=$request->input($field);
            } 
             
            $rules = [  
                'user_token'    => ['required','string','max:255']  
            ];
            
            $messages = [
                
            ]; 
            $validator = Validator::make($request->all(), $rules, $messages);

            if($validator->fails()){
                return response()->json([
                    'message' =>\Arr::flatten($validator->errors()->toArray())[0],
                    'success' =>0,
                ], 200); 
            }

            $verifyToken    =  $this->user_model->authUser($data); 

            if(!empty($verifyToken)) {        
                $common_model   =   new Common(); 
                $common_model->set_fields(['session_status'=>0]);
                $common_model->update_my_data('user_login_sessions',['session_user_id'=>$verifyToken['user_id']]);
                
                $arr                    =   array();
                $array['success']       =   1;          
                $array['message']       =   __('auth.logout_success');  
                $array['data']          =   [];  
                return response()->json($array, 200);   
            }else{     
                $arr                    =   array();
                $array['success']       =   2;         
                $array['message']       =   __('auth.invalid_access');   
                return response()->json($array, 200);   
            } 
        }
/*Update User Profile*/
    public function updateProfile(Request $request)
        {   
            $fields=array(   
                "user_token",
                "user_name",
                "user_email",
            );  
            
            /*getting post data in data variable*/
            $data=array();  
            foreach($fields as $field)  {
                $data[$field]=$request->input($field);
            }  

            $rules = [  
                'user_token'                => ['required','string','max:255'], 
                'user_name'                 => ['required','string','max:255'],
                // 'user_email'                => ['required','string','email','max:255'],   
            ];
            
            $messages = [
                'user_email.required'   => __("auth.email_required"),
                'user_email.email'      => __("auth.valid_email_address"),
                'user_email.string'     => __("auth.valid_email_address")
            ]; 
            $validator = Validator::make($request->all(), $rules, $messages);

            if($validator->fails()){
                return response()->json([
                    'message' =>\Arr::flatten($validator->errors()->toArray())[0],
                    'success' =>0,
                ], 200); 
            }



            $userAuth =$this->user_model->authUser($data);    
            if (!empty($userAuth)) { 
                
                if (!empty($user_data['user_profile_image'])) {  
                        $image_url  =    asset('assets/upload/users/thumbnail/'.$user_data['user_profile_image']);
                }else{
                    $image_url  =    asset('festival_inlancer/img/avatar/avatar-1.png');
                }      
                   

                $user_data  = [
                    'user_name'         => $data['user_name'],
                    'user_email'        => $data['user_email'],   
                ];
                if($request->file('user_profile_image')) {
                    $rand=rand(1111,9999);
                    $comman_time = time();
                    $hash1=md5($comman_time*$rand);
                    $originalImage= $request->file('user_profile_image');  
                    $original_name = pathinfo($originalImage->getClientOriginalName(), PATHINFO_FILENAME);  
                    $image_new_name=$hash1.Str::slug($originalImage->getClientOriginalName(),'-').'.'.$originalImage->extension();
                     
                    $originalImage      = $request->file('user_profile_image');
                    $thumbnailImage     = Image::make($originalImage->getRealPath());
                    $thumbnailPath      = public_path().'/assets/upload/users/thumbnail/';
                    $originalPath       = public_path().'/assets/upload/users/original/';  

                    $thumbnailImage->save($originalPath.$image_new_name); 
                    $thumbnailImage->resize(200, null, function ($constraint) {
                        $constraint->aspectRatio();
                    });
                    $thumbnailImage->save($thumbnailPath.$image_new_name);   

                    $user_data['user_profile_image']=$image_new_name;
                    /*$upload_image = [
                        'image_url'         =>  'assets/upload/users/original/'.$image_new_name,
                        'image_file_name'   =>  $original_name,
                        'image_name'        =>  $image_new_name ,
                        'image_details'     =>  'Post image',
                        'image_status'      =>  0,
                        'image_alt_tag'     =>  $data['post_name'],
                    ];
                    $image_id=\DB::table('images')->insertGetId($upload_image);*/ 
                }

                $where      = ['user_id'=>$userAuth['user_id']];
                $isUpdated  = \DB::table($this->table_name)->where($where)->update($user_data);
                if ($isUpdated==1) {  
                    $user_detail = $this->user_model->userInfo($where);
                    $arr                =   array();
                    $arr['success']     =   1;          
                    $arr['message']     =   __('auth.profile_updated'); 
                    $arr['data']        =   $user_detail;  
                    return response()->json($arr, 200); 
                }else{
                    $arr                =   array();
                    $arr['success']     =   0;          
                    $arr['message']     =   __('auth.profile_no_change');  
                    $arr['data']        =   ['user_details'=>$data];  
                    return response()->json($arr, 200); 
                }  
            }else{
                $arr                    =   array();
                $array['success']       =   2;          
                $array['message']       =   __('auth.invalid_access');  
                $array['data']          =   [];  
                return response()->json($array, 200);
            }  
        }         


}
