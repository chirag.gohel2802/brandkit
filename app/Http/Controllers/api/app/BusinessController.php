<?php

namespace App\Http\Controllers\api\app;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController; 
use Illuminate\Http\Request;
use Redirect;

use Illuminate\Support\Str;
/*Security & Session*/
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Session;

use Image;


/*Validation*/ 
use Illuminate\Support\Facades\Validator; 
 
/*Loading Models Here*/  
use App\Models\api\UserModel; 
use App\Models\api\BusinessModel; 
use App\Models\Common; 

class BusinessController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    public function __construct(){ 
        $this->common_model     = New Common; 
        $this->user_model       = New UserModel; 
        $this->business_model   = New BusinessModel; 
        $this->table_name       = 'business';
    }
    /*Category List*/
    public function categoryList(Request $request) // business List 
        {
            $data = $request->all();
            $rules = [  
                'user_token'        => ['required','string','max:255'],
                "page"              => ['required','integer','between:1,300000'],
                "search"            => ['string','max:255','nullable'],  
            ]; 
            
            $messages = []; 
            $validator = Validator::make($request->all(), $rules, $messages);  
            if($validator->fails()){
                return response()->json([
                    'message' =>\Arr::flatten($validator->errors()->toArray())[0],
                    'success' =>0,
                ], 200); 
            }
            $userAuth =$this->user_model->authUser($data);    
            if (!empty($userAuth)) { 
                $data['user_id']=$userAuth['user_id'];
                $businessList = $this->business_model->getBusinessCategoryList($data); 
                unset($userAuth['user_id']);
                unset($userAuth['session_id']);
                unset($userAuth['session_user_device_type']);
                unset($userAuth['session_expiry_timestamp']);
                unset($userAuth['session_status']);
                unset($userAuth['session_user_ip_address']);
                $arr                    =   array();
                $array['success']       =   1;          
                $array['message']       =   __('business.business_category_fetched');  
                $array['data']          =   $businessList;  
                return response()->json($array, 200);
            }else{
                $arr                    =   array();
                $array['success']       =   2;          
                $array['message']       =   __('auth.invalid_access');  
                $array['data']          =   [];  
                return response()->json($array, 200);
            }
        }
    public function myBusiness(Request $request) // business List 
        {
            $data = $request->all();
            $rules = [  
                'user_token'        => ['required','string','max:255'],
                "page"              => ['required','integer','between:1,300000'],
                "search"            => ['string','max:255','nullable'], 
                "business_category" => ['integer','between:1,1111111111','nullable'],
            ];
            
            $messages = []; 
            $validator = Validator::make($request->all(), $rules, $messages);  
            if($validator->fails()){
                return response()->json([
                    'message' =>\Arr::flatten($validator->errors()->toArray())[0],
                    'success' =>0,
                ], 200); 
            }
            $userAuth =$this->user_model->authUser($data);    
            if (!empty($userAuth)) { 
                $data['user_id']=$userAuth['user_id'];
                $businessList = $this->business_model->getMyBusinessList($data); 
                unset($userAuth['user_id']);
                unset($userAuth['session_id']);
                unset($userAuth['session_user_device_type']);
                unset($userAuth['session_expiry_timestamp']);
                unset($userAuth['session_status']);
                unset($userAuth['session_user_ip_address']);
                $arr                    =   array();
                $array['success']       =   1;          
                $array['message']       =   __('business.business_fetched');  
                $array['data']          =   $businessList;  
                return response()->json($array, 200);
            }else{
                $arr                    =   array();
                $array['success']       =   2;          
                $array['message']       =   __('auth.invalid_access');  
                $array['data']          =   [];  
                return response()->json($array, 200);
            }
        }
    public function saveBusiness(Request $request)
        {
            $data = $request->all();
            $rules = [  
                'user_token'            => ['required','string','max:255'],
                //"business_category"     => ['required','integer','between:1,1111111111'],
                "business_name"         => ['required','string','max:255'],
                "business_phone_no"     => ['required','string','min:10','max:12'], 
            ];
            if(!empty($data['business_email'])){
                $rules[ "business_email"]        = ['string','email','max:255','nullable'] ;
            }
            $messages = [
                /*'business_email.required'   => __("auth.email_required"),
                'business_email.email'      => __("auth.valid_email_address"),
                'business_email.string'     => __("auth.valid_email_address")*/
            ]; 
            $validator = Validator::make($request->all(), $rules, $messages);  
            if($validator->fails()){
                return response()->json([
                    'message' =>\Arr::flatten($validator->errors()->toArray())[0],
                    'success' =>0,
                ], 200); 
            }
            $userAuth =$this->user_model->authUser($data);    
            if (!empty($userAuth)) { 
                $businessData = [
                    'business_user_id'              => $userAuth['user_id'],
                    'created_by'                    => $userAuth['user_id'],
                    'business_name'                 => $data['business_name'],
                    'business_phone_no'             => $data['business_phone_no'],
                    'business_secondary_phone_no'   => $data['business_secondary_phone_no'],
                ];
                if(!empty($data['business_category'])){
                   $businessData['business_category'] = $data['business_category'];
                }else{
                   $businessData['business_category'] = 75;
                }
                if(!empty($data['business_email'])){
                    $businessData['business_email'] = $data['business_email'];
                }
                if(!empty($data['business_website'])){
                    $businessData['business_website'] = $data['business_website'];
                }
                if(!empty($data['business_address'])){
                    $businessData['business_address'] = $data['business_address'];
                }
                if($request->file('business_logo')) {
                    $rand=rand(1111,9999);
                    $comman_time = time();
                    $hash1=md5($comman_time*$rand);
                    $originalImage= $request->file('business_logo');  
                    $original_name = pathinfo($originalImage->getClientOriginalName(), PATHINFO_FILENAME);  
                    $image_new_name=$hash1.Str::slug($originalImage->getClientOriginalName(),'-').'.'.$originalImage->extension();
                     
                    $originalImage      = $request->file('business_logo'); 
                    $thumbnailImage     = Image::make($originalImage->getRealPath());
                    $thumbnailPath      = public_path().'/assets/upload/images/thumb/';
                    $originalPath       = public_path().'/assets/upload/images/original/';  

                    $thumbnailImage->save($originalPath.$image_new_name); 
                    $thumbnailImage->resize(500, null, function ($constraint) {
                        $constraint->aspectRatio();
                    });
                    $thumbnailImage->save($thumbnailPath.$image_new_name);   

                    $upload_image = [
                        'image_url'         =>  'assets/upload/images/original/'.$image_new_name,
                        'image_file_name'   =>  $original_name,
                        'image_name'        =>  $image_new_name ,
                        'image_details'     =>  'Business image',
                        'image_status'      =>  0,
                        'image_alt_tag'     =>  $data['business_name'],
                    ];       
                    $image_id=\DB::table('images')->insertGetId($upload_image); 
                    $businessData['business_logo']  = $image_id;

                }/*else{
                    if(empty($data['business_id'])){
                        return response()->json([
                            'message' =>__('business.business_logo_required'),
                            'success' => 0,
                        ], 200); 

                    }
                }*/ 
                /*End image*/
               
                if(empty($data['business_id']) ){
                    $businessId = \DB::table($this->table_name)->insertGetId($businessData);
                    if($businessId>0){
                        $arr                    =   array();
                        $array['success']       =   1;          
                        $array['message']       =   __('business.business_saved');  
                        $array['data']          =   ['business_id'   =>  $businessId];  
                        return response()->json($array, 200); 
                    }else{
                        $arr                    =   array();
                        $array['success']       =   0;          
                        $array['message']       =   __('business.business_saving_failed'); 
                        return response()->json($array, 200);
                    }
                }else{
                    if(empty($request->file('business_logo')) ){
                        $businessDetails = $this->business_model->getBusinessDetail(['business_id'=>$data['business_id'],'user_id'=>$userAuth['user_id']]);
                        $businessData['business_logo']  = $businessDetails['business_detail']->image_id;
                    }
                    $where      = ['business_id'=>$data['business_id']];
                    \DB::table($this->table_name)->where($where)->update($businessData);
                    $arr                    =   array();
                    $array['success']       =   1;          
                    $array['message']       =   __('business.business_updated');  
                    $array['data']          =   ['business_id'   =>  $data['business_id']];  
                    return response()->json($array, 200); 
                    
                }

                
            }else{
                $arr                    =   array();
                $array['success']       =   2;          
                $array['message']       =   __('auth.invalid_access');  
                $array['data']          =   [];  
                return response()->json($array, 200);
            }
        }
    public function removeBusiness(Request $request)
        {
            $data = $request->all();
            $rules = [  
                'user_token'      => ['required','string','max:255'],
                "business_id"     => ['required','integer','between:1,1111111111'], 
            ];
            $messages = []; 
            $validator = Validator::make($request->all(), $rules, $messages);  
            if($validator->fails()){
                return response()->json([
                    'message' =>\Arr::flatten($validator->errors()->toArray())[0],
                    'success' =>0,
                ], 200); 
            }
            $userAuth =$this->user_model->authUser($data);    
            if (!empty($userAuth)) {
                $where = ['business_id'=>$data['business_id'],'business_user_id'=>$userAuth['user_id']];
                \DB::table($this->table_name)->where($where)->update(['is_delete'=>1]);
                $arr                    =   array();
                $array['success']       =   1;          
                $array['message']       =   __('business.business_delete');  
                $array['data']          =   [];  
                return response()->json($array, 200); 
            }else{
                $arr                    =   array();
                $array['success']       =   2;          
                $array['message']       =   __('auth.invalid_access');  
                $array['data']          =   [];  
                return response()->json($array, 200);
            }
        }    


}
