<?php

namespace App\Http\Controllers\api\app;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController; 
use Illuminate\Http\Request; 
use Redirect;
   
use Illuminate\Support\Str;
/*Security & Session*/ 
use Illuminate\Support\Facades\Auth; 
use Illuminate\Support\Facades\Hash;
use Session;

use Image;


/*Validation*/ 
use Illuminate\Support\Facades\Validator;
 
/*Loading Models Here*/  
use App\Models\api\UserModel; 
use App\Models\api\CategoryModel; 
use App\Models\Common; 

class CategoryController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    public function __construct(){ 
        $this->common_model     = New Common; 
        $this->user_model       = New UserModel; 
        $this->category_model   = New CategoryModel; 
        $this->table_name       = 'category';
    }
/*Category List*/
    public function categoryList(Request $request) // category List 
        {
            $data = $request->all();
            $rules = [  
                'user_token'            => ['required','string','max:255'],
                "page"                  => ['required','integer','between:1,300000'],
                "search"                => ['string','max:255','nullable'], 
                "category_parent_id"    => ['integer','between:1,1111111111','nullable'],
                "category_type"         => ['integer','between:1,1111111111','nullable'],
            ];
            
            $messages = []; 
            $validator = Validator::make($request->all(), $rules, $messages);  
            if($validator->fails()){
                return response()->json([
                    'message' =>\Arr::flatten($validator->errors()->toArray())[0],
                    'success' =>0,
                ], 200); 
            }
            $userAuth =$this->user_model->authUser($data);    
            if (!empty($userAuth)) { 
                $data['user_id']=$userAuth['user_id'];
                if(!empty($data['category_parent_id'])){
                    \DB::table('user_track')->insert(['track_user_id'=>$userAuth['user_id'],'track_category_id'=>$data['category_parent_id'] ]);
                }

                $categoryList = $this->category_model->getCategoryList($data); 
                unset($userAuth['user_id']);
                unset($userAuth['session_id']);
                unset($userAuth['session_user_device_type']);
                unset($userAuth['session_expiry_timestamp']);
                unset($userAuth['session_status']);
                unset($userAuth['session_user_ip_address']);
                $arr                    =   array();
                $array['success']       =   1;          
                $array['message']       =   __('category.category_fetched');  
                $array['data']          =   $categoryList;  
                return response()->json($array, 200);
            }else{ 
                $arr                    =   array();
                $array['success']       =   2;          
                $array['message']       =   __('auth.invalid_access');  
                $array['data']          =   [];  
                return response()->json($array, 200);
            }
        }
/*Category List Search*/    
    public function categoryListSearch(Request $request) // category List 
        {
            $data = $request->all();
            $rules = [  
                'user_token'            => ['required','string','max:255'],
                "page"                  => ['required','integer','between:1,300000'],
                "search"                => ['required','string','max:255'], 
                "category_type"         => ['integer','between:1,1111111111','nullable'],
            ];
            $messages = []; 
            $validator = Validator::make($request->all(), $rules, $messages);  
            if($validator->fails()){
                return response()->json([
                    'message' =>\Arr::flatten($validator->errors()->toArray())[0],
                    'success' =>0,
                ], 200); 
            }
            $userAuth =$this->user_model->authUser($data);    
            if (!empty($userAuth)) { 
                $data['user_id']=$userAuth['user_id'];

                // \DB::table('user_track')->insert(['track_user_id'=>$userAuth['user_id'] ]);

                $categoryList = $this->category_model->getCategorySearchList($data); 
                unset($userAuth['user_id']);
                unset($userAuth['session_id']);
                unset($userAuth['session_user_device_type']);
                unset($userAuth['session_expiry_timestamp']);
                unset($userAuth['session_status']);
                unset($userAuth['session_user_ip_address']);
                $arr                    =   array();
                $array['success']       =   1;          
                $array['message']       =   __('category.category_fetched');  
                $array['data']          =   $categoryList;  
                return response()->json($array, 200);
            }else{ 
                $arr                    =   array();
                $array['success']       =   2;          
                $array['message']       =   __('auth.invalid_access');  
                $array['data']          =   [];  
                return response()->json($array, 200);
            }
        }
    public function categoryListCustom(Request $request) // category List 
        {
            $data = $request->all();
            $rules = [  
                'user_token'            => ['required','string','max:255'],
                "page"                  => ['required','integer','between:1,300000'],
                "search"                => ['string','max:255','nullable'], 
            ];
            $messages = []; 
            $validator = Validator::make($request->all(), $rules, $messages);  
            if($validator->fails()){
                return response()->json([
                    'message' =>\Arr::flatten($validator->errors()->toArray())[0],
                    'success' =>0,
                ], 200); 
            }
            $userAuth =$this->user_model->authUser($data);    
            if (!empty($userAuth)) { 
                $data['user_id']=$userAuth['user_id'];

                
                $categoryList = $this->category_model->getCategoryCustomList($data); 
                unset($userAuth['user_id']);
                unset($userAuth['session_id']);
                unset($userAuth['session_user_device_type']);
                unset($userAuth['session_expiry_timestamp']);
                unset($userAuth['session_status']);
                unset($userAuth['session_user_ip_address']);
                $arr                    =   array();
                $array['success']       =   1;          
                $array['message']       =   __('Custom Category list has been fetched ');  
                $array['data']          =   $categoryList;  
                return response()->json($array, 200);
            }else{ 
                $arr                    =   array();
                $array['success']       =   2;          
                $array['message']       =   __('auth.invalid_access');  
                $array['data']          =   [];  
                return response()->json($array, 200);
            }
        }
    /*public function saveCategory(Request $request)
        {
            $data = $request->all();
            $rules = [  
                'user_token'            => ['required','string','max:255'],
                "category_name"         => ['required','string','max:255'],
                "category_parent_id"    => ['integer','between:1,1111111111','nullable'], 
               
            ];
            
            $messages = []; 
            $validator = Validator::make($request->all(), $rules, $messages);  
            if($validator->fails()){
                return response()->json([
                    'message' =>\Arr::flatten($validator->errors()->toArray())[0],
                    'success' =>0,
                ], 200); 
            }

            $userAuth =$this->user_model->authUser($data);    
            if (!empty($userAuth)) { 
                 $categoryData = [
                    'category_name'             => $data['category_name'],
                    'category_slug'             => strtolower(trim(preg_replace('/[^A-Za-z0-9-]+/', '-', $data['category_name']))),
                    'category_description'      => $data['category_description'], 
                    'category_type'             => 1, 
                    'category_status'           => $data['category_status'], 
                    'created_by'                => $userAuth['user_id'],

                ];
                if($data['category_parent_id']!='' && !empty($data['category_parent_id'])){
                    $categoryData['category_parent_id'] = $data['category_parent_id'];
                }else{
                    $categoryData['category_parent_id'] = 0;
                }
                //image
                if($request->file('category_image')) {
                    $rand=rand(1111,9999);
                    $comman_time = time();
                    $hash1=md5($comman_time*$rand);
                    $originalImage= $request->file('category_image');  
                    $original_name = pathinfo($originalImage->getClientOriginalName(), PATHINFO_FILENAME);  
                    $image_new_name=$hash1.Str::slug($originalImage->getClientOriginalName(),'-').'.'.$originalImage->extension();
                     
                    $originalImage      = $request->file('category_image');
                    $thumbnailImage     = Image::make($originalImage->getRealPath());
                    $thumbnailPath      = public_path().'/assets/upload/images/thumb/';
                    $originalPath       = public_path().'/assets/upload/images/original/';  

                    $thumbnailImage->save($originalPath.$image_new_name); 
                    $thumbnailImage->resize(500, null, function ($constraint) {
                        $constraint->aspectRatio();
                    });
                    $thumbnailImage->save($thumbnailPath.$image_new_name);   

                    $upload_image = [
                        'image_url'         =>  'assets/upload/images/original/'.$image_new_name,
                        'image_file_name'   =>  $original_name,
                        'image_name'        =>  $image_new_name,
                        'image_details'     =>  'Category image',
                        'image_status'      =>  0,
                        'image_alt_tag'     =>  $data['category_name'],
                    ];       
                    $image_id=\DB::table('images')->insertGetId($upload_image); 
                }else{
                    if(empty($data['category_id'])){
                        return response()->json([
                            'message' =>__('category.category_image_required'),
                            'success' => 0,
                        ], 200); 

                    }
                } 
                End image
                if(empty($data['category_id']) ){
                    $categoryData['category_image']  = $image_id;
                    $categoryId = \DB::table($this->table_name)->insertGetId($categoryData);
                    if($categoryId>0){
                        $arr                    =   array();
                        $array['success']       =   1;          
                        $array['message']       =   __('category.category_saved');  
                        $array['data']          =   ['category_id'   =>  $categoryId];  
                        return response()->json($array, 200); 
                    }else{
                        $arr                    =   array();
                        $array['success']       =   0;          
                        $array['message']       =   __('category.category_saving_failed'); 
                        return response()->json($array, 200);
                    }
                }else{
                    if(empty($request->file('category_image')) ){
                        $categoryDetails = $this->category_model->getCategoryDetail([
                            'category_id'       =>$data['category_id'],
                            'category_parent_id'=>$data['category_parent_id']
                        ]);
                        $categoryData['category_image']  = $categoryDetails['category_detail']->category_image;
                    }else{
                        $categoryData['category_image']  = $image_id;
                    }
                    $where      = ['category_id'=>$data['category_id']];
                    \DB::table($this->table_name)->where($where)->update($categoryData);
                    $arr                    =   array();
                    $array['success']       =   1;          
                    $array['message']       =   __('category.category_updated');  
                    $array['data']          =   ['category_id'   =>  $data['category_id']];  
                    return response()->json($array, 200); 
                    
                }

                
            }else{
                $arr                    =   array();
                $array['success']       =   2;          
                $array['message']       =   __('auth.invalid_access');  
                $array['data']          =   [];  
                return response()->json($array, 200);
            }
        }
    public function removeCategory(Request $request)
        {
            $data = $request->all();
            $rules = [  
                'user_token'      => ['required','string','max:255'],
                "category_id"     => ['required','integer','between:1,1111111111'], 
            ];
            $messages = []; 
            $validator = Validator::make($request->all(), $rules, $messages);  
            if($validator->fails()){
                return response()->json([
                    'message' =>\Arr::flatten($validator->errors()->toArray())[0],
                    'success' =>0,
                ], 200); 
            }
            $userAuth = $this->user_model->authUser($data);    
            if (!empty($userAuth)) {
                $where = ['category_id'=>$data['category_id']];
                \DB::table($this->table_name)->where($where)->update(['is_delete'=>1]);
                
                $arr                    =   array();
                $array['success']       =   1;          
                $array['message']       =   __('category.category_delete');  
                $array['data']          =   [];  
                return response()->json($array, 200);
                
            }else{
                $arr                    =   array();
                $array['success']       =   2;          
                $array['message']       =   __('auth.invalid_access');  
                $array['data']          =   [];  
                return response()->json($array, 200);
            }
        }*/
    }
