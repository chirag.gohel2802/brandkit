<?php
namespace App\Http\Controllers\cms;
 
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests; 
use Illuminate\Support\Facades\Validator;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request; 
use Redirect; 
  
use Illuminate\Support\Str;   
 
/*Security & Session*/
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Session;

/*Excel Import Export*/
// Use Excel;
// Use Image;

/*Loading Models Here*/ 
use App\Models\Common; 


class CmsController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    
    public function __construct(){
        // parent::__construct();    
        $this->common_model         = New Common; 
    }
 
    // public function index(Request $request)
    //     { 
    //         $data=[
    //             'title'     =>  'Portal Dashboard',
    //             'data'      =>  'test',
    //             'active'    =>  'home',  
    //         ];
    //         return view('cms/index',compact('data')); 
    //     }
    public function blank_page(Request $request)
        {
            $data=[
                'title'     =>  'Blank',
                'data'      =>  'test',
                'active'    =>  'home',  
            ];
            return view('cms/home_page',compact('data'));   
        }
    public function terms()
        {
            $data=[
                'title'     =>  'Blank',
                'data'      =>  'test',
                'active'    =>  'home',  
            ];
            return view('cms/terms',compact('data'));
        }
    public function privacy()
        {
            $data=[
                'title'     =>  'Blank',
                'data'      =>  'test',
                'active'    =>  'home',  
            ];
            return view('cms/privacy',compact('data'));
        }             
}