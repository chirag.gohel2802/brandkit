<?php

namespace App\Http\Controllers\portal;
 
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests; 
use Illuminate\Support\Facades\Validator;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request; 
use Redirect; 
   
use Illuminate\Support\Str;   
 
/*Security & Session*/
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash; 
use Session;

/*Excel Import Export*/ 
// Use Excel;
// Use Image;

/*Loading Models Here*/ 
use App\Models\portal\master\Image_model;
use App\Models\portal\master\User_model;
use App\Models\portal\master\Category_model;
use App\Models\portal\Portal; 
use App\Models\portal\User;
use App\Models\Common; 


class PortalController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    
    public function __construct(){
        // parent::__construct();    
        $this->portal_model         = New Portal;    
        $this->common_model         = New Common; 
    }
 
    public function index(Request $request)
        { 
            $data=[
                'title'     =>  'Portal Dashboard',
                'data'      =>  'test',
                'active'    =>  'home',  
            ];
            // echo "success";

            $logged_user_role   =   session()->get('my_chacha')['user_role'];
            $logged_user_id     =   session()->get('my_chacha')['user_id'];
            // echo "<pre>"; print_r($logged_user_role); 
            // echo "<pre>"; print_r($logged_user_id); exit;

            $master_id          =   $this->common_model->get_master_id($logged_user_id);  
            if ($master_id==$logged_user_id) { 
                $is_master = 1;
            }else{
                $is_master = 0;
            }
            $portal_model           =   new Portal(); 
            $counts                 =   $portal_model->get_dashboard_data($master_id,$logged_user_id);  
             
            // echo "<pre>";print_r(session()->get('my_chacha'));exit;  
            
            return view('portal/index',compact('data','is_master','counts','logged_user_id','logged_user_role')); 
        } 

    public function login()
        { 
            $data=[
                'title'     =>  'Portal Login',
                'data'      =>  'test',
                'active'    =>  'login', 
            ];
            return view('portal.authentication.login',compact('data')); 
        }
    public function do_login(Request $request)
        {    
            $fields=array(
                "email",
                "password"
            ); 
            $data=array(); 
            foreach ($fields as $field) 
            {
                $data[$field]=$request->input($field);
            }   
            $portal_model=new Portal();   
            $portal_model->set_fields($data);
            $admin_data=$portal_model->get_admin_user_data();
            if (!empty($admin_data)) {  

                $tbl_password_hash      = $admin_data['user_password'];
                $tbl_user_email         = $admin_data['user_email']; 
                $input_password_hash    = Hash::make($data['password']); 
                $input_user_email       = $data['email']; 
 
                if (Hash::check($data['password'],$tbl_password_hash)) {
                    $user_token=md5($admin_data['user_id'].'-'.time() );
                    /*User Array To Store In Session For Authentication Purpose*/
                    if(is_numeric($admin_data['user_role_id']) && $admin_data['user_role_id']==1) {
                        $user_role = 'admin';
                    } 
                    else {
                        $user_role = 'user';
                    } 
                    $user_session=[
                        'user_id'           =>  $admin_data['user_id'],
                        'user_role_id'      =>  $admin_data['user_role_id'],
                        'user_portal'       =>  '/portal',
                        'user_role'         =>  $user_role, 
                        'user_name'         =>  $admin_data['user_name'],
                        'user_token'        =>  $user_token,  
                    ];
                    /*Set Organisatin IN Session*/                      
                    $admin_session_data=[
                        'session_user_id'           =>  $admin_data['user_id'],
                        'session_token'             =>  $user_token,
                        'session_status'            =>  1,  
                        'session_user_ip_address'   =>  $request->ip(),
                    ];
                    /*Saving Session In Database*/
                    $common_model=new Common();   
                    $common_model->set_fields($admin_session_data);
                    $session_id=$common_model->save_my_data('user_login_sessions'); 
    
                    $common_model->set_fields(array('user_session_id'   =>  $session_id));
                    $update_yes=$common_model->update_my_data('users',array('user_id'=>$admin_data['user_id'])); 
                    /*Set Sesssion For Accessing THings heres*/
                    session(['my_chacha'    => $user_session]);
                    session(['portal_url'   => '/portal']);
                    // echo "<pre>"; print_r($user_session); echo "<pre>"; print_r($admin_data); exit;
                    return redirect('/portal');
  
                    if($update_yes==1){ 
                        return redirect('/portal');
                    }
                    
                } else{ 
                     return redirect('/portal-login');
                }  
            }else{ 
                return redirect('/portal-login'); 
            }       

        }
    public function logout(Request $request)
        {
            $request->session()->flush();
            return redirect('/portal');
        }
    public function do_register(Request $request)
        {      
            $fields=array( 
                "user_name",
                "user_email",
                "user_password",
                "user_confirm_password",
                "agree",
            ); 
            $data=array(); 
            foreach ($fields as $field) 
            {
                $data[$field]=$request->input($field);
            } 

            if ($data['agree']!='on') { 
                return back()->with([
                    'error' =>'Please read and accept terms & conditions before registration',
                    'data'  =>$data,
                    // 'time'  =>1, /*In Seconds*/
                ]); 
            }

            /*Validating Fields*/
            $validate_fields=array( 
                "user_name",
                "user_email",
                "user_password",
                "user_confirm_password",
                "agree",   
            ); 
            foreach ($validate_fields as $key => $value) { 
                if ($data[$value]!='') { 
                }else{
                    return back()->with('error',$value.' field required!'); 
                }
            }    

            $data['email']  =   $data['user_email'];
            $portal_model=new Portal();   
            $portal_model->set_fields($data);
            $admin_data=$portal_model->get_admin_user_data();  
            if (empty($admin_data)) {    

                $user_data = $data; 
                unset($user_data['user_confirm_password']);
                unset($user_data['email']);
                unset($user_data['agree']);
                $user_data['user_sweet_word']   = $user_data['user_password']; 
                $user_data['user_sweet_words']  = json_encode([$user_data['user_password']]); 
                $user_data['user_password']     = Hash::make($user_data['user_password']); 
                $user_data['user_status']       = 2; 
                $user_data['user_role_id']      = null; 
                $user_data['user_master_id']    = 1; 


                if($request->file('user_profile_image')) {
                    $rand=rand(1111,9999);
                    $comman_time = time();
                    $hash1=md5($comman_time*$rand);
                    $originalImage= $request->file('user_profile_image');  
                    $image_new_name=$hash1.Str::slug($originalImage->getClientOriginalName(),'-').'.'.$originalImage->extension();
                     
                    $originalImage      = $request->file('user_profile_image');
                    $thumbnailImage     = Image::make($originalImage->getRealPath());
                    $thumbnailPath      = public_path().'/assets/uploads/users/thumbnail/';
                    $originalPath       = public_path().'/assets/uploads/users/main/'; 
                    /*Saving Original Image*/
                    $thumbnailImage->save($originalPath.$image_new_name);
                     
                    /*Saving 500 Size Thumb to main thumb*/
                    $thumbnailImage->resize(500, null, function ($constraint) {
                        $constraint->aspectRatio();
                    });
                    $thumbnailImage->save($thumbnailPath.$image_new_name);  
                    $user_data['user_profile_image']=$image_new_name;
                }


                // echo "<pre>";print_r($user_data);exit;

                $otp        =   rand(1000,9999);
                $user_data['user_otp']      =   $otp;
                $user_data['user_role_id']  =  2;
                /*Saving User In Database*/
                $common_model=new Common();   
                $common_model->set_fields($user_data);
                $user_id=$common_model->save_my_data('users'); 
                if ($user_id>0) { 

                    $name       =   $user_data['user_name']; 
                    $set_otp    =   chunk_split($otp, 1, ' ');
                    $email      =   $user_data['user_email'] ; 
                    $subject    =   'SAFRA - Verification Code';
                    $message    =   view('email.otp_template',compact('name','set_otp'))->render();  
                    $mail_message  = $this->send_email($email,$subject,$message);
 
                    return redirect('/portal-verify-email')->with([
                        'success'   =>"We have emailed you OTP, Please verify !",
                        'data'      =>[
                            'user_email'    =>$user_data['user_email'],
                            'mail_message'  =>$mail_message 
                        ],
                    ]);
                }else{ 
                   return back()->with('error','Registration failed !'); 
                }  
            }else{ 
                return back()->with([
                    'error' =>'This account is already exists!',
                    'data'  =>$data,
                    'time'  =>2, /*In Seconds*/
                ]); 
            }      

        } 

    public function subscription()
        {
            $data=[
                'title'     =>  'Subscription', 
                'active'    =>  'subscription', 
            ]; 
            if (session()->exists('applocale')) {
                app()->setLocale(session()->get('applocale'));
            }else{
                app()->setLocale('en');
            }
            $subscriptions = $this->portal_model->get_subscriptions();

            // echo "<pre>";print_r($subscriptions);exit;
            return view('portal.subscription.subscription',compact('data','subscriptions'));    
        }

    public function subscription_request(Request $request)
        {      
            $fields=array( 
                "plan_id",
                "user_id"
            ); 
            $data=array(); 
            foreach ($fields as $field) 
            {
                $data[$field]=$request->input($field);
            }  
            /*Validating Fields*/
            $validate_fields=array( 
                "plan_id",
                "user_id"     
            ); 
            foreach ($validate_fields as $key => $value) { 
                if ($data[$value]!='') { 
                }else{ 
                    $arr                = array();
                    $arr['status']      = 500;          
                    $arr['message']     = 'error'.$value.' field required!'; 
                    echo json_encode($arr); exit;
                }
            }    
            
            $sbData = [];
            $sbData['request_plan_id']      =   $data['plan_id'];
            $sbData['request_user_id']      =   $data['user_id'];

            /*Saving Request In Database*/
            $common_model=new Common();   
            $common_model->set_fields($sbData);
            $req_id=$common_model->save_my_data('subscription_requests'); 
            if ($req_id>0) { 
                $arr                =   array();
                $arr['status']      =   200;          
                $arr['message']     =   'Success'; 
                echo json_encode($arr); exit; 
            }else{ 
                $arr                =   array();
                $arr['status']      =   500;          
                $arr['message']     =   'Error'; 
                echo json_encode($arr); exit;
            }  
                  
        } 

    public function verify_email($email_id='')
        {
            $data=[
                'title'     =>  'Verify Email',
                'email'      =>  $email_id,
                'active'    =>  'login', 
            ]; 
            return view('portal.authentication.verify_email',compact('data')); 
        }

    public function send_otp(Request $request)
        {      
            $fields=array( 
                "user_email",
                "user_otp"
            ); 
            $data=array(); 
            foreach ($fields as $field) 
            {
                $data[$field]=$request->input($field);
            }  
            /*Validating Fields*/
            $validate_fields=array( 
                "user_email" 
            ); 
            foreach ($validate_fields as $key => $value) { 
                if ($data[$value]!='') { 
                }else{
                    return back()->with('error',$value.' field required!'); 
                }
            }     


            $data['email']  =   $data['user_email'];
            $portal_model=new Portal();   
            $portal_model->set_fields($data);
            $admin_data=$portal_model->get_admin_user_data();  
            if (!empty($admin_data)) {     
                
                if (isset($data['user_otp']) && $data['user_otp']!='') { 
                    if ($admin_data['user_otp']==$data['user_otp']) { 
                        
                        $otp = rand(1000,9999);  
                        /*Saving User In Database*/
                        $common_model=new Common();   
                        $common_model->set_fields(['user_otp'=>$otp,'user_email_is_verified'=>1]);
                        $is_updated=$common_model->update_my_data(
                            'users',
                            [
                                'user_email'=>$data['user_email']
                            ]
                        );   

                        return redirect('/portal-login')->with([
                            'success'   =>"You are verified successfully, access your account here",
                            'data'      =>[
                                'user_email'    =>  $data['user_email'] 
                            ],
                        ]);

                    }else{
                        return redirect('/portal-verify-email')->with([
                            'warning'   =>"Enter your email, to verify",
                            'data'      =>[
                                'user_email'    =>  $data['user_email'], 
                            ],
                        ]);
                    }
                }else{
                    $otp = rand(1000,9999);  
                    /*Saving User In Database*/
                    $common_model=new Common();   
                    $common_model->set_fields(['user_otp'=>$otp]);
                    $is_updated=$common_model->update_my_data(
                        'users',
                        [
                            'user_email'=>$data['user_email']
                        ]
                    );  
                    if ($is_updated==1) {   
                        $name       =   $admin_data['user_name']; 
                        $set_otp    =   chunk_split($otp, 1, ' ');
                        $email      =   $data['user_email'] ; 
                        $subject    =   'SAFRA - Verification Code';
                        $message    =   view('email.otp_template',compact('name','set_otp'))->render();  
                        $mail_message  = $this->send_email($email,$subject,$message); 

                        return redirect('/portal-verify-email')->with([
                            'success'   =>"Email sent successfully, verify otp here",
                            'data'      =>[
                                'user_email'    =>  $data['user_email'],
                                'mail_message'  =>  $mail_message 
                            ],
                        ]);
                    }else{ 
                       return back()->with('error','Sending email failed !'); 
                    }  
                }


               
            }else{ 
                return back()->with([
                    'error' =>  'Please enter valid email address!',
                    'data'  =>  $data,
                    'time'  =>  2, /*In Seconds*/
                ]); 
            }       
        }

    public function do_reset_password(Request $request)
        {       
            $fields=array( 
                "email"
            ); 
            $data=array(); 
            foreach ($fields as $field) 
            {
                $data[$field]=$request->input($field);
            }  
            /*Validating Fields*/
            $validate_fields=array( 
                "email" 
            ); 
            foreach ($validate_fields as $key => $value) { 
                if ($data[$value]!='') { 
                }else{
                    return back()->with('error',$value.' field required!'); 
                }
            }     

 
            $portal_model=new Portal();   
            $portal_model->set_fields($data);
            $admin_data=$portal_model->get_admin_user_data();  
            if (!empty($admin_data)) {     
                
                $user_token             =   md5(rand(1000,9999)*time());  
                $user_token_expiry      =   time()+(3*60*24*24);  
                // echo "<pre>";print_r($admin_data);exit;
                /*Saving User In Database*/
                $common_model=new Common();   
                $common_model->set_fields(['user_temp_token'=>$user_token,'user_token_expiry'=>$user_token_expiry]);
                $is_updated=$common_model->update_my_data(
                    'users',
                    [
                        'user_email'=>$data['email']
                    ]
                );  
                if ($is_updated==1) {   
                    $name       =   $admin_data['user_name']; 
                    $token      =   $user_token;
                    $email      =   $data['email'] ; 
                    $subject    =   'SAFRA - Reset Password Link';
                    $message    =   view('email.email_template',compact('name','token','email'))->render();  
                    $mail_message  = $this->send_email($email,$subject,$message);  
                    
                    // return view('email.email_template',compact('name','token','email')); 
                    return redirect('/portal-login')->with([
                        'success'   =>"Reset Password link sent to your email successfully, reset your password from mail inbox.",
                        'data'      =>[
                            'user_email'    =>  $data['email'],
                            'mail_message'  =>  $mail_message 
                        ],
                    ]);
                }else{ 
                   return back()->with('error','Sending email failed !'); 
                }  
                 


               
            }else{ 
                return back()->with([
                    'error' =>  'Please enter valid email address!',
                    'data'  =>  $data,
                    'time'  =>  2, /*In Seconds*/
                ]); 
            }       
        }

    public function user_reset_password(Request $request)
        {
           
            $fields=array( 
                "token"
            ); 
            $data=array(); 
            foreach ($fields as $field) 
            {
                $data[$field]=$request->input($field);
            }  
            /*Validating Fields*/
            $validate_fields=array( 
                "token" 
            ); 
            foreach ($validate_fields as $key => $value) { 
                if ($data[$value]!='') { 
                }else{
                    return redirect('/portal-reset-password')->with([
                        'error'   =>$value.' field required!',
                    ]); 
                }
            }     
           
            // echo "<pre>";print_r($data);exit;

            $portal_model=new Portal();   
            $portal_model->set_fields($data);
            $admin_data=$portal_model->get_token_user_data();  
            if (!empty($admin_data)) {     
                
                $token  =$data['token'];
                $data   =$admin_data;
                return view('portal/authentication/change_token_password',compact('data','token')); 


                // $user_password = Hash::make($user_data['user_password']);   
                // $common_model=new Common();   
                // $common_model->set_fields(['user_password'=>$user_password]);
                // $is_updated=$common_model->update_my_data(
                //     'users',
                //     [
                //         'user_id'=>$admin_data['user_id']
                //     ]
                // );  
                // if ($is_updated==1) {   
                //     $name       =   $admin_data['user_name']; 
                //     $token      =   $user_token;
                //     $email      =   $data['email'] ; 
                //     $subject    =   'SAFRA - Reset Password Link';
                //     $message    =   view('email.email_template',compact('name','token','email'))->render();  
                //     $mail_message  = $this->send_email($email,$subject,$message);  
                //     return redirect('/portal-login')->with([
                //         'success'   =>"Reset Password link sent to your email successfully, reset your password from mail inbox.",
                //         'data'      =>[
                //             'user_email'    =>  $data['email'],
                //             'mail_message'  =>  $mail_message 
                //         ],
                //     ]);
                // }else{ 
                //    return redirect('/portal-reset-password')->with('error','Password reset failed !'); 
                // }  
                 


               
            }else{ 
                return redirect('/portal-reset-password')->with([
                    'error' =>  'Link expired !',
                    'data'  =>  $data,
                    'time'  =>  2, /*In Seconds*/
                ]); 

            }       
        }

    public function user_reset_token_password(Request $request)
        {      
            $fields=array( 
                "password",
                "confirm_password",
                "token" 
            ); 
            $data=array(); 
            foreach ($fields as $field) 
            {
                $data[$field]=$request->input($field);
            } 
 
            /*Validating Fields*/
            $validate_fields=array( 
                "password",
                "confirm_password",
                "token"   
            ); 
            foreach ($validate_fields as $key => $value) { 
                if ($data[$value]!='') { 
                }else{
                    return back()->with('error',$value.' field required!'); 
                }
            }    

            if ($data['password']!=$data['confirm_password']) { 
                return back()->with('error','Password mismatch ! Please type both password as same.'); 
            }

            $portal_model=new Portal();   
            $portal_model->set_fields($data);
            $user_data=$portal_model->get_token_user_data();  
            if (!empty($user_data)) {      

                $password_data = [];
                $sweet_words=json_decode($user_data['user_sweet_words'],true); 
                array_push($sweet_words, $data['password']);
                $password_data['user_password']     =   Hash::make($data['password']);
                $password_data['user_sweet_words']  =   json_encode($sweet_words) ;
                $password_data['user_sweet_word']   =   $data['password'] ;
      
                /*update data if it is coming from edit form*/
                $where =array('user_id'=>$user_data['user_id']);
                $this->common_model->set_fields($password_data); 
                $is_updated = $this->common_model->update_my_data('users',$where); 
                if ($is_updated==1) { 
                    return redirect('/portal-login')->with([
                        'success'   =>"Password reset successfully ",
                        'data'      =>[
                            'user_email'    =>  $user_data['user_email']
                        ],
                    ]);
                }else{
                    return redirect('/portal-reset-password')->with([
                        'error' =>  'Technical error please try later!', 
                        'time'  =>  2, /*In Seconds*/
                    ]);
                }
                 
                 
               
            }else{ 
                return redirect('/portal-reset-password')->with([
                    'error' =>  'Link expired !',
                    'data'  =>  $data,
                    'time'  =>  2, /*In Seconds*/
                ]); 

            }           

        }
    /*Profile Update*/
    public function profile()
        { 
            $user_session = session()->get('my_chacha'); 
            $portal_model=new Portal();   
            $portal_model->set_fields(['user_id'=>$user_session['user_id'] ]);
            $profile_data=$portal_model->get_user_data(); 

            $data=[
                'title'             =>  'My Profile',
                'data'              =>  'test',
                'profile_data'      =>  $profile_data,
                'active'            =>  'profile',
            ];
 

            return view('portal/profile/my_profile',compact('data')); 
        }
        
    public function update_profile(Request $request)
        {  
            $fields=array(   
                "user_name",     
                "user_phone_no",        
            );

            $data=array();  
            foreach ($fields as $field) 
            {
                $data[$field]=$request->input($field);
            } 

            $v_fields=array(   
                array(
                    'field'=>"user_name",
                    'label'=>"User'sName"
                ),array(
                    'field'=>"user_phone_no",
                    'label'=>"Phone Number"
                )     
            );  
            foreach ($v_fields as $field) 
            {
                $v_value=$request->input($field['field']);
                if (!isset($v_value) OR $v_value=='') { 
                    $arr                = array();
                    $arr['status']      = 500;          
                    $arr['message']     = "Enter valid ".$field['label']; 
                    echo json_encode($arr); exit;
                }
            }  
               
            if($request->file('user_profile_image')) {
                $rand=rand(1111,9999);
                $comman_time = time();
                $hash1=md5($comman_time*$rand);
                $originalImage= $request->file('user_profile_image');  
                $image_new_name=$hash1.Str::slug($originalImage->getClientOriginalName(),'-').'.'.$originalImage->extension();
                 
                $originalImage      = $request->file('user_profile_image');
                $thumbnailImage     = Image::make($originalImage->getRealPath());
                $thumbnailPath      = public_path().'/assets/uploads/users/thumbnail/';
                $originalPath       = public_path().'/assets/uploads/users/main/'; 
                /*Saving Original Image*/
                $thumbnailImage->save($originalPath.$image_new_name);
                 
                /*Saving 500 Size Thumb to main thumb*/
                $thumbnailImage->resize(500, null, function ($constraint) {
                    $constraint->aspectRatio();
                });
                $thumbnailImage->save($thumbnailPath.$image_new_name); 

                // $current_user_profile_image=$request->input("current_user_profile_image");
                // if (isset($current_user_profile_image) && $current_user_profile_image!='') {
                //     unlink(public_path().'/assets/uploads/users/thumbnail/'.$current_user_profile_image);
                //     unlink(public_path().'/assets/uploads/users/main/'.$current_user_profile_image); 
                // }
                $data['user_profile_image']=$image_new_name;
            }

             
            $common_model=new Common();   
            /*update data if it is coming from edit form*/
            $where =array('user_id'=>session()->get('my_chacha')['user_id']);
            $common_model->set_fields($data); 
            $is_updated = $common_model->update_my_data('users',$where); 
            if ($is_updated==1) {  
                $arr                = array();
                $arr['status']      = 200;          
                $arr['message']     = "Profile Updated Successfully"; 
                echo json_encode($arr); exit;
            }else{
                $arr                = array();
                $arr['status']      = 500;          
                $arr['message']     = "Profile Remains Untouched"; 
                echo json_encode($arr); exit;
            } 
        }

    public function update_password(Request $request)
        {  
            $fields=array(   
                "old_password",     
                "new_password",     
                "new_repeat_password"
            );

            $data=array();  
            foreach ($fields as $field) 
            {
                $data[$field]=$request->input($field);
            } 

            $v_fields=array(   
                array(
                    'field'=>"old_password",
                    'label'=>"Old Password "
                ),array(
                    'field'=>"new_password",
                    'label'=>"New Password"
                ),array(
                    'field'=>"new_repeat_password",
                    'label'=>"Confirm New Password"
                )     
            );  
            foreach ($v_fields as $field) 
            {
                $v_value=$request->input($field['field']);
                if (!isset($v_value) OR $v_value=='') { 
                    $arr                = array();
                    $arr['status']      = 500;          
                    $arr['message']     = "Enter valid ".$field['label']; 
                    echo json_encode($arr); exit;
                }
            }  
            if ($data['new_password']!=$data['new_repeat_password']) {
                $arr                = array();
                $arr['status']      = 500;          
                $arr['message']     = "Enter Same New & Confirm password "; 
                echo json_encode($arr); exit;
            }
            $common_model=new Common();  
            $user_id_set = array('user_id'=>session()->get('my_chacha')['user_id']);
            $user_model=new User_model();    
            $user_details=$user_model->get_user_detail($user_id_set['user_id']); 
            if (!Hash::check($data['old_password'],$user_details['user_password'])) {
                $arr                = array();
                $arr['status']      = 500;          
                $arr['message']     = "Enter Valid Old password "; 
                echo json_encode($arr); exit;
            }
            $password_data = [];
            $sweet_words=json_decode($user_details['user_sweet_words'],true); 
            array_push($sweet_words, $data['new_password']);
            $password_data['user_password']     =   Hash::make($data['new_password']);
            $password_data['user_sweet_words']  =   json_encode($sweet_words) ;
            $password_data['user_sweet_word']   =   $data['new_password'] ;
 

            /*update data if it is coming from edit form*/
            $where =array('user_id'=>session()->get('my_chacha')['user_id']);
            $common_model->set_fields($password_data); 
            $is_updated = $common_model->update_my_data('users',$where); 
            if ($is_updated==1) {  
                $arr                = array();
                $arr['status']      = 200;          
                $arr['message']     = "Password Updated Successfully"; 
                echo json_encode($arr); exit;
            }else{
                $arr                = array();
                $arr['status']      = 500;          
                $arr['message']     = "Password Remains Untouched"; 
                echo json_encode($arr); exit;
            }
            /*$common_model=new Common();  
            $user_id_set = array('user_id'=>session()->get('my_chacha')['user_id']);
            $user_model=new User_model();    

            $user_details=$user_model->get_user_detail($user_id_set['user_id']); 
            if (!Hash::check($data['old_password'],$user_details['user_password'])) {
                $arr                = array();
                $arr['status']      = 500;          
                $arr['message']     = "Enter Valid Old password "; 
                echo json_encode($arr); exit;
            }
            $password_data = [];
            $sweet_words=json_decode($user_details['user_sweet_words'],true); 
        dd($user_details);
            array_push($sweet_words, $data['new_password']);
            $password_data['user_password']     =   Hash::make($data['new_password']);
            $password_data['user_sweet_words']  =   json_encode($sweet_words) ;
            $password_data['user_sweet_word']   =   $data['new_password'] ;
 
            $where =array('user_id'=>session()->get('my_chacha')['user_id']);
            $common_model->set_fields($password_data); 
            $is_updated = $common_model->update_my_data('users',$where); 
            if ($is_updated==1) {  
                $arr                = array();
                $arr['status']      = 200;          
                $arr['message']     = "Password Updated Successfully"; 
                echo json_encode($arr); exit;
            }else{
                $arr                = array();
                $arr['status']      = 500;          
                $arr['message']     = "Password Remains Untouched"; 
                echo json_encode($arr); exit;
            }*/ 
        }

    public function linkCategory(Request $request)
        {
            $params     =$request->all();
            $type       =\Arr::get($params, 'type');
            if($type=='category'){
                $categoryList =   Category_model::get_parent_list();
            }elseif($type=='post'){
                $categoryList =   Category_model::get_all_post_list();
            }else{
                $categoryList = [];
            }
            $html = '<option disabled selected>Select Category</option>';
            foreach($categoryList as $k=>$category){
                $html .= '<option data-type="'.$category->category_type.'" data-parentid="'.$category->category_parent_id.'" value="'.$category->category_id.'">'.$category->category_name.'</option>';
            }
            echo  $html; exit;
        }    
/*Send Message*/
    public function sendMessageAlert(Request $request)
        {

            $data=[
                'title'     =>  'Message Alert',
                'data'      =>  'test',
                'active'    =>  'sendMessageAlert',  
            ];

            $user_model     =   new User_model(); 

            $params = [];
            if(!empty($_GET['user_type'])){
                $params[] = ['user_role_id','=',$_GET['user_type']];
                $data['user_type'] = $_GET['user_type'];
            } 

            if(!empty($_GET['user_status'])){
                $params[] = ['user_status','=',$_GET['user_status']];
                $data['user_status'] = $_GET['user_status'];
            }

            $return_url = url('/send-message-alert');
            $users          =   $user_model->userList($params);
            $categoryList =   Category_model::get_parent_list();
            

            // echo "<pre>";print_r($categoryList);exit;
            // echo "<pre>";print_r($allCategory);exit;
            return view('portal/send_message_alert',compact('data','return_url','categoryList'));
        }
/*Save Message */
    public function doSendMessageAlert(Request $request)
    {
        $user_model     =   new User_model(); 
        $data =$request->all();   
        $rules = [  
            'message_title'     => ['required','string','max:2000'],  
            'message_body'      => ['required','string','max:2000'], 
            'message_type'      => ['required','string'],  
            'message_link'      => ['required','string','max:2000'],  
        ]; 
        $messages = []; 
        $validator = Validator::make($data, $rules, $messages); 
        if($validator->fails()){
            return response()->json([
                'message' =>\Arr::flatten($validator->errors()->toArray())[0],
                'success' =>500,
            ], 200); 
        }
        if ($data['message_title'] == 'Custom Title') {
            $data['message_title'] = $data['message_title_custom'];
        }  
        $saveData = [
            'message_title' => $data['message_title'],
            'message_body' => $data['message_body'],
            'message_type' => $data['message_type'], 
            'message_link' => $data['message_link'],
        ];

        $inserted_id = \DB::table('notifiction_message')->insertGetId($saveData);
         
        $notiData = [ 
            'title'      =>$data['message_title'],
            'body'       =>$data['message_body'],
            'post_type'  =>1,
            'type'       =>$data['message_type'],
            'link'       =>$data['message_link']
        ];   
        $message=array(
            "title"     =>  $notiData['title'],
            "message"   =>  $notiData['body']
        );  

        $users          =   $user_model->csvUserList();  
        $user_list = [];
        if (!empty($users)) { 
            foreach($users as $key=>$value)
            { 
                $user_list[] = $value->user_fcm_token;
            }
        }

        $user_list_chunk_arr = array_chunk(array_filter(array_unique($user_list)),1000);
        if(!empty($user_list_chunk_arr)){
            foreach($user_list_chunk_arr as $key => $user_list){
                $user_list_arr = json_encode($user_list);
                $this->common_model->send_multi_notification(
                    $user_list_arr,
                    $message,
                    $notiData,
                    1
                );    
            }
        }
        
        $arr                = array();
        $arr['status']      = 200;          
        $arr['message']     = "Announcement sent"; 
        echo json_encode($arr); exit;
    }

/*Send test notification Message*/
    public function test_notification(Request $request)
        {

            $data=[
                'title'     =>  'Message Alert',
                'data'      =>  'test',
                'active'    =>  'sendMessageAlert',  
            ];

            $user_model     =   new User_model(); 

            $params = [];
            if(!empty($_GET['user_type'])){
                $params[] = ['user_role_id','=',$_GET['user_type']];
                $data['user_type'] = $_GET['user_type'];
            } 

            if(!empty($_GET['user_status'])){
                $params[] = ['user_status','=',$_GET['user_status']];
                $data['user_status'] = $_GET['user_status'];
            }

            $return_url = url('/send-message-alert');
            $users        =   $user_model->userList($params);
            $categoryList =   Category_model::get_parent_list();

            return view('portal/test_notification',compact('data','users','return_url','categoryList'));
        }
/*Send test notification Message*/
    public function do_test_notification(Request $request)
    {
        $user_model     =   new User_model(); 
        $data =$request->all();   
        $rules = [  
            'message_title'     => ['required','string','max:2000'],  
            'message_body'      => ['required','string','max:2000'], 
            'message_type'      => ['required','string'],  
            'message_link'      => ['required','string','max:2000'],
            'single_fcm'        => ['required','string'],
        ]; 
        $messages = []; 
        $validator = Validator::make($data, $rules, $messages); 
        if($validator->fails()){
            return response()->json([
                'message' =>\Arr::flatten($validator->errors()->toArray())[0],
                'success' =>500,
            ], 200); 
        }  
        $saveData = [
            'message_title' => $data['message_title'],
            'message_body' => $data['message_body'],
            'message_type' => $data['message_type'], 
            'message_link' => $data['message_link'],
        ];

        $inserted_id = \DB::table('notifiction_message')->insertGetId($saveData);
         
        $notiData = [ 
            'title'      =>$data['message_title'],
            'body'       =>$data['message_body'],
            'post_type'  =>1,
            'type'       =>$data['message_type'],
            'link'       =>$data['message_link']
        ];   
        $message=array(
            "title"     =>  $notiData['title'],
            "message"   =>  $notiData['body']
        );

        $this->common_model->send_notification(
            $data['single_fcm'],
            $message,
            $notiData,
            1
        );    
        
        $arr                = array();
        $arr['status']      = 200;          
        $arr['message']     = "Announcement sent"; 
        echo json_encode($arr); exit;
    }

/*seo settings*/  
    public function seoSettings()
        {
            $user_session   = session()->get('my_chacha'); 
            $portal_model   =new Portal();   
            $portal_model->set_fields(['user_id'=>$user_session['user_id'] ]);
            $profile_data=$portal_model->get_user_data();  

            $data=[
                'title'             =>  'SEO Settings',
                'data'              =>  'test',
                'active'            =>  'seo-settings',  
            ];
            $fields=['home_meta_title','home_canonical_link','home_meta_keywords','home_meta_description',
                     'about_meta_title','about_canonical_link','about_meta_keywords','about_meta_description',
                     'contact_meta_title','contact_canonical_link','contact_meta_keywords','contact_meta_description'
                    ];
                foreach ($fields as $key => $value) {
                    $data[$value] = Portal::getSettings($value);    
                }
            return view('portal/setting/seo_settings',$data); 
        }
    public function setSeoSettings(Request $request)
        {
            $params = $request->all();
            $data=array();
            $common_model=new Common();

            $fields=['home_meta_title','home_canonical_link','home_meta_keywords','home_meta_description',
                     'about_meta_title','about_canonical_link','about_meta_keywords','about_meta_description',
                     'contact_meta_title','contact_canonical_link','contact_meta_keywords','contact_meta_description'
                    ];
            foreach ($fields as $field) 
            {
                $data[$field]= \Arr::get($params, $field);
            }

            $validator = Validator::make($params, [
                'home_meta_title'       => 'required|string',
                'about_meta_title'      => 'required|string',
                'contact_meta_title'    => 'required|string',
            ]);

            if($validator->fails()){
                return back()->with([
                    'error' =>$validator->errors(),
                    'data'  =>[],
                    'time'  =>2, /*In Seconds*/
                ]); 
            }else{

                foreach ($data as $key => $value) {
                    $where = array('setting_name'=>$key);
                    $common_model->set_fields(['setting_value'=>$value ]); 
                    $is_updated = $common_model->update_my_data('settings',$where);  
                }
                 return back()->with([
                    'success' =>"Settings Changed Successfully",
                    'data'  =>[],
                    'time'  =>1, /*In Seconds*/
                ]); 
            }
        }      
    /*End seo_settings*/
    public function settings()
        { 
            $user_session   = session()->get('my_chacha'); 
            $portal_model   =new Portal();   
            $portal_model->set_fields(['user_id'=>$user_session['user_id'] ]); 
            $profile_data=$portal_model->get_user_data();  
            
            $data=[
                'title'             =>  'My Settings',
                'data'              =>  'test',
                'profile_data'      =>  $profile_data,
                'active'            =>  'setting',  
            ];
            $fields=['phone_no1','email1','whatsapp_contact','watermark','how_it_work','product_brochure','general_brochure','analytics','sticky_logo','header_logo','footer_logo','email2','address'];
            foreach ($fields as $key => $value) {
                $data[$value] = Portal::getSettings($value); 
            }
            $data['watermark_image'] = Image_model::getImageDetail($data['watermark']['setting_value'],'watermark');
           /*$data['sticky_logo_image'] = Image_model::getImageDetail($data['sticky_logo']['setting_value'],'sticky_logo');
           $data['footer_logo_image'] = Image_model::getImageDetail($data['footer_logo']['setting_value'],'footer_logo');*/
           
           // $data['postList']          = Portal::getallPost();    
           // $data['businessCategoryList']   = Portal::getallPost();    
           // $data['categoryList']    = Portal::getallcategory();    
           
           // echo "<pre>"; print_r($data); exit;   
            return view('portal/setting/web_settings',$data); 
        }
    public function setSettings(Request $request)
        {
            $params = $request->all();
            $data=array();
            $common_model=new Common();

            $fields=['phone_no1','email1','whatsapp_contact','watermark','how_it_work'];
            foreach ($fields as $field) 
            {
                $data[$field]= \Arr::get($params, $field);
            }
            $validator = Validator::make($params, [
                'phone_no1' => 'required|string|min:10',
                'email1'    => 'required|email',
            ]);

            if($validator->fails()){
                return back()->with([
                    'error' =>$validator->errors(),
                    'data'  =>[],
                    'time'  =>2, /*In Seconds*/
                ]); 
            }else{
                /*if(!empty($data['launched_products'])){
                    $data['launched_products'] = implode(',',$data['launched_products']); 
                }
                $data['analytics']         = trim($data['analytics']);*/
                /*Brochers File Upload*/
                /*if(!empty($request->file('general_brochure'))){
                    $tmpName        = $request->file('general_brochure')->getPathname();
                    $fileName       = $request->file('general_brochure')->getClientOriginalName();
                    $uploadFileName = uniqid().'-'.$fileName;
                    $filePath       = public_path('assets/upload/brochure/');
                    if(move_uploaded_file($tmpName,$filePath.$uploadFileName)){
                        $data['general_brochure'] = $uploadFileName;
                    }
                }
                if(!empty($request->file('product_brochure'))){
                    $tmpName        = $request->file('product_brochure')->getPathname();
                    $fileName       = $request->file('product_brochure')->getClientOriginalName();
                    $uploadFileName = uniqid().'-'.$fileName;
                    $filePath       = public_path('assets/upload/brochure/');
                    if(move_uploaded_file($tmpName,$filePath.$uploadFileName)){
                        $data['product_brochure'] = $uploadFileName;
                    }
                }*/

                /*End File*/
                foreach ($data as $key => $value) {
                    /*if (!empty($data['sticky_logo'])) { 
                      $this->imageUpdate($data['sticky_logo'],'',0,'sticky_logo'); //Main image  Update
                    }if (!empty($data['header_logo'])) { 
                      $this->imageUpdate($data['header_logo'],'',0,'header_logo'); //backlit image Update
                    }if (!empty($data['footer_logo'])) { 
                      $this->imageUpdate($data['footer_logo'],'',0,'footer_logo'); //backlit image Update
                    }*/
                    if(!empty($data['watermark'])) { 
                      $this->imageUpdate($data['watermark'],'',0,'watermark'); //WaterMark image Update
                    }
                    $where = array('setting_name'=>$key);
                    $common_model->set_fields(['setting_value'=>$value]); 
                    $is_updated = $common_model->update_my_data('settings',$where);  
                }
                return back()->with([
                    'success' =>"Settings Changed Successfully",
                    'data'  =>[],
                    'time'  =>1, /*In Seconds*/
                ]); 
            }

        }
    public function pdfFileUpload($file)
        {
            $filenamewithextension  = $file->getClientOriginalName();
            $original_name          = pathinfo($filenamewithextension, PATHINFO_FILENAME);
            $extension              = $file->getClientOriginalExtension();

            $pdfPath          = 'assets/upload/pdf/';
            if (!is_dir($pdfPath)) {
                mkdir($pdfPath, 0775, true);
            }
            if($extension =='pdf'){
                $uploadedFile =$file->move($pdfPath, $file);
                return $uploadedFile;
            }
        }
    public function imageUpdate($product_image='',$iati1='',$image_type='',$image_details='')
        {
            $updateData=[
                'image_status'  =>1, 
                'image_type'    =>$image_type,
                'image_details' =>$image_details, 
            ];
            if (!empty($iati1)) {
                $updateData['image_alt_tag']=$iati1;
            }
            \DB::table('images')->where('image_id', $product_image)->update($updateData);
        }    

    /*Localization*/
    public function updateLanguage(Request $request)
        { 
            $logged_user_id = session()->get('my_chacha')['user_id'];
            $portal_model   =   new Portal();     
            $common_model   =   new Common();     

            $lang           =   $request->input('lang');  
            $lang_data      =   $portal_model->get_language_detail($lang);      
            
            $where =array('user_id'=>$logged_user_id);
            $common_model->set_fields(['user_language_id'=>$lang ]); 
            $is_updated = $common_model->update_my_data('users',$where);  
            if (isset($lang) && $lang>0 && !empty($lang_data)) {
                session(['applocale'=>$lang_data->language_slug]); 
            } 
            return back()->with([
                'success'   =>'Language Changed Successfully',
                'data'      =>[],
                'time'      =>1.5, /*In Seconds*/
            ]);
        }
    public function blank_page(Request $request)
        {
            $data=[
                'title'     =>  'Portal Dashboard',
                'data'      =>  'test',
                'active'    =>  'home',  
            ];
            return view('portal/blank_page',compact('data')); 
        }    


}
