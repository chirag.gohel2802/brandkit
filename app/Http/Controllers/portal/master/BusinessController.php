<?php

namespace App\Http\Controllers\portal\master; 
 
use App\Http\Controllers\Controller; 
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator; 
use Illuminate\Support\Facades\Http;

use App\Models\portal\master\Business_model; 
use App\Models\portal\master\Image_model; 

class BusinessController extends Controller
{    
    private $table_name; 
    private $view_title;
    private $active;
    private $sub;
    private $dt_table_display_name;  
    private $dt_table_small_name;
    private $route_name;
    private $add_edit_type;
    private $grid_add_button_name;
    private $grid_title;
    private $view_path;

    public function __construct(){

        $this->table_name = 'business';
        $this->view_title = 'Business Management';  
        $this->active = 'business';
        $this->sub = 'business';
        $this->grid_title = 'Business List';
        $this->dt_table_display_name = 'Business';
        $this->dt_table_small_name = strtolower($this->dt_table_display_name);
        $this->route_name = 'business';
        $this->add_edit_type = ''; 
        $this->grid_add_button_name = 'Add '.$this->route_name;
        $this->view_path = 'portal/master/business/';
    }

    public function index()
        { 
            $data=['title'=>$this->view_title,'active'=>$this->active,'sub'=>$this->sub];
            return view('portal/master/master',compact('data')); 
        }

    public function dt_col()
        {
            $data=['title'=>$this->view_title,'active'=>$this->active,'sub'=>$this->sub];
            /*Here we will use grid's data for making it dynamic*/ 
            $grid_columns = [
                [
                    'name'=>'No',
                    'width'=>'width="10%"',
                    'sortable'=>'true', 
                    'style'=>'style=""',
                    'class'=>'class="text-left"',
                ],
                [
                    'name'=> 'Name',
                    'width'=>'width="15%"',
                    'sortable'=>'true',
                    'style'=>'style=""',
                    'class'=>'',
                ],
                [
                    'name'=> 'Contact',
                    'width'=>'width="20%"',
                    'sortable'=>'true',
                    'style'=>'style=""',
                    'class'=>'', 
                ],
                [
                    'name'=> 'Time',
                    'width'=>'width="15%"',
                    'sortable'=>'true',
                    'style'=>'style=""',
                    'class'=>'', 
                ],
                [
                    'name'=> 'Address',
                    'width'=>'width="15%"',
                    'sortable'=>'true',
                    'style'=>'style=""',
                    'class'=>'', 
                ],
                [
                    'name'=> 'Status',
                    'width'=>'width="10%"',
                    'sortable'=>'false',
                    'style'=>'style=""',
                    'class'=>'', 
                ],
                [
                    'name'=>'Action',
                    'width'=>'width="15%"',
                    'sortable'=>'false',
                    'style'=>'style=""',
                    'class'=>'', 
                ]
            ];

            $table_style='bBusiness-collapse: collapse; bBusiness-spacing: 0; width: -webkit-fill-available;';
            $table_class='table table-striped nowrap table-bBusinessed dt-responsive nowrap';

            if($this->add_edit_type == 'model'){
                $data["extra_pages"] = ['portal/master/'.$this->route_name.'/add_modal'];
                $add_url = false;
            }else{
                $add_url = url('/'.$this->route_name.'-add');
            }

            $data['grid'] = [
                    'grid_name'             =>  $this->dt_table_display_name,
                    'grid_add_button'       =>  false,
                    'grid_add_button_name'  =>  $this->grid_add_button_name,
                    'grid_add_url'          =>  $add_url,
                    'grid_dt_url'           =>  url('/'.$this->route_name.'-list'),
                    'grid_delete_url'       =>  url('/'.$this->route_name.'-delete/'),
                    'grid_status_url'       =>  url('/'.$this->route_name.'-status/'),
                    'grid_data_url'         =>  url('/'.$this->route_name.'-edit/'), 
                    'grid_columns'          =>  $grid_columns,
                    'grid_order_by'         =>  '0',
                    'grid_order_by_type'    =>  'DESC',
                    'grid_tbl_name'         =>  $this->dt_table_small_name,
                    'grid_title'            =>  $this->grid_title,
                    'grid_tbl_display_name' =>  $this->dt_table_display_name,
                    'grid_tbl_length'       =>  '10',
                    'grid_tbl_style'        =>  $table_style,
                    'grid_tbl_class'        =>  $table_class
            ];
            return view('portal/master/master',$data); 
        }

    public function dt_list( $id = -1 )
        { 

            $start_index    = $_GET['iDisplayStart']!=null?$_GET['iDisplayStart']:0;
            $end_index      = $_GET['iDisplayLength']?$_GET['iDisplayLength']:10;      
            $search_text    = $_GET['sSearch']?$_GET['sSearch']:''; 
            $aColumns       = ['business.business_id','business.business_name','business.business_email','business.created_at','business.business_address'];
            $aColumns_where = ['business.business_id','business.business_name','business.business_email','business.created_at','business.business_phone_no','business.business_address'];

            $order_by       = "";
            $where          = "";
            $order_by_type  = "ASC";

            if ( $_GET['iSortCol_0'] !== FALSE ){
                for ( $i=0 ; $i<intval($_GET['iSortingCols']); $i++ ){ if ($_GET['bSortable_'.intval($_GET['iSortCol_'.$i])] == "true" ){ $order_by = $aColumns[ intval( ( $_GET['iSortCol_'.$i] ) ) ]; $order_by_type = $this->mres( $_GET['sSortDir_'.$i] ); }
                }
            }

            for ( $i=0 ; $i<count($aColumns_where) ; $i++ ){ if ( isset($_GET['bSearchable_'.$i])  && $_GET['bSearchable_'.$i] == "true" && $_GET['sSearch_'.$i] != '' ){if($where != ''){$where .= " AND ";} $where .= $aColumns_where[$i]." = '".$this->mres($_GET['sSearch_'.$i])."' ";}
            }

            if( isset($_GET['sSearch'])  ){
                $where .= '('; $or = '';foreach( $aColumns_where as $row ){ $where .= $or.$row." LIKE '%".str_replace("'","\\\\\''",$this->mres($_GET['sSearch']))."%'"; if($or== ''){$or =' OR ';} }$where .= ')';
            }
            
            $filter='';
            
            /*Get Data From Model*/
            $pass_data =   array(
                'limit_start'       =>  $start_index,
                'limit_length'      =>  $end_index,
                'where_raw'         =>  $where.$filter,
                "order_by"          =>  $order_by,
                "order_by_type"     =>  $order_by_type,
            );

            $all_data = Business_model::dt_list_data($pass_data);
            $data           = [];
            $i=$start_index;

            foreach( $all_data['result'] as $row ){
                $row_dt   = [];
                $row_dt[] = "# ".$row->business_id;
                $row_dt[] = $row->business_name;
                $row_dt[] = $row->business_email."<br>".$row->business_phone_no."<br>".$row->business_secondary_phone_no;
                
                $row_dt[] = $this->format_indian_time($row->created_at);
    
                $row_dt[] = $row->business_address;

                $business_status = 'Free';
                if($row->is_delete == 1){
                    $business_status .= ' - DELETED'; 
                }else{
                    $business_status .= ''; 
                }
                $row_dt[] = $business_status;

                $action = ''; 
                $action .= '<a class="dropdown-item" href="'.url('business-edit').'/'.$row->business_id.'"  title="Edit '.$this->route_name.'"> <i class="fa fa-edit"></i> &nbsp;&nbsp;Edit</a>';

                $action .= '<a class="dropdown-item" href="'.url('user-view').'/'.$row->business_user_id.'"  title="View User'.$this->route_name.'"> <i class="fa fa-eye"></i> &nbsp;&nbsp;View User</a>';
                
                /*$action .= '<a class="dropdown-item"  href="#" onclick="js_delete('.$row->business_id.')"  title="Delete '.$this->route_name.'"> <i class="fa fa-trash"></i> &nbsp;&nbsp;Delete</a>';*/

                $row_dt[] = '<button class="btn btn-outline-primary dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> Action </button>
                        <div class="dropdown-menu">'.$action.'
                    </div>';
                
                $data[] = $row_dt;
            }
            $response['iTotalRecords'] = $response['iTotalDisplayRecords'] = $all_data['total'];
            $response['aaData'] = $data;
            return response()->json($response);
        }

    public function add()
        {
            $data = array();
            if($this->add_edit_type == 'model'){
                return redirect('/'.$this->route_name.'-master');
            }else{
                // echo "<pre>"; print_r($data['category_list']); exit;
                return view($this->view_path.'add',$data);
            }
        }

    public function edit($passed_id)
        { 
            $data = Business_model::get_edit_detail($passed_id);
            /*if(!empty($data['business_logo'])){
                $data['business_logo'] = Image_model::get_edit_detail($data['business_logo']);
            }*/
            /*$data['otherImages'] = [];
            if(!empty($data['business_other_image'])){
                $businessImages = explode(',',$data['business_other_image'] );
                foreach($businessImages as $key=>$img_id)
                {
                  $data['otherImages'][$key]= Image_model::get_edit_detail($img_id);
                } 
            }*/
            //$data['otherImages'] = array_filter($data['otherImages']);
            $data['businessCategory'] = Business_model::get_business_category_list();
            // echo "<pre>"; print_r($data); exit;
            if($this->add_edit_type == 'model'){
                return view($this->view_path.'edit_modal',$data); 
            }else{
                return view($this->view_path.'edit',$data); 
            }
        }

    public function save(Request $request)
        {
            $params = $request->all();

            $data=array();
            $fields=array("business_category","business_name","business_phone_no","business_email","business_logo","business_website","business_address","business_secondary_phone_no");
            foreach ($fields as $field) 
            {
                $data[$field]= \Arr::get($params, $field);
            }

            $id=\Arr::get($params, 'id');
            $mode=\Arr::get($params, 'mode');

            $validator = Validator::make($params, [
                'business_category' => 'required',
                'business_name'    => 'required|string',
            ]);

            if($validator->fails()){
                return response()->json(['status'=>500,'message'=>\Arr::flatten($validator->errors()->toArray())[0]]);
            }

            $check_arr = array();
            $check_arr[] = array('business_name','=',$data['business_name']);
            $check_arr[] = array('business_category','=',$data['business_category']);

            if(!empty($id)){
                $check_arr[] = array('business_id','<>',$id);
            }
            $business_exists = Business_model::check_business_exists($check_arr);
            if ($business_exists) { 
                return response()->json(['status'=>500,'message'=>'Business Already Exists']);
            }
            if ($mode=='add') {
                $inserted_id = \DB::table($this->table_name)->insertGetId($data);
                if($inserted_id){
                    if (!empty($data['business_logo'])) { 
                      $this->imageUpdate($data['business_logo'],$_POST['iati1'],1,'business Logo'); //Main image Update at insert
                    }
                }   
                return $this->save_json();
            }else{   
                if (!empty($data['business_logo'])) { 
                  $this->imageUpdate($data['business_logo'],$_POST['iati1'],1,'business Logo'); 
                  //Main image  Update at update
                }
                \DB::table($this->table_name)->where('business_id', $id)->update($data);
                return $this->update_json();
            } 
        }
    
        
    public function delete(Request $request)
        {    
            $params = $request->all();
            $id=\Arr::get($params, 'id'); 
            
            $is_updated = \DB::table($this->table_name)->where('business_id', $id)->update(['is_delete' => 1]);
            return $this->success_json('delete');
        }
    public function removeImages(Request $request)
        {
            $params = $request->all();
            $images_id=\Arr::get($params, 'images_id');
            $business_id=\Arr::get($params, 'business_id');
            if(!empty($images_id))
                {
                    $image_name = Image_model::get_edit_detail($images_id);
                    if(!empty($image_name['image_name'])){
                        unlink('assets/upload/images/original/'.$image_name['image_name']);
                        unlink('assets/upload/images/thumb/'.$image_name['image_name']);
                    }
                    $is_updated=\DB::table('images')->where('image_id', $images_id)->update(['image_status'=>0,'is_delete' => 1]);
                    if($is_updated){
                        return $this->success_json('delete');
                    }
                }
        }    
    /*public function status(Request $request)
    {
        $params = $request->all();
        $id=\Arr::get($params, 'id');
        $status=\Arr::get($params, 'status');

        $is_updated = \DB::table($this->table_name)->where('business_id', $id)->update(['status_name' => $status]);
        return $this->success_json('status');
    }*/


 




}
