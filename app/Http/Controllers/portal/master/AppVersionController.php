<?php

namespace App\Http\Controllers\portal\master;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Http;

use App\Models\portal\master\App_version_model;

class AppVersionController extends Controller
{
    private $table_name;
    private $view_title;
    private $active;
    private $sub;
    private $dt_table_display_name;
    private $dt_table_small_name;
    private $route_name;
    private $add_edit_type;
    private $grid_add_button_name;
    private $grid_title;
    private $view_path;

    public function __construct(){

        $this->table_name = 'system_updates';
        $this->view_title = 'App Version Management';  
        $this->active = 'app-version';
        $this->sub = 'app-version';
        $this->grid_title = 'App Version List';
        $this->dt_table_display_name = 'App Version';
        $this->dt_table_small_name = 'app_version';
        $this->route_name = 'app-version';
        $this->add_edit_type = 'model';
        $this->grid_add_button_name = 'Add New App version';
        $this->view_path = 'portal/master/app-version/';

    }

    public function index()
    { 
        $data=['title'=>$this->view_title,'active'=>$this->active,'sub'=>$this->sub];
        return view('portal/master/master',compact('data')); 
    }


    public function dt_col()
    {
        $data=['title'=>$this->view_title,'active'=>$this->active,'sub'=>$this->sub];

        /*Here we will use grid's data for making it dynamic*/ 
        $grid_columns = [
            [
                'name'=>'No',
                'width'=>'width="2%"',
                'sortable'=>'false', 
                'style'=>'style=""',
                'class'=>'class="text-center"',
            ],
            [
                'name'=> 'Type',
                'width'=>'width="15%"',
                'sortable'=>'false',
                'style'=>'style=""',
                'class'=>'',
            ],
            [
                'name'=> 'version',
                'width'=>'width="10%"',
                'sortable'=>'false',
                'style'=>'style=""',
                'class'=>'', 
            ],
            [
                'name'=> 'Date',
                'width'=>'width="10%"',
                'sortable'=>'true',
                'style'=>'style=""',
                'class'=>'', 
            ],
            [
                'name'=> 'Feature / Remarks',
                'width'=>'width="45%"',
                'sortable'=>'false',
                'style'=>'style=""',
                'class'=>'', 
            ],
            [
                'name'=>'Action',
                'width'=>'width="15%"',
                'sortable'=>'false',
                'style'=>'style=""',
                'class'=>'', 
            ]
        ];

        $table_style='border-collapse: collapse; border-spacing: 0; width: -webkit-fill-available;';
        $table_class='table table-striped nowrap table-bordered dt-responsive nowrap';

        if($this->add_edit_type == 'model'){
            $data["extra_pages"] = ['portal/master/'.$this->route_name.'/add_modal'];
            $add_url = false;
        }else{
            $add_url = url('/'.$this->route_name.'-add');
        }

        $data['grid'] = [
                'grid_name'             =>  $this->dt_table_display_name,
                'grid_add_button'       =>  true,
                'grid_add_button_name'  =>  $this->grid_add_button_name,
                'grid_add_url'          =>  $add_url,
                'grid_dt_url'           =>  url('/'.$this->route_name.'-list'),
                'grid_delete_url'       =>  url('/'.$this->route_name.'-delete/'),
                'grid_status_url'       =>  url('/'.$this->route_name.'-status/'),
                'grid_data_url'         =>  url('/'.$this->route_name.'-edit/'), 
                'grid_columns'          =>  $grid_columns,
                'grid_order_by'         =>  '1',
                'grid_order_by_type'    =>  'DESC',
                'grid_tbl_name'         =>  $this->dt_table_small_name,
                'grid_title'            =>  $this->grid_title,
                'grid_tbl_display_name' =>  $this->dt_table_display_name,
                'grid_tbl_length'       =>  '10',
                'grid_tbl_style'        =>  $table_style,
                'grid_tbl_class'        =>  $table_class
        ];
        
        return view('portal/master/master',$data); 
    }


    public function dt_list( $id = -1 )
    { 

        $start_index    = $_GET['iDisplayStart']!=null?$_GET['iDisplayStart']:0;
        $end_index      = $_GET['iDisplayLength']?$_GET['iDisplayLength']:10;      
        $search_text    = $_GET['sSearch']?$_GET['sSearch']:''; 
        $aColumns       = ["update_id","update_device","update_version","update_features","update_date"];
        $aColumns_where = ["update_id","update_device","update_version","update_features","update_remarks","update_date"];

        $order_by       = "";
        $where          = "";
        $order_by_type  = "DESC";

        if ( $_GET['iSortCol_0'] !== FALSE ){
            for ( $i=0 ; $i<intval($_GET['iSortingCols']); $i++ ){ if ($_GET['bSortable_'.intval($_GET['iSortCol_'.$i])] == "true" ){ $order_by = $aColumns[ intval( ( $_GET['iSortCol_'.$i] ) ) ]; $order_by_type = $this->mres( $_GET['sSortDir_'.$i] ); }
            }
        }

        for ( $i=0 ; $i<count($aColumns_where) ; $i++ ){ if ( isset($_GET['bSearchable_'.$i])  && $_GET['bSearchable_'.$i] == "true" && $_GET['sSearch_'.$i] != '' ){if($where != ''){$where .= " AND ";} $where .= $aColumns_where[$i]." = '".$this->mres($_GET['sSearch_'.$i])."' ";}
        }

        if( isset($_GET['sSearch'])  ){
            $where .= '('; $or = '';foreach( $aColumns_where as $row ){ $where .= $or.$row." LIKE '%".str_replace("'","\\\\\''",$this->mres($_GET['sSearch']))."%'"; if($or== ''){$or =' OR ';} }$where .= ')';
        }
        
        $filter='';
        
        /*Get Data From Model*/
        $pass_data =   array(
            'limit_start'       =>  $start_index,
            'limit_length'      =>  $end_index,
            'where_raw'         =>  $where.$filter,
            "order_by"          =>  $order_by,
            "order_by_type"     =>  $order_by_type,
        );

        $all_data = App_version_model::dt_list_data($pass_data);

        $data           = [];
        $i=$start_index;

        foreach( $all_data['result'] as $row ){
            $row_dt   = [];

            $row_dt[] = ++$i;

            if($row->update_device == 1){
                $row_dt[] = 'Android Consumer App';
            }elseif($row->update_device == 2){
                $row_dt[] = 'iOS Consumer App';
            }elseif($row->update_device == 3){
                $row_dt[] = 'Android Driver App';
            }elseif($row->update_device == 4){
                $row_dt[] = 'Android Vendor App';
            }
            
            $row_dt[] = $row->update_version;
            $row_dt[] = date('d-m-Y', strtotime($row->update_date)); 

            $row_dt[] = '<b>Features:</b> '.$row->update_features.'<br>
                         <b>Remarks :</b> '.$row->update_remarks;

            $action = ''; 

            $action .= '<a class="dropdown-item"  href="#" onclick="js_edit('.$row->update_id.')"  title="Edit '.$this->route_name.'"> <i class="fa fa-edit"></i> Edit</a>';

            $action .= '<a class="dropdown-item"  href="#" onclick="js_delete('.$row->update_id.')"  title="Delete '.$this->route_name.'"> <i class="fa fa-trash"></i> Delete</a>';

            $row_dt[] = '<button class="btn btn-outline-primary dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> Action </button>
                    <div class="dropdown-menu">'.$action.'                      
                </div>';
            
            $data[] = $row_dt;
        }

        $response['iTotalRecords'] = $response['iTotalDisplayRecords'] = $all_data['total'];
        $response['aaData'] = $data;
        
        return response()->json($response);
    }

    public function add()
    {
        if($this->add_edit_type == 'model'){
            return redirect('/'.$this->route_name.'-master');
        }else{
            $data = array();
            return view($this->view_path.'add',$data);
        }
    }

    public function edit($passed_id)
    { 
        $data = App_version_model::get_edit_detail($passed_id);
        
        if($this->add_edit_type == 'model'){
            return view($this->view_path.'edit_modal',$data); 
        }else{
            return view($this->view_path.'edit',$data); 
        }
    }


    public function save(Request $request)
    { 
        $params = $request->all();

        $data=array();
        $fields=array("update_date","update_version","update_device","update_features","update_remarks","update_status");

        foreach ($fields as $field) 
        {
            $data[$field]= \Arr::get($params, $field);
        }

        $id=\Arr::get($params, 'id');
        $mode=\Arr::get($params, 'mode');

        if ($mode=='add') { 
            
            \DB::table($this->table_name)->insert($data);
            return $this->save_json();

        }else{   

            \DB::table($this->table_name)->where('update_id', $id)->update($data);
            return $this->update_json();

        } 
    }


    public function delete(Request $request)
    {    
        
        $params = $request->all();
        $id=\Arr::get($params, 'id');
        
        $is_updated = \DB::table($this->table_name)->where('update_id', $id)->update(['is_delete' => 1]);
        return $this->success_json('delete');
    }

    public function status(Request $request)
    {
        $params = $request->all();
        $id=\Arr::get($params, 'id');
        $status=\Arr::get($params, 'status');

        $is_updated = \DB::table($this->table_name)->where('update_id', $id)->update(['update_status' => $status]);
        return $this->success_json('status');

    }


 




}
