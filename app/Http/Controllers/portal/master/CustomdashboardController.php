<?php

namespace App\Http\Controllers\portal\master; 
 
use App\Http\Controllers\Controller; 
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator; 
use Illuminate\Support\Facades\Http;
 
use App\Models\portal\master\Custom_dashboard_model;
use App\Models\portal\master\Category_model;
use DB;

class CustomdashboardController extends Controller
{   
    private $table_name;  
    private $view_title;
    private $active; 
    private $sub; 
    private $dt_table_display_name;  
    private $dt_table_small_name;
    private $route_name;
    private $add_edit_type;
    private $grid_add_button_name;
    private $grid_title;
    private $view_path;

    public function __construct(){

        $this->table_name = 'custom_dashboard';
        $this->view_title = 'Custom Dashboard Management';  
        $this->active = 'custom_dashboard';
        $this->sub = 'custom_dashboard';
        $this->grid_title = 'Custom Dashboard Settings';
        $this->dt_table_display_name = 'Custom Dashboard';
        $this->dt_table_small_name = strtolower($this->dt_table_display_name);
        $this->route_name = 'custom-dashboard';
        $this->add_edit_type = ''; 
        $this->grid_add_button_name = 'Add '.$this->route_name;
        $this->view_path = 'portal/master/custom-dashboard/';
    }

    public function index()
        { 
            $data=['title'=>$this->view_title,'active'=>$this->active,'sub'=>$this->sub];
            return view('portal/master/master',compact('data')); 
        }

    public function dt_col()
        {

            $data=['title'=>$this->view_title,'active'=>$this->active,'sub'=>$this->sub];
            /*Here we will use grid's data for making it dynamic*/ 
            $grid_columns = [
                [
                    'name'=>'No',
                    'width'=>'width="10%"',
                    'sortable'=>'false', 
                    'style'=>'style=""',
                    'class'=>'class="text-left"',
                ],
                [
                    'name'=> 'Name',
                    'width'=>'width="30%"',
                    'sortable'=>'true',
                    'style'=>'style=""',
                    'class'=>'',
                ],
                [
                    'name'=> 'Position',
                    'width'=>'width="10%"',
                    'sortable'=>'true',
                    'style'=>'style=""',
                    'class'=>'',
                ],
                [
                    'name'=> 'App Status',
                    'width'=>'width="20%"',
                    'sortable'=>'true',
                    'style'=>'style=""',
                    'class'=>'',
                ],
                [
                    'name'=>'Action',
                    'width'=>'width="20%"',
                    'sortable'=>'false',
                    'style'=>'style=""',
                    'class'=>'', 
                ]
            ];

            $table_style='bCustom Dashboard-collapse: collapse; bCustom Dashboard-spacing: 0; width: -webkit-fill-available;';
            $table_class='table table-striped nowrap table-bCustom Dashboarded dt-responsive nowrap';

            if($this->add_edit_type == 'model'){
                $data["extra_pages"] = ['portal/master/'.$this->route_name.'/add_modal'];
                $add_url = false;
            }else{
                $add_url = url('/'.$this->route_name.'-add');
            }

            $data['grid'] = [
                    'grid_name'             =>  $this->dt_table_display_name,
                    'grid_add_button'       =>  true,
                    'grid_add_button_name'  =>  $this->grid_add_button_name,
                    'grid_add_url'          =>  $add_url,
                    'grid_dt_url'           =>  url('/'.$this->route_name.'-list'),
                    'grid_delete_url'       =>  url('/'.$this->route_name.'-delete/'),
                    'grid_status_url'       =>  url('/'.$this->route_name.'-status/'),
                    'grid_data_url'         =>  url('/'.$this->route_name.'-edit/'), 
                    'grid_columns'          =>  $grid_columns,
                    'grid_order_by'         =>  '2',
                    'grid_order_by_type'    =>  'ASC',
                    'grid_tbl_name'         =>  'custom_dashboard',
                    'grid_title'            =>  $this->grid_title,
                    'grid_tbl_display_name' =>  $this->dt_table_display_name,
                    'grid_tbl_length'       =>  '10',
                    'grid_tbl_style'        =>  $table_style,
                    'grid_tbl_class'        =>  $table_class
            ];
            return view('portal/master/master',$data); 
        }

    public function dt_list( $id = -1 )
        { 


            $start_index    = $_GET['iDisplayStart']!=null?$_GET['iDisplayStart']:0;
            $end_index      = $_GET['iDisplayLength']?$_GET['iDisplayLength']:10;      
            $search_text    = $_GET['sSearch']?$_GET['sSearch']:''; 
            $aColumns       = ['custom_dashboard.custom_dashboard_id','custom_dashboard.custom_dashboard_name','custom_dashboard.custom_dashboard_position','custom_dashboard.custom_dashboard_status'];
            $aColumns_where = ['custom_dashboard.custom_dashboard_id','custom_dashboard.custom_dashboard_name','custom_dashboard.custom_dashboard_position','custom_dashboard.custom_dashboard_status'];

            $order_by       = "";
            $where          = "";
            $order_by_type  = "DESC";

            if ( $_GET['iSortCol_0'] !== FALSE ){
                for ( $i=0 ; $i<intval($_GET['iSortingCols']); $i++ ){ if ($_GET['bSortable_'.intval($_GET['iSortCol_'.$i])] == "true" ){ $order_by = $aColumns[ intval( ( $_GET['iSortCol_'.$i] ) ) ]; $order_by_type = $this->mres( $_GET['sSortDir_'.$i] ); }
                }
            }

            for ( $i=0 ; $i<count($aColumns_where) ; $i++ ){ if ( isset($_GET['bSearchable_'.$i])  && $_GET['bSearchable_'.$i] == "true" && $_GET['sSearch_'.$i] != '' ){if($where != ''){$where .= " AND ";} $where .= $aColumns_where[$i]." = '".$this->mres($_GET['sSearch_'.$i])."' ";}
            }

            if( isset($_GET['sSearch'])  ){
                $where .= '('; $or = '';foreach( $aColumns_where as $row ){ $where .= $or.$row." LIKE '%".str_replace("'","\\\\\''",$this->mres($_GET['sSearch']))."%'"; if($or== ''){$or =' OR ';} }$where .= ')';
            }
            
            $filter='';
            
            /*Get Data From Model*/
            $pass_data =   array(
                'limit_start'       =>  $start_index,
                'limit_length'      =>  $end_index,
                'where_raw'         =>  $where.$filter,
                "order_by"          =>  $order_by,
                "order_by_type"     =>  $order_by_type,
            );

            $all_data = Custom_dashboard_model::dt_list_data($pass_data);
            $data           = [];
            $i=$start_index;

            foreach( $all_data['result'] as $row ){
                $row_dt   = [];
                $row_dt[] = '#'.$row->custom_dashboard_id;
                $row_dt[] = $row->custom_dashboard_name;
                $row_dt[] = $row->custom_dashboard_position;
                if($row->custom_dashboard_status == 0){
                    $row_dt[] = '<div style="cursor:pointer"  class="badge badge-danger">Hide</div>';
                    $status = '<i class="fa fa-check"></i> &nbsp;&nbsp; Show';
                    $status_type = 1;
                }else{
                    $row_dt[] = '<div style="cursor:pointer"  class="badge badge-success">Show</div>';
                    $status = '<i class="fa fa-ban"></i> &nbsp;&nbsp; Hide'; 
                    $status_type = 0;
                }    
                
                $action = ''; 
                $action .= '<a class="dropdown-item" href="'.url('custom-dashboard-edit').'/'.$row->custom_dashboard_id.'"   title="Edit '.$this->route_name.'"> <i class="fa fa-edit"></i> &nbsp;&nbsp;Edit</a>';

                $action .= '<a class="dropdown-item"  href="#" onclick="js_delete('.$row->custom_dashboard_id.')"  title="Delete '.$this->route_name.'"> <i class="fa fa-trash"></i> &nbsp;&nbsp;Delete</a>';

                $action .= '<a class="dropdown-item" style="color:black !important;" href="javascript:;" onclick="js_status('.$row->custom_dashboard_id.','.$status_type.')">'.$status.'</a>';

                $row_dt[] = '<button class="btn btn-outline-primary dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> Action </button>
                        <div class="dropdown-menu">'.$action.'
                    </div>';
                
                $data[] = $row_dt;
            }

            $response['iTotalRecords'] = $response['iTotalDisplayRecords'] = $all_data['total'];
            $response['aaData'] = $data;
            
            return response()->json($response);
        }

    public function add()
        {
            $data = array();
            
            $data['category_list'] = Category_model::get_custom_category_ajax_list();
            if($this->add_edit_type == 'model'){
                return redirect('/'.$this->route_name.'-master',$data);
            }else{
                return view($this->view_path.'add',$data);
            }
        }

    public function edit($passed_id)
        { 
            $data = Custom_dashboard_model::get_edit_detail($passed_id);
            // echo "<pre>"; print_r($data); exit;
            $data['category_list'] = Category_model::get_custom_category_ajax_list();
            if($this->add_edit_type == 'model'){
                return view($this->view_path.'edit_modal',$data); 
            }else{
                return view($this->view_path.'edit',$data); 
            }
        }

    public function save(Request $request)
        {
            $params = $request->all();
            $data=array();
            $fields=array("custom_dashboard_name","custom_dashboard_position","is_horizontal","sub_category","start_date","end_date","show_date","postion");
            foreach ($fields as $field) 
            {
                $data[$field]= \Arr::get($params, $field);
            }

            $id=\Arr::get($params, 'id');
            $mode=\Arr::get($params, 'mode');

            $validator = Validator::make($params, [
                'custom_dashboard_name'    => 'required|string',
                'custom_dashboard_position'    => 'required|numeric',
            ]);

            if($validator->fails()){
                return response()->json(['status'=>500,'message'=>\Arr::flatten($validator->errors()->toArray())[0]]);
            }
            
            $upcomingData = [];
            $custom_dashboardata = [
                'custom_dashboard_name'=>$data['custom_dashboard_name'],'custom_dashboard_position'=>$data['custom_dashboard_position'],
                'is_horizontal'=>$data['is_horizontal'],
            ];
            if ($mode=='add') {
                $custom_dashboard_id = \DB::table($this->table_name)->insertGetId($custom_dashboardata);
                
                $i=0; 
                foreach($data['sub_category'] as $key=>$value){
                    $upcomingData = [
                        'custom_upcoming_dashboard_id' =>$custom_dashboard_id,
                        'custom_upcoming_category_id'  =>$value,
                        'custom_upcoming_start_date'   =>$data['start_date'][$i],
                        'custom_upcoming_end_date'     =>$data['end_date'][$i],
                        'custom_upcoming_show_date'    =>$data['show_date'][$i],
                        'custom_upcoming_postion'      =>$data['postion'][$i],
                    ];
                    \DB::table('custom_upcoming')->insert($upcomingData);
                $i++;    
                }
                return $this->save_json();
            }else{  
                $custom_dashboardUpcomingOld = Custom_dashboard_model::get_edit_detail($id)['custom_upcoming'];
                if(!empty($custom_dashboardUpcomingOld)){
                    foreach($custom_dashboardUpcomingOld as $k => $value)
                    {
                        DB::table('custom_upcoming')->where('custom_upcoming_dashboard_id',$value->custom_upcoming_dashboard_id)->where('custom_upcoming_id',$value->custom_upcoming_id)->delete();
                    }
                    $i=0; 
                    foreach($data['sub_category'] as $key=>$value){
                        $upcomingData = [
                            'custom_upcoming_dashboard_id' =>$id,
                            'custom_upcoming_category_id'  =>$value,
                            'custom_upcoming_start_date'   =>$data['start_date'][$i],
                            'custom_upcoming_end_date'     =>$data['end_date'][$i],
                            'custom_upcoming_show_date'    =>$data['show_date'][$i],
                            'custom_upcoming_postion'      =>$data['postion'][$i],
                        ];
                        \DB::table('custom_upcoming')->insert($upcomingData);
                    $i++;    
                    }  
                }
                /*update custom_dashboard*/
                \DB::table($this->table_name)->where('custom_dashboard_id',$id)->update($custom_dashboardata);
                return $this->update_json();
            } 
        }
    public function delete(Request $request)
        {    
            $params = $request->all();
            $id=\Arr::get($params, 'id');
            $custom_dashboardUpcomingOld = Custom_dashboard_model::get_edit_detail($id)['upcoming'];
                if(!empty($custom_dashboardUpcomingOld)){
                    foreach($custom_dashboardUpcomingOld as $k => $value)
                    {
                        
                        \DB::table('custom_upcoming')->where('custom_upcoming_dashboard_id',$value->custom_upcoming_dashboard_id)->where('custom_upcoming_id',$value->custom_upcoming_id)->update(['is_delete' => 1]);
                    }
                }
            $is_updated = \DB::table($this->table_name)->where('custom_dashboard_id', $id)->update(['is_delete' => 1]);
            return $this->success_json('delete');
        }
    public function status(Request $request)
        {
            $params = $request->all();
            $id=\Arr::get($params, 'id');
            $status=\Arr::get($params, 'status');
            $is_updated = \DB::table($this->table_name)->where('custom_dashboard_id', $id)->update(['custom_dashboard_status' => $status]);
            return $this->success_json('status');

        }    



 




}
