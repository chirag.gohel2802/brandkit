<?php

namespace App\Http\Controllers\portal\master;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Http;
 
use Image; 
use App\Models\portal\master\Image_model;

class ImageController extends Controller 
{
    private $table_name;
    private $view_title;  
    private $active; 
    private $sub;
    private $dt_table_display_name; 
    private $dt_table_small_name;
    private $route_name;
    private $add_edit_type;
    private $grid_add_button_name;
    private $grid_title;
    private $view_path;

    public function __construct(){

        $this->table_name   = 'images';
        $this->view_title   = 'Images Management';  
        $this->active       = 'images';
        $this->sub          = 'images';
        $this->grid_title = 'Images List';
        $this->dt_table_display_name = 'Images';
        $this->dt_table_small_name = strtolower($this->dt_table_display_name);
        $this->route_name = 'images'; 
        $this->add_edit_type = 'model';
        $this->grid_add_button_name = 'Add '.$this->route_name;
        $this->view_path = 'portal/master/images/';
    }
    public function uploadImage(Request $request)
        {
            $params = $request->all();
            $data=array();
            $fields=array("image_alt_tag");
            
            
            foreach ($fields as $field) 
            {
                $data[$field]= \Arr::get($params, $field);
            }
            $validator = Validator::make($params, [
                'image_file' => 'mimes:jpeg,jpg,png,gif,svg|required',
            ]);

            if($validator->fails()){ 
                return response()->json(['status'=>500,'message'=>\Arr::flatten($validator->errors()->toArray())[0]]);
            }  

            $image = $request->file('image_file');
            $filenamewithextension  = $image->getClientOriginalName();
            $original_name          = pathinfo($filenamewithextension, PATHINFO_FILENAME);
            $extension              = $image->getClientOriginalExtension();

            $thumbnailPath          = 'public/assets/upload/images/thumb/';
            $originalPath           = 'public/assets/upload/images/original/'; 

            if (!is_dir($originalPath)) {
                mkdir($originalPath, 0775, true);
            }
            if (!is_dir($thumbnailPath)) {
                mkdir($thumbnailPath, 0775, true);
            }
            $uploadFileName = uniqid().'-'.$original_name.'.'.$extension; // new file name
            if($extension =='svg'){
                $image->move($originalPath, $uploadFileName);
            }else{
                /*Watermark image in Orignal*/
                $img = Image::make($image->path());

                /*$img->text('', 250, 200, function($font) { 
                    $font->size(200);  
                    $font->color('#7e7ec7');  
                    $font->align('center');  
                    $font->valign('bottom');  
                })->save($originalPath.$uploadFileName);*/
                
                $img->save($originalPath.$uploadFileName);
                $img->resize(200,200, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($thumbnailPath.$uploadFileName);

                /*compress*/
                /*cloudinary()->upload($image->getRealPath(), [
                        'folder' => $compPath,
                        'transformation' => [
                                  'quality' => auto,
                                  'fetch_format' => auto
                        ]
                    ])->getSecurePath();*/
                /*Compress End*/
                
            }
            // $image->move($originalPath, $uploadFileName);

            /*Start save*/
            $upload_image = [
                'image_url'         =>  'public/assets/upload/images/original/'.$uploadFileName,
                'image_file_name'   =>  $original_name,
                'image_name'        =>  $uploadFileName ,
                'image_details'     =>  'image',
                'image_status'      =>  0,
                'image_alt_tag'     =>  $data['image_alt_tag'],
            ];       
            $image_id=\DB::table($this->table_name)->insertGetId($upload_image); //insert image in database
            return response()->json(['status'=>200,'image_id'=>$image_id]);
            /*End Save*/  
        }    


   
}
