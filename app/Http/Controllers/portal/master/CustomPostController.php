<?php

namespace App\Http\Controllers\portal\master; 
 
use App\Http\Controllers\Controller; 
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator; 
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Str; 

use Image;
use App\Models\portal\master\Custom_post_model;
use App\Models\portal\master\Category_model;  
use App\Models\portal\master\Image_model;  
use App\Models\portal\master\User_model;
use DB;

class CustomPostController extends Controller
{  
    private $table_name; 
    private $view_title; 
    private $active;
    private $sub;
    private $dt_table_display_name; 
    private $dt_table_small_name;
    private $route_name;
    private $add_edit_type;
    private $grid_add_button_name;
    private $grid_title;
    private $view_path;

    public function __construct(){

        $this->table_name = 'post';
        $this->view_title = 'Custom post Management';  
        $this->active = 'custom-post';
        $this->sub = 'custom-post';
        $this->grid_title = 'Custom post List';
        $this->dt_table_display_name = 'custom_post';
        $this->dt_table_small_name = strtolower($this->dt_table_display_name);
        $this->route_name = 'custom-post';
        $this->add_edit_type = ''; 
        $this->grid_add_button_name = 'Add '.$this->route_name;
        $this->view_path = 'portal/master/custom-post/';
    }

    public function index()
        { 
            $data=['title'=>$this->view_title,'active'=>$this->active,'sub'=>$this->sub];
            return view('portal/master/master',compact('data')); 
        }

    public function dt_col()
        {
            $data=['title'=>$this->view_title,'active'=>$this->active,'sub'=>$this->sub];

            // for filter 
            $get_data='?';
            $data['parent_category_id'] = 0;

            if(!empty($_GET['parent_category_id'])){
                $get_data .= 'parent_category_id='.$_GET['parent_category_id'].'&';
                $data['parent_category_id'] = $_GET['parent_category_id'];
            }


            $data['return_url'] = url('/custom-post-master');
            $data['filter_file'] = 'portal.master.custom-post.filter';
    
            $grid_dt_url = url('/'.$this->route_name.'-list').$get_data;
            $data['sub_list'] = Category_model::get_custom_category_ajax_list();
            // for filter

            /*Here we will use grid's data for making it dynamic*/ 
            $grid_columns = [
                [
                    'name'=>'No',
                    'width'=>'width="5%"',
                    'sortable'=>'true', 
                    'style'=>'style=""',
                    'class'=>'class="text-center"',
                ],
                [
                    'name'=> 'Categories',
                    'width'=>'width="15%"',
                    'sortable'=>'true',
                    'style'=>'style=""',
                    'class'=>'',
                ],
                [
                    'name'=> 'Name',
                    'width'=>'width="20%"',
                    'sortable'=>'true',
                    'style'=>'style=""',
                    'class'=>'',
                ],
                [
                    'name'=> 'Post',
                    'width'=>'width="15%"',
                    'sortable'=>'false',
                    'style'=>'style=""',
                    'class'=>'', 
                ],
                [
                    'name'=> 'Date',
                    'width'=>'width="20%"',
                    'sortable'=>'true',
                    'style'=>'style=""',
                    'class'=>'', 
                ],
                [
                    'name'=> 'Type',
                    'width'=>'width="10%"',
                    'sortable'=>'true',
                    'style'=>'style=""',
                    'class'=>'', 
                ],
                [
                    'name'=> 'Status',
                    'width'=>'width="10%"',
                    'sortable'=>'false',
                    'style'=>'style=""',
                    'class'=>'', 
                ],
                [
                    'name'=>'Action',
                    'width'=>'width="10%"',
                    'sortable'=>'false',
                    'style'=>'style=""',
                    'class'=>'', 
                ]
            ];

            $table_style='bPost-collapse: collapse; bPost-spacing: 0; width: -webkit-fill-available;';
            $table_class='table table-striped nowrap table-bPosted dt-responsive nowrap';

            if($this->add_edit_type == 'model'){
                $data["extra_pages"] = ['portal/master/'.$this->route_name.'/add_modal'];
                $add_url = false;
            }else{
                $add_url = url('/'.$this->route_name.'-add');
            }

            $data['grid'] = [
                    'grid_name'             =>  $this->dt_table_display_name,
                    'grid_add_button'       =>  true,
                    'grid_add_button_name'  =>  $this->grid_add_button_name,
                    'grid_add_url'          =>  $add_url,
                    'grid_dt_url'           =>  $grid_dt_url,
                    'grid_delete_url'       =>  url('/'.$this->route_name.'-delete/'),
                    'grid_status_url'       =>  url('/'.$this->route_name.'-status/'),
                    'grid_data_url'         =>  url('/'.$this->route_name.'-edit/'), 
                    'grid_columns'          =>  $grid_columns,
                    'grid_order_by'         =>  '0',
                    'grid_order_by_type'    =>  'DESC',
                    'grid_tbl_name'         =>  $this->dt_table_small_name,
                    'grid_title'            =>  $this->grid_title,
                    'grid_tbl_display_name' =>  $this->dt_table_display_name,
                    'grid_tbl_length'       =>  '10',
                    'grid_tbl_style'        =>  $table_style,
                    'grid_tbl_class'        =>  $table_class
            ];
            return view('portal/master/master',$data); 
        }

    public function dt_list( $id = -1 )
        {

            $start_index    = $_GET['iDisplayStart']!=null?$_GET['iDisplayStart']:0;
            $end_index      = $_GET['iDisplayLength']?$_GET['iDisplayLength']:10;      
            $search_text    = $_GET['sSearch']?$_GET['sSearch']:''; 
            $aColumns       = ['post.post_id','post.post_name','post.post_category','post.post_image','post.post_date','post.post_type','post.post_package','post.post_status'];
            $aColumns_where = ['post.post_id','post.post_name','post.post_category','post.post_image','post.post_date','post.post_type','post.post_package','post.post_status'];

            $order_by       = "";
            $where          = "";
            $order_by_type  = "DESC";

            if ( $_GET['iSortCol_0'] !== FALSE ){
                for ( $i=0 ; $i<intval($_GET['iSortingCols']); $i++ ){ if ($_GET['bSortable_'.intval($_GET['iSortCol_'.$i])] == "true" ){ $order_by = $aColumns[ intval( ( $_GET['iSortCol_'.$i] ) ) ]; $order_by_type = $this->mres( $_GET['sSortDir_'.$i] ); }
                }
            }

            for ( $i=0 ; $i<count($aColumns_where) ; $i++ ){ if ( isset($_GET['bSearchable_'.$i])  && $_GET['bSearchable_'.$i] == "true" && $_GET['sSearch_'.$i] != '' ){if($where != ''){$where .= " AND ";} $where .= $aColumns_where[$i]." = '".$this->mres($_GET['sSearch_'.$i])."' ";}
            }

            if( isset($_GET['sSearch'])  ){
                $where .= '('; $or = '';foreach( $aColumns_where as $row ){ $where .= $or.$row." LIKE '%".str_replace("'","\\\\\''",$this->mres($_GET['sSearch']))."%'"; if($or== ''){$or =' OR ';} }$where .= ')';
            }
            
            $filter='';
            // for filter 
            if(!empty($_GET['parent_category_id'])){
                $filter .= ' AND ( post.post_category = '.$_GET['parent_category_id'].' ) ';
            }

            /*Get Data From Model*/
            $pass_data =   array(
                'limit_start'       =>  $start_index,
                'limit_length'      =>  $end_index,
                'where_raw'         =>  $where.$filter,
                "order_by"          =>  $order_by,
                "order_by_type"     =>  $order_by_type,
            );

            $all_data = Custom_post_model::dt_list_data($pass_data);

            $data           = [];
            $i=$start_index;

            foreach( $all_data['result'] as $row ){
                $parent_category =  DB::table('category')->select('category.category_name')->where('category_id',$row->category_parent_id)->first()->category_name;
                $row_dt   = [];
                $row_dt[] = '# '.$row->post_id;
                $row_dt[] = $row->category_name.'<br>'.$parent_category;
                $row_dt[] = $row->post_name;
                if(!empty($row->image_name)){
                    $row_dt[] = '<img src="'.url('assets/upload/images/thumb/'.$row->image_name).'" style="height: 80px; width: ; object-fit: cover;">';
                }else{
                    $row_dt[] = '';
                }
                if(!empty($row->post_date)){
                    $row_dt[] = date('m-d-Y',strtotime($row->post_date));
                }else{
                    $row_dt[] = ' - ';
                }
                $row_dt[] = ucfirst($row->post_type).'<br>'.ucfirst($row->post_package);
                if ($row->post_status==1) { 
                    $row_dt[]= '<div style="cursor:pointer"  class="badge badge-success">Active</div>';
                    $status = '<i class="fa fa-ban"></i> &nbsp;&nbsp; InActive';
                    $status_type = 0;
                }else{ 
                    $row_dt[]= '<div style="cursor:pointer"  class="badge badge-danger">InActive</div>';
                    $status = '<i class="fa fa-check"></i> &nbsp;&nbsp; Active '; 
                    $status_type = 1;
                }

                $action = ''; 
                $action .= '<a class="dropdown-item" href="'.url('custom-post-edit').'/'.$row->post_id.'"  title="Edit '.$this->route_name.'"> <i class="fa fa-edit"></i> &nbsp;&nbsp;Edit</a>';
                
                $action .= '<a class="dropdown-item"  href="#" onclick="js_delete('.$row->post_id.')"  title="Delete '.$this->route_name.'"> <i class="fa fa-trash"></i> &nbsp;&nbsp;Delete</a>';

                $action .= '<a class="dropdown-item" style="color:black !important;" href="javascript:;" onclick="js_status('.$row->post_id.','.$status_type.')">'.$status.'</a>';

                $row_dt[] = '<button class="btn btn-outline-primary dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> Action </button>
                        <div class="dropdown-menu">'.$action.'
                    </div>';
                
                $data[] = $row_dt;
            }

            $response['iTotalRecords'] = $response['iTotalDisplayRecords'] = $all_data['total'];
            $response['aaData'] = $data;
            
            return response()->json($response);
        }

    public function add()
        {
            $data = array();
            $data=['title'=>$this->view_title,'active'=>$this->active,'sub'=>$this->sub];

            $data['category_list'] = Category_model::get_custom_category_ajax_list();
            if($this->add_edit_type == 'model'){
                return redirect('/'.$this->route_name.'-master');
            }else{
                // echo "<pre>"; print_r($data['category_list']); exit;
                return view($this->view_path.'add',$data);
            }
        }

    public function edit($passed_id)
        { 
            $data=['title'=>$this->view_title,'active'=>$this->active,'sub'=>$this->sub];
            $data = Custom_post_model::get_edit_detail($passed_id);
            $data['category_list'] = Category_model::get_custom_category_ajax_list();
            // echo "<pre>"; print_r($data); exit;
            if($this->add_edit_type == 'model'){
                return view($this->view_path.'edit_modal',$data); 
            }else{
                return view($this->view_path.'edit',$data); 
            }
        }

    public function multiple($passed_id)
        {
            $frame = DB::table($this->table_name)->where('Custom_post_id',$passed_id)->get()->first();
            
            $frame->Custom_post_id = NULL;
            $newframe = [
                            'Custom_post_name'        =>$frame->Custom_post_name,
                            'Custom_post_package'     =>$frame->Custom_post_package,
                            'Custom_post_image'       =>$frame->Custom_post_image,
                            'assigned_user'     =>$frame->assigned_user,
                            'Custom_post_data'        =>$frame->Custom_post_data,
                            'Custom_post_address_icon'=>$frame->Custom_post_address_icon,
                            'Custom_post_website_icon'=>$frame->Custom_post_website_icon,
                            'Custom_post_phone_icon'  =>$frame->Custom_post_phone_icon,
                            'Custom_post_email_icon'  =>$frame->Custom_post_email_icon,
                            'Custom_post_status'      =>1,
                        ];
            $inserted_id = \DB::table($this->table_name)->insertGetId($newframe);
            return redirect('frame-edit/'.$inserted_id);
        }    
    public function save(Request $request)
        {
            $params = $request->all();
            $data=array();
            $fields=array("post_category","post_name","post_date","post_type","post_package","post_search_keyword","custom_post_json");
            foreach ($fields as $field) 
            {
                $data[$field]= \Arr::get($params, $field);
            }

            $id=\Arr::get($params, 'id');
            $mode=\Arr::get($params, 'mode');

            $post_image=\Arr::get($params, 'post_image');
            $validator = Validator::make($params, [
                'post_category' => 'required',
                'post_name'    => 'required|string',
            ]);
            
            $data['is_custom_post'] = 'yes';


            if($validator->fails()){
                return response()->json(['status'=>500,'message'=>\Arr::flatten($validator->errors()->toArray())[0]]);
            }

            if ($mode=='add') {

                $post_name = $data['post_name'];
                $post_image_arr = array_filter(explode(',', $post_image));
                $data['post_image'] = $post_image;

                foreach($post_image_arr as $key => $value) {
                    $data['post_image'] = $value;
                    if(count($post_image_arr) > 1){
                        $data['post_name'] = $post_name.'-'.rand(10000,99999);
                    }
                    $inserted_id = \DB::table($this->table_name)->insertGetId($data);
                    if($inserted_id){
                        if (!empty($data['post_image'])) { 
                          $this->imageUpdate($data['post_image'],$_POST['iati1'],1,'Post'); //Main image Update at insert
                        }
                    }
                }
                
                return $this->save_json();
            }else{
                $data['post_image'] = $post_image;
                if (!empty($data['post_image'])) { 
                  $this->imageUpdate($data['post_image'],$_POST['iati1'],1,'Post'); //Main image  Update at update
                }
                \DB::table($this->table_name)->where('post_id', $id)->update($data);
                return $this->update_json();
            } 
        }
    
    public function delete(Request $request)
        {    
            $params = $request->all();
            $id=\Arr::get($params, 'id');
            
            $is_updated = \DB::table($this->table_name)->where('post_id', $id)->update(['is_delete' => 1]);
            return $this->success_json('delete');
        }
    public function removeImages(Request $request)
        {
            $params = $request->all();
            $images_id=\Arr::get($params, 'images_id');
            $post_id=\Arr::get($params, 'post_id');
            if(!empty($images_id))
                {
                    $image_name = Image_model::get_edit_detail($images_id);
                    if(!empty($image_name['image_name'])){
                        unlink('assets/upload/images/original/'.$image_name['image_name']);
                        unlink('assets/upload/images/thumb/'.$image_name['image_name']);
                    }
                    $is_updated=\DB::table('images')->where('image_id', $images_id)->update(['image_status'=>0,'is_delete' => 1]);
                    if($is_updated){
                        return $this->success_json('delete');
                    }
                }
        }    
    public function status(Request $request)
    {
        $params = $request->all();
        $id=\Arr::get($params, 'id');
        $status=\Arr::get($params, 'status');

        $is_updated = \DB::table($this->table_name)->where('post_id', $id)->update(['post_status' => $status]);
        return $this->success_json('status');
    }

 




}
