<?php

namespace App\Http\Controllers\portal\master;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator; 
use Illuminate\Support\Facades\Http;
use DB;
use App\Models\portal\master\Category_model; 
use App\Models\portal\master\Post_model; 
 
class CategoryController extends Controller
{ 
    private $table_name; 
    private $view_title;
    private $active;
    private $sub;
    private $dt_table_display_name; 
    private $dt_table_small_name;
    private $route_name;
    private $add_edit_type;
    private $grid_add_button_name; 
    private $grid_title;
    private $view_path;

    public function __construct(){

        $this->table_name = 'category';
        $this->view_title = 'Category Management';  
        $this->active = 'category';
        $this->sub = 'category';
        $this->grid_title = 'Category List';
        $this->dt_table_display_name = 'Category';
        $this->dt_table_small_name = strtolower($this->dt_table_display_name);
        $this->route_name = 'category';
        $this->add_edit_type = 'model';
        $this->grid_add_button_name = 'Add '.$this->route_name;
        $this->view_path = 'portal/master/category/';

    }

    public function index()
    { 
        $data=['title'=>$this->view_title,'active'=>$this->active,'sub'=>$this->sub];
        return view('portal/master/master',compact('data')); 
    }


    public function dt_col()
    {
        $data=['title'=>$this->view_title,'active'=>$this->active,'sub'=>$this->sub];
        if (\Session::has('category_id')){
            \Session::forget('category_id');
        }
        
        if(!empty($_GET['category_id'])){
            session(['category_id' => $_GET['category_id']]);
            
            $category_name = \DB::table('category')->select('category_name')->where('category_id',$_GET['category_id'])->get()->first();
            if(!empty($category_name)){
                $sub_list_name = $category_name->category_name;
            }else{
                $sub_list_name = '';
            }

            $this->grid_title = $sub_list_name;
            $this->grid_add_button_name = 'Add Sub Category';
            $this->dt_table_display_name = 'Sub Category';
            $grid_dt_url =  url('/'.$this->route_name.'-list').'?category_id='.$_GET['category_id'];
        }else{
            $grid_dt_url =  url('/'.$this->route_name.'-list').'?category_id=0';
        }
        /*Here we will use grid's data for making it dynamic*/ 
        $grid_columns = [
            [
                'name'  =>'No',
                'width' =>'width="5%"',
                'sortable'=>'true', 
                'style' =>'style=""',
                'class' =>'class="text-center"',
            ],
            [
                'name'=> 'Name',
                'width'=>'width="15%"',
                'sortable'=>'true',
                'style'=>'style=""',
                'class'=>'',
            ],
            [
                'name'=> 'Details',
                'width'=>'width="10%"',
                'sortable'=>'false',
                'style'=>'style=""',
                'class'=>'',
            ], [
                'name'=> 'Share Link',
                'width'=>'width="35%"',
                'sortable'=>'false',
                'style'=>'style=""',
                'class'=>'',
            ],
            [
                'name'=> 'Category Status',
                'width'=>'width="15%"',
                'sortable'=>'false',
                'style'=>'style=""',
                'class'=>'', 
            ],
            [
                'name'=>'Action',
                'width'=>'width="20%"',
                'sortable'=>'false',
                'style'=>'style=""',
                'class'=>'', 
            ]
        ];

        $table_style='bCategory-collapse: collapse; bCategory-spacing: 0; width: -webkit-fill-available;';
        $table_class='table table-striped nowrap table-bCategoryed dt-responsive nowrap';

        if($this->add_edit_type == 'model'){
            $data["extra_pages"] = ['portal/master/'.$this->route_name.'/add_modal'];
            $add_url = false;
        }else{
            $add_url = url('/'.$this->route_name.'-add');
        }

        $data['grid'] = [
                'grid_name'             =>  $this->dt_table_display_name,
                'grid_add_button'       =>  true,
                'grid_add_button_name'  =>  $this->grid_add_button_name,
                'grid_add_url'          =>  $add_url,
                'grid_dt_url'           =>  $grid_dt_url,
                'grid_delete_url'       =>  url('/'.$this->route_name.'-delete/'),
                'grid_status_url'       =>  url('/'.$this->route_name.'-status/'),
                'grid_data_url'         =>  url('/'.$this->route_name.'-edit/'), 
                'grid_columns'          =>  $grid_columns,
                'grid_order_by'         =>  '0',
                'grid_order_by_type'    =>  'DESC',
                'grid_tbl_name'         =>  $this->dt_table_small_name,
                'grid_title'            =>  $this->grid_title,
                'grid_tbl_display_name' =>  $this->dt_table_display_name,
                'grid_tbl_length'       =>  '10',
                'grid_tbl_style'        =>  $table_style,
                'grid_tbl_class'        =>  $table_class
        ];
        return view('portal/master/master',$data); 
    }

    public function dt_list( $id = -1 )
    { 

        $start_index    = $_GET['iDisplayStart']!=null?$_GET['iDisplayStart']:0;
        $end_index      = $_GET['iDisplayLength']?$_GET['iDisplayLength']:10;      
        $search_text    = $_GET['sSearch']?$_GET['sSearch']:''; 
        $aColumns       = ["category.category_id","category.category_name","category.category_type","category.category_status"];
        $aColumns_where = ["category.category_id","category.category_name","category.category_type","category.category_status"];

        $order_by       = "";
        $where          = "";
        $order_by_type  = "DESC";

        if ( $_GET['iSortCol_0'] !== FALSE ){
            for ( $i=0 ; $i<intval($_GET['iSortingCols']); $i++ ){ if ($_GET['bSortable_'.intval($_GET['iSortCol_'.$i])] == "true" ){ $order_by = $aColumns[ intval( ( $_GET['iSortCol_'.$i] ) ) ]; $order_by_type = $this->mres( $_GET['sSortDir_'.$i] ); }
            }
        }

        for ( $i=0 ; $i<count($aColumns_where) ; $i++ ){ if ( isset($_GET['bSearchable_'.$i])  && $_GET['bSearchable_'.$i] == "true" && $_GET['sSearch_'.$i] != '' ){if($where != ''){$where .= " AND ";} $where .= $aColumns_where[$i]." = '".$this->mres($_GET['sSearch_'.$i])."' ";}
        }

        if( isset($_GET['sSearch'])  ){
            $where .= '('; $or = '';foreach( $aColumns_where as $row ){ $where .= $or.$row." LIKE '%".str_replace("'","\\\\\''",$this->mres($_GET['sSearch']))."%'"; if($or== ''){$or =' OR ';} }$where .= ')';
        }
        
        $filter='';
        if (!empty($_GET['category_id'])){
            $filter= ' AND category_parent_id = '.$_GET['category_id'];
        }else{
            $filter= ' AND category_parent_id = 0';
        }
        /*Get Data From Model*/
        $pass_data =   array(
            'limit_start'       =>  $start_index,
            'limit_length'      =>  $end_index,
            'where_raw'         =>  $where.$filter,
            "order_by"          =>  $order_by,
            "order_by_type"     =>  $order_by_type,
        );

        $all_data = Category_model::dt_list_data($pass_data);

        $data           = [];
        $i=$start_index;

        foreach( $all_data['result'] as $row ){
            $row_dt   = [];
            $row_dt[] = '# '.$row->category_id;
            $row_dt[] = $row->category_name;

            if(empty($_GET['category_id'])){
                $custom_post = '';
                if(!empty($row->is_custom) && $row->is_custom==1){
                    $custom_post = '<b>Custom Post</b>';
                }
                $row_dt[] = '<small>Total Sub</small> : '.$row->totalsubcategory.'<br>'.$custom_post;
                $row_dt[] =url('open-app-categories').'/category_parent_id='.mt_rand(11111111,99999999).base64_encode($_GET['category_id']).'&category_type='.mt_rand(11111111,99999999).base64_encode($row->category_type);

            }else{
                $row_dt[] ='<small>Total Post</small> : '. DB::table('post')->select('post_id')->where('post_category','=',$row->category_id)->where('is_delete',0)->get()->count();
                $row_dt[] =url('open-app-post-list').'/post_category='.mt_rand(11111111,99999999).base64_encode($row->category_id).'&page='.mt_rand(11111111,99999999).base64_encode(1);
            }

            if ($row->category_status==1) { 
                $row_dt[]= '<div style="cursor:pointer"  class="badge badge-success">Active</div>';
                $status = '<i class="fa fa-ban"></i> &nbsp;&nbsp; InActive';
                $status_type = 0;
            }elseif ($row->category_status==0) { 
                $row_dt[]= '<div style="cursor:pointer"  class="badge badge-danger">InActive</div>';
                $status = '<i class="fa fa-check"></i> &nbsp;&nbsp; Active '; 
                $status_type = 1;
            }

            $action = ''; 
            if(empty($_GET['category_id'])){
                $action .= '<a class="dropdown-item" href="'.url('category-master').'?category_id='.$row->category_id.'" title="View sub category"> <i class="fa fa-plus"></i> &nbsp;&nbsp;View Sub category</a>';
            }
            $action .= '<a class="dropdown-item" href="javascript:void(0)" onclick="js_edit('.$row->category_id.')"  title="Edit '.$this->route_name.'"> <i class="fa fa-edit"></i> &nbsp;&nbsp;Edit</a>';
            $action .= '<a class="dropdown-item"  href="#" onclick="js_delete('.$row->category_id.')"  title="Delete '.$this->route_name.'"> <i class="fa fa-trash"></i> &nbsp;&nbsp;Delete</a>';
            $action .= '<a class="dropdown-item" style="color:black !important;" href="javascript:;" onclick="js_status('.$row->category_id.','.$status_type.')">'.$status.'</a>';
            $row_dt[] = '<button class="btn btn-outline-primary dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> Action </button>
                    <div class="dropdown-menu">'.$action.'
                </div>';
            $data[] = $row_dt;
        }

        $response['iTotalRecords'] = $response['iTotalDisplayRecords'] = $all_data['total'];
        $response['aaData'] = $data;
        
        return response()->json($response);
    }

    public function add()
    {
        if($this->add_edit_type == 'model'){
            return redirect('/'.$this->route_name.'-master');
        }else{
            $data = array();
            return view($this->view_path.'add',$data);
        }
    }

    public function edit($passed_id)
    { 
        $data = Category_model::get_edit_detail($passed_id);
        if(\Session::has('category_id')){
            
        }
        if($this->add_edit_type == 'model'){
            return view($this->view_path.'edit_modal',$data); 
        }else{
            return view($this->view_path.'edit',$data); 
        }
    }

    public function save(Request $request)
    {
        $params = $request->all();

        $data=array();
        $fields=array("category_name","category_slug","category_parent_id","is_custom","category_date","category_type","category_search_keyword","category_description","category_image");
        
        foreach ($fields as $field) 
        {
            $data[$field]= \Arr::get($params, $field);
        }

        $id=\Arr::get($params, 'id');
        $mode=\Arr::get($params, 'mode');

        $validator = Validator::make($params, [
            'category_name' => 'required|string',
            
        ]);

        if($validator->fails()){
            return response()->json(['status'=>500,'message'=>\Arr::flatten($validator->errors()->toArray())[0]]);
        }
        

        $check_arr = array();
        $check_arr[] = array('category_name','=',$data['category_name']);
        $check_arr[] = array('category_type','=',$data['category_type']);
        $check_arr[] = array('category_parent_id','=',$data['category_parent_id']);
        
        if(!empty($id)){
            $check_arr[] = array('category_id','<>',$id);
        }

        $category_exists = Category_model::check_category_exists($check_arr);
        if ($category_exists) { 
            return response()->json(['status'=>500,'message'=>'Category Already Exists']);
        }

        if ($mode=='add') { 
            if(empty($data['category_parent_id'])){
                $data['category_parent_id'] = 0; 
            }
            $inserted_id = \DB::table($this->table_name)->insertGetId($data);
            if($inserted_id){
                if (!empty($data['category_image'])) { 
                  $this->imageUpdate($data['category_image'],$_POST['iati1'],1,'category_image'); //imageUpdate
                }
            }   
            return $this->save_json();

        }else{   
            if (!empty($data['category_image'])) { 
              $this->imageUpdate($data['category_image'],$_POST['iati1'],1,'category_image'); //imageUpdate
            }
            \DB::table($this->table_name)->where('category_id', $id)->update($data);
            return $this->update_json();
        } 
    }
    public function get_cotegory_list(Request $request)
        {
            $params     =$request->all();
            $parent_id  =\Arr::get($params, 'parent_id');
            $type       =\Arr::get($params, 'type');
            
            if(\Arr::exists($params, 'selected')){
                $selected       =\Arr::get($params, 'selected');
            }
           
            if(!empty($parent_id) && !empty($type)){
                $categoryList= Category_model::get_sub_category($parent_id,$type);
            }else{
                $categoryList= Category_model::get_ajax_list();
            }
            $html = '<option value="" disabled >Select Sub Category</option>';
            foreach($categoryList as $k=>$category){

                if(!empty($selected) && !empty(explode(',', $selected))){
                    $selected_arr = explode(',', $selected);
                    $is_selected = '';
                    if(in_array($category->category_id, $selected_arr)){
                        $is_selected = 'selected';
                    }
                    $html .= '<option '.$is_selected.' value="'.$category->category_id.'">'.$category->parent_category_name.' > '.$category->category_name.'</option>';

                }else{
                    $html .= '<option value="'.$category->category_id.'">'.$category->parent_category_name.' > '.$category->category_name.'</option>';
                }
                
            }
            echo  $html; exit;
        }    
    public function delete(Request $request)
    {    
        
        $params = $request->all();
        $id=\Arr::get($params, 'id');
        $parentData = Category_model::checkParentCategory($id);
        if(!empty($parentData)){
            foreach($parentData as $key=>$value){
            \DB::table($this->table_name)->where('category_id', $value->category_id)->where('category_parent_id',$id)->update(['is_delete' => 1]);
            }
        }
        $is_updated = \DB::table($this->table_name)->where('category_id', $id)->update(['is_delete' => 1]);
        return $this->success_json('delete');
        
    }

    public function status(Request $request)
    {
        $params = $request->all();
        $id=\Arr::get($params, 'id');
        $status=\Arr::get($params, 'status');

        $is_updated = \DB::table($this->table_name)->where('category_id', $id)->update(['category_status' => $status]);
        return $this->success_json('status');

    }


 




}
