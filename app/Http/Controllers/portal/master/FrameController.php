<?php

namespace App\Http\Controllers\portal\master; 
 
use App\Http\Controllers\Controller; 
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator; 
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Str; 

use Image;
use App\Models\portal\master\Frame_model;
use App\Models\portal\master\Category_model;  
use App\Models\portal\master\Image_model;  
use App\Models\portal\master\User_model;
use DB;

class FrameController extends Controller
{  
    private $table_name; 
    private $view_title; 
    private $active;
    private $sub;
    private $dt_table_display_name; 
    private $dt_table_small_name;
    private $route_name;
    private $add_edit_type;
    private $grid_add_button_name;
    private $grid_title;
    private $view_path;

    public function __construct(){

        $this->table_name = 'frames';
        $this->view_title = 'Frame Management';  
        $this->active = 'frame';
        $this->sub = 'frame';
        $this->grid_title = 'Frame List';
        $this->dt_table_display_name = 'Frame';
        $this->dt_table_small_name = strtolower($this->dt_table_display_name);
        $this->route_name = 'frame';
        $this->add_edit_type = ''; 
        $this->grid_add_button_name = 'Add '.$this->route_name;
        $this->view_path = 'portal/master/frame/';
    }

    public function index()
        { 
            $data=['title'=>$this->view_title,'active'=>$this->active,'sub'=>$this->sub];
            return view('portal/master/master',compact('data')); 
        }

    public function dt_col()
        {
            $data=['title'=>$this->view_title,'active'=>$this->active,'sub'=>$this->sub];
            /*Here we will use grid's data for making it dynamic*/ 
            $grid_columns = [
                [
                    'name'=>'No',
                    'width'=>'width="10%"',
                    'sortable'=>'true', 
                    'style'=>'style=""',
                    'class'=>'class="text-left"',
                ],
                [
                    'name'=> 'Name',
                    'width'=>'width="20%"',
                    'sortable'=>'true',
                    'style'=>'style=""',
                    'class'=>'',
                ],
                [
                    'name'=> 'Frame',
                    'width'=>'width="20%"',
                    'sortable'=>'false',
                    'style'=>'style=""',
                    'class'=>'', 
                ],
                [
                    'name'=> 'Package',
                    'width'=>'width="15%"',
                    'sortable'=>'false',
                    'style'=>'style=""',
                    'class'=>'', 
                ],
                [
                    'name'=> 'Status',
                    'width'=>'width="15%"',
                    'sortable'=>'false',
                    'style'=>'style=""',
                    'class'=>'', 
                ],
                [
                    'name'=>'Action',
                    'width'=>'width="15%"',
                    'sortable'=>'false',
                    'style'=>'style=""',
                    'class'=>'', 
                ]
            ];

            $table_style='bPost-collapse: collapse; bPost-spacing: 0; width: -webkit-fill-available;';
            $table_class='table table-striped nowrap table-bPosted dt-responsive nowrap';

            if($this->add_edit_type == 'model'){
                $data["extra_pages"] = ['portal/master/'.$this->route_name.'/add_modal'];
                $add_url = false;
            }else{
                $add_url = url('/'.$this->route_name.'-add');
            }

            $data['grid'] = [
                    'grid_name'             =>  $this->dt_table_display_name,
                    'grid_add_button'       =>  true,
                    'grid_add_button_name'  =>  $this->grid_add_button_name,
                    'grid_add_url'          =>  $add_url,
                    'grid_dt_url'           =>  url('/'.$this->route_name.'-list'),
                    'grid_delete_url'       =>  url('/'.$this->route_name.'-delete/'),
                    'grid_status_url'       =>  url('/'.$this->route_name.'-status/'),
                    'grid_data_url'         =>  url('/'.$this->route_name.'-edit/'), 
                    'grid_columns'          =>  $grid_columns,
                    'grid_order_by'         =>  '0',
                    'grid_order_by_type'    =>  'DESC',
                    'grid_tbl_name'         =>  $this->dt_table_small_name,
                    'grid_title'            =>  $this->grid_title,
                    'grid_tbl_display_name' =>  $this->dt_table_display_name,
                    'grid_tbl_length'       =>  '10',
                    'grid_tbl_style'        =>  $table_style,
                    'grid_tbl_class'        =>  $table_class
            ];
            return view('portal/master/master',$data); 
        }

    public function dt_list( $id = -1 )
        { 

            $start_index    = $_GET['iDisplayStart']!=null?$_GET['iDisplayStart']:0;
            $end_index      = $_GET['iDisplayLength']?$_GET['iDisplayLength']:10;      
            $search_text    = $_GET['sSearch']?$_GET['sSearch']:''; 
            $aColumns       = ['frames.frame_id','frames.frame_name','frames.frame_image','frames.frame_status'];
            $aColumns_where = ['frames.frame_id','frames.frame_name','frames.frame_image','frames.frame_status'];

            $order_by       = "";
            $where          = "";
            $order_by_type  = "DESC";

            if ( $_GET['iSortCol_0'] !== FALSE ){
                for ( $i=0 ; $i<intval($_GET['iSortingCols']); $i++ ){ if ($_GET['bSortable_'.intval($_GET['iSortCol_'.$i])] == "true" ){ $order_by = $aColumns[ intval( ( $_GET['iSortCol_'.$i] ) ) ]; $order_by_type = $this->mres( $_GET['sSortDir_'.$i] ); }
                }
            }

            for ( $i=0 ; $i<count($aColumns_where) ; $i++ ){ if ( isset($_GET['bSearchable_'.$i])  && $_GET['bSearchable_'.$i] == "true" && $_GET['sSearch_'.$i] != '' ){if($where != ''){$where .= " AND ";} $where .= $aColumns_where[$i]." = '".$this->mres($_GET['sSearch_'.$i])."' ";}
            }

            if( isset($_GET['sSearch'])  ){
                $where .= '('; $or = '';foreach( $aColumns_where as $row ){ $where .= $or.$row." LIKE '%".str_replace("'","\\\\\''",$this->mres($_GET['sSearch']))."%'"; if($or== ''){$or =' OR ';} }$where .= ')';
            }
            
            $filter='';
            
            /*Get Data From Model*/
            $pass_data =   array(
                'limit_start'       =>  $start_index,
                'limit_length'      =>  $end_index,
                'where_raw'         =>  $where.$filter,
                "order_by"          =>  $order_by,
                "order_by_type"     =>  $order_by_type,
            );

            $all_data = Frame_model::dt_list_data($pass_data);

            $data           = [];
            $i=$start_index;

            foreach( $all_data['result'] as $row ){
                $row_dt   = [];
                $row_dt[] = '# '.$row->frame_id;
                $row_dt[] = $row->frame_name;
                if(!empty($row->image_name)){
                    $row_dt[] = '<img src="'.url('assets/upload/images/thumb/'.$row->image_name).'" style="height: 80px; width: ; object-fit: cover;">';
                }else{
                    $row_dt[] = '-';
                }
                $row_dt[] = $row->frame_package;

                if ($row->frame_status==1) { 
                    $row_dt[]= '<div style="cursor:pointer"  class="badge badge-success">Active</div>';
                    $status = '<i class="fa fa-ban"></i> &nbsp;&nbsp; InActive';
                    $status_type = 0;
                }else{ 
                    $row_dt[]= '<div style="cursor:pointer"  class="badge badge-danger">InActive</div>';
                    $status = '<i class="fa fa-check"></i> &nbsp;&nbsp; Active '; 
                    $status_type = 1;
                }
                
                $action = '';
                $action .= '<a class="dropdown-item" href="'.url('frame-edit').'/'.$row->frame_id.'"  title="Edit '.$this->route_name.'"> <i class="fa fa-edit"></i> &nbsp;&nbsp;Edit</a>';
                $action .= '<a class="dropdown-item"  href="#" onclick="js_delete('.$row->frame_id.')"  title="Delete '.$this->route_name.'"> <i class="fa fa-trash"></i> &nbsp;&nbsp;Delete</a>';
                $action .= '<a class="dropdown-item" style="color:black !important;" href="javascript:void(0);" onclick="js_status('.$row->frame_id.','.$status_type.')">'.$status.'</a>';

                $action .= '<a class="dropdown-item" style="color:black !important;" href="'.url('frame-multiple').'/'.$row->frame_id.'" title="Multiple"><i class="fa fa-plus-circle"></i>&nbsp;&nbsp; Multiple </a>';

                $row_dt[] = '<button class="btn btn-outline-primary dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> Action </button>
                        <div class="dropdown-menu">'.$action.'
                    </div>';
                
                $data[] = $row_dt;
            }

            $response['iTotalRecords'] = $response['iTotalDisplayRecords'] = $all_data['total'];
            $response['aaData'] = $data;
            
            return response()->json($response);
        }

    public function add()
        {
            $data = array();
            $data['user_list'] = User_model::get_ajax_list();

            if($this->add_edit_type == 'model'){
                return redirect('/'.$this->route_name.'-master');
            }else{
                return view($this->view_path.'add',$data);
            }
        }

    public function edit($passed_id)
        { 
            $data = Frame_model::get_edit_detail($passed_id);
            $data['user_list'] = User_model::get_ajax_list();
            if($this->add_edit_type == 'model'){
                return view($this->view_path.'edit_modal',$data); 
            }else{
                return view($this->view_path.'edit',$data); 
            }
        }
    public function multiple($passed_id)
        {
            $frame = DB::table($this->table_name)->where('frame_id',$passed_id)->get()->first();
            
            $frame->frame_id = NULL;
            $newframe = [
                            'frame_name'        =>$frame->frame_name,
                            'frame_package'     =>$frame->frame_package,
                            'frame_image'       =>$frame->frame_image,
                            'assigned_user'     =>$frame->assigned_user,
                            'frame_data'        =>$frame->frame_data,
                            'frame_address_icon'=>$frame->frame_address_icon,
                            'frame_website_icon'=>$frame->frame_website_icon,
                            'frame_phone_icon'  =>$frame->frame_phone_icon,
                            'frame_email_icon'  =>$frame->frame_email_icon,
                            'frame_status'      =>1,
                        ];
            $inserted_id = \DB::table($this->table_name)->insertGetId($newframe);
            return redirect('frame-edit/'.$inserted_id);
        }    
    public function save(Request $request)
        {
            $params = $request->all();
            $data=array();
            $fields=array("frame_name","frame_package","frame_image","frame_data");
            foreach ($fields as $field) 
            {
                $data[$field]= \Arr::get($params, $field);
            }
            
            
            if(!empty($request->post('assigned_user'))){ 
                $data['assigned_user'] = implode(',',$request->post('assigned_user'));
            }

            $id=\Arr::get($params, 'id');
            $mode=\Arr::get($params, 'mode');

            $validator = Validator::make($params, [
                'frame_name'    => 'required|string',
                'frame_data'    => 'required|string',
            ]);

            if($validator->fails()){
                return response()->json(['status'=>500,'message'=>\Arr::flatten($validator->errors()->toArray())[0]]);
            }
            /*Icon Upload*/
            if(!empty($request->file('address_icon')) ){
                $data['frame_address_icon']=$this->frame_icon($request->file('address_icon'));
            }else{
                $data['frame_address_icon']=$request->post('address_icon_edit');
            }
            if(!empty($request->file('website_icon')) ){
                $data['frame_website_icon']=$this->frame_icon($request->file('website_icon'));
            }else{
                $data['frame_website_icon']=$request->post('website_icon_edit');
            }
            if(!empty($request->file('phone_icon')) ){
                $data['frame_phone_icon']=$this->frame_icon($request->file('phone_icon'));
            }else{
                $data['frame_phone_icon']=$request->post('phone_icon_edit');
            }
            if(!empty($request->file('email_icon')) ){
                $data['frame_email_icon']=$this->frame_icon($request->file('email_icon'));
            }else{
                $data['frame_email_icon']=$request->post('email_icon_edit');
            }
            /*End*/


            if ($mode=='add') {
                if(array_key_exists('frame_list',$params)){
                    $frame_list=\Arr::get($params, 'frame_list');
                    $frame_data = array_filter(array_unique( explode(',',$frame_list) ));
                }else{
                    $frame_data[] = \Arr::get($params, 'frame_image');
                }
                foreach ($frame_data as $key => $value) {
                    $data['frame_image'] = $value;
                    $inserted_id = \DB::table($this->table_name)->insertGetId($data);
                    if($inserted_id){
                        if (!empty($data['frame_image'])) { 
                            $this->imageUpdate($data['frame_image'],$_POST['iati1'],1,'frame'); //Main image Update at insert
                        }
                    }
                }
                return $this->save_json();
            }else{
    
                $data['frame_image'] = $frame_image =\Arr::get($params, 'frame_image');
                if (!empty($data['frame_image'])) { 
                  $this->imageUpdate($data['frame_image'],$_POST['iati1'],1,'frame'); //Main image  Update at update
                }
                \DB::table($this->table_name)->where('frame_id', $id)->update($data);
                return $this->update_json();
            } 
        }
    public function frame_icon($iconName)    
        {
            $rand=rand(1111,9999);
            $comman_time = time();
            $hash1=md5($comman_time*$rand);
            $originalImage  = $iconName;  
            $original_name  = pathinfo($originalImage->getClientOriginalName(), PATHINFO_FILENAME);  
            $image_new_name = $hash1.Str::slug($originalImage->getClientOriginalName(),'-').'.'.$originalImage->extension();
             
            $originalImage  = $iconName; 
            $thumbnailImage = Image::make($originalImage->getRealPath());
            $thumbnailPath  = public_path().'/assets/upload/images/thumb/';
            $originalPath   = public_path().'/assets/upload/images/original/';  
            $thumbnailImage->resize(80, null, function ($constraint) {
                $constraint->aspectRatio();
            });
            $thumbnailImage->save($originalPath.$image_new_name); 
            $thumbnailImage->resize(100, null, function ($constraint) {
                $constraint->aspectRatio();
            });
            $thumbnailImage->save($thumbnailPath.$image_new_name);
            return $image_new_name;
        }
    public function delete(Request $request)
        {    
            $params = $request->all();
            $id=\Arr::get($params, 'id');
            
            $is_updated = \DB::table($this->table_name)->where('frame_id', $id)->update(['is_delete' => 1]);
            return $this->success_json('delete');
        }
    public function removeIcon(Request $request)
        {
            $params = $request->all();
            $id=\Arr::get($params, 'id');
            $icon =\Arr::get($params, 'iconname');
            $data = Frame_model::get_edit_detail($id);
            if(!empty($data[$icon])){
                unlink('public/assets/upload/images/original/'.$data[$icon]);
                unlink('public/assets/upload/images/thumb/'.$data[$icon]);
            }
            $is_updated = \DB::table($this->table_name)->where('frame_id', $id)->update([$icon => '']);
            return $this->success_json('delete');   
        }    
     
    public function status(Request $request)
    {
        $params = $request->all();
        $id=\Arr::get($params, 'id');
        $status=\Arr::get($params, 'status');

        $is_updated = \DB::table($this->table_name)->where('frame_id', $id)->update(['frame_status' => $status]);
        return $this->success_json('status');
    }


 




}
