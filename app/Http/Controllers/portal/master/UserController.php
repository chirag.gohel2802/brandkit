<?php

namespace App\Http\Controllers\portal\master;

use App\Http\Controllers\Controller; 
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator; 
use Illuminate\Support\Facades\Http;

use Illuminate\Support\Str;     
use Redirect; 

use App\Models\portal\master\User_model;
use App\Models\portal\master\Business_model;
use App\Models\portal\master\Frame_model;
use App\Models\Common; 
Use Image; 
use DB;
 

class UserController extends Controller 
{ 
    private $table_name;
    private $view_title; 
    private $active; 
    private $sub;
    private $dt_table_display_name;
    private $dt_table_small_name;
    private $route_name;
    private $add_edit_type;
    private $grid_add_button_name; 
    private $grid_title;
    private $view_path;

    public function __construct(){

        $this->table_name = 'users';
        $this->view_title = 'User Management';  
        $this->active = 'user';
        $this->sub = 'user';
        $this->grid_title = 'User List';
        $this->dt_table_display_name = 'User';
        $this->dt_table_small_name = strtolower($this->dt_table_display_name);
        $this->route_name = 'user';
        $this->add_edit_type = 'page';
        $this->grid_add_button_name = 'Add '.$this->route_name;
        $this->view_path = 'portal/master/user/';
        $this->common_model         = New Common; 


    }

    public function index()
    { 
        $data=['title'=>$this->view_title,'active'=>$this->active,'sub'=>$this->sub];
        return view('portal/master/master',compact('data')); 
    }


    public function dt_col()
    {
        $data=['title'=>$this->view_title,'active'=>$this->active,'sub'=>$this->sub];

        $grid_columns = [
            [
                'name'=>'No',
                'width'=>'width="5%"',
                'sortable'=>'true', 
                'style'=>'style=""',
                'class'=>'class="text-center"',
            ],
            [
                'name'=> 'Name',
                'width'=>'width="15%"',
                'sortable'=>'true',
                'style'=>'style=""',
                'class'=>'',
            ],
            [
                'name'=>'Contact',
                'width'=>'width="25%"',
                'sortable'=>'true',
                'style'=>'style=""',
                'class'=>'', 
            ],
            [
                'name'=>'Created At',
                'width'=>'width="20%"',
                'sortable'=>'true',
                'style'=>'style=""',
                'class'=>'', 
            ],
            [
                'name'=>'Total Business',
                'width'=>'width="10%"',
                'sortable'=>'false',
                'style'=>'style=""',
                'class'=>'', 
            ],
            [
                'name'=>'Status',
                'width'=>'width="10%"',
                'sortable'=>'false',
                'style'=>'style=""',
                'class'=>'', 
            ],
            [
                'name'=>'Action',
                'width'=>'width="15%"',
                'sortable'=>'false',
                'style'=>'style=""',
                'class'=>'', 
            ]
        ];

        $table_style='border-collapse: collapse; border-spacing: 0; width: -webkit-fill-available;';
        $table_class='table table-striped nowrap table-bordered dt-responsive nowrap';
        
        $add_url = true;
        $data["extra_pages"] = [];
        
        $data['grid'] = [
                'grid_name'             =>  $this->dt_table_display_name,
                'grid_add_button'       =>  false,
                'grid_add_button_name'  =>  $this->grid_add_button_name,
                'grid_add_url'          =>  $add_url,
                'grid_dt_url'           =>  url('/'.$this->route_name.'-list'),
                'grid_delete_url'       =>  url('/'.$this->route_name.'-delete/'),
                'grid_view_url'         =>  url('/'.$this->route_name.'-view/'),
                'grid_status_url'       =>  url('/'.$this->route_name.'-status/'),
                'grid_data_url'         =>  url('/'.$this->route_name.'-edit/'), 
                'grid_columns'          =>  $grid_columns,
                'grid_order_by'         =>  '0',
                'grid_order_by_type'    =>  'DESC',
                'grid_tbl_name'         =>  $this->table_name,
                'grid_title'            =>  $this->grid_title,
                'grid_tbl_display_name' =>  $this->dt_table_display_name,
                'grid_tbl_length'       =>  '10',
                'grid_tbl_style'        =>  $table_style,
                'grid_tbl_class'        =>  $table_class
        ];
        return view('portal/master/master',$data); 
    }

    public function dt_list( $id = -1 )
    { 

        $start_index    = $_GET['iDisplayStart']!=null?$_GET['iDisplayStart']:0;
        $end_index      = $_GET['iDisplayLength']?$_GET['iDisplayLength']:10;      
        $search_text    = $_GET['sSearch']?$_GET['sSearch']:''; 
        $aColumns       = ["users.user_id","users.user_name","user_roles.role_name","users.user_phone_no","users.created_at","users.user_status"];
        $aColumns_where = ["users.user_id","users.user_name","user_roles.role_name","users.user_phone_no","users.created_at","users.user_status","users.user_email"];

        $order_by       = "";
        $where          = "";
        $order_by_type  = "DESC";

        if ( $_GET['iSortCol_0'] !== FALSE ){
            for ( $i=0 ; $i<intval($_GET['iSortingCols']); $i++ ){ if ($_GET['bSortable_'.intval($_GET['iSortCol_'.$i])] == "true" ){ $order_by = $aColumns[ intval( ( $_GET['iSortCol_'.$i] ) ) ]; $order_by_type = $this->mres( $_GET['sSortDir_'.$i] ); }
            }
        }

        for ( $i=0 ; $i<count($aColumns_where) ; $i++ ){ if ( isset($_GET['bSearchable_'.$i])  && $_GET['bSearchable_'.$i] == "true" && $_GET['sSearch_'.$i] != '' ){if($where != ''){$where .= " AND ";} $where .= $aColumns_where[$i]." = '".$this->mres($_GET['sSearch_'.$i])."' ";}
        }

        if( isset($_GET['sSearch'])  ){
            $where .= '('; $or = '';foreach( $aColumns_where as $row ){ $where .= $or.$row." LIKE '%".str_replace("'","\\\\\''",$this->mres($_GET['sSearch']))."%'"; if($or== ''){$or =' OR ';} }$where .= ')';
        }
        
        $filter='';
        
        /*Get Data From Model*/
        $pass_data =   array(
            'limit_start'       =>  $start_index,
            'limit_length'      =>  $end_index,
            'where_raw'         =>  $where.$filter,
            "order_by"          =>  $order_by,
            "order_by_type"     =>  $order_by_type,
        );

        $all_data = User_model::dt_list_data($pass_data);

        $data           = [];
        $i=$start_index;
        // echo "<pre>"; print_r($all_data); exit;
        foreach( $all_data['result'] as $row ){
            $row_dt   = [];

            // $row_dt[] = ++$i;
            $row_dt[] = '#'.$row->user_id;
            $row_dt[] = $row->user_name;
            $row_dt[] = 'Contact: '.$row->user_phone_no.'<br>
                        Email: '.$row->user_email;
            $row_dt[] = $this->format_indian_time($row->created_at);
            $row_dt[] = DB::table('business')->select('business_id')->where('business_user_id','=',$row->user_id)->where('is_delete',0)->get()->count();
            $status_type = 0;
            $status = '<i class="fa fa-ban"></i> &nbsp;&nbsp; InActive';
            if ($row->user_status==1) { 
                $row_dt[]= '<div style="cursor:pointer"  class="badge badge-success"><i class="fa fa-check"></i> &nbsp;&nbsp; Active</div>';
                $status = '<i class="fa fa-ban"></i> &nbsp;&nbsp; InActive';
                $status_type = 0;
            }elseif ($row->user_status==0) { 
                $row_dt[]= '<div style="cursor:pointer"  class="badge badge-danger"><i class="fa fa-ban"></i>&nbsp;&nbsp; InActive</div>';
                $status = '<i class="fa fa-check"></i> &nbsp;&nbsp; Active '; 
                $status_type = 1;
            }else{ 
                $row_dt[]= '-';
                // $status = '<i class="fa fa-check"></i> &nbsp;&nbsp; Pending';
                // $status = '<i class="fa fa-check"></i> &nbsp;&nbsp; Active '; 
                // $status_type = 2;
            }

            $action = ''; 

            $action .= '<a class="dropdown-item" href="'.url("user-view/".$row->user_id).'" title="View'.$this->route_name.'"><i class="fa fa-eye"></i> &nbsp;&nbsp;View</a>';
            $action .= '<a class="dropdown-item" href="'.url("report-view/".$row->user_id).'" title="Report '.$this->route_name.'"><i class="fa fa-eye"></i> &nbsp;&nbsp;Report</a>';

            $action .= '<a class="dropdown-item"  href="#" onclick="js_delete('.$row->user_id.')"  title="Delete '.$this->route_name.'"> <i class="fa fa-trash"></i> &nbsp;&nbsp;Delete</a>';
            
            $action .= '<a class="dropdown-item" style="color:black !important;" href="javascript:;" onclick="js_status('.$row->user_id.','.$status_type.')">'.$status.'</a>';


            $row_dt[] = '<button class="btn btn-outline-primary dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> Action </button>
                    <div class="dropdown-menu">'.$action.'                      
                </div>';
            
            $data[] = $row_dt;
        }

        $response['iTotalRecords'] = $response['iTotalDisplayRecords'] = $all_data['total'];
        $response['aaData'] = $data;
        
        return response()->json($response);
    }
    public function view($passed_id)
        {
            $data=['title'=>'Business Management','active'=>$this->active,'sub'=>$this->sub];
                $grid_columns = [
                    [
                        'name'=>'No',
                        'width'=>'width="5%"',
                        'sortable'=>'false', 
                        'style'=>'style=""',
                        'class'=>'class="text-center"',
                    ],
                    [
                        'name'=> 'Name',
                        'width'=>'width="20%"',
                        'sortable'=>'true',
                        'style'=>'style=""',
                        'class'=>'',
                    ],
                    [
                        'name'=> 'Email',
                        'width'=>'width="20%"',
                        'sortable'=>'true',
                        'style'=>'style=""',
                        'class'=>'', 
                    ],
                    [
                        'name'=> 'Mobile',
                        'width'=>'width="20%"',
                        'sortable'=>'true',
                        'style'=>'style=""',
                        'class'=>'', 
                    ],
                    [
                        'name'=> 'Address',
                        'width'=>'width="25%"',
                        'sortable'=>'true',
                        'style'=>'style=""',
                        'class'=>'', 
                    ],
                    [
                        'name'=> 'Status',
                        'width'=>'width="10%"',
                        'sortable'=>'false',
                        'style'=>'style=""',
                        'class'=>'', 
                    ],
                    
                ];

                $table_style='bBusiness-collapse: collapse; bBusiness-spacing: 0; width: -webkit-fill-available;';
                $table_class='table table-striped nowrap table-bBusinessed dt-responsive nowrap';
                
                $data["extra_pages"] = [];

                $data['grid'] = [
                        'grid_business_name'             =>  'Business',
                        'grid_business_add_button'       =>  false,
                        'grid_business_add_button_name'  =>  $this->grid_add_button_name,
                        'grid_business_dt_url'           =>  url('/user-business-list?user_id='.$passed_id),
                        'grid_business_delete_url'       =>  url('/business-delete/'),
                        'grid_business_status_url'       =>  url('/business-status/'),
                        'grid_business_data_url'         =>  url('/business-edit/'), 
                        'grid_business_columns'          =>  $grid_columns,
                        'grid_business_order_by'         =>  '1',
                        'grid_business_order_by_type'    =>  'DESC',
                        'grid_business_tbl_name'         =>  'business',
                        'grid_business_title'            =>  'Business List',
                        'grid_business_tbl_display_name' =>  'Business',
                        'grid_business_tbl_length'       =>  '25',
                        'grid_business_tbl_style'        =>  $table_style,
                        'grid_business_tbl_class'        =>  $table_class
                ];
            $data['user_detail'] = User_model::get_edit_detail($passed_id);
            $data['frame_data'] = Frame_model::frame_assign_list($passed_id);
            if(!empty($data['user_detail'])){
                return view($this->view_path.'user_master',$data); 
            }
        }
    public function update_user(Request $request)
        {  

            $params = $request->all();
            $data=array();
            $fields=array("user_name","user_email","user_phone_no","user_status","user_id");
            foreach ($fields as $field) 
            {
                $data[$field]= \Arr::get($params, $field);
            }

            $validator = Validator::make($params, [
                'user_name'         => 'required',
                'user_email'        => 'required|string|email',
                'user_phone_no'     => 'required|numeric|digits_between:10,12',
            ]);

            if($validator->fails()){
                $arr                = array();
                $arr['status']      = 500;          
                $arr['message']     = \Arr::flatten($validator->errors()->toArray())[0]; 
                echo json_encode($arr); exit;
            }
   
               
            if($request->file('user_profile_image')) {
                $rand=rand(1111,9999);
                $comman_time = time();
                $hash1=md5($comman_time*$rand);
                $originalImage= $request->file('user_profile_image');  
                $image_new_name=$hash1.Str::slug($originalImage->getClientOriginalName(),'-').'.'.$originalImage->extension();
                 
                $originalImage      = $request->file('user_profile_image');
                $thumbnailImage     = Image::make($originalImage->getRealPath());
                $thumbnailPath      = public_path().'/assets/upload/users/thumbnail/';
                $originalPath       = public_path().'/assets/upload/users/original/'; 
                /*Saving Original Image*/
                $thumbnailImage->save($originalPath.$image_new_name);
                 
                /*Saving 500 Size Thumb to main thumb*/
                $thumbnailImage->resize(200, null, function ($constraint) {
                    $constraint->aspectRatio();
                });
                $thumbnailImage->save($thumbnailPath.$image_new_name); 

                $current_user_profile_image=$request->input("current_user_profile_image");
                if (isset($current_user_profile_image) && $current_user_profile_image!='') {
                    unlink(public_path().'/assets/upload/users/thumbnail/'.$current_user_profile_image);
                    unlink(public_path().'/assets/upload/users/original/'.$current_user_profile_image); 
                }
                $data['user_profile_image']=$image_new_name;
            }

             
            //$common_model=new Common();   
            /*update data if it is coming from edit form*/
            
            $is_updated =  \DB::table($this->table_name)->where('user_id',$data['user_id'])->update($data);
            
            /*$where =array('user_id'=>$data['user_id']);
            $common_model->set_fields($data); 
            $is_updated = $common_model->update_my_data('users',$where);*/ 

            if ($is_updated==1) {  
                $arr                = array();
                $arr['status']      = 200;          
                $arr['message']     = "User Updated Successfully"; 
                echo json_encode($arr); exit;
            }else{
                $arr                = array();
                $arr['status']      = 500;          
                $arr['message']     = "User Remains Untouched"; 
                echo json_encode($arr); exit;
            } 
        }
    public function user_dt_list()
        { 
                $user_id        = $_GET['user_id'];
                $start_index    = $_GET['iDisplayStart']!=null?$_GET['iDisplayStart']:0;
                $end_index      = $_GET['iDisplayLength']?$_GET['iDisplayLength']:10;      
                $search_text    = $_GET['sSearch']?$_GET['sSearch']:''; 
                $aColumns       = ['business.business_id','business.business_name','business.business_email','business.business_phone_no','business.business_address'];
                $aColumns_where = ['business.business_id','business.business_name','business.business_email','business.business_phone_no','business.business_address'];

                $order_by       = "";
                $where          = "";
                $order_by_type  = "DESC";
                $filter         = "";

                if ( $_GET['iSortCol_0'] !== FALSE ){
                    for ( $i=0 ; $i<intval($_GET['iSortingCols']); $i++ ){ if ($_GET['bSortable_'.intval($_GET['iSortCol_'.$i])] == "true" ){ $order_by = $aColumns[ intval( ( $_GET['iSortCol_'.$i] ) ) ]; $order_by_type = $this->mres( $_GET['sSortDir_'.$i] ); }
                    }
                }

                for ( $i=0 ; $i<count($aColumns_where) ; $i++ ){ if ( isset($_GET['bSearchable_'.$i])  && $_GET['bSearchable_'.$i] == "true" && $_GET['sSearch_'.$i] != '' ){if($where != ''){$where .= " AND ";} $where .= $aColumns_where[$i]." = '".$this->mres($_GET['sSearch_'.$i])."' ";}
                }

                if( isset($_GET['sSearch'])  ){
                    $where .= '('; $or = '';foreach( $aColumns_where as $row ){ $where .= $or.$row." LIKE '%".str_replace("'","\\\\\''",$this->mres($_GET['sSearch']))."%'"; if($or== ''){$or =' OR ';} }$where .= ')';
                }
                /*Get Data From Model*/
                $pass_data =   array( 
                    'limit_start'       =>  $start_index,
                    'limit_length'      =>  $end_index,
                    'where_raw'         =>  $where.$filter,
                    "order_by"          =>  $order_by,
                    "order_by_type"     =>  $order_by_type,
                    "user_id"           =>  $user_id,
                );

                $all_data = Business_model::dt_list_data_byUser($pass_data);
                $data           = [];
                $i=$start_index;

                foreach($all_data['result'] as $row ){
                    $row_dt   = [];
                    $row_dt[] = ++$i;
                    $row_dt[] = $row->business_name;
                    $row_dt[] = $row->business_email;
                    $row_dt[] = $row->business_phone_no;
                    $row_dt[] = $row->business_address;
                    
                    $business_status = 'Free';
                    if($row->is_delete == 1){
                        $business_status .= ' - DELETED'; 
                    }else{
                        $business_status .= ''; 
                    }
                    $row_dt[] = $business_status;
                    $data[] = $row_dt;
                }

                $response['iTotalRecords'] = $response['iTotalDisplayRecords'] = $all_data['total'];
                $response['aaData'] = $data;
                return response()->json($response);
        }
    public function delete(Request $request)
        {    
            
            $params = $request->all();
            $id=\Arr::get($params, 'id');
            
            $is_updated = \DB::table($this->table_name)->where('user_id', $id)->update(['is_delete' => 1]);
            return $this->success_json('delete');
        }
    public function status(Request $request)
        {
            $params = $request->all();
            $id=\Arr::get($params, 'id');
            $status=\Arr::get($params, 'status');
            $is_updated = \DB::table($this->table_name)->where('user_id', $id)->update(['user_status' => $status]);
            return $this->success_json('status');
        }
   



}
