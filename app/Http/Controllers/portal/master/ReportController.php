<?php

namespace App\Http\Controllers\portal\master;

use App\Http\Controllers\Controller; 
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator; 
use Illuminate\Support\Facades\Http;

use Illuminate\Support\Str;     
use Redirect; 

use App\Models\portal\master\User_model;
use App\Models\portal\master\Business_model;
use App\Models\portal\master\Frame_model;
use App\Models\portal\master\Report_model;
use App\Models\Common; 
Use Image; 
use DB;
 

class ReportController extends Controller 
{ 
    private $table_name;
    private $view_title; 
    private $active;
    private $sub;
    private $dt_table_display_name;
    private $dt_table_small_name;
    private $route_name;
    private $add_edit_type;
    private $grid_add_button_name; 
    private $grid_title;
    private $view_path;

    public function __construct(){

        $this->table_name = 'report';
        $this->view_title = 'Report Management';  
        $this->active = 'report';
        $this->sub = 'report';
        $this->grid_title = 'Report List';
        $this->dt_table_display_name = 'Report';
        $this->dt_table_small_name = strtolower($this->dt_table_display_name);
        $this->route_name = 'report';
        $this->add_edit_type = 'page';
        $this->grid_add_button_name = 'Add '.$this->route_name;
        $this->view_path = 'portal/master/report/';
        $this->common_model         = New Common; 

    }

    public function index()
    { 
        $data=['title'=>'User Report','active'=>$this->active,'sub'=>$this->sub];
        $data['userList'] = User_model::get_ajax_list();
        return view($this->view_path.'index',$data); 
    }
    public function view($passed_id)
        {
            $data=['title'=>'Business Management','active'=>$this->active,'sub'=>$this->sub];
                $grid_columns = [
                    [
                        'name'=>'No',
                        'width'=>'width="5%"',
                        'sortable'=>'false', 
                        'style'=>'style=""',
                        'class'=>'class="text-center"',
                    ],
                    [
                        'name'=> 'Name',
                        'width'=>'width="20%"',
                        'sortable'=>'true',
                        'style'=>'style=""',
                        'class'=>'',
                    ],
                    [
                        'name'=> 'Email',
                        'width'=>'width="20%"',
                        'sortable'=>'true',
                        'style'=>'style=""',
                        'class'=>'', 
                    ],
                    [
                        'name'=> 'Mobile',
                        'width'=>'width="20%"',
                        'sortable'=>'true',
                        'style'=>'style=""',
                        'class'=>'', 
                    ],
                    [
                        'name'=> 'Address',
                        'width'=>'width="25%"',
                        'sortable'=>'true',
                        'style'=>'style=""',
                        'class'=>'', 
                    ],
                    [
                        'name'=> 'Status',
                        'width'=>'width="10%"',
                        'sortable'=>'false',
                        'style'=>'style=""',
                        'class'=>'', 
                    ],
                    
                ];

                $table_style='bBusiness-collapse: collapse; bBusiness-spacing: 0; width: -webkit-fill-available;';
                $table_class='table table-striped nowrap table-bBusinessed dt-responsive nowrap';
                
                $data["extra_pages"] = [];

                $data['grid'] = [
                        'grid_business_name'             =>  'Business',
                        'grid_business_add_button'       =>  false,
                        'grid_business_add_button_name'  =>  $this->grid_add_button_name,
                        'grid_business_dt_url'           =>  url('/user-business-list?user_id='.$passed_id),
                        'grid_business_delete_url'       =>  url('/business-delete/'),
                        'grid_business_status_url'       =>  url('/business-status/'),
                        'grid_business_data_url'         =>  url('/business-edit/'), 
                        'grid_business_columns'          =>  $grid_columns,
                        'grid_business_order_by'         =>  '1',
                        'grid_business_order_by_type'    =>  'DESC',
                        'grid_business_tbl_name'         =>  'business',
                        'grid_business_title'            =>  'Business List',
                        'grid_business_tbl_display_name' =>  'Business',
                        'grid_business_tbl_length'       =>  '25',
                        'grid_business_tbl_style'        =>  $table_style,
                        'grid_business_tbl_class'        =>  $table_class
                ];
            $data['user_detail'] = User_model::get_edit_detail($passed_id);
            $data['frame_data'] = Frame_model::frame_assign_list($passed_id);
            $data['track_data']   = Report_model::userReportData($passed_id);
            if(!empty($data['user_detail'])){
                return view($this->view_path.'report_user',$data); 
            }
        }
    public function reports()
        {
            $data=['title'=>'Reports','active'=>'reports','sub'=>'reports'];
            $data['mostuse'] = Report_model::mostUseReport();
            return view($this->view_path.'general_report',$data);

        }    




}
