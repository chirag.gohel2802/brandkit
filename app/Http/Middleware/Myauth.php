<?php

namespace App\Http\Middleware;

use Illuminate\Support\Facades\Route;

use Closure; 
use App\Models\portal\Portal; 
use App\Models\Common; 

class Myauth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    { 
        if (!$request->session()->exists('my_chacha')) {
            /*user value cannot be found in session*/ 
            return redirect('/portal-login');
        }else{
            $userdata = session('my_chacha'); 
            if (isset($userdata['user_id']) && $userdata['user_id']>0 && $userdata['user_role']=='site_user') { 
                return redirect('/site-portal-login'); 
            }
            if (isset($userdata['user_id']) && $userdata['user_id']>0 && $userdata['user_role']=='admin') { 
                return $next($request);
            }else{ 
               return redirect('/portal-login'); 
            }
        }
    }


}
