<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Carbon\Carbon;

class DatabaseBackUp extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'database:backup';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $filename = "backup-".time().".zip";  
        $command = "mysqldump -h ".env('DB_HOST')." -u" .env('DB_USERNAME')." -p'". env('DB_PASSWORD')."' ".env('DB_DATABASE')."  | zip > " . storage_path() . "/app/backup/" . $filename;

        $returnVar = NULL;
        $output = NULL;  
        exec($command, $output, $returnVar);


        $file_name_with_full_path = storage_path() . "/app/backup/" . $filename;
        $target_url='http://account.activeandpresent.com/upload.php';
        if (function_exists('curl_file_create')) { // php 5.5+
          $cFile = curl_file_create($file_name_with_full_path);
        } else { // 
          $cFile = '@' . realpath($file_name_with_full_path);
        }
        $post = array('title'=>'Backup','extra_info' => 'Khodiyar Imitation Backup','file_contents'=> $cFile);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$target_url);
        curl_setopt($ch, CURLOPT_POST,1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
        $result=curl_exec ($ch);
        // echo "<pre>";print_r($result);exit;

        curl_close ($ch);
    }
}