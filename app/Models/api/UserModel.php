<?php
namespace App\Models\api;

use DB; 
use Illuminate\Database\Eloquent\Model;
use App\Models\Master;
use App\Models\Common; 

class UserModel extends Master
{
    private static $table_name = 'users';
    public function __construct() {
        parent::__construct();      
        $this->common_model=New Common; 
    }   


    public function getUserData($params)
        {
            if(empty($params)){
                return false;
            }
            $user_phone_no      =   $params['user_phone_no'];  

            $query  =   DB::table(static::$table_name)   
                            ->where('users.is_delete',0)
                            ->where('users.user_role_id','<>','1');
            $query->where('users.user_phone_no',$user_phone_no);
            $data  = $query->get();  
            return $this->common_model->validSingle($data); 
        }


    public function authUser($params)
        {
            if(empty($params)){
                return false;
            }
            $token  =   $params['user_token']; 
            $query  =   DB::table('user_login_sessions AS s')  
                            ->leftJoin('users as u',    'u.user_id', '=', 's.session_user_id')
                            ->select(
                                'u.user_id',
                                'u.user_name',
                                'u.user_phone_no',
                                'u.user_email', 
                                'u.user_country_id',   
                                'u.user_status',  
                                's.session_id',
                                's.session_user_device_type',
                                's.session_expiry_timestamp',
                                's.session_status',
                                's.session_user_ip_address',   
                            )
                            ->where('u.is_delete',0)
                            ->where('u.user_role_id','<>','1');
            $query->where('s.session_token',$token);
            $data   = $query->get();   
            return $this->common_model->validSingle($data); 
        }

   /* public function userInfo($params)
        {
            if(empty($params)){
                return false;
            }
            $userId  =   $params['user_id']; 
            $assetUrl = asset('assets/upload/users/thumbnail/').'/'; 
            $query  =   DB::table('users AS u') 
                            ->select('u.user_name','u.user_phone_no','u.user_email', )  
                            ->where('u.is_delete',0)
                            ->where('u.user_role_id','<>','1');
            $query->where('u.user_id',$userId);
            $data   = $query->get();   
            return $this->common_model->validSingle($data); 
        }*/
    public function userInfo($params)
        {
            if (empty($params)) { 
                return false;
            }    

            $filter = '';  
            if(!empty($params['user_id']) && $params['user_id']>0){
                $filter .=  '  AND (u.user_id='.$params['user_id'].') ';  
            }    
            $assetUrl = asset('assets/upload/users/thumbnail/').'/'; 
            $query = "SELECT 
                u.user_name, 
                u.user_phone_no,  
                u.user_email,
                CASE WHEN u.user_status =1 THEN 'Active' WHEN u.user_status =2 THEN 'Block' WHEN u.user_status =0 THEN 'InAcive' END AS status,
                CONCAT('".$assetUrl."',u.user_profile_image) AS user_profile_image
            FROM users AS u
            WHERE u.is_delete=0 
            ".$filter."
            LIMIT 1 ";     
            $userDetails = DB::select($query); 
            if (!empty($userDetails) && !empty($userDetails)) { 
                $userData = $userDetails;
            } 
            return $userData;
        }    

}
