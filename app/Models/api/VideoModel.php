<?php
namespace App\Models\api;

use DB; 
use Illuminate\Database\Eloquent\Model;
use App\Models\Master;
use App\Models\Common; 

class VideoModel extends Master
{
    private static $table_name = 'videos';
    public function __construct() {
        parent::__construct();      
        $this->common_model=New Common; 
    }   
 
    public function getVideoList($params)
        {
            if (empty($params)) { 
                return false; 
            }    

            $filter = '';
            $limit          = 8 ;
            if(!empty($params['page']) && $params['page']>0){
                $page       =   $params['page'];
                $sp         =   ($page-1)*$limit;
            }else{
                $page       =   1 ;
                $sp         =   0 ;
            }

            if(!empty($params['video_category']) && $params['video_category']>0){
                $filter .=  '  AND (v.video_category='.$params['video_category'].') ';  
            }
            if(!empty($params['video_date']) && $params['video_date']>0){
                $dateFormat = date('Y-m-d',strtotime($params['video_date']));
                $filter .=  '  AND (v.video_date LIKE "%'.$dateFormat.'%" ) ';   
            } 
            if(!empty($params['video_type']) && $params['video_type']!=''){
                $filter .=  '  AND (v.video_type LIKE "%'.$params['video_type'].'%" )';  
            } 
            if(!empty($params['search']) && $params['search']!=''){
                $filter .=  'AND (v.video_name LIKE "%'.$params['search'].'%" ) OR (v.video_search_keyword LIKE "%'.$params['search'].'%" )';  
            }  
 
            $assetUrl           = asset('assets/upload/images/thumb/').'/';
            $assetOriginalUrl   = asset('assets/upload/images/original/').'/';
            $assetVideoUrl      = asset('assets/upload/videos/').'/';

            $query = "SELECT 
                v.video_id,  
                v.video_name,  
                v.video_package,
                v.video_type,
                DATE_FORMAT(v.video_date,'%d/%m/%Y') AS video_date, 
                CASE WHEN v.video_status =1 THEN 'Active' WHEN v.video_status =0 THEN 'InAcive' END AS video_status_text,
                c.category_name  as video_category_name, 
                CONCAT('".$assetVideoUrl."',v.video_file) AS video_url, 
                CONCAT('".$assetUrl."',i1.image_name) AS video_image,
                CONCAT('".$assetOriginalUrl."',i1.image_name) AS video_original_image 
            FROM videos AS v
            LEFT JOIN images as i1 ON i1.image_id=v.video_image
            LEFT JOIN category  as c ON c.category_id=v.video_category
            WHERE v.is_delete=0 
            AND c.is_delete=0 
            AND v.video_status =1 
            ".$filter."
            ORDER BY v.video_id DESC
            LIMIT ".$sp.",".$limit." ";     
            $videoList = DB::select($query);

            $tquery = "SELECT  COUNT(v.video_id) as total
                FROM videos AS v
                LEFT JOIN images    as i1 ON i1.image_id=v.video_image
                LEFT JOIN category  as c ON c.category_id=v.video_category
                WHERE v.is_delete=0 AND v.video_status =1  
                ".$filter." ";     
            $total = DB::select($tquery);  
            $data =[
                'video_list'   =>$videoList,
                'total'        =>ceil($total[0]->total),
                'total_page'   =>ceil($total[0]->total/$limit),
                'current_page' =>(int)$page,
            ];
            return $data;
        }
    public function getVideoDetail($params)
        {
            if (empty($params)) { 
                return false;
            }    

            $filter = '';  
            if(!empty($params['video_id']) && $params['video_id']>0){
                $filter .=  '  AND (v.video_id='.$params['video_id'].') ';  
            }    
            $assetUrl           = asset('assets/upload/images/thumb/').'/'; 
            $assetOriginalUrl   = asset('assets/upload/images/thumb/').'/';
            $assetVideoUrl      = asset('assets/upload/videos/').'/';

            $query = "SELECT 
                v.video_id,  
                v.video_name, 
                v.video_date,  
                v.video_package,
                CASE WHEN v.video_status =1 THEN 'Active' WHEN v.video_status =0 THEN 'InAcive' END AS video_status_text,
                c.category_name as video_category_name,
                i1.image_id, 
                CONCAT('".$assetVideoUrl."',v.video_file) AS video_file_url,
                CONCAT('".$assetUrl."',i1.image_name) AS video_image,
                CONCAT('".$assetOriginalUrl."',i1.image_name) AS video_original_image 
            FROM videos AS v
            LEFT JOIN images  as i1 ON i1.image_id=v.video_image
            LEFT JOIN category  as c ON c.category_id=v.video_category
            WHERE v.is_delete=0 AND v.video_status =1
            ".$filter."
            LIMIT 1 ";     
            $videoDetails = DB::select($query); 
            
            $videoData = [];
            if (!empty($videoDetails) && !empty($videoDetails[0])) { 
                $videoData = $videoDetails[0];
            } 
            $returnData = $videoData;
            $data =[
                'video_detail'         =>$returnData
            ];
            return $data;
        }        
    
}
