<?php
namespace App\Models\api;

use DB; 
use Illuminate\Database\Eloquent\Model;
use App\Models\Master;
use App\Models\Common; 

class NotificationModel extends Master
{
    private static $table_name = 'notifiction_message';
    public function __construct() {
        parent::__construct();      
        $this->common_model=New Common; 
    }   
 
    public function getnotifictionList($params)
        {
            if (empty($params)) { 
                return false; 
            }    

            $filter = '';
            $limit          = 8 ;
            if(!empty($params['page']) && $params['page']>0){
                $page       =   $params['page'];
                $sp         =   ($page-1)*$limit;
            }else{
                $page       =   1 ;
                $sp         =   0 ;
            }

             
            if(!empty($params['type']) && $params['type']!=''){
                $filter .=  '  AND (n.message_type LIKE "%'.$params['type'].'%" )';  
            } 
            if(!empty($params['search']) && $params['search']!=''){
                $filter .=  '  AND (n.message_title LIKE "%'.$params['search'].'%" )';  
            }  
 
            $assetUrl = asset('assets/upload/images/thumb/').'/';
            $assetOriginalUrl = asset('assets/upload/images/original/').'/';

            $query = "SELECT 
                n.message_id,  
                n.message_title as title,  
                n.message_body as body,
                n.message_link as link,
                n.message_type as type,
                CONCAT('".$assetUrl."',i1.image_name) AS message_image, 
                CONCAT('".$assetOriginalUrl."',i1.image_name) AS message_original_image 
            FROM notifiction_message AS n
            LEFT JOIN images as i1 ON i1.image_id=n.message_image
            WHERE n.is_delete=0
            ".$filter."
            LIMIT ".$sp.",".$limit." ";     
            $notificationList = DB::select($query);
            if(!empty($notificationList)){
                foreach ($notificationList as $key => $value) {
                    $notificationList[$key]->post_type = 1;
                }
            }
            $tquery = "SELECT  COUNT(n.message_id) as total
                FROM notifiction_message AS n
                LEFT JOIN images  as i1 ON i1.image_id=n.message_image
                WHERE n.is_delete=0  
                ".$filter." ";     
            $total = DB::select($tquery);  
            $data =[
                'notification_list' =>$notificationList,
                'total'             =>ceil($total[0]->total),
                'total_page'        =>ceil($total[0]->total/$limit),
                'current_page'      =>(int)$page,
            ];
            return $data;
        }
}
