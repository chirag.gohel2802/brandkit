<?php
namespace App\Models\api;

use DB; 
use Illuminate\Database\Eloquent\Model;
use App\Models\Master;
use App\Models\Common; 

class DashboardModel extends Master
{
    private static $table_name = '';
    public function __construct() {
        parent::__construct();      
        $this->common_model=New Common; 
    }   

    public function getBannerList()
        {
            $filter = '';
            $limit          = 8 ;
            if(!empty($params['page']) && $params['page']>0){
                $page       =   $params['page'];
                $sp         =   ($page-1)*$limit;
            }else{
                $page       =   1 ;
                $sp         =   0 ;
            }

 
            $assetUrl = asset('assets/upload/images/thumb/').'/';
            $assetrealUrl = asset('assets/upload/images/original/').'/';

            $query = "SELECT 
                b.banner_name,  
                b.banner_link,  
                b.banner_title,
                b.banner_text,
                CONCAT('".$assetUrl."',i1.image_name) AS banner_image, 
                CONCAT('".$assetrealUrl."',i1.image_name) AS banner_original_image 
            FROM banners AS b
            LEFT JOIN images as i1 ON i1.image_id=b.banner_image
            WHERE b.is_delete=0 
            ".$filter."
            LIMIT ".$sp.",".$limit." ";     
            $bannerList = DB::select($query);

            $tquery = "SELECT  COUNT(b.banner_id) as total
                FROM banners AS b
                LEFT JOIN images as i1 ON i1.image_id=b.banner_image
                WHERE b.is_delete=0   
                ".$filter." ";     
            $total = DB::select($tquery);  
            $data =[
                'banner_list'       =>$bannerList,
                'total'             =>ceil($total[0]->total),
                'total_page'        =>ceil($total[0]->total/$limit),
                'current_page'      =>(int)$page,
            ];
            return $data;
        }
    public function getDashboardList()
        {
            $filter = '';
            $limit          = 8 ;
            if(!empty($params['page']) && $params['page']>0){
                $page       =   $params['page'];
                $sp         =   ($page-1)*$limit;
            }else{
                $page       =   1 ;
                $sp         =   0 ;
            }
            $assetUrl = asset('assets/upload/images/thumb/').'/';
            $assetOriginalUrl = asset('assets/upload/images/original/').'/';

            $query = "SELECT 
                d.dashboard_id,  
                d.dashboard_name,  
                d.dashboard_value,
                d.dashboard_parent_category, 
                d.dashboard_url,
                d.is_horizontal,
                d.dashboard_position
            FROM dashboard AS d
            WHERE d.is_delete=0 AND d.dashboard_status=1
            ORDER BY d.dashboard_position ASC
            ".$filter."
            LIMIT ".$sp.",".$limit." ";     
            $dashboardList = DB::select($query);

            if(!empty($dashboardList)){

                foreach($dashboardList as $k=>$value){
                $dashboardList[$k]->category_data = DB::select("
                    SELECT DATE_FORMAT(u.upcoming_show_date, '%d %b') AS category_date,
                        c.category_id,c.category_parent_id,c.category_name,c.category_description,
                        CASE WHEN c.category_type =1 THEN 'Post' WHEN c.category_type =2 THEN 'Video' END AS category_type,
                        CONCAT('".$assetUrl."',i1.image_name) AS category_image, 
                        CONCAT('".$assetOriginalUrl."',i1.image_name) AS category_original_image
                    FROM upcoming AS u
                    LEFT JOIN category AS c ON c.category_id = u.upcoming_category_id
                    LEFT JOIN images    as i1 ON i1.image_id=c.category_image
                    WHERE u.upcoming_dashboard_id IN (".$value->dashboard_id.")
                    AND u.is_delete=0
                    AND ( NOW() BETWEEN u.upcoming_start_date  AND u.upcoming_end_date)
                    ORDER BY u.upcoming_postion ASC 
                    ");
               }
            }    
            $tquery = "SELECT  COUNT(d.dashboard_id) as total
                FROM dashboard AS d
                WHERE d.is_delete=0 AND d.dashboard_status=1   
                ".$filter." ";     
            $total = DB::select($tquery);  
            $data =[
                'dashboard_list'    =>$dashboardList,
                'total'             =>ceil($total[0]->total),
                'total_page'        =>ceil($total[0]->total/$limit),
                'current_page'      =>(int)$page,
            ];
            return $data;
        }
    public function getCustomDashboardList()
        {
            $filter = '';
            $limit          = 8 ;
            if(!empty($params['page']) && $params['page']>0){
                $page       =   $params['page'];
                $sp         =   ($page-1)*$limit;
            }else{
                $page       =   1 ;
                $sp         =   0 ;
            }
            $assetUrl = asset('assets/upload/images/thumb/').'/';
            $assetOriginalUrl = asset('assets/upload/images/original/').'/';

            $query = "SELECT 
                d.custom_dashboard_id,  
                d.custom_dashboard_name,  
                d.custom_dashboard_value,
                d.custom_dashboard_parent_category, 
                d.custom_dashboard_url,
                d.is_horizontal,
                d.custom_dashboard_position
            FROM custom_dashboard AS d
            WHERE d.is_delete=0 AND d.custom_dashboard_status=1
            ORDER BY d.custom_dashboard_position ASC
            ".$filter."
            LIMIT ".$sp.",".$limit." ";     
            $customDashboardList = DB::select($query);

            if(!empty($customDashboardList)){
                foreach($customDashboardList as $m=>$value){
                $customDashboardList[$m]->category_data = DB::select("
                    SELECT DATE_FORMAT(u.custom_upcoming_show_date, '%d %b') AS category_date,
                        c.category_id,c.category_parent_id,c.category_name,c.category_description,
                        CASE WHEN c.category_type =1 THEN 'Post' WHEN c.category_type =2 THEN 'Video' END AS category_type,
                        CONCAT('".$assetUrl."',i1.image_name) AS category_image, 
                        CONCAT('".$assetOriginalUrl."',i1.image_name) AS category_original_image
                    FROM custom_upcoming AS u
                    LEFT JOIN category AS c ON c.category_id = u.custom_upcoming_category_id
                    LEFT JOIN images AS i1 ON i1.image_id=c.category_image
                    WHERE u.custom_upcoming_dashboard_id IN (".$value->custom_dashboard_id.")
                    AND u.is_delete=0
                    AND ( NOW() BETWEEN u.custom_upcoming_start_date  AND u.custom_upcoming_end_date)
                     ORDER BY u.custom_upcoming_postion ASC
                    ");
               }
            }    
            $tquery = "SELECT  COUNT(d.dashboard_id) as total
                FROM dashboard AS d
                WHERE d.is_delete=0 AND d.dashboard_status=1   
                ".$filter." ";     
            $total = DB::select($tquery);  
            $data =[
                'custom_dashboard_list' =>$customDashboardList,
                'total'             =>ceil($total[0]->total),
                'total_page'        =>ceil($total[0]->total/$limit),
                'current_page'      =>(int)$page,
            ];
            return $data;
        }
    public function getHelpSupportList($params="")
        {
            $query = "SELECT 
                    s.setting_id,
                    s.setting_name,
                    s.setting_value
                FROM settings AS s
                WHERE s.is_delete=0 
                AND s.setting_status=1
                AND s.setting_name='".$params."' ";     
                $result = DB::select($query);
            return (array)$result;    
            /*$result = DB::table('settings')
                            ->select('settings.*')
                            ->where('setting_name','=',$params)
                            ->first();
                return (array)$result;*/
        }        
    public function getUpcomingList($params) // upcoming dates
        {
            if (empty($params)) { 
                return false;
            }    

            $filter = '';
            $limit          = 8;
            if(!empty($params['page']) && $params['page']>0){
                $page       =   $params['page'];
                $sp         =   ($page-1)*$limit;
            }else{
                $page       =   1 ;
                $sp         =   0 ;
            }
            if(!empty($params['post_type']) ){
                $filter .=  '  AND (p.post_type="'.$params['post_type'].'") ';  
            }
            if(!empty($params['dateCurrent']) && !empty($params['endDate']))
            {
               $filter .=  'AND (p.post_date BETWEEN "'.$params['dateCurrent'].'"  AND "'.$params['endDate'].'") ';
            }
            $assetUrl = asset('assets/upload/images/thumb/').'/';
            $assetrealUrl = asset('assets/upload/images/original/').'/';

            $query = "SELECT 
                p.post_name,  
                p.post_package,
                p.post_type,
                DATE_FORMAT(p.post_date,'%d/%m/%Y') AS post_date, 
                CASE WHEN p.post_status =1 THEN 'Active' WHEN p.post_status =0 THEN 'InAcive' END AS post_status_text,
                c.category_name  as post_category_name, 
                CONCAT('".$assetUrl."',i1.image_name) AS post_image, 
                CONCAT('".$assetrealUrl."',i1.image_name) AS post_original_image 
            FROM post AS p
            LEFT JOIN images as i1 ON i1.image_id=p.post_image
            LEFT JOIN category  as c ON c.category_id=p.post_category
            WHERE p.is_delete=0 AND p.post_status =1 
            ".$filter."
            LIMIT ".$sp.",".$limit." ";     
            $upcomingList = DB::select($query);

            $tquery = "SELECT  COUNT(p.post_id) as total
                FROM post AS p
                LEFT JOIN images    as i1 ON i1.image_id=p.post_image
                LEFT JOIN category  as c ON c.category_id=p.post_category
                WHERE p.is_delete=0 AND p.post_status =1  
                ".$filter." ";     
            $total = DB::select($tquery);  
            $data =[
                'upcoming_list'         =>$upcomingList,
                'total'             =>ceil($total[0]->total),
                'total_page'        =>ceil($total[0]->total/$limit),
                'current_page'      =>(int)$page,
            ];
            return $data;
        }    
    
}
