<?php
namespace App\Models\api;

use DB; 
use Illuminate\Database\Eloquent\Model;
use App\Models\Master;
use App\Models\Common; 

class FrameModel extends Master
{
    private static $table_name = 'frames'; 
    public function __construct() {
        parent::__construct();      
        $this->common_model=New Common;  
    }   

    public function getFrameList($params = [])
        {
            if (empty($params)) { 
                return false;
            }    

            $filter = '';
            $limit          = 8 ;
            if(!empty($params['page']) && $params['page']>0){
                $page       =   $params['page'];
                $sp         =   ($page-1)*$limit;
            }else{
                $page       =   1 ;
                $sp         =   0 ;
            }

            if(!empty($params['search']) && $params['search']!=''){
                $filter .=  '  AND (f.frame_name LIKE "%'.$params['search'].'%" ) ';  
            } 
            
            $assetUrl = asset('assets/upload/images/thumb/').'/';
            $assetOriginalUrl = asset('assets/upload/images/original/').'/';

            $query = "SELECT 
                f.frame_id, 
                f.frame_name, 
                f.frame_package,
                f.frame_data,
                f.frame_image,
                CONCAT('".$assetOriginalUrl."',f.frame_address_icon) AS frame_address_icon, 
                CONCAT('".$assetOriginalUrl."',f.frame_website_icon) AS frame_website_icon, 
                CONCAT('".$assetOriginalUrl."',f.frame_phone_icon) AS frame_phone_icon, 
                CONCAT('".$assetOriginalUrl."',f.frame_email_icon) AS frame_email_icon, 
                CONCAT('".$assetUrl."',i1.image_name) AS frame_image, 
                CONCAT('".$assetOriginalUrl."',i1.image_name) AS frame_original_image 
            FROM frames AS f
            LEFT JOIN images as i1 ON i1.image_id=f.frame_image
            WHERE f.is_delete=0 AND f.frame_status =1
            AND f.assigned_user IS NULL
            ".$filter."
            LIMIT ".$sp.",".$limit." ";
            $FrameList = DB::select($query); 
            if(!empty($FrameList)){
                $j=0;
                foreach ($FrameList as $key => $value) {
                    $frame= json_decode($value->frame_data,true);
                    if(!empty($frame)){ 
                        $k=0;
                        foreach($frame as $fk=>$fv){ 
                            $newValue = $fv['value'];                     
                            if ($fv['type']=='phone1' OR $fv['type']=='phone2') { 
                                $icon  = $value->frame_phone_icon; 
                            }elseif ($fv['type']=='email' ) { 
                                $icon  = $value->frame_email_icon; 
                            }elseif ($fv['type']=='address' ) { 
                                $icon  = $value->frame_address_icon; 
                            }elseif ($fv['type']=='website' ) { 
                                $icon  = $value->frame_website_icon; 
                            }
                            else{
                                $icon  = ''; 
                            }
                            $item = [
                                'name'      =>'symbol',
                                'value'     => $icon
                            ]; 
                            if($icon!=''){ 
                                $newValue[]=$item;
                            } 
                            $frame[$k]['value']=$newValue ;  
                            $k++;
                        }  
                    }
                    $value->frame_data = json_encode($frame);
                    $j++;
                }  
            }
                $user_specific_frame_query = "SELECT 
                    f.frame_id, 
                    f.frame_name, 
                    f.frame_package,
                    f.frame_data,
                    f.frame_image, 
                    CONCAT('".$assetUrl."',i1.image_name) AS frame_image, 
                    CONCAT('".$assetOriginalUrl."',i1.image_name) AS frame_original_image 
                FROM frames AS f
                LEFT JOIN images as i1 ON i1.image_id=f.frame_image
                WHERE f.is_delete=0 AND f.frame_status =1
                AND FIND_IN_SET(".$params['user_id'].", f.assigned_user)
                ".$filter."
                LIMIT ".$sp.",".$limit." ";

                $user_specific_frame = DB::select($user_specific_frame_query); 
                
                $FrameList = array_merge($user_specific_frame,$FrameList);

            $tquery = "SELECT  COUNT(f.frame_id) as total
                FROM frames AS f
                LEFT JOIN images    as i1 ON i1.image_id=f.frame_image
                WHERE f.is_delete=0 AND f.frame_status =1  
                ".$filter." ";     
            $total = DB::select($tquery); 
            $data =[
                'Frame_list'        => $FrameList,
                'total'             =>ceil($total[0]->total),
                'total_page'        =>ceil($total[0]->total/$limit),
                'current_page'      =>(int)$page,
            ];
            return $data;
        }
          
    
}
