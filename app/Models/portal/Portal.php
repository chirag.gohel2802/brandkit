<?php

namespace App\Models\portal;

use App\Models\Master;
use DB;

use App\Models\Common;



class Portal extends Master
{

    private $organisation_id;
    public function __construct()
    {
        parent::__construct();
        $this->organisation_id  = session()->get('org_id');
        $this->common_model = new Common;
    }

    public function get_token_user_data()
    {
        $query = "SELECT u.* 
                FROM users as u 
                WHERE u.user_temp_token='" . $this->data['token'] . "'   
                AND user_token_expiry>" . time();

        $result = DB::select($query);
        if ($result) {
            $result = $result[0];
            $result = (array)$result;
            return $result;
        }
        return false;
    }
    public function get_admin_user_data()
    {
        $query = "SELECT a.*,r.role_permission as user_roles
                FROM users as a
                LEFT JOIN user_roles as r ON user_role_id=r.role_id
                WHERE a.user_email='" . $this->data['email'] . "' 
                AND user_role_id=1 ";

        $result = DB::select($query);
        if ($result) {
            $result = $result[0];
            return (array)$result;
        }
        return false;
    }
    public function get_user_data()
    {
        $query = "SELECT u.*,r.*
                FROM users as u
                LEFT JOIN user_roles as r ON u.user_role_id=r.role_id
                WHERE u.is_delete=0 
                AND u.user_id='" . $this->data['user_id'] . "' ";

        $result = DB::select($query);
        if ($result) {
            $result = $result[0];
            // echo "<pre>";print_r($result);exit;
            return (array)$result;
        }
        return false;
    }
    public function get_user_session_data()
    {
        $query = "SELECT u.user_email as email,u.user_status,u.user_id,u.user_session_id,u.added_by as user_added_by, 
                r.role_id,r.role_name,r.role_permission_ids,r.role_status
                FROM users as u
                LEFT JOIN user_roles as r ON u.user_role_id=r.role_id
                WHERE u.is_delete=0 
                AND u.user_id='" . $this->data['user_id'] . "' ";

        $result = DB::select($query);
        if ($result) {
            $result = $result[0];
            // echo "<pre>";print_r($result);exit;
            return (array)$result;
        }
        return false;
    }
    public function get_user_pass()
    {
        $query = "SELECT u.user_password,u.user_phone_no
                FROM users as u
                WHERE u.is_delete=0 ";

        $result = DB::select($query);
        if ($result) {
            $result = $result;
            return $result;
        }
        return false;
    }
    public function get_unique_number($column, $table)
    {
        $is_unique = 0;

        while ($is_unique == 1) {
            # code...
        }
        $query = "SELECT COUNT(popular_id) as total_popular
                FROM popular_vendors  
                WHERE is_delete=0  
                AND popular_vendor_type=" . $type;

        $result = DB::select($query);
        if (!empty($result)) {
            $data = $result[0]->total_popular;
        }

        return $data;
    }
    public function get_dashboard_data($master_id, $logged_user_id)
    {
        $data = [

            'total_users'       => 0,
            'active_users'      => 0,
            'inactive_users'    => 0,
            'pending_users'     => 0,
        ];


        /*Total users*/
        if (session()->get('my_chacha')['user_role'] == 'admin') {
            $query = "SELECT COUNT(user_id) as total
                        FROM users
                        WHERE is_delete =0 ";
        } elseif ($master_id == $logged_user_id) {
            $query = "SELECT COUNT(user_id) as total
                        FROM users
                        WHERE is_delete =0 ";
        } else {
            $query = "SELECT COUNT(user_id) as total
                        FROM users
                        WHERE is_delete =0";
        }
        $result = DB::select($query);
        if ($result) {
            $data['total_users'] = $result[0]->total;
        }
        /*End Total users*/

        /*Active users*/
        if (session()->get('my_chacha')['user_role'] == 'admin') {
            $query = "SELECT COUNT(user_id) as total
                        FROM users
                        WHERE is_delete =0
                        AND user_status=1
                         ";
        } elseif ($master_id == $logged_user_id) {
            $query = "SELECT COUNT(user_id) as total
                        FROM users
                        WHERE is_delete =0 
                        AND user_status=1
                        ";
        } else {
            $query = "SELECT COUNT(user_id) as total
                        FROM users
                        WHERE is_delete =0 
                        AND user_status=1
                        ";
        }
        $result = DB::select($query);
        if ($result) {
            $data['active_users'] = $result[0]->total;
        }
        /*End Active users*/


        /*InActive users*/
        if (session()->get('my_chacha')['user_role'] == 'admin') {
            $query = "SELECT COUNT(user_id) as total
                        FROM users
                        WHERE is_delete =0
                        AND user_status=0
                         ";
        } elseif ($master_id == $logged_user_id) {
            $query = "SELECT COUNT(user_id) as total
                        FROM users
                        WHERE is_delete =0 
                        AND user_status=0
                        ";
        } else {
            $query = "SELECT COUNT(user_id) as total
                        FROM users
                        WHERE is_delete =0 
                        AND user_status=0
                        ";
        }
        $result = DB::select($query);
        if ($result) {
            $data['inactive_users'] = $result[0]->total;
        }
        /*InActive Total users*/

        /*Pending users*/
        if (session()->get('my_chacha')['user_role'] == 'admin') {
            $query = "SELECT COUNT(user_id) as total
                        FROM users
                        WHERE is_delete =0
                        AND user_status=2
                         ";
        } elseif ($master_id == $logged_user_id) {
            $query = "SELECT COUNT(user_id) as total
                        FROM users
                        WHERE is_delete =0 
                        AND user_status=2
                        ";
        } else {
            $query = "SELECT COUNT(user_id) as total
                        FROM users
                        WHERE is_delete =0 
                        AND user_status=2
                        ";
        }
        $result = DB::select($query);
        if ($result) {
            $data['pending_users'] = $result[0]->total;
        }
        /*Pending Total users*/

        /*Total post*/
        if (session()->get('my_chacha')['user_role'] == 'admin') {
            $query = "SELECT COUNT(post_id) as total
                    FROM post
                    WHERE is_delete =0 
                    AND is_custom_post='no'
                    ";
        } elseif ($master_id == $logged_user_id) {
            $query = "SELECT COUNT(post_id) as total
                    FROM post
                    WHERE is_delete =0 
                    AND is_custom_post='no'
                    ";
        } else {
            $query = "SELECT COUNT(post_id) as total
                    FROM post
                    WHERE is_delete =0
                    AND is_custom_post='no'
                    ";
        }
        $result = DB::select($query);
        if ($result) {
            $data['total_post'] = $result[0]->total;
        }
        /*End Total post*/

        /*Active post*/
        if (session()->get('my_chacha')['user_role'] == 'admin') {
            $query = "SELECT COUNT(post_id) as total
                    FROM post
                    WHERE is_delete =0
                    AND post_status=1
                    AND is_custom_post='no'
                     ";
        } elseif ($master_id == $logged_user_id) {
            $query = "SELECT COUNT(post_id) as total
                    FROM post
                    WHERE is_delete =0 
                    AND post_status=1
                    AND is_custom_post='no'
                    ";
        } else {
            $query = "SELECT COUNT(post_id) as total
                    FROM post
                    WHERE is_delete =0 
                    AND post_status=1
                    AND is_custom_post='no'
                    ";
        }
        $result = DB::select($query);
        if ($result) {
            $data['active_post'] = $result[0]->total;
        }
        /*End Active post*/


        /*InActive post*/
        if (session()->get('my_chacha')['user_role'] == 'admin') {
            $query = "SELECT COUNT(post_id) as total
                    FROM post
                    WHERE is_delete =0
                    AND post_status=0
                    AND is_custom_post='no'
                     ";
        } elseif ($master_id == $logged_user_id) {
            $query = "SELECT COUNT(post_id) as total
                    FROM post
                    WHERE is_delete =0 
                    AND post_status=0
                    AND is_custom_post='no'
                    ";
        } else {
            $query = "SELECT COUNT(post_id) as total
                    FROM post
                    WHERE is_delete =0 
                    AND post_status=0
                    AND is_custom_post='no'
                    ";
        }
        $result = DB::select($query);
        if ($result) {
            $data['inactive_post'] = $result[0]->total;
        }
        /*InActive Total post*/

        /*Total category*/
        if (session()->get('my_chacha')['user_role'] == 'admin') {
            $query = "SELECT COUNT(category_id) as total
                    FROM category
                    WHERE is_delete =0 ";
        } elseif ($master_id == $logged_user_id) {
            $query = "SELECT COUNT(category_id) as total
                    FROM category
                    WHERE is_delete =0 ";
        } else {
            $query = "SELECT COUNT(category_id) as total
                    FROM category
                    WHERE is_delete =0";
        }
        $result = DB::select($query);
        if ($result) {
            $data['total_category'] = $result[0]->total;
        }
        /*End Total category*/

        /*Active category*/
        if (session()->get('my_chacha')['user_role'] == 'admin') {
            $query = "SELECT COUNT(category_id) as total
                    FROM category
                    WHERE is_delete =0
                    AND category_status=1
                     ";
        } elseif ($master_id == $logged_user_id) {
            $query = "SELECT COUNT(category_id) as total
                    FROM category
                    WHERE is_delete =0 
                    AND category_status=1
                    ";
        } else {
            $query = "SELECT COUNT(category_id) as total
                    FROM category
                    WHERE is_delete =0 
                    AND category_status=1
                    ";
        }
        $result = DB::select($query);
        if ($result) {
            $data['active_category'] = $result[0]->total;
        }
        /*End Active category*/


        /*InActive category*/
        if (session()->get('my_chacha')['user_role'] == 'admin') {
            $query = "SELECT COUNT(category_id) as total
                    FROM category
                    WHERE is_delete =0
                    AND category_status=0
                     ";
        } elseif ($master_id == $logged_user_id) {
            $query = "SELECT COUNT(category_id) as total
                    FROM category
                    WHERE is_delete =0 
                    AND category_status=0
                    ";
        } else {
            $query = "SELECT COUNT(category_id) as total
                    FROM category
                    WHERE is_delete =0 
                    AND category_status=0
                    ";
        }
        $result = DB::select($query);
        if ($result) {
            $data['inactive_category'] = $result[0]->total;
        }
        /*InActive Total category*/

        /*Total video*/
        if (session()->get('my_chacha')['user_role'] == 'admin') {
            $query = "SELECT COUNT(video_id) as total
                    FROM videos
                    WHERE is_delete =0 ";
        } elseif ($master_id == $logged_user_id) {
            $query = "SELECT COUNT(video_id) as total
                    FROM videos
                    WHERE is_delete =0 ";
        } else {
            $query = "SELECT COUNT(video_id) as total
                    FROM videos
                    WHERE is_delete =0";
        }
        $result = DB::select($query);
        if ($result) {
            $data['total_video'] = $result[0]->total;
        }
        /*End Total video*/

        /*Active video*/
        if (session()->get('my_chacha')['user_role'] == 'admin') {
            $query = "SELECT COUNT(video_id) as total
                    FROM videos
                    WHERE is_delete =0
                    AND video_status=1
                     ";
        } elseif ($master_id == $logged_user_id) {
            $query = "SELECT COUNT(video_id) as total
                    FROM videos
                    WHERE is_delete =0 
                    AND video_status=1
                    ";
        } else {
            $query = "SELECT COUNT(video_id) as total
                    FROM videos
                    WHERE is_delete =0 
                    AND video_status=1
                    ";
        }
        $result = DB::select($query);
        if ($result) {
            $data['active_video'] = $result[0]->total;
        }
        /*End Active video*/


        /*InActive video*/
        if (session()->get('my_chacha')['user_role'] == 'admin') {
            $query = "SELECT COUNT(video_id) as total
                    FROM videos
                    WHERE is_delete =0
                    AND video_status=0
                     ";
        } elseif ($master_id == $logged_user_id) {
            $query = "SELECT COUNT(video_id) as total
                    FROM videos
                    WHERE is_delete =0 
                    AND video_status=0
                    ";
        } else {
            $query = "SELECT COUNT(video_id) as total
                    FROM videos
                    WHERE is_delete =0 
                    AND video_status=0
                    ";
        }
        $result = DB::select($query);
        if ($result) {
            $data['inactive_video'] = $result[0]->total;
        }
        /*InActive Total video*/

        /*Total post*/
        if (session()->get('my_chacha')['user_role'] == 'admin') {
            $query = "SELECT COUNT(post_id) as total
                    FROM post
                    WHERE is_delete =0 
                    AND is_custom_post='yes'
                    ";
        } elseif ($master_id == $logged_user_id) {
            $query = "SELECT COUNT(post_id) as total
                    FROM post
                    WHERE is_delete =0 
                    AND is_custom_post='yes'
                    ";
        } else {
            $query = "SELECT COUNT(post_id) as total
                    FROM post
                    WHERE is_delete =0
                    AND is_custom_post='yes'
                    ";
        }
        $result = DB::select($query);
        if ($result) {
            $data['total_custom_post'] = $result[0]->total;
        }
        /*End Total post*/

        /*Active post*/
        if (session()->get('my_chacha')['user_role'] == 'admin') {
            $query = "SELECT COUNT(post_id) as total
                    FROM post
                    WHERE is_delete =0
                    AND post_status=1
                    AND is_custom_post='yes'
                     ";
        } elseif ($master_id == $logged_user_id) {
            $query = "SELECT COUNT(post_id) as total
                    FROM post
                    WHERE is_delete =0 
                    AND post_status=1
                    AND is_custom_post='yes'
                    ";
        } else {
            $query = "SELECT COUNT(post_id) as total
                    FROM post
                    WHERE is_delete =0 
                    AND post_status=1
                    AND is_custom_post='yes'
                    ";
        }
        $result = DB::select($query);
        if ($result) {
            $data['active_custom_post'] = $result[0]->total;
        }
        /*End Active post*/


        /*InActive post*/
        if (session()->get('my_chacha')['user_role'] == 'admin') {
            $query = "SELECT COUNT(post_id) as total
                    FROM post
                    WHERE is_delete =0
                    AND post_status=0
                    AND is_custom_post='yes'
                     ";
        } elseif ($master_id == $logged_user_id) {
            $query = "SELECT COUNT(post_id) as total
                    FROM post
                    WHERE is_delete =0 
                    AND post_status=0
                    AND is_custom_post='yes'
                    ";
        } else {
            $query = "SELECT COUNT(post_id) as total
                    FROM post
                    WHERE is_delete =0 
                    AND post_status=0
                    AND is_custom_post='yes'
                    ";
        }
        $result = DB::select($query);
        if ($result) {
            $data['inactive_custom_post'] = $result[0]->total;
        }
        /*InActive Total post*/

        /*Total frame*/
        if (session()->get('my_chacha')['user_role'] == 'admin') {
            $query = "SELECT COUNT(frame_id) as total
                    FROM frames
                    WHERE is_delete =0 ";
        } elseif ($master_id == $logged_user_id) {
            $query = "SELECT COUNT(frame_id) as total
                    FROM frames
                    WHERE is_delete =0 ";
        } else {
            $query = "SELECT COUNT(frame_id) as total
                    FROM frames
                    WHERE is_delete =0";
        }
        $result = DB::select($query);
        if ($result) {
            $data['total_frame'] = $result[0]->total;
        }
        /*End Total frame*/

        /*Active frame*/
        if (session()->get('my_chacha')['user_role'] == 'admin') {
            $query = "SELECT COUNT(frame_id) as total
                    FROM frames
                    WHERE is_delete =0
                    AND frame_status=1
                     ";
        } elseif ($master_id == $logged_user_id) {
            $query = "SELECT COUNT(frame_id) as total
                    FROM frames
                    WHERE is_delete =0 
                    AND frame_status=1
                    ";
        } else {
            $query = "SELECT COUNT(frame_id) as total
                    FROM frames
                    WHERE is_delete =0 
                    AND frame_status=1
                    ";
        }
        $result = DB::select($query);
        if ($result) {
            $data['active_frame'] = $result[0]->total;
        }
        /*End Active frame*/


        /*InActive frame*/
        if (session()->get('my_chacha')['user_role'] == 'admin') {
            $query = "SELECT COUNT(frame_id) as total
                    FROM frames
                    WHERE is_delete =0
                    AND frame_status=0
                     ";
        } elseif ($master_id == $logged_user_id) {
            $query = "SELECT COUNT(frame_id) as total
                    FROM frames
                    WHERE is_delete =0 
                    AND frame_status=0
                    ";
        } else {
            $query = "SELECT COUNT(frame_id) as total
                    FROM frames
                    WHERE is_delete =0 
                    AND frame_status=0
                    ";
        }
        $result = DB::select($query);
        if ($result) {
            $data['inactive_frame'] = $result[0]->total;
        }
        /*InActive Total frame*/

        return $data;
    }
    public function get_subscription_data($user_id)
    {
        $query = "SELECT s.*,pp.*,p.*
                FROM user_subscription as s
                LEFT JOIN subscription_plans as p ON p.plan_id = s.subscription_plan_id
                LEFT JOIN paypal_requests as pp ON pp.paypal_id = s.subscription_paypal_id
                WHERE s.is_delete=0 
                AND s.subscription_status = 1
                AND s.subscription_user_id =" . $user_id . "
                ORDER BY s.subscription_id DESC
                LIMIT 1";
        $result = DB::select($query);

        if (!empty($result)) {
            return (array)$result[0];
        }
        return false;
    }
    public function get_subscription_request($user_id)
    {
        $query = "SELECT r.*
                FROM subscription_requests as r
                WHERE r.is_delete=0 
                AND r.request_status = 0
                AND r.request_user_id =" . $user_id;
        $result = DB::select($query);

        if (!empty($result)) {
            return (array)$result;
        }
        return false;
    }
    public function get_notification_list($user_id)
    {
        $query = "SELECT n.*
                FROM notifications as n  
                WHERE n.is_delete=0 
                AND n.noti_status = 1
                AND n.noti_user_id =" . $user_id . "
                ORDER BY n.noti_id DESC";
        $result = DB::select($query);

        if (!empty($result)) {
            return (array)$result;
        }
        return false;
    }
    public function get_notification_count($user_id)
    {
        $query = "SELECT count(noti_id) as noti_count
                FROM notifications  
                WHERE  is_delete=0 
                AND noti_status = 1
                AND noti_user_id =" . $user_id;
        $result = DB::select($query);

        if (!empty($result)) {
            return (array)$result[0];
        }
        return 0;
    }
    /*public function get_module_data()
        {
            $modules = []; 
            if ( session()->get('my_chacha')['user_role']=='admin') {  
                $query="SELECT module_id,module_name 
                    FROM modules
                    WHERE is_delete=0 
                    AND module_status=1"; 
                $result = DB::select($query);
                if ($result)  {  
                    $modules = $result;   
                    $i=0;
                    foreach ($modules as $key => $value) { 
                        $p_query="SELECT permission_id,permission_name 
                            FROM permissions
                            WHERE is_delete=0 
                            AND permission_status=1
                            AND permission_module_id =".$value->module_id; 
                        $p_result = DB::select($p_query);
                        if (!empty($p_result))  {  
                            $modules[$i]->permissions = $p_result; 
                        }   
                        $i++;
                    } 
                } 
            }else{

                //$user_module_ids        = session()->get('my_chacha')['user_modules_ids'];
                $user_permission_ids    = session()->get('my_chacha')['user_permission_ids'];


                $user_module_ids    =   explode(',', $user_module_ids);
                $user_module_ids    =   array_filter(array_unique($user_module_ids));
                $user_module_ids    =   implode(',',$user_module_ids);

                $user_permission_ids    =   explode(',', $user_permission_ids);
                $user_permission_ids    =   array_filter(array_unique($user_permission_ids));
                $user_permission_ids    =   implode(',',$user_permission_ids);
 
 
                if (!empty($user_module_ids)  && !empty($user_permission_ids)) {   
                    $m_query="SELECT module_id,module_name 
                        FROM modules
                        WHERE is_delete=0 
                        AND module_status=1
                        AND module_id IN (".$user_module_ids.")"; 
                    $m_result = DB::select($m_query);
                    if ($m_result)  {  
                        $modules = $m_result;   
                        $i=0;
                        foreach ($modules as $key => $value) { 
                            $p_query="SELECT permission_id,permission_name 
                                FROM permissions
                                WHERE is_delete=0 
                                AND permission_status=1
                                AND permission_module_id =".$value->module_id."
                                AND permission_id IN (".$user_permission_ids.")"; 

                            $p_result = DB::select($p_query);
                            if (!empty($p_result))  {  
                                $modules[$i]->permissions = $p_result; 
                            }   
                            $i++;
                        } 
                    }  
                }
 

            }



            return $modules;
             
        }*/
    /* public function get_role_modules($role_id)
        {  
            $query="SELECT role_module_ids,role_permission_ids
                FROM user_roles
                WHERE is_delete=0 
                AND role_status=1
                AND role_id =".$role_id; 
            $result = DB::select($query);
            if (!empty($result))  {  
               return $result[0];    
            }  
            return false; 
        }*/
    public function get_languages()
    {
        $query = "SELECT *
                FROM languages
                WHERE is_delete=0 ";
        $result = DB::select($query);

        if (!empty($result)) {
            return $result;
        }
        return false;
    }
    public function get_language_detail($lang_id)
    {
        $query = "SELECT *
                FROM languages
                WHERE is_delete=0 
                AND language_id = " . $lang_id;

        $result = DB::select($query);

        if (!empty($result)) {
            return $result[0];
        }
        return false;
    }
    public function get_subscriptions()
    {
        $query = "SELECT plan_id,plan_price,plan_price_usd,plan_allowed_forms,plan_allowed_users,plan_duration,plan_name,plan_support,plan_template,plan_type
                FROM subscription_plans
                WHERE is_delete=0 
                AND plan_status = 1 
                ORDER BY plan_price ASC";

        $result = DB::select($query);

        if (!empty($result)) {
            return $result;
        }
        return false;
    }

    /*get Settings*/
    public static function getSettings($params = "")
    {
        $result = DB::table('settings')
            ->select('settings.*')
            ->where('setting_name', '=', $params)
            ->first();

        return (array)$result;
    }
    /*get All Products*/
    /*public static function getallPost()
        {
            $result = DB::table('post')
                        ->where('is_delete','=',0)
                        ->where('post_status','=',1)
                        ->get()->toArray();
                    return $result;

        }
    */
    /*End*/
}
