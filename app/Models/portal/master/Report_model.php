<?php

namespace App\Models\portal\master;

use DB;
use Illuminate\Database\Eloquent\Model;

class Report_model extends Model 
{
    private static $table_name = 'user_track';
    public function __construct() 
    {
        parent::__construct();
    }
    public static function userReportData($passed_id=''){
        $data = array();    
        $assetUrl = asset('assets/upload/images/thumb/').'/';
        $assetOriginalUrl = asset('assets/upload/images/original/').'/';   
        $trackData = DB::table(static::$table_name)
                ->select('*')
                ->where('track_user_id',$passed_id)
                ->orderBy('track_id','ASC')
                ->get()->toArray();
        
        $postids  = array_filter(array_unique(array_column($trackData, 'track_post_save_id')));
        $frameids = array_filter(array_unique(array_column($trackData, 'track_frame_save_id')));
        if(!empty($frameids)){
            $data['saveFrameData'] = self::get_frame_data($frameids);
        }
        if(!empty($postids)){
            $data['savePostData'] = self::get_post_data($postids);        
        }
        return $data;
    }
    public static function mostUseReport(){
        $data = [];
        $mostFrametrack =  DB::select("SELECT *, COUNT(*) as total FROM user_track GROUP BY track_frame_save_id HAVING COUNT(*) > 1 ORDER BY total DESC");        

        $mostPosttrack =  DB::select("SELECT *, COUNT(*) as total FROM user_track GROUP BY track_post_save_id HAVING COUNT(*) > 1 ORDER BY total DESC"); 

        $frameids  = array_filter(array_unique(array_column($mostFrametrack, 'track_frame_save_id')));
        $postids  = array_filter(array_unique(array_column($mostPosttrack, 'track_post_save_id')));

        if(!empty($frameids)){
            $data['mostFrameData'] = self::get_frame_data($frameids);
        }
        if(!empty($postids)){
            $data['mostPostData'] = self::get_post_data($postids);        
        }
        return $data;
    }
    public static function get_post_data($postids)
        {
            $assetUrl = asset('assets/upload/images/thumb/').'/';
            $assetOriginalUrl = asset('assets/upload/images/original/').'/';
            
            $PostQuery = "SELECT p.post_id,p.post_name,c.category_name,
            CONCAT('".$assetUrl."',i1.image_name) AS post_image,CONCAT('".$assetOriginalUrl."',i1.image_name) AS post_original_image 
                    FROM post AS p 
                    LEFT JOIN images as i1 on i1.image_id=p.post_image
                    LEFT JOIN category as c on c.category_id=p.post_category
                    WHERE post_id IN (".implode(",", array_map("intval", $postids)) .") 
                    AND p.post_status=1 
                    AND p.is_delete=0
                    ORDER BY p.post_name ASC ";
            $savepost = DB::select($PostQuery); 
            if(!empty($savepost)){
                return $savepost;
            }
        }
    public static function get_frame_data($frameids)
        {
            $assetUrl = asset('assets/upload/images/thumb/').'/';
            $assetOriginalUrl = asset('assets/upload/images/original/').'/';

            $FrameQuery = "SELECT f.frame_id,f.frame_name,
            CONCAT('".$assetUrl."',i1.image_name) AS frame_image,CONCAT('".$assetOriginalUrl."',i1.image_name) AS frame_original_image 
                FROM frames AS f 
                    LEFT JOIN images as i1 on i1.image_id=f.frame_image
                    WHERE frame_id IN (".implode(",", array_map("intval", $frameids)) .") 
                    AND f.frame_status=1 
                    AND f.is_delete=0
                    ORDER BY f.frame_name ASC ";
            $saveframe = DB::select($FrameQuery); 
            if(!empty($saveframe)){
                return $saveframe;
            }        
        }    


}
