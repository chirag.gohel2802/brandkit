<?php

namespace App\Models\portal\master;

use DB;
use Illuminate\Database\Eloquent\Model;

class Image_model extends Model
{
    private static $table_name = 'images';
    
    public function __construct()
    {
        parent::__construct();
    }

    
    public static function dt_list_data($params = [])
    {
        if(empty($params)){
            return false;
        }

        $order_by           =   $params['order_by'];
        $order_by_type      =   $params['order_by_type'];
        $limit_start        =   $params['limit_start'];
        $limit_length       =   $params['limit_length'];
        $where_raw          =   $params['where_raw'];

        $query = DB::table(static::$table_name)
                        ->select('images.image_id','images.image_name','images.image_file_name','images.image_url','images.image_alt_tag','images.image_user_id','images.image_details','images.image_caption','images.image_type','images.image_status')
                        ->where('images.is_delete',0);

        if (!empty($where_raw)) {
            $query = $query->WhereRaw($where_raw);
        }

        if (!empty($order_by)) {
            $query = $query->orderBy($order_by,$order_by_type);
        }

        $total = $query->get()->count();
        $query = $query->limit($limit_length)->offset($limit_start); 
        $data = $query->get();
        return array('total'=>$total,"result"=>$data->toArray());
    }

   
    public static function get_edit_detail($passed_id = '')
    {
        $result = DB::table(static::$table_name)
                        ->select('images.*')
                        ->where('images.image_id',$passed_id)
                        ->where('images.is_delete',0)
                        ->first();

        return (array)$result;
    }
    public static function getImageDetail($passed_id = '',$image_details='')
    {
        $result = DB::table(static::$table_name)
                        ->select('images.*')
                        ->where('images.image_id',$passed_id)
                        ->where('images.image_details',$image_details)
                        ->where('images.is_delete',0)
                        ->first();

        return (array)$result;
    }

    public static function check_images_exists($params = []){

        $result = DB::table(static::$table_name)
            ->where('is_delete',0)
            ->where($params)
            ->get()->count();

        if($result <= 0){
            return false;
        }
        return true;
    }

    
    public static function get_images_status($params = []){

        $result = DB::table(static::$table_name)
            ->where('is_delete',0)
            ->where('images_status',1)
            ->where($params)
            ->get()->toArray();
        
        return $result;
    }




}
