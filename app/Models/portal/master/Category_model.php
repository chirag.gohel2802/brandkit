<?php
namespace App\Models\portal\master;

use DB;
use Illuminate\Database\Eloquent\Model;

class Category_model extends Model
{
    private static $table_name = 'category';
    
    public function __construct()
    {
        parent::__construct();
    }
 
    
    public static function dt_list_data($params = [])   
    {
        if(empty($params)){
            return false;
        }
        $order_by           =   $params['order_by'];
        $order_by_type      =   $params['order_by_type'];
        $limit_start        =   $params['limit_start'];
        $limit_length       =   $params['limit_length'];
        $where_raw          =   $params['where_raw'];

          
        
        $query = DB::table(static::$table_name)
                        ->select('category.category_id','category.category_parent_id','category.category_name','category.category_slug','category.category_image','category.category_status','category.category_type','category.category_date','category.is_custom')
                        ->where('category.is_delete',0);

        if (!empty($where_raw)) {
            $query = $query->WhereRaw($where_raw);
        }
        if (!empty($order_by)) {
            $query = $query->orderBy($order_by,$order_by_type);
        }
        
        $total = $query->get()->count();
        $query = $query->limit($limit_length)->offset($limit_start); 
        $data = $query->get()->toArray();
        if(!empty($data)){
            foreach ($data as $key => $value) {
                $total_sub = 0;
                $subquery = DB::table(static::$table_name)->select('category_id')->where('category_parent_id','=',$value->category_id)->where('is_delete',0)->get()->count();
                if($subquery !=''){
                    $total_sub = $subquery;
                }
                $data[$key]->totalsubcategory = $total_sub;
            }
        }
        return array('total'=>$total,"result"=>$data);
    }


    public static function get_parent_list()
    {  
        $result = DB::table(static::$table_name)
            ->select('category_id','category_parent_id','category_name','category_type','category_date')
            ->where('category_parent_id','=',0)
            ->where('is_delete', 0)
            ->where('category_status', 1)
            ->orderBy('category_name')
            ->get()->toArray();
        
        return $result;
    }
    public static function get_ajax_list($where = [])
    {  
        $result = DB::table(static::$table_name)
            ->select('category_id','category_parent_id','category_name','category_type','category_date')
            ->where('category_parent_id','<>',0)
            ->where('is_delete', 0)
            ->where($where)
            ->orderBy('category_name')
            ->get()->toArray();

        if(!empty($result)){
            foreach ($result as $key => $value) {
                $parent_name = DB::table(static::$table_name)->select('category_name')->where('category_id', $value->category_parent_id)->where('is_delete',0)->first();
                if(!empty($parent_name)){
                    $result[$key]->parent_category_name = $parent_name->category_name;
                }else{
                    $result[$key]->parent_category_name = '-';
                }
            }
        }    
        return $result;
    }

    public static function get_all_post_list()
    {  
        $result = DB::table(static::$table_name)
            ->select('category_id','category_parent_id','category_name','category_type','category_date')
            ->where('category_parent_id','<>',0)
            ->where('is_delete', 0)
            ->orderBy('category_name')
            ->get()->toArray();

        if(!empty($result)){
            foreach ($result as $key => $value) {
                $parent_name = DB::table(static::$table_name)->select('category_name')->where('category_id', $value->category_parent_id)->where('is_delete',0)->first();
                if(!empty($parent_name)){
                    $result[$key]->parent_category_name = $parent_name->category_name;
                }else{
                    $result[$key]->parent_category_name = '-';
                }
            }
        }    
        return $result;
    }
    public static function get_custom_category_ajax_list($where = [])
    {  
        $result = DB::table(static::$table_name)
            ->select('category_id','category_name as parent_category_name')
            ->where('category_parent_id','=',0)
            ->where('is_custom','=',1)
            ->where('is_delete', 0)
            ->where($where)
            ->orderBy('category_name')
            ->get()->toArray();

        $new_arr = [];
        if(!empty($result)){
            foreach ($result as $key => $value) {
                $child_data = DB::table(static::$table_name)
                            ->select('category_id','category_parent_id','category_name','category_type','category_date')
                            ->where('category_parent_id', $value->category_id)
                            ->where('is_delete',0)
                            ->get()->toArray();

                if(!empty($child_data)){
                    foreach ($child_data as $child_key => $child_value) {
                        $child_value->parent_category_name = $value->parent_category_name;
                        $new_arr[] = $child_value;
                    }
                }
            }
        }    
        return $new_arr;
    }
    public static function get_ajax_list_post()
    {  
        $result = DB::table(static::$table_name)
            ->select('category_id','category_parent_id','category_name','category_date')
            ->where('category_parent_id','<>',0)
            ->where('category_type',1)
            ->where('is_delete', 0)
            ->orderBy('category_name')
            ->get()->toArray();
        return $result;
    }
    public static function get_ajax_list_video()
    {  
        $result = DB::table(static::$table_name)
            ->select('category_id','category_parent_id','category_name','category_date')
            ->where('category_parent_id','<>',0)
            ->where('category_type',2)
            ->where('is_delete', 0)
            ->orderBy('category_name')
            ->get()->toArray();
        return $result;
    }
    public static function get_sub_category($passed_id='',$type='')
    {  
        $result = DB::table(static::$table_name)
            ->select('category_id','category_parent_id','category_name','category_date')
            ->where('category_parent_id','=',$passed_id)
            ->where('category_type',$type)
            ->where('is_delete', 0)
            ->orderBy('category_name')
            ->get()->toArray();
        if(!empty($result)){
            foreach ($result as $key => $value){
                $parent_name = DB::table(static::$table_name)->select('category_name')->where('category_id', $value->category_parent_id)->where('is_delete',0)->first();
                if(!empty($parent_name)){
                    $result[$key]->parent_category_name = $parent_name->category_name;
                }else{
                    $result[$key]->parent_category_name = '-';
                }
            }
        }    
        return $result;
    }

    public static function get_edit_detail($passed_id = '')
    {
        $result = DB::table(static::$table_name)
                        ->leftJoin('images', 'images.image_id', '=', 'category.category_image')
                        ->select('category.*','images.image_name','images.image_file_name','images.image_url','images.image_alt_tag')
                        ->where('category.category_id',$passed_id)
                        ->where('category.is_delete',0)
                        ->first();

        return (array)$result;
    }

    public static function check_category_exists($params = []){

        $result = DB::table(static::$table_name)
            ->where('is_delete',0)
            ->where($params)
            ->get()->count();

        if($result <= 0){
            return false;
        }
        return true;
    }
    public static function checkParentCategory($params = []){

        $result = DB::table(static::$table_name)
            ->select('category_id','category_parent_id')
            ->where('is_delete',0)
            ->where('category_parent_id',$params)
            ->get()->toArray();
        
        return $result;
        
    }

    public static function get_category_status($params = []){

        $result = DB::table(static::$table_name)
            ->where('is_delete',0)
            ->where('category_status',1)
            ->where($params)
            ->get()->toArray();
        return $result;
    }




}
