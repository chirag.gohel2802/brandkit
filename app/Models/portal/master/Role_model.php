<?php

namespace App\Models\portal\master;

use DB;
use Illuminate\Database\Eloquent\Model;

class Role_model extends Model
{
    private static $table_name = 'user_roles';
    
    public function __construct()
    {
        parent::__construct();
    }

    
    public static function dt_list_data($params = [])
    {
        if(empty($params)){
            return false;
        }

        $order_by           =   $params['order_by'];
        $order_by_type      =   $params['order_by_type'];
        $limit_start        =   $params['limit_start'];
        $limit_length       =   $params['limit_length'];
        $where_raw          =   $params['where_raw'];

        $query = DB::table(static::$table_name)
                        ->select('role_id','role_name','role_status')
                        ->where('is_delete',0);

        if (!empty($where_raw)) {
            $query = $query->WhereRaw($where_raw);
        }

        if (!empty($order_by)) {
            $query = $query->orderBy($order_by, $order_by_type);
        }

        $total = $query->get()->count();
        $query = $query->limit($limit_length)->offset($limit_start); 
        $data = $query->get();
        return array('total'=>$total,"result"=>$data->toArray());
        
    }

    public static function get_ajax_list()
    {  
        $result = DB::table(static::$table_name)
            ->where('is_delete', 0)
            ->orderBy('role_id')
            ->get()->toArray();
        
        return $result;
    }

    public static function get_edit_detail($passed_id = '')
    {
        $result = DB::table(static::$table_name)
            ->select('*','role_id as id')
            ->where('is_delete', 0)
            ->where('role_id', $passed_id)
            ->first();

        return (array)$result;
    }



    public static function get_module_data()
    {
        $modules = []; 

        // if ( session()->get('my_chacha')['user_role']=='admin') {
            
            $result = DB::table('modules')
                ->select('module_id','module_name')
                ->where('is_delete', 0)
                ->where('module_status', 1)
                ->get()->toArray();

            if (!empty($result)) {
                $modules = $result;
                $i=0;
                foreach ($result as $value) {

                    $sub_result = DB::table('permissions')
                        ->select('permission_id','permission_name')
                        ->where('is_delete', 0)
                        ->where('permission_status', 1)
                        ->where('permission_module_id', $value->module_id)
                        ->get()->toArray();

                    if (!empty($sub_result)){  
                        $modules[$i]->permissions = $sub_result; 
                    }   
                    $i++;
                } 
            } 
        // }else{

        //     $user_module_ids        = session()->get('my_chacha')['user_modules_ids'];
        //     $user_permission_ids    = session()->get('my_chacha')['user_permission_ids'];


        //     $user_module_ids    =   explode(',', $user_module_ids);
        //     $user_module_ids    =   array_filter(array_unique($user_module_ids));
        //     $user_module_ids    =   implode(',',$user_module_ids);

        //     $user_permission_ids    =   explode(',', $user_permission_ids);
        //     $user_permission_ids    =   array_filter(array_unique($user_permission_ids));
        //     $user_permission_ids    =   implode(',',$user_permission_ids);


        //     if (!empty($user_module_ids)  && !empty($user_permission_ids)) {   
        //         $m_query="SELECT module_id,module_name 
        //             FROM modules
        //             WHERE is_delete=0 
        //             AND module_status=1
        //             AND module_id IN (".$user_module_ids.")"; 
        //         $m_result = DB::select($m_query);
        //         if ($m_result)  {  
        //             $modules = $m_result;   
        //             $i=0;
        //             foreach ($modules as $key => $value) { 
        //                 $p_query="SELECT permission_id,permission_name 
        //                     FROM permissions
        //                     WHERE is_delete=0 
        //                     AND permission_status=1
        //                     AND permission_module_id =".$value->module_id."
        //                     AND permission_id IN (".$user_permission_ids.")"; 

        //                 $p_result = DB::select($p_query);
        //                 if (!empty($p_result))  {  
        //                     $modules[$i]->permissions = $p_result; 
        //                 }   
        //                 $i++;
        //             } 
        //         }  
        //     }
        // }
        return $modules;
    }
 

             





}