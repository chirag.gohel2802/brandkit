<?php

namespace App\Models\portal\master;

use DB;
use Illuminate\Database\Eloquent\Model;

class App_version_model extends Model
{
    private static $table_name = 'system_updates';
    
    public function __construct()
    {
        parent::__construct();
    }

    
    public static function dt_list_data($params = [])
    {
        if(empty($params)){
            return false;
        }

        $order_by           =   $params['order_by'];
        $order_by_type      =   $params['order_by_type'];
        $limit_start        =   $params['limit_start'];
        $limit_length       =   $params['limit_length'];
        $where_raw          =   $params['where_raw'];

        $query = DB::table(static::$table_name)
                        ->where('is_delete',0);

        if (!empty($where_raw)) {
            $query = $query->WhereRaw($where_raw);
        }

        if (!empty($order_by)) {
            $query = $query->orderBy($order_by, $order_by_type);
        }
        // print_r($query->toSql());exit;
        $total = $query->get()->count();
        $query = $query->limit($limit_length)->offset($limit_start); 
        $data = $query->get();
        return array('total'=>$total,"result"=>$data->toArray());
    }

    public static function get_edit_detail($passed_id = '')
    {
        $result = DB::table(static::$table_name)
            ->select('*','update_id as id')
            ->where('is_delete', 0)
            ->where('update_id', $passed_id)
            ->first();

        return (array)$result;
    }



}
