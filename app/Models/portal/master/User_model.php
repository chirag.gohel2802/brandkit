<?php

namespace App\Models\portal\master;

use DB;
use Illuminate\Database\Eloquent\Model;


class User_model extends Model
{  
    private $organisation_id; 
    private static $table_name = 'users';
   
    public function __construct()
    {
        parent::__construct();  
        $this->organisation_id=session()->get('org_id'); 
    } 
    
    public static function dt_list_data($params = [])
    {
        if(empty($params)){
            return false;
        }

        $order_by           =   $params['order_by'];
        $order_by_type      =   $params['order_by_type'];
        $limit_start        =   $params['limit_start'];
        $limit_length       =   $params['limit_length'];
        $where_raw          =   $params['where_raw'];

        $query = DB::table(static::$table_name)
                        ->leftJoin('user_roles','users.user_role_id','=','user_roles.role_id')
                        ->select('users.user_id','users.user_name','users.user_phone_no','users.user_email','users.user_role_id','users.created_at','user_roles.role_id','user_roles.role_name as user_role_text','user_roles.role_name','users.user_status','users.added_by')
                        ->where('users.is_delete',0)
                        ->where('user_roles.role_id',2);

        if (!empty($where_raw)) {
            $query = $query->WhereRaw($where_raw);
        }

        if (!empty($order_by)) {
            $query = $query->orderBy($order_by, $order_by_type);
        }

        $total = $query->get()->count();
        $query = $query->limit($limit_length)->offset($limit_start); 
        $data = $query->get();
        return array('total'=>$total,"result"=>$data->toArray());
    }

    /*General Purpose User list*/
    public static function get_ajax_list($params = [])
    {
        $result = DB::table(static::$table_name)
            ->where('is_delete', 0)
            ->where($params)
            ->orderBy('user_name')
            ->get()->toArray();
        
        return $result;   
    }

    public static function get_edit_detail($passed_id = '')
    {
        $result = DB::table(static::$table_name)
            ->select('*','user_id as id')
            ->where('is_delete', 0)
            ->where('user_id', $passed_id)
            ->first();

        return (array)$result;
    }

    public function get_master_user_list($master_id)
    {  
        $session_user_id = session()->get('my_chacha')['user_id'];

        $result = DB::table(static::$table_name)
            ->where('user_id', $session_user_id)
            ->where('is_delete', 0)
            ->where('session_user_id', $session_user_id)
            ->where('user_master_id', $master_id)
            ->orderBy('user_name')
            ->get()->toArray();
        return $result;
    }

    public function get_role_user_list($role_id)
    {  
        $role_id = explode(',',$role_id);
        $result = DB::table(static::$table_name)
            ->select('user_id','user_name','user_role_id','user_phone_no','user_email')
            ->where('is_delete', 0)
            ->whereIn('user_role_id', $role_id)
            ->orderBy('user_name','ASC')
            ->get()->toArray();

        return $result;
    }
    public static function userList($params = [])
    {
        $result = DB::table(static::$table_name)
            ->select('user_id','user_name','user_phone_no','user_email','user_fcm_token')
            ->where('is_delete', 0)
            ->where($params)
            ->orderBy('user_name')
            ->get()->toArray();
        
        return $result;   
    }
    
    public static function csvUserList()
    {
        $result = DB::select("SELECT u.user_id,u.user_name,u.user_phone_no,u.user_email,u.user_fcm_token  
            FROM users as u 
            WHERE u.is_delete = 0 
            AND u.user_role_id = 2  
            AND u.user_fcm_token IS NOT NULL 
            GROUP BY u.user_fcm_token"); 
        if(!empty($result)) 
        {     
            return (array)$result ;   
        }
        return false;  
        
    }

    public static function businessList()
    {
        $result = DB::select("SELECT 
            user_id,
            user_name,
            user_phone_no  
            FROM users  
            WHERE is_delete = 0 
            AND  user_role_id = 2 "); 
        if(!empty($result)) 
        {      
            $users = $result ;  

            $business = [];
            $i=0;
            foreach($users as $user){
                $bInfo = [];
                $result = DB::select("SELECT 
                    '".$user->user_id."' as user_id,
                    '".$user->user_name."' as user_name,
                    '".$user->user_phone_no."' as user_phone_no,
                    b.business_id,  
                    b.business_name,
                    b.business_phone_no,
                    b.business_secondary_phone_no,
                    b.business_email,
                    b.business_address,
                    b.business_website, 
                    c.business_category_name
                    FROM business as b   
                    LEFT JOIN business_category as c ON b.business_category=c.business_category_id
                    WHERE b.is_delete = 0 
                    AND b.business_user_id='".$user->user_id."'  "); 
                if(!empty($result)) 
                { 
                    $bInfo = $result;
                }else{
                    $bInfo[0] = [
                        'user_id'                       => $user->user_id,  
                        'user_name'                     => $user->user_name,  
                        'user_phone_no'                 => $user->user_phone_no,  
                        'business_id'                   => '',  
                        'business_name'                 => '',
                        'business_phone_no'             => '',
                        'business_secondary_phone_no'   => '',
                        'business_email'                => '',
                        'business_address'              => '',
                        'business_website'              => '', 
                        'business_category_name'        => ''
                    ];
                }  

                $business[$i]=$bInfo;

                $i++;
            }

            $bList = [];
            $i=0;
            foreach ($business as $key => $value) {
                foreach($value as $k=>$v){
                    $bList[$i]=$v;
                    $i++;
                }
            }
 
            return $bList;
        }
        return false;  
    }

    public function get_role_user_ids($role_id)
    {  
        $query="SELECT GROUP_CONCAT(users.user_id) as user_ids
            FROM users as c  
            WHERE users.is_delete = 0   
            AND users.user_role_id IN (".$role_id.") "; 
        
        $result = DB::select($query);  
        if ($result>0) 
            { 
                return $result[0]->user_ids ; 
            }
        return false;   
    }

    public function get_account_user_count($master_id)
    {
        $query="SELECT COUNT(user_id) as userCount
            FROM users 
            WHERE  is_delete = 0   
            AND  user_master_id =".$master_id; 
        
        $result = DB::select($query);  
        if ($result>0) 
            { 
                return $result[0]->userCount+1 ; 
            }
        return 1;  
    }


    
    /*Bap hoi eva users nu list*/
    public function get_parent_user_list()
    {  
        $result = DB::table(static::$table_name)
            ->select('user_id','user_name','user_phone_no','user_email')
            ->where('user_role_id', 1)
            ->where('user_role_id','<>',2)
            ->where('is_delete', 0)
            ->where('user_master_id', 1)
            ->orderBy('user_name','ASC')
            ->get()->toArray();

        return $result;
    }


    /*User Details General*/
    public function get_user_detail($user_id)
    {  
        if(!empty($user_id)){
            $query="SELECT u.*,u.user_id as id,r.*,r.role_name as user_role_text
                FROM users as u   
                LEFT JOIN user_roles as r ON u.user_role_id=r.role_id
                WHERE u.is_delete = 0   
                AND u.user_id='".$user_id."' "; 
             
            $result = DB::select($query); 
            if ($result) 
                $result = $result[0]; 
                {   
                    return (array)$result ;   
                }
            }
        return false;   
    }


    /*User Subsciption Details*/
    public function get_subscription_detail()
    {  
        $query="SELECT u.*,s.*
            FROM user_subscription as u
            LEFT JOIN subscription_plans as s ON s.plan_id=u.subscription_plan_id
            WHERE u.is_delete = 0   
            AND u.subscription_user_id='".$this->data['user_id']."' 
            ORDER BY u.subscription_id DESC "; 
         
        $result = DB::select($query); 
        if ($result)  
            {   
                return $result ;   
            }
        return false;   
    }

     
    public function get_user_fcm($user_id,$user_type)
    {   
        $query="SELECT  user_fcm_token as fcm_token,user_id
            FROM users
            WHERE is_delete = 0  
            AND user_id =".$user_id."  "; 
        
        // echo $query;exit;
        $result = DB::select($query);  

        if (!empty($result))  { 
            return $result[0]->fcm_token; 
        }
        return false;   
    }  
        
    /*User Exists*/
    public function check_user_exist()
        {  
            if (isset($this->data['user_id']) && $this->data['user_id']!='') {  
                // echo "<pre>";print_r($this->data);exit;
                $query="SELECT  user_id
                    FROM users
                    WHERE is_delete = 0  
                    AND user_phone_no ='".$this->data['user_phone_no']."' 
                    AND user_id <>".$this->data['user_id'];        
            }else{
                $query="SELECT  user_id
                    FROM users
                    WHERE is_delete = 0  
                    AND user_phone_no ='".$this->data['user_phone_no']."' ";
            }
            
            $result = DB::select($query);  
            if (!empty($result))  { 
                return true; 
            }
            return false;   
        }

    public function is_user_access_exists()
        {   
            if (isset($this->data['id']) && $this->data['id']!='') {  
                // echo "<pre>";print_r($this->data);exit;
                $query="SELECT  user_id 
                    FROM users
                    WHERE is_delete = 0   
                    AND user_email ='".$this->data['user_email']."'  
                    AND user_id <>".$this->data['id'];        
            }else{
                $query="SELECT  user_id 
                    FROM users
                    WHERE is_delete = 0    
                    AND user_email ='".$this->data['user_email']."' ";
            }
            
            $result = DB::select($query);  
            if (!empty($result))  { 
                return true; 
            }
            return false; 
        }



    /*Logged Users List*/ 
    public function get_logged_user_data()  
        { 
            $query1="SELECT u.*
                FROM users as u  
                WHERE u.is_delete = 0   
                AND u.user_id='".session('vendor_chacha')['user_id']."' "; 
             
            $result1 = DB::select($query1);
            /*Here result will be multiple stdclasses array*/

            /*getting first record for single row $row[0]*/
            if ($result1)  
                {   
                    $result1 = $result1[0]; 
                    $user_info=(array)$result1 ;  
                    return $user_info; 
                } 
            return false;
 
        }




    public function get_authentic_user()
        {  
            $query="SELECT  s.*,u.*,user_roles.*
                FROM user_login_sessions as s
                LEFT JOIN  users as u ON u.user_id=s.session_user_id
                LEFT JOIN  user_roles as r ON u.user_role_id=user_roles.role_id
                WHERE s.is_delete = 0  
                AND s.session_token = '".$this->data['user_token']."'
                AND s.session_status =1  
                AND s.session_expiry_timestamp >='".date("Y-m-d H:i:s")."' "; 
            
            $result = DB::select($query);  

            if (!empty($result))  { 
                $result_data=$result[0]; 

                $user_module_ids            = $result_data->user_module_ids;
                $user_permission_ids        = $result_data->user_permission_ids;
                $user_role_module_ids       = isset($result_data->role_module_ids) ? $result_data->role_module_ids : '' ;
                $user_role_permission_ids   = isset($result_data->role_permission_ids) ? $result_data->role_permission_ids : '' ; 

                if (!empty($user_module_ids)) { 
                    $user_module_ids = explode(',', $user_module_ids); 
                }else{
                    $user_module_ids = []; 
                }
                if(!empty($user_role_module_ids)) {
                    $user_role_module_ids = explode(',', $user_role_module_ids); 
                }else{
                    $user_role_module_ids = [];
                }  
                if (!empty($user_permission_ids)) { 
                    $user_permission_ids = explode(',', $user_permission_ids); 
                }else{
                    $user_permission_ids = []; 
                }
                if(!empty($user_role_permission_ids)) {
                    $user_role_permission_ids = explode(',', $user_role_permission_ids); 
                }else{
                    $user_role_permission_ids = [];
                }  
                $user_module_ids  =  array_filter(array_unique(array_merge($user_module_ids,$user_role_module_ids)));
                $user_permission_ids  =  array_filter(array_unique(array_merge($user_permission_ids,$user_role_permission_ids))); 

              
                $result_data->user_modules      =$user_module_ids;
                $result_data->user_permissions  =$user_permission_ids;
                 
                return $result_data;
 

            }
            return false;   
        } 
 

}
