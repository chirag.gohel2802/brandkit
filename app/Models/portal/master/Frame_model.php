<?php

namespace App\Models\portal\master;

use DB;
use Illuminate\Database\Eloquent\Model;

class Frame_model extends Model 
{
    protected static $table_name = 'frames';
    // protected $table = 'users';
    public function __construct() 
    {
        parent::__construct();
    }

    
    public static function dt_list_data($params = [])
    { 
        if(empty($params)){
            return false;
        }
        $order_by           =   $params['order_by'];
        $order_by_type      =   $params['order_by_type'];
        $limit_start        =   $params['limit_start'];
        $limit_length       =   $params['limit_length'];
        $where_raw          =   $params['where_raw'];

        $query = DB::table(static::$table_name)
                        ->leftJoin('images','images.image_id','=','frames.frame_image')
                        ->select('frames.*','images.image_name','images.image_url')
                        ->where('frames.is_delete',0);

        if (!empty($where_raw)) {
            $query = $query->WhereRaw($where_raw);
        }
        if (!empty($order_by)) {
            $query = $query->orderBy($order_by,$order_by_type);
        }
        $total = $query->get()->count();
        $query = $query->limit($limit_length)->offset($limit_start); 
        $data = $query->get();
        return array('total'=>$total,"result"=>$data->toArray());
    }
    
    public static function get_edit_detail($passed_id = '')
        {
            $assetUrl = asset('assets/upload/images/thumb/').'/';
            $assetOriginalUrl = asset('assets/upload/images/original/').'/';

            $result = DB::table(static::$table_name)
                        ->leftJoin('images','images.image_id','=','frames.frame_image')
                        ->select('frames.*','images.image_name','images.image_url','images.image_alt_tag')
                        ->where('frames.frame_id',$passed_id)
                        ->where('frames.is_delete',0)
                        ->first();
            $returnData = (array)$result;
            if(!empty($returnData['frame_address_icon'])){
                $returnData['address_icon_thumb'] = $assetUrl.$returnData['frame_address_icon'];
            }if(!empty($returnData['frame_website_icon'])){
                $returnData['website_icon_thumb'] = $assetUrl.$returnData['frame_website_icon'];
            }if(!empty($returnData['frame_phone_icon'])){
                $returnData['phone_icon_thumb'] = $assetUrl.$returnData['frame_phone_icon'];
            }if(!empty($returnData['frame_email_icon'])){
                $returnData['email_icon_thumb'] = $assetUrl.$returnData['frame_email_icon'];
            }
            return $returnData;
            // echo "<pre>";print_r($returnData);exit;
        }

    /*Assign User Frame*/
    public static function frame_assign_list($params = [])
        {
            if(empty($params)){
                return false;
            }
            $assetUrl = asset('assets/upload/images/thumb/').'/';
            $assetOriginalUrl = asset('assets/upload/images/original/').'/';
            $query = "SELECT 
                f.*,
                CONCAT('".$assetUrl."',i1.image_name) AS frame_image, 
                CONCAT('".$assetOriginalUrl."',i1.image_name) AS frame_original_image 
            FROM frames AS f
            LEFT JOIN images as i1 ON i1.image_id=f.frame_image
            WHERE f.is_delete=0 AND f.frame_status =1
            AND FIND_IN_SET(".$params.", f.assigned_user) ";
            $data = DB::select($query); 
            return $data;
        }      
    public static function check_frame_exists($params = []){

        $result = DB::table(static::$table_name)
            ->where('is_delete',0)
            ->where($params)
            ->get()->count();

        if($result <= 0){
            return false;
        }
        return true;
    }

    public static function get_frame_status($params = []){
        $result = DB::table($this->table_name)
            ->where('is_delete',0)
            ->where('frame_status',1)
            ->where($params)
            ->get()->toArray();
        
        return $result;
    }




}