<?php

namespace App\Models\portal\master;

use DB;
use Illuminate\Database\Eloquent\Model;

class Banner_model extends Model 
{
    private static $table_name = 'banners';
    
    public function __construct() 
    {
        parent::__construct();
    }

    
    public static function dt_list_data($params = [])
    {
        if(empty($params)){
            return false;
        }
        $order_by           =   $params['order_by'];
        $order_by_type      =   $params['order_by_type'];
        $limit_start        =   $params['limit_start'];
        $limit_length       =   $params['limit_length'];
        $where_raw          =   $params['where_raw'];

        $query = DB::table(static::$table_name)
                        ->leftJoin('images','images.image_id','=','banners.banner_image')
                        ->select('banners.*','images.image_name','images.image_url')
                        ->where('banners.is_delete',0);

        if (!empty($where_raw)) {
            $query = $query->WhereRaw($where_raw);
        }
        if (!empty($order_by)) {
            $query = $query->orderBy($order_by,$order_by_type);
        }

        $total = $query->get()->count();
        $query = $query->limit($limit_length)->offset($limit_start); 
        $data = $query->get();
        return array('total'=>$total,"result"=>$data->toArray());
    }

    
    public static function get_edit_detail($passed_id = '')
        {
            $result = DB::table(static::$table_name)
                        ->leftJoin('images','images.image_id','=','banners.banner_image')
                        ->select('banners.*','images.image_name','images.image_url','images.image_alt_tag')
                        ->where('banners.banner_id',$passed_id)
                        ->where('banners.is_delete',0)
                        ->first();
            return (array)$result;
        }

    public static function check_banners_exists($params = []){

        $result = DB::table(static::$table_name)
            ->where('is_delete',0)
            ->where($params)
            ->get()->count();

        if($result <= 0){
            return false;
        }
        return true;
    }



}
