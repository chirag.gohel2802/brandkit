<?php 
namespace App\Models; 

use DB;
class Master  
{
    protected   $data  ;
    
    public function __construct()
		{ 
		  	$this->data=array();   	
		}

	public function set_fields($data){
        $this->data=$data;
    }

    public function stdClass_to_array($data){
        return (array)$data;
    } 
	
}
