<?php

namespace App\Models; 
use App\Models\Master;
use DB;

class Common extends Master
{
    public function __construct()
		{
		  	parent::__construct(); 	  	
		}
    public function validSingle($data)
        {
            if (!$data->isEmpty() ) { 
                return  (array)$data->toArray()[0]; 
            }else{
                return  false; 
            }
        }
    
    public function validList($data)
        {
            if (!$data->isEmpty() ) { 
                return $data->toArray(); 
            }else{
                return  false; 
            }
        }         
    public function send_email($email,$subject,$message) 
        {  
            $token      =   'maravadamalilughas'; 
            $url        =   'https://safra.cloud/safra_email.php';

            // echo ($message);exit;
            $myvars =   'subject=' . $subject. 
                        '&message=' . $message. 
                        '&email=' . $email. 
                        '&token=' . $token;

            $ch = curl_init( $url );
            curl_setopt( $ch, CURLOPT_POST, 1);
            curl_setopt( $ch, CURLOPT_POSTFIELDS, $myvars);
            curl_setopt( $ch, CURLOPT_FOLLOWLOCATION, 1);
            curl_setopt( $ch, CURLOPT_HEADER, 0);
            curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1);

            $response = curl_exec( $ch ); 
            return $response;
        }

    public function get_account_user_count($master_id)
        {
            $query="SELECT COUNT(user_id) as userCount
                FROM users 
                WHERE  is_delete = 0   
                AND  user_master_id =".$master_id; 
            
            $result = DB::select($query);  
            if ($result>0) 
                { 
                    return $result[0]->userCount+1 ; 
                }
            return 1;  
        }

    public function get_account_form_count($master_id)
        {
            $query="SELECT COUNT(form_id) as formCount
                FROM forms
                WHERE  is_delete = 0   
                AND  form_master_id=".$master_id; 
            
            $result = DB::select($query);  
            if ($result>0){ 
                return $result[0]->formCount; 
            }
            return 0; 
        }
        
	public function save_my_data($tableName)
		{			 
			$inserted_id = DB::table($tableName)->insertGetId($this->data);   
			return $inserted_id;	

			// $inserted_id = DB::table($tableName)->insertGetId($this->data,$is_column_name);  

			/*Just Insert Without Getting Inserted Id*/
			// DB::table('users')->insert([
			//     $this->data;
			// ]);
		}

    public function get_form_detail()
        {  
            $query="SELECT c.*,c.form_id as id
                FROM forms as c  
                WHERE c.is_delete = 0   
                AND c.form_unique_id='".$this->data['form_unique_id']."' "; 
             
            $result = DB::select($query);
            /*Here result will be multiple stdclasses array*/

            /*getting first record for single row $row[0]*/
            if ($result) 
                $result = $result[0]; 
                {  
                    // return $this->stdClass_to_array($result); 
                    return (array)$result ;  
                    /*Converting Result that in stdClass to in Array*/
                }
            return false;   
        }

     public function get_common_user_detail($user_id)
        {  
            $query="SELECT u.*
                FROM users as u  
                WHERE u.is_delete = 0   
                AND u.user_id=".$user_id; 
             
            $result = DB::select($query); 
            if ($result)   {   
                $result = $result[0]; 
                return (array)$result ;   
            }
            return false;   
        }

    public function get_role_list()
        {  
            $query="SELECT role_id,role_name
                FROM user_roles   
                WHERE is_delete = 0  
                ORDER BY role_name  ASC "; 
            
            $result = DB::select($query);  
            if ($result>0) 
                { 
                    return $result ; 
                }
            return false;
        }

    public function get_language_list($value='')
        {
                $query="SELECT language_id,language_title,language_slug,CONCAT('".url('')."','/',language_url) as language_url
                    FROM languages   
                    WHERE is_delete = 0 "; 
                
                $result = DB::select($query);  
                if ($result>0) 
                    { 
                        return $result ; 
                    }
                return false;   
        }
        
	public function save_my_batch_data($tableName)
		{			 
			$is_inserted = DB::table($tableName)->insert($this->data);   
			return $is_inserted;	 
		}

	public function update_my_data($tableName,$where)
		{
			 
			$return = DB::table($tableName)   
						->where($where)
						->update($this->data); 
			return $return;
		}

	public function remove_my_data($tableName,$where)
		{
			return $this->data; 
		}

	public function remove_my_batch_data($tableName,$where)
		{
			return $this->data;
		}

	public function generate_unique_code($table_name,$column_name)
		{
			$code=rand(100000,999999);

			$query="SELECT ".$column_name."
                    FROM ".$table_name."
                    WHERE is_delete = 0    
                    AND ".$column_name."='".$code."'"; 

            $result = DB::select($query);  
            if ($result)  {   
                $this->generate_unique_code($table_name,$column_name) ;   
            }else{
            	return $code;
            }   
		} 

    public function get_master_id($user_id)
        {  
             return 33;
        } 
    
    public function get_permission_list($user_id)
        {  
            $master_id = $this->get_master_id($user_id);
            // echo $user_id;exit;
            $query="SELECT u.user_module_ids,u.user_permission_ids,
                    r.role_module_ids,r.role_permission_ids
                    FROM users as u
                    LEFT JOIN user_roles as r ON r.role_id=u.user_role_id
                    WHERE u.is_delete = 0  
                    AND u.user_id =".$master_id;  
            $result = DB::select($query);   
            if (!empty($result))  {      
                
                $result_data = $result[0];
                $user_module_ids            = $result_data->user_module_ids;
                $user_permission_ids        = $result_data->user_permission_ids;
                $user_role_module_ids       = isset($result_data->role_module_ids) ? $result_data->role_module_ids : '' ;
                $user_role_permission_ids   = isset($result_data->role_permission_ids) ? $result_data->role_permission_ids : '' ; 

                if (!empty($user_module_ids)) { 
                    $user_module_ids = explode(',', $user_module_ids); 
                }else{
                    $user_module_ids = []; 
                }
                if(!empty($user_role_module_ids)) {
                    $user_role_module_ids = explode(',', $user_role_module_ids); 
                }else{
                    $user_role_module_ids = [];
                }  
                if (!empty($user_permission_ids)) { 
                    $user_permission_ids = explode(',', $user_permission_ids); 
                }else{
                    $user_permission_ids = []; 
                }
                if(!empty($user_role_permission_ids)) {
                    $user_role_permission_ids = explode(',', $user_role_permission_ids); 
                }else{
                    $user_role_permission_ids = [];
                }  
                $user_module_ids  =  array_filter(array_unique(array_merge($user_module_ids,$user_role_module_ids)));
                $user_permission_ids  =  array_filter(array_unique(array_merge($user_permission_ids,$user_role_permission_ids))); 
  

                $module_ids         = implode(',', $user_module_ids);
                $permission_ids     = implode(',', $user_permission_ids); 
                $modules = []; 
     
                if (!empty($module_ids)  && !empty($permission_ids)) {   
                    $m_query="SELECT module_id,LOWER(module_name) as module_name 
                        FROM modules
                        WHERE is_delete=0 
                        AND module_status=1
                        AND module_id IN (".$module_ids.")"; 
                    $m_result = DB::select($m_query);
                    if ($m_result)  {  
                        $modules = $m_result;   
                        $i=0;
                        foreach ($modules as $key => $value) { 
                            $p_query="SELECT permission_id,LOWER(permission_name) as permission_name 
                                FROM permissions
                                WHERE is_delete=0 
                                AND permission_status=1
                                AND permission_module_id =".$value->module_id."
                                AND permission_id IN (".$permission_ids.")"; 

                            $p_result = DB::select($p_query);
                            if (!empty($p_result))  {  
                                $modules[$i]->permissions = $p_result; 
                            }   
                            $i++;
                        } 
                    }  
                } 


                return $modules;
            }else{
                return false;
            }    
        } 


    public function get_id_by_column($table_name,$table_alias,$column_name,$value,$org_id)
        {  
            $query="SELECT ".$table_alias."_id as id
                    FROM ".$table_name."
                    WHERE is_delete = 0    
                    AND ".$table_alias."_organisation_id=".$org_id."
                    AND ".$column_name."='".$value."'"; 

            $result = DB::select($query);  
            if ($result)  {   
                return $result[0]->id;
            }else{
                return 0;
            }  

        }

    public function get_user_list_by_csv($user_ids)
        {  
            $query="SELECT c.user_id,c.user_name,c.user_fcm_token 
                FROM users as c  
                WHERE c.is_delete = 0   
                AND c.user_id IN (".$user_ids.") "; 
            
            $result = DB::select($query);  
            if ($result>0) 
                { 
                    return $result ; 
                }
            return false;   
        }


    public function get_notification_list($noti_reference_id,$noti_module,$noti_module_part)
        {  
            $query="SELECT *
                    FROM notifications
                    WHERE is_delete = 0    
                    AND noti_reference_id=".$noti_reference_id."
                    AND noti_module=".$noti_module."
                    AND noti_module_part=".$noti_module_part; 

            $result = DB::select($query);  
            if ($result)  {   
                return $result;
            }else{
                return false;
            }  

        }

    public function get_all_notifications($user_id)
        {  
            $query="SELECT noti_id,noti_title,noti_reference_id,noti_module,noti_module_part,noti_status,created_at
                    FROM notifications
                    WHERE is_delete = 0  
                    AND noti_status = 1     
                    AND noti_user_id=".$user_id; 

            $result = DB::select($query);  
            if ($result)  {   
                return $result;
            }else{
                return [];
            }  

        }

	public function send_notification($fcm_token='',$message='',$data,$device)
        {    
            $expiry     = time()+(60*60*24);
            $seconds    = 4500; 
            /*Android*/
            if ($device==1)  {  
                $fields='{
                        "to" : "'.$fcm_token.'", 
                        "data":'.json_encode($data).',
                        "apns":{
                            "headers":{
                                "apns-expiration":"'.$expiry.'"
                            }
                        },
                        "android":{
                            "ttl":"'.$seconds.'s"
                        },
                        "webpush":{
                            "headers":{
                                "TTL":"'.$seconds.'"
                            }
                        }

                    }';
            } 
            /*IOS*/
            if ($device==2)  {
                $fields='{
                        "to" : "'.$fcm_token.'",
                        "notification" :  {
                                "title" : "'.$message["title"].'" , 
                                "body"  : "'.$message["message"].'" 
                        }, 
                        "data":'.json_encode($data).',
                        "apns":{
                            "headers":{
                                "apns-expiration":"'.$expiry.'"
                            }
                        },
                        "android":{
                            "ttl":"'.$seconds.'s"
                        },
                        "webpush":{
                            "headers":{
                                "TTL":"'.$seconds.'"
                            }
                        }
                    }';
            
            } 
            $headers = array(
                'Content-Type:application/json',
                'Authorization:key=AAAAh96qZZY:APA91bHfOsYicsGW0QBqRot-THpx_4Ciro1wXgUAFp1__y52nlUhHuv3LoH0NmrSco1iX_Pj0I5uutbPTxmtkekNLwZNnzj9yjI64GCE287Oo86LPgAD5B6nh8m2Kp_OK4UzDtjht4Wd'
            );
            $ch = curl_init();
            curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
            curl_setopt( $ch,CURLOPT_POST, true );
            curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers);
            curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
            curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
            curl_setopt( $ch,CURLOPT_POSTFIELDS, $fields);
            $result = curl_exec($ch );  
            curl_close( $ch ); 
            return $result;
        } 

    public function send_multi_notification($fcm_token_list=[],$message='',$data,$device)
        {    
            $expiry     = time()+(60*60*24);
            $seconds    = 4500; 
            /*Android*/
            if ($device==1)  {  
                $fields='{
                        "registration_ids" : '.$fcm_token_list.', 
                        "data":'.json_encode($data).',
                        "apns":{
                            "headers":{
                                "apns-expiration":"'.$expiry.'"
                            }
                        },
                        "android":{
                            "ttl":"'.$seconds.'s"
                        },
                        "webpush":{
                            "headers":{
                                "TTL":"'.$seconds.'"
                            }
                        }

                    }';
            } 
            /*IOS*/
            if ($device==2)  {
                $fields='{
                        "registration_ids" : '.$fcm_token_list.',
                        "notification" :  {
                                "title" : "'.$message["title"].'" , 
                                "body"  : "'.$message["message"].'" 
                        }, 
                        "data":'.json_encode($data).',
                        "apns":{
                            "headers":{
                                "apns-expiration":"'.$expiry.'"
                            }
                        },
                        "android":{
                            "ttl":"'.$seconds.'s"
                        },
                        "webpush":{
                            "headers":{
                                "TTL":"'.$seconds.'"
                            }
                        }
                    }';
            
            }
            
            $headers = array(
                'Content-Type:application/json',
                'Authorization:key=AAAAh96qZZY:APA91bHfOsYicsGW0QBqRot-THpx_4Ciro1wXgUAFp1__y52nlUhHuv3LoH0NmrSco1iX_Pj0I5uutbPTxmtkekNLwZNnzj9yjI64GCE287Oo86LPgAD5B6nh8m2Kp_OK4UzDtjht4Wd'
            );
            $ch = curl_init();
            curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
            curl_setopt( $ch,CURLOPT_POST, true );
            curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers);
            curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
            curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
            curl_setopt( $ch,CURLOPT_POSTFIELDS, $fields);
            $result = curl_exec($ch );  
            curl_close( $ch ); 
            return $result;
        } 

    /*Getting Distance Between 2 Lat Long*/
    public function distance_between_lat_long($lat1, $lon1, $lat2, $lon2, $unit) {

        $theta = $lon1 - $lon2;
        $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
        $dist = acos($dist);
        $dist = rad2deg($dist);
        $miles = $dist * 60 * 1.1515;
        $unit = strtoupper($unit);

        if ($unit == "K") {
            return ($miles * 1.609344);/*In Kilometer*/
        } else if ($unit == "N") {
            return ($miles * 0.868976);/*In Notical Mile*/
        } else {
            return $miles; /*In Mile*/
        }
    }

    public function isJson($string) {
        json_decode($string);
        return (json_last_error() == JSON_ERROR_NONE);
    }   


    /* Unset Value From Multi Dimentional Value */
    public function remove_multi_element($array,$keys) { 
        foreach($array as $key=>$val){   
            foreach ($keys as $element) { 
                if (is_array($val)) { 
                    unset($val[$element]); 
                } 
                if (is_object($val)) { 
                    unset($val->$element); 
                } 
            }
        }
        return $array; 
    }

    public function remove_single_element($array,$keys) {   
        foreach ($keys as $element) {  
            if (is_array($array)) { 
                unset($array[$element]);  
            } 
            if (is_object($array)) { 
                unset($array->$element);  
            } 
            
        } 
        return $array; 
    }
	

    // used for api only
    public function check_for_permissions($passed_permission_id, $passed_user_permission_ids)
    {
        if(empty($passed_user_permission_ids)){
            $arr                    =   array();
            $array['success']       =   2;
            $array['message']       =   "No permissions found.";
            $array['data']          =   [];
            return response()->json($array, 200);
        }else{
            $user_permission_ids = explode(',', $passed_user_permission_ids); 
            if(!in_array($passed_permission_id, $user_permission_ids)){
                $arr                    =   array();
                $array['success']       =   2;
                $array['message']       =   "Permission denied.";
                $array['data']          =   [];
                return response()->json($array, 200);
                echo "failed";
                exit;
            }
        }
    }	

    public function check_for_access($permission_id, $user_permission_ids)
    {
        if(empty($user_permission_ids)){
            $arr                    =   array();
            $array['success']       =   2;
            $array['message']       =   "No permissions found.";
            $array['data']          =   [];
            return response()->json($array, 200);
        }else{
            // echo "<pre>";print_r($user_permission_ids);exit;
            if(!in_array($permission_id, $user_permission_ids)){
                $arr                    =   array();
                $array['success']       =   2;
                $array['message']       =   "Permission denied.";
                $array['data']          =   [];
                return response()->json($array, 200);
            }
        }
    }   
	 
}
