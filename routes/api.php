<?php

use Illuminate\Http\Request; 
use Illuminate\Support\Facades\Route; 

/*
|--------------------------------------------------------------------------
| API Routes
|-------------------------------------------------------------------------- 
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which 
| is assigned the "api" middleware group. Enjoy building your API!
|
*/  

$this->api_path = "App\Http\Controllers\api\app\\";  

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
}); 
 
/*API*/
/*Authentication*/
    Route::post('/app-send-otp'                     ,$this->api_path.'UserController@sendOtp');
    Route::post('/app-verify-otp'                   ,$this->api_path.'UserController@verifyOtp'); 
    Route::post('/app-signout'                      ,$this->api_path.'UserController@signOut');   
    Route::post('/app-user-update-profile'          ,$this->api_path.'UserController@updateProfile');   
    
/*Dashboard*/
    Route::post('/app-dashboard'                    ,$this->api_path.'DashboardController@index');
    Route::post('/app-custom-dashboard'             ,$this->api_path.'DashboardController@customDashboardList');
    Route::post('/app-help-support'                 ,$this->api_path.'DashboardController@help_support');   

/*CMS*/
    Route::post('/app-general'                       ,$this->api_path.'DashboardController@general');

    // Route::post('/app-banners'                      ,$this->api_path.'DashboardController@banners');   
    // Route::post('/app-upcoming-festival'            ,$this->api_path.'DashboardController@upcoming_festival');   
    // Route::post('/app-business-categories'          ,$this->api_path.'DashboardController@business_category');   
    // Route::post('/app-general-categories'           ,$this->api_path.'DashboardController@general_category');   

/*Category*/
    Route::post('/app-categories'                   ,$this->api_path.'CategoryController@categoryList');   
    Route::post('/app-categories-search'            ,$this->api_path.'CategoryController@categoryListSearch');   
    Route::post('/app-categories-custom'            ,$this->api_path.'CategoryController@categoryListCustom');   
/*Frames*/    
    Route::post('/app-frames'                       ,$this->api_path.'FrameController@frameList');   
    // Route::post('/app-save-category'                ,$this->api_path.'CategoryController@saveCategory');   
    // Route::post('/app-remove-category'              ,$this->api_path.'CategoryController@removeCategory');

/*BusinerssCategory*/
    Route::post('/app-business-category'            ,$this->api_path.'BusinessController@categoryList');   

/*Business*/
    Route::post('/app-user-business'                ,$this->api_path.'BusinessController@myBusiness');   
    Route::post('/app-save-business'                ,$this->api_path.'BusinessController@saveBusiness');   
    Route::post('/app-remove-business'              ,$this->api_path.'BusinessController@removeBusiness');   

/*Videos*/
    Route::post('/app-video-list'                   ,$this->api_path.'VideoController@videoList');   
    // Route::post('/app-video-save'                ,$this->api_path.'VideoController@save_track'); 

/*Post*/
    Route::post('/app-post-list'                   ,$this->api_path.'PostController@postList');   
    Route::post('/app-post-save'                   ,$this->api_path.'PostController@save_track');   
    // Route::post('/app-save-post'                   ,$this->api_path.'PostController@savePost');   
    // Route::post('/app-remove-post'                 ,$this->api_path.'PostController@removePost');

/*Custom*/
    Route::post('/app-custom-post-list'             ,$this->api_path.'CustomPostController@postList');   
    Route::post('/app-custom-frame-list'            ,$this->api_path.'FrameController@frameList');

/*Clint*/
    Route::post('/app-client-list'                  ,$this->api_path.'ClientController@clientList');   

/*Notification*/

    Route::post('/app-notification-list'            ,$this->api_path.'NotificationController@notificationList');   
/*Plan*/
    Route::post('/app-plan-list'                   ,$this->api_path.'PostController@postList');   
    // Route::post('/app-save-plan'                   ,$this->api_path.'PostController@savePost');   
    // Route::post('/app-remove-post'                 ,$this->api_path.'PostController@removePost');
     
