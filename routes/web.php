<?php

use Illuminate\Support\Facades\Route;

/* 
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These 
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/ 
Route::fallback(function () {
    return abort(404);
});  

    $this->export_path = "App\Http\Controllers\portal\aexport\\";
    $this->master_path = "App\Http\Controllers\portal\master\\";
 
    $this->portal_path = "App\Http\Controllers\portal\\";
    $this->front_path = "App\Http\Controllers\front\\";
    $this->cms_path = "App\Http\Controllers\cms\\"; 

/*Export Data*/

    Route::get('/export',$this->export_path.'ExportController@index'); 


/* 
|---------------------------------------------------------------------------
| CMS Poutes
|---------------------------------------------------------------------------
*/
    Route::get('/',$this->cms_path.'CmsController@blank_page'); 
    Route::get('/terms-conditions',$this->cms_path.'CmsController@terms'); //terms & conditions
    Route::get('/privacy-policy',$this->cms_path.'CmsController@privacy'); //privacy policy
/* 
|---------------------------------------------------------------------------
| Admin Panel Poutes
|---------------------------------------------------------------------------
*/

Route::group(['middleware' => 'myauth'], function () {
    $common_master =[ 
        ['get','-master','@dt_col'], 
        ['get','-dt','@index'],
        ['get','-list','@dt_list'],
        ['get','-add','@add'],
        ['get','-edit/{id}','@edit'],
        ['get','-view/{id}','@view'],
        ['post','-delete','@delete'],
        ['post','-save','@save'],
        ['post','-status','@status'],
    ];

    Route::post('/common-image-upload',$this->master_path.'ImageController@uploadImage'); //Single Image Upload
    // Route::post('/product-images-remove',$this->master_path.'ProductController@removeImages'); // product images delete
    Route::get('/user-business-list', $this->master_path.'UserController@user_dt_list');  
    Route::post('/user-update',$this->master_path.'UserController@update_user');
    Route::post('/get-category', $this->master_path.'CategoryController@get_cotegory_list');  
    Route::post('/frame-delete-icon',$this->master_path.'FrameController@removeIcon'); // product images delete
    Route::get('/frame-multiple/{id}',$this->master_path.'FrameController@multiple'); // product images delete

/*Report*/    
    Route::get('/report-master', $this->master_path.'ReportController@reports');  
    Route::get('/report-user', $this->master_path.'ReportController@index');  
    Route::get('/report-view/{id}', $this->master_path.'ReportController@view');    

    $masters = array();
    //$masters[] = ['role',$this->master_path.'RoleController'];
    $masters[] = ['user',$this->master_path.'UserController'];
    $masters[] = ['banner',$this->master_path.'BannerController'];
    $masters[] = ['client',$this->master_path.'ClientController'];
    $masters[] = ['dashboard',$this->master_path.'DashboardController'];
    $masters[] = ['custom-dashboard',$this->master_path.'CustomdashboardController'];
    $masters[] = ['category',$this->master_path.'CategoryController'];
    $masters[] = ['post',$this->master_path.'PostController'];
    $masters[] = ['custom-post',$this->master_path.'CustomPostController'];
    $masters[] = ['video',$this->master_path.'VideoController'];
    $masters[] = ['frame',$this->master_path.'FrameController'];
    $masters[] = ['business',$this->master_path.'BusinessController'];
    $masters[] = ['business-category',$this->master_path.'BusinessCategoryController'];

    foreach ($masters as $master){
        foreach ($common_master as $common){
            if($common[0] == 'get'){
                Route::get('/'.$master[0].$common[1], $master[1].$common[2]);
            }elseif($common[0] == 'get'){
                Route::post('/'.$master[0].$common[1], $master[1].$common[2]);
            }else{
                Route::any('/'.$master[0].$common[1], $master[1].$common[2]);
            }
        }
    }
    Route::any('/notifications',$this->portal_path.'PortalController@notifications');
    Route::post('/remove-noti',$this->portal_path.'PortalController@remove_notification');
    
//Portal
    Route::get('/portal', $this->portal_path.'PortalController@index');  
 
//Profile Master
    Route::get('/my-profile', $this->portal_path.'PortalController@profile');
    Route::post('/portal-update-profile',$this->portal_path.'PortalController@update_profile');
    Route::post('/portal-update-password',$this->portal_path.'PortalController@update_password');

//Settings
    Route::get('/portal-settings',$this->portal_path.'PortalController@settings');
    Route::post('/set-settings',$this->portal_path.'PortalController@setSettings');

//Push Notification & Message
    Route::get('/send-message-alert',$this->portal_path.'PortalController@sendMessageAlert');
    Route::post('/do-send-message-alert',$this->portal_path.'PortalController@doSendMessageAlert');
    Route::post('/get-link-category',$this->portal_path.'PortalController@linkCategory');

    Route::get('/test-notification',$this->portal_path.'PortalController@test_notification');
    Route::post('/do-test-notification',$this->portal_path.'PortalController@do_test_notification');
});

/*Portal Authentication*/
    Route::get('/portal-login',$this->portal_path.'PortalController@login');
    /*Route::get('/portal-registration',$this->portal_path.'PortalController@register');
    Route::get('/portal-reset-password',$this->portal_path.'PortalController@reset_passwoxrd');*/
    /*Route::get('/reset-your-password',$this->portal_path.'PortalController@user_reset_password');
    Route::post('/portal-do-change-token-password',$this->portal_path.'PortalController@user_reset_token_password');*/
    
    /*Route::get('/portal-verify-email',$this->portal_path.'PortalController@verify_email');
    Route::post('/portal-send-otp',$this->portal_path.'PortalController@send_otp');*/

    Route::post('/portal-do-login',$this->portal_path.'PortalController@do_login');
    /*Route::post('/portal-do-register',$this->portal_path.'PortalController@do_register');
    Route::post('/portal-do-reset',$this->portal_path.'PortalController@do_reset_password');*/
    Route::get('/portal-logout', $this->portal_path.'PortalController@logout');  

    Route::get('/app-ads.txt', function () {
        $content = view('add-text');
        return response($content, 200)
            ->header('content-Type', 'text');
    });