<?php if($message = Session::get('success')): ?> 
<div class="alert alert-success alert-dismissible show fade">
  <div class="alert-body">
    <button class="close" data-dismiss="alert">
      <span>×</span>
    </button>
    <?php echo e($message); ?>

  </div>
</div>
<?php endif; ?>


<?php if($message = Session::get('error')): ?>
<div class="alert alert-danger alert-dismissible show fade">
  <div class="alert-body">
    <button class="close" data-dismiss="alert">
      <span>×</span>
    </button>
    <?php echo e($message); ?>

  </div>
</div>
<?php endif; ?>


<?php if($message = Session::get('warning')): ?>
<div class="alert alert-warning alert-dismissible show fade">
  <div class="alert-body">
    <button class="close" data-dismiss="alert">
      <span>×</span>
    </button>
    <?php echo e($message); ?>

  </div>
</div>
<?php endif; ?>


<?php if($message = Session::get('info')): ?>
<div class="alert alert-info alert-dismissible show fade">
  <div class="alert-body">
    <button class="close" data-dismiss="alert">
      <span>×</span>
    </button>
    <?php echo e($message); ?>

  </div>
</div>
<?php endif; ?>


<?php if($errors->any()): ?> 
<div class="alert alert-danger alert-dismissible show fade">
  <div class="alert-body">
    <button class="close" data-dismiss="alert">
      <span>×</span>
    </button>
   Please check the form below for errors
  </div>
</div>
<?php endif; ?>

<script type="text/javascript">
	<?php if(Session::get('time')>0): ?>
    var time  = <?php echo e(Session::get('time')); ?>;
    setTimeout(function(){ $(".alert").addClass('d-none'); }, time*1000); 
  <?php endif; ?>
</script><?php /**PATH /var/www/festival-app/resources/views/messages/flash-message.blade.php ENDPATH**/ ?>