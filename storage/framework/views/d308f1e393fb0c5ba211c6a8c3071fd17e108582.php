<link rel="stylesheet" type="text/css" href="<?php echo e(asset('assets/indrop/inlancer_drop.css')); ?>">
<?php 
$parent_category_id = 0; 
$page_title = 'Add Category';
$route_name = 'category';
$mode = 'add';
$save_url = url($route_name.'-save');
$model_size = 'modal-lg'; 

$title = "Category";
if (\Session::has('category_type')){
    $category_type  = session('category_type');
}
if (\Session::has('category_id')){
    $parent_category_id  = session('category_id');
    $page_title = 'Add Sub Category';
    $title = "Sub Category";
}
?>
<style type="text/css"> 
label{
    cursor: pointer;
}
</style>

<div class="modal right fade <?php echo e($mode); ?>_<?php echo e($route_name); ?>_modal" id="<?php echo e($mode); ?>_<?php echo e($route_name); ?>_modal" tabindex="-1" role="dialog" style="display: none;" aria-hidden="true" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog <?php echo e($model_size); ?>" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title mb-1" id="myModalLabel"><?php echo e($page_title); ?></h5>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body" style="overflow-y:auto;">
                <form class="form add_form" method="post" id="add_form" action="<?php echo e($save_url); ?>" enctype="multipart/form-data">
                    <input type="hidden" name="mode" value="<?php echo e($mode); ?>">
                    <?php echo csrf_field(); ?>
                    <div id="message"></div>
                    <div class="form-body">
                        <div class="row">
                            <div class="col-md-12 col-lg-12 col-xl-12">
                                <div>
                                    <div class="card-body">
                                        <div id="accordion">
                                            <div class="accordion">
                                                <div class="mb-2 p-2" style="box-shadow: 0 2px 6px #acb5f6;background-color: #c60b008a;color: #fff;border-radius: 8px;">
                                                    <p>Illegal Drugs, Horrifying & Scary Elements, Nazi Symbols - swastika symbol 卐, Violence Towards Vulnerable or Defenseless Characters, Sexual Material, Offensive Language, Tobacco, Creatures Behave Like Humans (Aliens), Violence, Age-Restricted Physical Goods, Lottery</p>
                                                </div>
                                                <div class="accordion-header collapsed" role="button" data-toggle="collapse" data-target="#edit-panel-body-1" aria-expanded="true">
                                                    <h4>Step-1 : Category Basic Info</h4>
                                                </div>
                                                <div class="accordion-body collapse show" id="edit-panel-body-1" data-parent="#accordion" style="">
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="row">
                                                                <div class="col-md-12">
                                                                    <div class="form-group">
                                                                        <?php if(!empty($parent_category_id)){ ?>
                                                                        <label for="withdrawinput1">Category Name : <span class="text-danger">(Shown in App)</span> </label>
                                                                        <?php }else{ ?>
                                                                        <label for="withdrawinput1">Category Name : </label>
                                                                        <?php } ?>

                                                                        <input type="text" name="category_name" id="category_name" class="form-control">
                                                                        <input type="hidden" name="category_parent_id" value="<?php echo e($parent_category_id); ?>">
                                                                        <input type="hidden"  name="category_slug" id="category_slug" class="form-control" readonly>
                                                                        <input type="hidden" name="category_type" value="1">
                                                                    <label><span id='category_slug_span'></span></label>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!-- <div class="col-md-6">
                                                            <div class="row">
                                                                <div class="col-md-12">
                                                                    <div class="form-group">
                                                                        <label for="withdrawinput1">Category Type : </label>
                                                                        <select name="category_type" class="form-control select2">
                                                                            <option value="0" disabled>Select Type</option>
                                                                            <option value="1" selected>Post</option>
                                                                            <option value="2">Video</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div> -->
                                                        
                                                        <?php 
                                                        $css_class = '';
                                                        $css_class_child = 'd-none';
                                                        if($parent_category_id != 0){
                                                            $css_class = 'd-none';
                                                            $css_class_child = '';
                                                        } ?>
                                                        <div class="col-md-6">
                                                            
                                                            <div class="row <?php echo $css_class; ?>">
                                                                <div class="col-md-12"> 
                                                                    <div class="form-group">
                                                                        <label for="withdrawinput1"> Is Custom: </label>
                                                                         <input type="checkbox" name="is_custom" value="1" >
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="row <?php echo $css_class_child; ?>">
                                                                <div class="col-md-12">
                                                                    <div class="form-group">
                                                                        <label for="withdrawinput1">Category Date:<span class="text-danger">(Shown in App)</span> </label>
                                                                        <input type="text" name="category_date" value="<?php echo e(date('d-m-Y')); ?>" class="form-control">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="row d-none">
                                                                <div class="col-md-12"> 
                                                                    <div class="form-group">
                                                                        <label for="withdrawinput1">Category Image Alt Tag: </label>
                                                                        <input type="text" name="iati1" class="form-control">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        
                                                        <div class="col-md-6">
                                                            <div class="row">  
                                                                <div class="col-md-12">
                                                                    <div class="form-group">
                                                                        <label for="withdrawinput1">Search Keyword</label>
                                                                        <textarea class="form-control" name="category_search_keyword"></textarea>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <!-- category_image -->
                                                            <div id="i1" class="drop-area" data-throwback="storePerformance" onclick="document.getElementById('fileElem1').click()" style="margin-top: 30px;"> 
                                                                <div class="text-center" id="imgPreviewi1">
                                                                  <i class="bi bi-cloud-arrow-up mb-3" style="color:#445dbe;font-size: 60px;"></i>
                                                                  <h3 style="color: #445dbe;font-size: 18px;line-height: 60px;">Click "Here" or drop your Image here</h3>
                                                                </div>
                                                                <input class="d-none" type="file" id="fileElem1" accept="" onchange="handleFiles(this.files,'i1','storePerformance')">   
                                                            </div>
                                                            <progress id="progress-bar" class="d-none" max=100 value=0></progress>
                                                            <input type="hidden" id="category_image" name="category_image" data-image="i1">
                                                            <!-- category_image -->
                                                        </div>
                                                        <div class="col-md-12">
                                                            <div class="row">  
                                                                <div class="col-md-12">
                                                                    <div class="form-group">
                                                                        <label for="withdrawinput1">Category Description:</label>
                                                                        <textarea class="form-control" name="category_description"></textarea>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                        <br>
                                        <button type="button" class="btn btn-outline-danger waves-effect waves-light float-left" data-dismiss="modal" tabindex="99">Close</button>
                                        <button type="submit" class="btn btn-success waves-effect waves-light float-right"><i class="fa fa-spinner fa-spin d-none" tabindex="20"></i> Save</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>


<script src="<?php echo e(asset('js/jquery.form.js')); ?>"></script>
<script src="<?php echo e(asset('assets/indrop/inlancer_drop.js')); ?>"></script>
<script type="text/javascript">
/*Start NEW*/
$('#category_name').bind('keyup keypress blur', function() 
{  
    var myStr = $(this).val();
    myStr=myStr.toLowerCase();
    myStr=myStr.replace(/[^a-zA-Z0-9]/g,'-').replace(/\s+/g, "-");
    $('#category_slug_span').text(myStr);
    $('#category_slug').val(myStr); 
    $('#category_slug').val(); 
});
$('#category_slug').bind('keyup keypress blur', function() 
{  
    var myStr = $(this).val();
    myStr=myStr.toLowerCase();
    myStr=myStr.replace(/[^a-zA-Z0-9]/g,'-').replace(/\s+/g, "-");
    $('#category_slug_span').text(myStr);
    $('#category_slug').val(myStr); 
    $('#category_slug').val(); 
});
function storePerformance(file_id,returnData) {    
    $("[data-image='"+file_id+"']").val(returnData.image_id);  
}
/*function storePerformance2(file_id,returnData) {    
    $("[data-image='"+file_id+"']").val(returnData.image_id);  
}*/
jQuery(document).ready(function() { 
    var dd = {
        beforeSend: function() { 
            $('.fa-spinner').removeClass('d-none');
        },
        uploadProgress: function(event, position, total, percentComplete) { 
        },
        success: function() {},
        complete: function(response) {
            var result = jQuery.parseJSON(response.responseText);
            $('.fa-spinner').removeClass('d-none');
            $('.fa-spinner').addClass('d-none');
            if (result.status == 200) {
                Swal.fire({
                    type: 'success',
                    title: result.message,
                    showConfirmButton: false,
                    timer: 1500
                });
                jQuery('.modal').modal('hide');
                jQuery('#add_form').trigger("reset");
                jQuery(".select2").val('').trigger('change');
                <?php echo e($route_name); ?>_tbl._fnAjaxUpdate();
                window.location.reload();
            } else {
                Swal.fire({
                    type: 'warning',
                    title: 'Oops',
                    text: result.message,
                    showConfirmButton: false,
                    timer: 2000
                });
            }
        },
        error: function() { 
        }
    };
    jQuery("#add_form").ajaxForm(dd);
});

/*END NEW*/

</script><?php /**PATH /var/www/festival-app/resources/views/portal/master/category/add_modal.blade.php ENDPATH**/ ?>