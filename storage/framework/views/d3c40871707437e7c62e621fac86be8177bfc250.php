<?php 
$page_title = 'Add Business Category';
$route_name = 'business_category';
$mode = 'add';
$save_url = url('business-category-save');
$model_size = 'modal-md';

$title = "Business Category";

?>
<style type="text/css">
label{
    cursor: pointer;
}
</style> 

<div class="modal right fade <?php echo e($mode); ?>_<?php echo e($route_name); ?>_modal" id="<?php echo e($mode); ?>_<?php echo e($route_name); ?>_modal" tabindex="-1" role="dialog" style="display: none;" aria-hidden="true" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog <?php echo e($model_size); ?>" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title mb-1" id="myModalLabel"><?php echo e($page_title); ?></h5>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body" style="overflow-y:auto;">
                <form class="form add_form" method="post" id="add_form" action="<?php echo e($save_url); ?>" enctype="multipart/form-data">
                    <input type="hidden" name="mode" value="<?php echo e($mode); ?>">
                    <?php echo csrf_field(); ?>
                    <div id="message"></div>
                    <div class="form-body">
                        <div class="row">
                            <div class="col-md-12 col-lg-12 col-xl-12">
                                <div>
                                    <div class="card-body">
                                        <div id="accordion">
                                            <div class="accordion">
                                                <div class="accordion-header collapsed" role="button" data-toggle="collapse" data-target="#edit-panel-body-1" aria-expanded="true">
                                                    <h4>Step-1 : Business Category Details</h4>
                                                </div>
                                                <div class="accordion-body collapse show" id="edit-panel-body-1" data-parent="#accordion" style="">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="row">
                                                                <div class="col-md-12">
                                                                    <div class="form-group">
                                                                        <label for="withdrawinput1">Category Name : </label>
                                                                        <input type="text" name="business_category_name" id="business_category_name" class="form-control">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        
                                                        
                                                        
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                        <br>
                                        <button type="button" class="btn btn-outline-danger waves-effect waves-light float-left" data-dismiss="modal" tabindex="99">Close</button>
                                        <button type="submit" class="btn btn-success waves-effect waves-light float-right"><i class="fa fa-spinner fa-spin d-none" tabindex="20"></i> Save</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>


<script src="<?php echo e(asset('js/jquery.form.js')); ?>"></script>

<script type="text/javascript">
jQuery(document).ready(function() { 
    var dd = {
        beforeSend: function() { 
            $('.fa-spinner').removeClass('d-none');
        },
        uploadProgress: function(event, position, total, percentComplete) { 
        },
        success: function() {},
        complete: function(response) {
            var result = jQuery.parseJSON(response.responseText);
            $('.fa-spinner').removeClass('d-none');
            $('.fa-spinner').addClass('d-none');
            if (result.status == 200) {
                Swal.fire({
                    type: 'success',
                    title: result.message,
                    showConfirmButton: false,
                    timer: 1500
                });
                jQuery('.modal').modal('hide');
                jQuery('#add_form').trigger("reset");
                jQuery(".select2").val('').trigger('change');
                <?php echo e($route_name); ?>_tbl._fnAjaxUpdate();
                window.location.reload();
            } else {
                Swal.fire({
                    type: 'warning',
                    title: 'Oops',
                    text: result.message,
                    showConfirmButton: false,
                    timer: 2000
                });
            }
        },
        error: function() { 
        }
    };
    jQuery("#add_form").ajaxForm(dd);
});

/*END NEW*/

</script><?php /**PATH /var/www/festival-app/resources/views/portal/master/business-category/add_modal.blade.php ENDPATH**/ ?>