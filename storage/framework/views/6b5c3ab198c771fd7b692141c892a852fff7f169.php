 <link rel="stylesheet" type="text/css" href="<?php echo e(asset('assets/indrop/inlancer_drop.css')); ?>">
<?php 
$page_title = 'Edit Category';
$route_name = 'category';
$mode = 'edit';
$save_url = url($route_name.'-save');
$model_size = 'modal-lg';
$title = 'Category';
if (!empty($sub_category)){
    $page_title = 'Edit Sub Category';
    $title = 'Sub Category';
}
?> 
<style type="text/css">
    textarea{
        height: unset;
    }
</style>
<div class="modal right fade <?php echo e($mode); ?>_<?php echo e($route_name); ?>_modal" id="<?php echo e($mode); ?>_<?php echo e($route_name); ?>_modal" tabindex="-1" role="dialog" style="display: none;" aria-hidden="true" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog <?php echo e($model_size); ?>" role="document">
        <div class="modal-content" style="overflow-y: auto;">
            <div class="modal-header">
                <h5 class="modal-title mb-3" id="myModalLabel"><?php echo e($page_title); ?></h5>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <form class="form edit_form" method="post" id="edit_form" action="<?php echo e($save_url); ?>" enctype="multipart/form-data">
                    <input type="hidden" name="mode" value="<?php echo e($mode); ?>">
                    <input type="hidden" name="id" value="<?php echo e($category_id); ?>">
                    <?php echo csrf_field(); ?>
                    <div id="message"></div>
                    <div class="form-body">
                        <div class="row">
                            <div class="col-md-12 col-lg-12 col-xl-12">
                                <div>
                                    <div class="card-body">
                                        <div id="accordion">
                                            <div class="accordion">
                                                <div class="mb-2 p-2" style="box-shadow: 0 2px 6px #acb5f6;background-color: #c60b008a;color: #fff;border-radius: 8px;">
                                                    <p>Illegal Drugs, Horrifying & Scary Elements, Nazi Symbols - swastika symbol 卐, Violence Towards Vulnerable or Defenseless Characters, Sexual Material, Offensive Language, Tobacco, Creatures Behave Like Humans (Aliens), Violence, Age-Restricted Physical Goods, Lottery</p>
                                                </div>
                                                <div class="accordion-header collapsed" role="button" data-toggle="collapse" data-target="#edit-panel-body-1" aria-expanded="true">
                                                    <h4>Step-1 : Category Details</h4>
                                                </div>
                                                <div class="accordion-body collapse show" id="edit-panel-body-1" data-parent="#accordion" style="">
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="row">
                                                                <div class="col-md-12">
                                                                    <div class="form-group">
                                                                        
                                                                        <?php if(!empty($category_parent_id)){ ?>
                                                                        <label for="withdrawinput1">Category Name : <span class="text-danger">(Shown in App)</span> </label>
                                                                        <?php }else{ ?>
                                                                        <label for="withdrawinput1">Category Name : </label>
                                                                        <?php } ?>

                                                                        <input type="text" name="category_name" id="category_name" value="<?php echo e($category_name); ?>" class="form-control">
                                                                        <input type="hidden" name="category_parent_id" value="<?php echo e($category_parent_id); ?>">
                                                                        <input type="hidden"  name="category_slug" id="category_slug" class="form-control" readonly value="<?php echo e($category_slug); ?>">
                                                                        <input type="hidden" name="category_type" value="1">
                                                                    <label><span id='category_slug_span'><?php echo e($category_slug); ?></span></label>
                                                                    </div>

                                                                </div>
                                                            </div>
                                                        </div>
                                                        <?php 
                                                        $css_class = '';
                                                        $css_class_child = 'd-none';
                                                        if($category_parent_id != 0){
                                                            $css_class = 'd-none';
                                                            $css_class_child = '';
                                                        }
                                                        if(!empty($category_date) || $category_date != null ){
                                                           $datevalue = $category_date;
                                                        }else{
                                                           $datevalue = date('d-m-Y');     
                                                        } ?>
                                                        <div class="col-md-6">
                                                            <div class="row <?php echo $css_class; ?>">
                                                                <div class="col-md-12"> 
                                                                    <div class="form-group">
                                                                        <label for="withdrawinput1"> Is Custom: </label>
                                                                        <?php if(!empty($is_custom) && $is_custom==1){ ?>
                                                                            <input type="checkbox" checked name="is_custom" value="1" >
                                                                        <?php }else{ ?>
                                                                            <input type="checkbox"  name="is_custom" value="1" >
                                                                        <?php } ?>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="row <?php echo $css_class_child; ?>">
                                                                <div class="col-md-12">
                                                                    <div class="form-group">
                                                                        <label for="withdrawinput1">Category Date:<span class="text-danger">(Shown in App)</span> </label>
                                                                        <input type="text" name="category_date" value="<?php echo e($datevalue); ?>" class="form-control">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="row d-none">
                                                                <div class="col-md-12"> 
                                                                    <div class="form-group">
                                                                        <label for="withdrawinput1">Category Image Alt Tag: </label>
                                                                        <input type="text" name="iati1" class="form-control">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                       
                                                        <div class="col-md-6">
                                                            <div class="row">  
                                                                <div class="col-md-12">
                                                                    <div class="form-group">
                                                                        <label for="withdrawinput1">Search Keyword</label>
                                                                        <textarea class="form-control" name="category_search_keyword"><?php echo e($category_search_keyword); ?></textarea>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <!-- category_image -->
                                                            <div id="i1" class="drop-area" data-throwback="storePerformance" onclick="document.getElementById('fileElem1').click()" style="margin-top: 30px;"> 
                                                                <div class="text-center" id="imgPreviewi1">
                                                                    <i class="bi bi-cloud-arrow-up mb-3" style="color:#445dbe;font-size: 60px;"></i>
                                                                    <h3 style="color: #445dbe;font-size: 18px;line-height: 50px;">Click "Here" or drop your files here</h3>
                                                                </div>
                                                                <input class="d-none" type="file" id="fileElem1" accept="" onchange="handleFiles(this.files,'i1','storePerformance')">   
                                                            </div>
                                                            <progress id="progress-bar" class="d-none" max=100 value=0></progress>
                                                            <input type="hidden" id="category_image"  name="category_image" data-image="i1" value="<?php echo e($category_image); ?>">
                                                        </div>
                                                        <div class="col-md-12">
                                                            <div class="row">  
                                                                <div class="col-md-12">
                                                                    <div class="form-group">
                                                                        <label for="withdrawinput1">Category Description: </label>
                                                                        <textarea class="form-control" name="category_description"><?php echo e($category_description); ?></textarea>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        
                                                        
                                                        
                                                    </div>
                                                </div>
 

                                            </div>
                                        </div>
                                        <br>
                                        <button type="button" class="btn btn-outline-danger waves-effect waves-light float-left" data-dismiss="modal" tabindex="99">Close</button>
                                        <button type="submit" class="btn btn-success waves-effect waves-light float-right"><i class="fa fa-spinner fa-spin d-none" tabindex="20"></i> Save</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
$('#category_name').bind('keyup keypress blur', function() 
{  
    var myStr = $(this).val()
    myStr=myStr.toLowerCase();
    myStr=myStr.replace(/[^a-zA-Z0-9]/g,'-').replace(/\s+/g, "-");
    $('#category_slug_span').text(myStr);
    $('#category_slug').val(myStr); 
    $('#category_slug').val(); 
});
$('#category_slug').bind('keyup keypress blur', function() 
{  
    var myStr = $(this).val()
    myStr=myStr.toLowerCase();
    myStr=myStr.replace(/[^a-zA-Z0-9]/g,'-').replace(/\s+/g, "-");
    $('#category_slug_span').text(myStr);
    $('#category_slug').val(myStr); 
    $('#category_slug').val(); 
});
/*image Show */
  var image = '<?php echo e($image_name); ?>';
  var file_id = 'i1';  
  if (typeof image!=='undefined' && image!=='') {
    var isrc = APPLICATION_URL+'/assets/upload/images/thumb/'+image;
    $('#imgPreview'+file_id).empty();   
    var img = '<img onerror="setImage(this);"  class="previewImage" src="'+isrc+'">';  
    var div = '<div class="col-12"><div style="padding: 5px;margin-bottom: 8px;">'+img+'</div></div>';
    $('#imgPreview'+file_id).append(div);    
  }
function setImage(img){
console.log(img);
    img.src="<?php echo e(url('/assets/img/placeholder.png')); ?>";  
    img.style.height="80px";
}
function storePerformance(file_id,returnData) {    
    $("[data-image='"+file_id+"']").val(returnData.image_id);  
}  
/*End */
jQuery(document).ready(function() { 
    var dd = {
        beforeSend: function() { 
            $('.fa-spinner').removeClass('d-none');
        },
        uploadProgress: function(event, position, total, percentComplete) { 
        },
        success: function() {},
        complete: function(response) {
            console.log(response.responseText);
            var result = jQuery.parseJSON(response.responseText);
            $('.fa-spinner').removeClass('d-none');
            $('.fa-spinner').addClass('d-none');
            if (result.status == 200) {
                Swal.fire({
                    type: 'success',
                    title: 'Successfully saved',
                    showConfirmButton: false,
                    timer: 1500
                });  
                jQuery('.modal').modal('hide');
                <?php echo e($route_name); ?>_tbl._fnAjaxUpdate();
                window.location.reload();
            } else {
                Swal.fire({
                    type: 'warning',
                    title: 'Oops',
                    text: result.message,
                    showConfirmButton: false,
                    timer: 2000
                });
            }
        },
        error: function() { 
        }
    }; 
    jQuery("#edit_form").ajaxForm(dd); 
});

</script><?php /**PATH /var/www/festival-app/resources/views/portal/master/category/edit_modal.blade.php ENDPATH**/ ?>