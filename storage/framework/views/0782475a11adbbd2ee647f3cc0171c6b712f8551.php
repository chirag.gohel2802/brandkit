<div class="card">
    <div class="card-header text-center">
        <h4>Filters</h4>
        <div class="card-header-action">
            <a class="btn btn-outline-danger" href="<?php if(!empty($return_url)){ echo $return_url;} ?>">Clear all filters</a>
            <a data-collapse="#mycard-collapse" class="btn btn-info" href="#">Show</a>
        </div>
    </div>
    <div class="collapse" id="mycard-collapse" style="">
            <div class="card-body">
                <div class="row mt-3">
                    <div class="col-md-3 col-12 font-weight-bold" style=" display: flex; align-items: center; ">
                        Select Sub Category :
                    </div>
                    <div class="col-md-5 col-12 ml-md-4">
                        <select class="form-control select2"  id="parent_category_id" >
                            <option selected disabled>Select </option>
                            <?php 
                            if(!empty($sub_list)){
                                foreach ($sub_list as $key => $value) { ?>
                                
                                <option 
                                <?php if(!empty($value) && $value->category_id == $parent_category_id ){ echo 'selected'; } ?> 
                                    value="<?php echo $value->category_id ?>"
                                >
                                    <?php echo $value->category_name ?>
                                </option>
                            <?php 
                                }
                            }
                             ?>
                        </select>
                    </div>
                </div>

                <div class="row mt-3">
                    <div class="col-12 text-center">
                        <button 
                            class="btn btn-outline-primary" 
                            type="button" 
                            onclick="apply_filter()"
                        ><i class="fa fa-eye fa-lg"> </i> Apply
                        </button>
                    </div>
                </div>
            </div>
    </div>
</div>
<script type="text/javascript">
function apply_filter(){
    parent_category_id = $('#parent_category_id option').filter(':selected').val();
    url = "<?php echo e($return_url); ?>?&parent_category_id="+parent_category_id;
    window.open(url,'_self');
}
</script><?php /**PATH /var/www/festival-app/resources/views/portal/master/video/filter.blade.php ENDPATH**/ ?>