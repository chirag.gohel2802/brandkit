<div class="content">
    <section class="single-product-area single-product section-padding">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="product-details-tab">
                        <!--Speacification Content-->
                        <div class="tab-content product-details-specification">
                            <div class="tab-pane fade in active">
                                <h2 class="mt-0">Our Mission</h2>
                                <p>Our mission is to bring everyone the inspiration to create a business as their choice. To do that, we show you personalized content and ads we think you’ll be interested in based on information we collect for you. We only use that information where we have a proper legal basis for doing so.</p>
                                <p>We wrote this policy to help you to understand what information we collect, how we use it and what choices you have about it. Because we’re an internet company, some of the concepts below are a little technical, but we’ve tried our best to explain things in a simple and clear way. We welcome your questions and comments on this policy.</p>
                                <p>Versatile Techno built the BrandKit application as commercial application to provides user with various option and ideas to expand their business with divergent way of connection in the market.</p>
                                <h2>Definition: </h2>
                                <p><strong>We, Us, Our:</strong> Individually and collectively refer to each entity being part of the platform.(BrandKit)</p>
                                <p><strong>You, You’re, Yourself:</strong> Refer to the users. This Privacy Policy is a contract between you and the respective BrandKit entity whose Website & Web Application You use or access or you otherwise deal with. This Privacy Policy shall be read together with the respective terms and condition of the BrandKit entity and its respective Website or nature of business of the Website.</p>
                                <p><strong>Platform:</strong> The word platform means the said services and business providing by BrandKit. Hereinafter referred as platform.</p>
                                <p><strong>User:</strong> A person who registered their name under said platform.</p>
                                <h2>When you give it to us or give us permission to obtain it</h2>
                                <p>When you sign up for or use the said platform, you voluntarily share certain information including your name, email address, phone number, OTP, comments, and any other information you give us. The information that we request will be retained by us and used as described in this privacy policy.</p>
                                <p>The said application does use third services that may collect information used to identify you.</p>
                                <h2>We also get technical information when you use our platform</h2>
                                <p>When you use a website, mobile application or other internet service, certain internet and electronic network activity information gets created and logged automatically. Here are some of the types of information we collect:</p>
                                <p><strong>Log data: </strong>When you use Platform, our servers record information (“log data”), including information that your browser automatically sends whenever you visit a website, or that your mobile app automatically sends when you’re using it. This log data includes your Internet Protocol address (which we use to infer your approximate location), the address of and activity on websites you visit that incorporate the platform features, searches, browser type and settings, the date and time of your request, how you used Platform, cookie data and device dataand statistics.</p>
                                <p><strong>Cookies:</strong> Cookies are files with a small amount of data that are commonly used as anonymous unique identifiers. These are sent to your browser from the websites that you visit and are stored on your device’s internal memory. Some of the cookies we use are associated with your platform account (including information about you, such as the email address you gave us) and other cookies are not.</p>
                                <p>This service does not use these “cookies” explicitly. However, the application may use third party code and libraries that use “cookies” to collect information and improve their services. You have the option to either accept or refuse these cookies and know when a cookie is being sent to your device. If you choose to refuse our cookies, you may not be able to use some portion of this service.</p>
                                <p><strong>Device information:</strong> In addition to log data, we collect information about the device you’re using the platform on, including the type of device, operating system, settings, unique device identifiers and crash data that helps us understand when something breaks.</p>
                                <p><strong>Clickstream data and inferences: </strong>When you’re on Platform, we use your activity—such as which sector you click on, boards you create, and any text that you add in a comment or description—along with information you provided when you first signed up and information from our partners and advertisers to make inferences about you and your preferences. For example, if you click profession, we may infer you are engaged in the same profession. We may also infer information about your business or professional experience based on your activity. By these features, user may easily get the new designs, ideas and quotes relating to their activities.</p>
                                <p><strong>Business Transfer:</strong> We may share your Personal Information or any other information collected and stored by the Service with our parent, subsidiaries and affiliates for internal reasons in connection with the provision of the Services to You. We also reserve the right to disclose and transfer all such information:</p>
                                <p>To a subsequent owner, co-owner or operator of the Services or applicable database.</p>
                                <h2>What we do with the info we collect</h2>
                                <p>We have a legitimate interest for using your info in these ways. It's fundamental to what we do at platform and necessary in order to make Platform and its features relevant and personalized to you.</p>
                                <p>We also have a legitimate interest in making Platform safe and improving our product features, so you keep finding the inspiration you want. We give all benefit when we are using your information to:</p>
                                <p>Suggest other people who have similar interests. For example, if you follow any activities relating to advocacy, we may suggest activities relating to advocacy that you might like.</p>
                                <p>We have a legitimate interest in using information we collect to customize your Platform experience based on your offsite behaviour. For example, if you visit websites that you using for marketing purpose, we may suggest you to other propaganda relating to market by way of various pictures, quotes. When we identify your interests based on your offsite behaviour with cookies, we will obtain any consent that we may need.</p>In addition to the specific circumstances above, we’ll only use your information with your consent in order to:
                                <ul class="mt-10">
                                    <li>Send you marketing materials by email, text, push notification or phone call depending on your account or operating system settings. Each time we send you marketing materials, we give you the option to unsubscribe.</li>
                                </ul>
                                <h2>Service Providers</h2>
                                <p>We may employ third-party companies and individuals due to the following reasons:</p>
                                <ul class="mt-10">
                                    <li>To facilitate our Service;</li>
                                    <li>To provide the Service on our behalf;</li>
                                    <li>To perform Service-related services; or</li>
                                    <li>To assist us in analyzing how our Service is used.</li>
                                </ul>
                                <p>We want to inform users of this Service that these third parties have access to your Personal Information. The reason is to perform the tasks assigned to them on our behalf. However, they are obligated not to disclose or use the information for any other purpose</p>
                                <h2>Security</h2>
                                <p>We value your trust in providing us your Personal Information, thus we are striving to use commercially acceptable means of protecting it. But remember that no method of transmission over the internet, or method of electronic storage is 100% secure and reliable, and we cannot guarantee its absolute security.</p>
                                <h2>Links to Other Sites</h2>
                                <p>This Service may contain links to other sites. If you click on a third-party link, you will be directed to that site. Note that these external sites are not operated by us. Therefore, we strongly advise you to review the Privacy Policy of these websites. We have no control over and assume no responsibility for the content, privacy policies, or practices of any third-party sites or services.</p>
                                <h2>Children’s Privacy</h2>
                                <p>These Services do not address anyone under the age of 13. We do not knowingly collect personally identifiable information from children under 13. In the case we discover that a child under 13 has provided us with personal information, we immediately delete this from our servers. If you are a parent or guardian and you are aware that your child has provided us with personal information, please contact us so that we will be able to do necessary actions.</p>
                                <h2>Governing Law and Dispute Resolution</h2>
                                <p>This Policy shall be governed by and construed in accordance with the laws of India. The courts at Ahmedabad, India shall have exclusive jurisdiction in relation to any disputes arising out of or in connection with this Policy.</p>
                                <h2>Intellectual Property Rights</h2>
                                <p>The logo and name of the platform has been registered under the Trademark Act, 1999 and also have proprietary right over copyright under copyright Act, 1957 for the said website and web application. Therefore, any kind of stealing data and copying the name or content from the said platform amounting to infringement and platform may reserve a right to take action against infringer.</p>
                                <h2>Refund & cancellation policy</h2>
                                <p>If at any time you are not satisfied with the quality of services of our application, you may raise a ticket in our application's customer help and support with a reason for dissatisfaction or call customer care number of BrandKit. If your issue is found to be genuine, your subscription may be considered for a claim of credit, replacement or refund. Otherwise, your complaint will be noted and considered for the respective resolution. Furthermore, you hereby confirm and agree that you will not claim or allege anything against BrandKit concerning the digital content provided to you by the app.</p>
                                <h2>No Waiver</h2>
                                <p>The rights and remedies available under this Policy may be exercised as often as necessary and are cumulative and not exclusive of rights or remedies provided by law. It may be waived only in writing. Delay in exercising or non-exercise of any such right or remedy does not constitute a waiver of that right or remedy, or any other right or remedy.</p>
                                <h2>Contact Us</h2>
                                <p>If we change our privacy policies and procedures, we will post those changes on our website to keep you aware of what information we collect, how we use it and under what circumstances we may disclose it. Changes to this Privacy Policy are effective when they are posted on this page.</p>
                                <p>If you have any questions or suggestions about our Privacy Policy, do not hesitate to contact us at 
                                    <a target="_blank" href="mailto:wearebrandkit@gmail.com"><span>wearebrandkit@gmail.com</span></a>
                                    <!-- <a href="wearebrandkit@gmail.com"><span class="__cf_email__" data-cfemail="wearebrandkit@gmail.com">[email&#160;protected]</span></a> -->
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
    <?php /**PATH /var/www/festival-app/resources/views/cms/privacy.blade.php ENDPATH**/ ?>