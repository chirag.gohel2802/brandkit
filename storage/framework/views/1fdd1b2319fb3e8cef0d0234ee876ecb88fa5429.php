 
<?php $__env->startSection('content'); ?>  
<style type="text/css">
th{
  text-transform: capitalize;
}
</style>
<link rel="stylesheet" type="text/css" href="<?php echo e(asset('festival_inlancer/modules/datatables/datatables.min.css')); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo e(asset('festival_inlancer/modules/datatables/DataTables-1.10.16/css/dataTables.bootstrap4.min.css')); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo e(asset('festival_inlancer/modules/datatables/Select-1.2.4/css/select.bootstrap4.min.css')); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo e(asset('festival_inlancer/vendors/dataTable/datatables.min.css')); ?>">  
<style type="text/css">
  table.dataTable.dtr-inline.collapsed>tbody>tr[role="row"]>td:first-child:before, table.dataTable.dtr-inline.collapsed>tbody>tr[role="row"]>th:first-child:before {
      top: 12px;
      left: 4px;
      height: 14px;
      width: 14px;
      display: block;
      position: absolute;
      color: white;
      border: none;
      border-radius: 14px;
      box-shadow: 0 0 3px #444;
      box-sizing: content-box;
      text-align: center;
      text-indent: 0 !important;
      font-family: 'Courier New', Courier, monospace;
      line-height: 14px;
      content: '+';
      background-color: #262b71;
  }
  .dt-bootstrap4 .row{
    margin: auto 1.5rem !important;
  }
</style>
<div class="main-content">
    <section class="section">
        <div class="section-header">
            <div class="row" style="width:100%">
              <div class="col-md-6 col-12"> 
                  <div class="section-header-breadcrumb ml-4 pl-4">
                      <div class="breadcrumb-item"><a href="<?php echo url('portal');?>">Dashboard</a></div> 
                      <div class="breadcrumb-item"><?php echo $grid['grid_name'];?></div>
                  </div>
                  <h1 class="section-title"><?php echo $grid['grid_title'];?></h1>
              </div>
              <div class="col-md-6 col-12 text-right">
                  <?php if (isset($grid['grid_add_button']) && $grid['grid_add_button']==false){
                  }else { if (isset($grid['grid_add_url']) && $grid['grid_add_url']!='') { ?> 
                      <a href="<?php echo $grid['grid_add_url']?>" class="btn btn-outline-primary btn-lg " href="<?php echo $grid['grid_add_url']?>">
                          <?php echo $grid['grid_add_button_name']; ?>
                      </a>
                  <?php } else {  ?>
                      <a href="javascript:;" type="button" class="btn add_<?php echo $grid['grid_tbl_name']; ?> btn-outline-primary btn-lg " data-toggle="modal" data-target=".add_<?php echo $grid['grid_tbl_name']; ?>_modal">
                          <?php echo $grid['grid_add_button_name']; ?>
                      </a>
                  <?php } }  ?>
              </div>
            </div>
        </div>


        <!-- for filter  -->
        <?php if(!empty($filter_file)): ?>
          <?php echo $__env->make($filter_file, \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
        <?php endif; ?>
        <!-- for filter  -->
        
        <div class="section-body"> 
            <div class="card"> 
                <div class="card-body px-0 pb-2">
                    <form  name="<?php echo $grid['grid_tbl_name']; ?>_form" id="<?php echo $grid['grid_tbl_name']; ?>_form" method="post"> 
                        <div class="table-responsive1">
                            <table id="<?php echo $grid['grid_tbl_name']; ?>_tbl" class="table table-striped table-loading" width="100%">
                                <thead>
                                    <tr>
                                        <?php $i=0; foreach ($grid['grid_columns'] as $key => $value) {  ?>
                                          <th 
                                          <?php echo $value['width'] ?> <?php echo $value['style'] ?> <?php echo $value['class'] ?> 
                                          <?php ++$i; if($i == 2){echo 'data-priority="1"';}   ?>
                                          > 
                                            <?php echo $value['name'];?> 
                                          </th> 
                                        <?php  } ?>
                                    </tr>
                                </thead>
                                
                            </table>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
</div>

<div class="appends"></div> 


<?php if(isset($extra_pages) && !empty($extra_pages)): ?> 
  <?php $__currentLoopData = $extra_pages; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
    <?php echo $__env->make($value, \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?> 
  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> 
<?php endif; ?> 


<script type="text/javascript">
  jQuery("input:text:visible:first").focus();
  
  function js_status(id,status) { 
      jQuery.ajax({
            type: "POST",
            data: {'_token':'<?php echo e(csrf_token()); ?>', id:id, status:status},
            url: "<?php echo $grid['grid_status_url']?>",
            cache: false,
            success: function(response) {
              console.log(response);
              var json_data = response;       
              if (json_data.status == 200) { 
                Swal.fire({
                  title: "Status Changed!",
                  type: "success",
                  timer: 800  });
                  <?php echo $grid['grid_tbl_name']; ?>_tbl._fnAjaxUpdate(); 
              }else{
                  Swal.fire("Oops","Try Again.","error")};
              }
          }); 
  }

  function js_edit(id) { 
      jQuery.ajax({
      type: "GET", 
      url: '<?php echo $grid['grid_data_url']?>/'+id,
      success: function(response) 
          {   
            jQuery('.appends').html(response);
            jQuery('#edit_<?php echo $grid['grid_tbl_name']; ?>_modal').modal('show');
            ! function(e) {
                "use strict";
                  var t = function() {};
                  t.prototype.init = function() {
                      e(".select2").select2({
                          width: "100%"
                      }) 
                  }, e.AdvancedForm = new t, e.AdvancedForm.Constructor = t
                }(window.jQuery),
                function(t) {
                    "use strict";
                    window.jQuery.AdvancedForm.init()
                }();
          },
                 
      });  
  }
  function js_delete(id){
    var form_data={_token:'<?php echo e(csrf_token()); ?>',id:id}; 
    Swal.fire({
      title: "Confirm",
      text: "Are you sure you want to delete this data!",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#1FAB45",
      confirmButtonText: "Yes, Delete it.",
      cancelButtonText: "Cancel",
      buttonsStyling: true
    }).then((result) => {
        if (result.value == true) {
          jQuery.ajax({
            headers: {
                'X-CSRF-TOKEN': '<?php echo e(csrf_token()); ?>'
            },
            type: "POST",
            data: form_data,
            url: "<?php echo $grid['grid_delete_url']?>",
            cache: false,
            success: function(response) {
                var json_data = response;       
              if (json_data.status=="200") { 
                Swal.fire({
                title: "Success!",
                text: "Data deleted successfully!",
                type: "success",
                timer: 800});
                <?php echo $grid['grid_tbl_name']; ?>_tbl._fnAjaxUpdate(); 
              }else{
                  Swal.fire(
                  "Internal Error",
                  "Oops,Error Occurred.",
                  "error"
                  )};
            }
          });
        } else{
            Swal.fire({
            title: "Cancelled",
            text: "Your data is safe Now! ",
            type:"error",
            timer:800
            })  ;
        }
    }, 
    function (dismiss) {
      if (dismiss === "cancel") {
        Swal.fire(
        "Internal Error",
        "Oops, Some Error Occurred.",
        "error"
        );
      }
    })
  }

jQuery( "#add_<?php echo $grid['grid_tbl_name']; ?>_up_modal" ).on('shown', function(){ 
  jQuery('.datepicker').css('z-index', 500);
});


jQuery(document).ready(function() {  
    var dd = { 
        beforeSend: function() 
        { 
        
        },
        uploadProgress: function(event, position, total, percentComplete) 
        {
          
        },
        success: function() 
        {
        },
        complete: function(response) 
        {
          console.log(response.responseText);
          var result = jQuery.parseJSON(response.responseText)  ;
          if (result.status == 200) 
          {
            Swal.fire({
              type: 'success',
              title: 'Successfully saved',
              showConfirmButton: false,
              timer: 1500
            });
            <?php echo $grid['grid_tbl_name']; ?>_tbl._fnAjaxUpdate();
             
            jQuery('#<?php echo $grid['grid_tbl_name']; ?>').trigger("reset"); 
            
          }else{
            Swal.fire({
              type: 'warning',
              title: 'Oops',
              text: result.message,
              showConfirmButton: false,
              timer: 2000
            });
          } 
        },
        error: function()
        {

        } 
    }; 

    jQuery("#<?php echo $grid['grid_tbl_name']; ?>").ajaxForm(dd);
});   

var is_first_time_load = true;
jQuery(document).ready(function(){ 
  <?php echo $grid['grid_tbl_name']; ?>_tbl = jQuery('#<?php echo $grid['grid_tbl_name']; ?>_tbl').dataTable({
                      "oLanguage": {
                          "sSearch": "Search",
                          "sLengthMenu": "Show _MENU_ enteries",
                          "sInfo":  " Showing  _START_  to  _END_  of  _TOTAL_  entries ", 
                      },
                      "language": {
                        "paginate": {
                          "previous": "Previous",
                          "next": "Next",
                        },
                       "emptyTable": "No data available in table"
                      },
                      "processing": true,
                      "fixedHeader": true,
                      "serverSide": true, 
                      "bAutoWidth": false, 
                      "responsive": true,
                      /*"scrollY":300,   */
                      "iDisplayLength": <?php echo $grid['grid_tbl_length']; ?>,
                      "ajaxSource": "<?php echo $grid['grid_dt_url']?>",
                      "aoColumns": [<?php foreach($grid['grid_columns'] as $key=>$value) { if($value['sortable']=='true'){ echo "{ 'bSortable' : true}," ;}  else {echo "{ 'bSortable' : false}," ;} }?>                     
                      ],
                      "order":[['<?php echo e($grid["grid_order_by"]); ?>','<?php echo e($grid["grid_order_by_type"]); ?>']],
                      "sDom": "<'row'<'col-sm-9 col-xs-9'l><'col-sm-3 col-xs-3'f>r>t<'row'<'col-sm-5 hidden-xs paging-class'i><'col-sm-7 col-xs-12 clearfix'p>>",
                      
                      'fnDrawCallback' : function(){
                        jQuery('th:first-child').removeClass('sorting_desc');
                        jQuery('th:first-child').removeClass('sorting');
                        if(is_first_time_load){
                          jQuery('#<?php echo $grid['grid_tbl_name']; ?>_tbl_length select').addClass("form-control");
                          jQuery('#<?php echo $grid['grid_tbl_name']; ?>_tbl_filter input').addClass("form-control"); 
                          is_first_time_load = false;
                        } 
                      },
  });  


  jQuery('#<?php echo $grid['grid_tbl_name']; ?>_tbl').delay(100).css("width","100%");  
});

jQuery(window).resize(function(){ 
  // jQuery('#<?php echo $grid['grid_tbl_name']; ?>_tbl').dataTable().fnAdjustColumnSizing();
});

   
  jQuery(document).ready(function() {
  jQuery('#<?php echo e($grid["grid_tbl_name"]); ?>_tbl').on('init.dt',function() {
        jQuery("#<?php echo e($grid["grid_tbl_name"]); ?>_tbl").removeClass('table-loading').show();
      });
  setTimeout(function(){
    jQuery('#<?php echo e($grid["grid_tbl_name"]); ?>_tbl').dataTable();
  }, 3000);
    
});
</script>

<script src="<?php echo e(asset('festival_inlancer/js/jquery.form.js')); ?>"></script>  
<script src="<?php echo e(asset('festival_inlancer/vendors/dataTable/datatables.min.js')); ?>"></script>
 
<?php $__env->stopSection(); ?>
<?php echo $__env->make('portal.template.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/festival-app/resources/views/portal/master/master.blade.php ENDPATH**/ ?>