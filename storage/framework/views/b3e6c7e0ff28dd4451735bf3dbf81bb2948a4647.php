<link rel="stylesheet" type="text/css" href="<?php echo e(asset('assets/indrop/inlancer_drop.css')); ?>">
<script src="<?php echo e(asset('festival_inlancer/vendors/select2/js/select2.min.js')); ?>"></script> 
<link rel="stylesheet" type="text/css" href="<?php echo e(asset('festival_inlancer/vendors/select2/css/select2.min.css')); ?>">

<?php 
$page_title = 'Edit Dashboard';
$route_name = 'dashboard';
$mode = 'edit';
$save_url = url($route_name.'-save');
$model_size = 'modal-lg';
$title = 'Dashboard';
 
?>
<style type="text/css">
    textarea{ 
        height: unset;
    }
</style>
<div class="modal right fade <?php echo e($mode); ?>_<?php echo e($route_name); ?>_modal" id="<?php echo e($mode); ?>_<?php echo e($route_name); ?>_modal" tabindex="-1" role="dialog" style="display: none;" aria-hidden="true" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog <?php echo e($model_size); ?>" role="document">
        <div class="modal-content" style="overflow-y: auto;">
            <div class="modal-header">
                <h5 class="modal-title mb-3" id="myModalLabel"><?php echo e($page_title); ?></h5>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <form class="form edit_form" method="post" id="edit_form" action="<?php echo e($save_url); ?>" enctype="multipart/form-data">
                    <input type="hidden" name="mode" value="<?php echo e($mode); ?>">
                    <input type="hidden" name="id" value="<?php echo e($dashboard_id); ?>">
                    <?php echo csrf_field(); ?>
                    <div id="message"></div>
                    <div class="form-body">
                        <div class="row">
                            <div class="col-md-12 col-lg-12 col-xl-12">
                                <div>
                                    <div class="card-body">
                                        <div id="accordion">
                                            <div class="accordion">
                                                <div class="accordion-header collapsed" role="button" data-toggle="collapse" data-target="#edit-panel-body-1" aria-expanded="true">
                                                    <h4>Step-1 : Dashboards Details</h4>
                                                </div>
                                                <div class="accordion-body collapse show" id="edit-panel-body-1" data-parent="#accordion" style="">
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="row">
                                                                <div class="col-md-12">
                                                                    <div class="form-group">
                                                                        <label for="withdrawinput1">Dashboard Name: <span class="text-danger">(Shown in App)</span></label>
                                                                        <input type="text" name="dashboard_name" value="<?php echo e($dashboard_name); ?>" class="form-control">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="row">
                                                                <div class="col-md-12">
                                                                    <div class="form-group">
                                                                        <label for="withdrawinput1">Dashboard Postion: </label>
                                                                        <input type="number" name="dashboard_position" value="<?php echo e($dashboard_position); ?>" class="form-control" placeholder="Ex. 10 ">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="row">
                                                                <div class="col-md-12">
                                                                    <div class="form-group">
                                                                        <label for="withdrawinput1">Main Category: </label>
                                                                        <input type="hidden" name="dashboard_parent_category" 
                                                                             id="dashboard_parent_category">
                                                                        <select id="parent_category" class="form-control select2" onchange="getSubCategory(this);">
                                                                            <option  disabled>Select Category</option>
                                                                        <?php if($dashboard_parent_category==0){ ?>
                                                                            <option selected value="all">All Categories</option>
                                                                        <?php }else{ ?>
                                                                            <option value="all">All Categories</option>
                                                                        <?php } ?>
                                                                        <?php 
                                                                        if(!empty($category_list)){ foreach ($category_list as $key => $value) {
                                                                            $selected = '';
                                                                            if($value->category_id == $dashboard_parent_category){
                                                                                $selected = 'selected';
                                                                            } ?>
                                                                            <option 
                                                                                data-type="<?php echo e($value->category_type); ?>" value="<?php echo e($value->category_id); ?>" <?php echo e($selected); ?>> <?php echo e($value->category_name); ?> - <?php if($value->category_type==1): ?> Post <?php else: ?> Video <?php endif; ?>      
                                                                            </option>
                                                                        <?php } } ?>
                                                                            
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="row">
                                                                <div class="col-md-12">
                                                                    <div class="form-group">
                                                                        <label for="withdrawinput1">Sub Category: (Main category name > sub category name)</label>
                                                                        <select id="sub_category" 
                                                                            name="sub_category[]" 
                                                                            class="form-control form-control-sm select2" 
                                                                            multiple 
                                                                            data-value="<?php echo $dashboard_value; ?>"
                                                                        >
                                                                            <option selected disabled>Select Sub Category</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12 d-none">
                                                            <div class="row">
                                                                <div class="col-md-12">
                                                                    <div class="form-group">
                                                                        <label for="withdrawinput1">Dashboard Link: </label>
                                                                        <input type="text" name="dashboard_url" value="" class="form-control">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        
                                                    </div>
                                                </div>
 

                                            </div>
                                        </div>
                                        <br>
                                        <button type="button" class="btn btn-outline-danger waves-effect waves-light float-left" data-dismiss="modal" tabindex="99">Close</button>
                                        <button type="submit" class="btn btn-success waves-effect waves-light float-right"><i class="fa fa-spinner fa-spin d-none" tabindex="20"></i> Save</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
function getSubCategory(parent){
    $('#dashboard_parent_category').val(parent.value);
    var type = parent.options[parent.selectedIndex].dataset.type;
    if (parent.value=='all') {
        var form_data={_token:'<?php echo e(csrf_token()); ?>',parent_id:'',type:''}; 
    }else{
        var form_data={_token:'<?php echo e(csrf_token()); ?>',parent_id:parent.value,type:type}; 
    }

    jQuery.ajax({
        type: "POST",
        data: form_data,
        url: '<?php echo e(url("/get-category")); ?>',
        cache: false,
        success: function(response) {  
          $('#sub_category').empty();
          $('#sub_category').append(response);
        }
    });
}
jQuery(document).ready(function() { 
    
    var parent  = $("#parent_category").val();
    $('#dashboard_parent_category').val(parent);

    var type    = $("#parent_category option:selected").attr('data-type');
    var selected    = $("#sub_category").attr('data-value');

    form_data = {_token:'<?php echo e(csrf_token()); ?>',parent_id:parent,type:type,selected:selected};
      jQuery.ajax({
        type: "POST",
        data: form_data,
        url: '<?php echo e(url("/get-category")); ?>',
        cache: false,
        success: function(response) {  
          $('#sub_category').empty();
          $('#sub_category').append(response);
        }
    });


    jQuery('#sub_category').select2();
    var dd = {
        beforeSend: function() { 
            $('.fa-spinner').removeClass('d-none');
        },
        uploadProgress: function(event, position, total, percentComplete) { 
        },
        success: function() {},
        complete: function(response) {
            console.log(response.responseText);
            var result = jQuery.parseJSON(response.responseText);
            $('.fa-spinner').removeClass('d-none');
            $('.fa-spinner').addClass('d-none');
            if (result.status == 200) {
                Swal.fire({
                    type: 'success',
                    title: 'Successfully saved',
                    showConfirmButton: false,
                    timer: 1500
                });  
                jQuery('.modal').modal('hide');
                <?php echo e($route_name); ?>_tbl._fnAjaxUpdate();
                window.location.reload();
            } else {
                Swal.fire({
                    type: 'warning',
                    title: 'Oops',
                    text: result.message,
                    showConfirmButton: false,
                    timer: 2000
                });
            }
        },
        error: function() { 
        }
    }; 
    jQuery("#edit_form").ajaxForm(dd); 
});

</script><?php /**PATH /var/www/festival-app/resources/views/portal/master/dashboard/edit_modal.blade.php ENDPATH**/ ?>