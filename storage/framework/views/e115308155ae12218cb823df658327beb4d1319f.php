  
<?php $__env->startSection('content'); ?> 

    <div class="main-content">
        <section class="section">
            <div class="section-header">
                <h1>Test Notification </h1>
                <div class="section-header-breadcrumb">
                    <div class="breadcrumb-item active"><a href="<?php echo e(url('/portal')); ?>">Dashboard</a></div>
                    <div class="breadcrumb-item">Test Notification</div>
                </div>
            </div>


            <div class="section-body"> 
                <div class="row mt-sm-4"> 
                    <div class="col-12">
                        <div class="card">
                            
                        </div>
                        <div class="card"> 
                            <form id="sendAlert" method="post" action="<?php echo e(url('do-test-notification')); ?>" class="needs-validation" novalidate="">
                                <?php echo csrf_field(); ?> 
                                <div class="card-body">
                                    <div class="row">
                                        <div class="form-group col-md-6 col-12">
                                            <label>Title</label>
                                            <input type="text" class="form-control" name="message_title" value="" required="required" >
                                            <div class="invalid-feedback">
                                                Please fill in the Title
                                            </div>
                                        </div>
                                        
                                        <div class="form-group col-md-6 col-12">
                                            <label>Message</label>
                                            <textarea  required="required" class="form-control" name="message_body"></textarea>
                                            <div class="invalid-feedback">
                                                Please fill in the details
                                            </div>
                                        </div>
                                        <div class="form-group col-md-6 col-12">
                                            <label>Type</label>
                                            <select name="message_type" id="message_type" class="form-control " onchange="getCategory(this);">
                                                <option selected="selected" disabled>Select Type</option>
                                                <option value="category" >Category</option>
                                                <option value="post" >Post</option>
                                            </select>
                                            <div class="invalid-feedback">
                                                Please Select the Type
                                            </div>
                                        </div>
                                        <div class="form-group col-md-6 col-12">
                                            <label>Select Category For Link</label>
                                            <select id="categorySelection" class="form-control select2" onchange="getLink(this);">
                                                <option selected="selected" disabled>Select Category</option>
                                            </select>
                                        </div>
                                        <div class="form-group col-md-6 col-12">
                                            <label>Link:</label>
                                            <input type="text" readonly="readonly" class="form-control" id="message_link" name="message_link"  required="required" >
                                        </div>
                                        <div class="form-group col-md-6 col-12">
                                            <label>Select Single User ( Single fcm )</label>
                                            <select class="form-control select2" name="single_fcm">
                                                <option value="" disabled selected>Select User</option>
                                                <?php if(!empty($users)): ?>
                                                    <?php $__currentLoopData = $users; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                        <?php if(!empty($val->user_fcm_token)): ?>
                                                            <option value="<?php echo $val->user_fcm_token; ?>"><?php echo $val->user_phone_no ?></option>
                                                        <?php endif; ?>
                                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                <?php endif; ?>
                                            </select>
                                        </div>
                                       
                                    </div><hr>
                                </div>
                                        
                                <div class="card-footer text-right">
                                    <button style="font-size: 15px !important;" type="submit" class="btn btn-success waves-effect waves-light"><i class="fa-pulse fa fa-spinner d-none"></i>Send Alert</button>
                                </div>
                            </form> 
                        </div>
                    </div>
                    
                </div>
            </div>
        </section>
    </div>

<input type="hidden" value="" id="type_id">
<input type="hidden" value="" id="parent_category_id">
<input type="hidden" value="" id="post_category_id">
<input type="hidden" value="" id="category_type">

<script src="<?php echo e(asset('js/jquery.form.js')); ?>"></script>
<script type="text/javascript">
    /*Get Category & post Category*/
    function getCategory(pass)
    {
        var form_data={_token:'<?php echo e(csrf_token()); ?>',type:pass.value}; 
        jQuery.ajax({
            type: "POST",
            data: form_data,
            url: '<?php echo e(url("/get-link-category")); ?>',
            cache: false,
            success: function(response) {  
               $('#categorySelection').empty();
               $('#categorySelection').append(response);
            }
        });

        var first = ''; 
        if(pass.value=='post'){
            first = 'app-post-list/post_category=';//post_category=32&page=1
        }else{
            first = 'app-categories/category_parent_id=';//category_parent_id=1&category_type=1
        } 
        $('#type_id').val(first);
        $('#message_link').val('');
        $('#parent_category_id').val('');
        $('#post_category_id').val('');
        changeLink();
    }
    function getLink(pass)
    {
        var id = pass.value;
        var type  = $("#categorySelection").select2().find(":selected").data("type");
        var parentid  = $("#categorySelection").select2().find(":selected").data("parentid");

        if(parentid==0){
            $('#parent_category_id').val(id+'&category_type='+type);
        }else{
            $('#post_category_id').val(id+'&page=1');
        }
        changeLink();
        
    }
    function changeLink()
    {
        var type_id = $('#type_id').val();
        var parent_category_id = $('#parent_category_id').val();
        var post_category_id = $('#post_category_id').val();
        if(parent_category_id != ""){
            $('#message_link').val(type_id+parent_category_id);
        }
        if(post_category_id != ""){
            $('#message_link').val(type_id+post_category_id);
        }
    }        


    function storePerformance(file_id,returnData) {    
        $("[data-image='"+file_id+"']").val(returnData.image_id);  
    }    
    function selectAll(type){
        if(type==1) { 
            $(':checkbox').each(function() {
                this.checked = true;                         
            });
            $('#select').addClass('d-none');
            $('#unselect').removeClass('d-none');

        }else {
            $(':checkbox').each(function() {
                this.checked = false;                       
            }); 
            $('#unselect').addClass('d-none');
            $('#select').removeClass('d-none');
        }
    }
    jQuery(document).ready(function() { 
        var type  = $("#message_type").val();
        console.log(type);
        if(type=''){
            Swal.fire({
                type: 'warning',
                title: 'Oops',
                text: "Please Select Type",
                showConfirmButton: false,
                timer: 2000
            });
        }

        var dd = {
            beforeSend: function() { 
                var r = confirm('Are you sure ! Think again read again!');
                if (r == true) {
                } else {
                    return false;
                }
                $('.fa-spinner').removeClass('d-none');
            },
            uploadProgress: function(event, position, total, percentComplete) { 
            },
            success: function() {},
            complete: function(response) {
                console.log(response.responseText);
                var result = jQuery.parseJSON(response.responseText);
                $('.fa-spinner').removeClass('d-none');
                $('.fa-spinner').addClass('d-none');
                if (result.status == 200) {
                    Swal.fire({
                        type: 'success',
                        title: 'Successfully saved',
                        showConfirmButton: false,
                        timer: 1500
                    });  
                    setTimeout(function(){ location.reload(); }, 1500);
                } else {
                    Swal.fire({
                        type: 'warning',
                        title: 'Oops',
                        text: result.message,
                        showConfirmButton: false,
                        timer: 2000
                    });
                }
            },
            error: function() { 
            }
        }; 
        jQuery("#sendAlert").ajaxForm(dd);   
    });
</script>

<script type="text/javascript">
function apply_filter(){
    user_type = $('#user_type option').filter(':selected').val();
    user_status = $('#user_status option').filter(':selected').val();
    url = "<?php echo e($return_url); ?>?user_type="+user_type+"&user_status="+user_status;
    window.open(url,'_self');
}
</script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('portal.template.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/festival-app/resources/views/portal/test_notification.blade.php ENDPATH**/ ?>