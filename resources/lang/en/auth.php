<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'These credentials do not match our records.',
    'password' => 'The provided password is incorrect.',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',

    /*Phone No*/
        'phone_required'                =>  'Phone No is required',
    /*OPT*/
        'otp_required'                  =>  'OTP is required',    
        'otp_success'                   =>  'OTP sent successful',    
        'otp_verified'                  =>  'OTP Verified successful',    
        'otp_faild'                     =>  'OTP Verification Failed',    

    /*Email*/ 
        'email_required'                =>  'Email address is required',
        'valid_email_address'           =>  'Please fill valid email address', 
        'email_already_registered'      =>  'This email address is already registered with us',

    /*Terms Check*/
        'accept_terms'      =>  'Please accept terms and condition using checkbox before proceeding',

    /*Registration*/
        'registration_failed_try_later'     =>  'Registration Failed, Please try again after some time',
        'registration_done_verify_email'    =>  'Registration success! Please verify your email address',
        'registration_mail_failed'          =>  'We couldn\'t send verification mail to you! Please contact help support',

    /*Login*/
        'account_in_review'     =>  'Your account is in review', 
        'account_is_blocked'    =>  'Your account has been blocked!, Kindly contact help support',
        'login_success'         =>  'You have been logged in to your account',
        'login_failed'          =>  'Sorry ! Failed to logged in to your account',
        'otp_valid'             =>  'Sorry ! Please enter valid OTP',
        'enter_valid_password'  =>  'Please enter valid password',
        'not_registered'        =>  'You are not registered with us, Please signup to access the premium',


    /*Access*/
        'invalid_access'    => 'Invalid Credentials',
        'logout_success'    => 'Successfully logged out from this device',

    /*Dashboard*/    
        'dashboard'         => 'Dashboard details fetched',
        'help_fetched'      => 'Help & Support details fetched',
    /*User Profile*/
        'profile_sent'      => 'Profile details fetched',
        'profile_updated'   => 'Your profile has been updated',
        'profile_no_change' => 'No changes found', 
        'setting_sent'      => 'User setting fetched', 
        'setting_updated'   => 'User setting updated', 


    /*Update | Change Password*/ 
        'invalid_current_password'  => 'Please enter valid current password', 
        'password_changed'          => 'Your password has been changed', 
        'password_unchanged'        => 'Password remains same as current password',

    /*Forgot Password*/ 
        'reset_password_link_sent'      => 'We have sent reset password link to your email address',   
        'reset_password_link_failed'    =>  'Failed to send mail, please try after sometime', 

    



];
