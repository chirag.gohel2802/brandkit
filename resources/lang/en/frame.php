<?php
return [
    /*
    |--------------------------------------------------------------------------
    | Language Lines for 
    |--------------------------------------------------------------------------
    | Category List api
    	add. edit. delete. view  
    */ 
/*Business*/    
        'frame_saved'            =>  'Frame has been saved successfully',
        'frame_updated'          =>  'Frame has been updated successfully',
        'frame_fetched'          =>  'Frames list has been fetched',
        'frame_saving_failed'    =>  'Frame saving failed', 
        'frame_detail_fetched'   =>  'Frame details fetched',
        'frame_delete'           =>  'Data Deleted Successfully',

    /*Business Logo Required*/
        'frame_image_required'    => 'Frmae image is compulsary required'
/*Category*/

];