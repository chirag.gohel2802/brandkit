<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Language Lines for 
    |--------------------------------------------------------------------------
    | Business List api
    	add. edit. delete. view  
    */ 


/*Business*/    
        'business_saved'            =>  'Business has been saved successfully',
        'business_updated'          =>  'Business has been updated successfully',
        'business_fetched'          =>  'Business list has been fetched',
        'business_category_fetched' =>  'Business Category list has been fetched',
        'business_saving_failed'    =>  'Business saving failed', 
        'business_detail_fetched'   =>  'Business details fetched',
        'business_delete'           =>  'Data Deleted Successfully',

    /*Business Logo Required*/
        'business_logo_required'    => 'Business Logo is compulsary required'
/*Category*/

];