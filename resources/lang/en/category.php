<?php
return [
    /*
    |--------------------------------------------------------------------------
    | Language Lines for 
    |--------------------------------------------------------------------------
    | Category List api
    	add. edit. delete. view  
    */ 
/*Business*/    
        'category_saved'            =>  'Category has been saved successfully',
        'category_updated'          =>  'Category has been updated successfully',
        'category_fetched'          =>  'Category list has been fetched',
        'category_saving_failed'    =>  'Category saving failed', 
        'category_detail_fetched'   =>  'Category details fetched',
        'category_delete'           =>  'Data Deleted Successfully',

    /*Business Logo Required*/
        'category_image_required'    => 'Category Logo is compulsary required'
/*Category*/

];