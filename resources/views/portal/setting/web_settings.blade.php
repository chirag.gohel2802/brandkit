@extends('portal.template.app')  
@section('content')
<link rel="stylesheet" type="text/css" href="{{asset('assets/indrop/inlancer_drop.css')}}">
<script src="{{ asset('festival_inlancer/vendors/select2/js/select2.min.js')}}"></script> 
<link rel="stylesheet" type="text/css" href="{{asset('festival_inlancer/vendors/select2/css/select2.min.css')}}"> 

 <div class="main-content">
    <section class="section">
         
        <div class="row">
            <div class="col-md-12"> 
                <div class="card"> 
                    <div class="card-header text-center">
                        <h4 >{{__('Settings')}}</h4>  
                    </div>
                    @include('messages.flash-message')
                    @php
                        $lang = session()->get('applocale');
                    @endphp
                    <form class="form web-setting-form" method="post" id="web-setting-form" action="{{url('set-settings')}}" enctype="multipart/form-data">
                        @csrf
                        <div id="message"></div> 
                        <div class="form-body">
                            <div class="row">
                                <div class="col-md-12 col-lg-12 col-xl-12">
                                    <div class="card-body">
                                        <div id="accordion">
                                            <div class="accordion">
                                                <!-- <div class="accordion-header collapsed" role="button" data-toggle="collapse" data-target="#product-panel-body-3" aria-expanded="true">
                                                    <h4>Dashboard Settings</h4>
                                                </div>
                                                <div class="accordion-body collapse show" id="product-panel-body-3" data-parent="#accordion" style="">
                                                  <div class="row">
                                                      <div class="col-md-12">
                                                          <div class="row">
                                                              <div class="col-md-6">
                                                                  <div class="form-group">
                                                                     <label for="withdrawinput1">Upcoming Festival </label>
                                                                     <select class="form-control form-control-sm select2" name="upcoming_festival[]" multiple id="upcoming_festival">
                                                                  <php $already_upcoming = explode(',', $upcoming_festival['setting_value']);

                                                                  if(!empty($postList)){
                                                                  foreach($postList as $key => $post){
                                                                      if(in_array($post->post_id,$already_upcoming)){ ?>
                                                                      <option value="<php echo $post->post_id ?>" selected="selected">
                                                                          <php echo $post->post_name; ?>
                                                                      </option>
                                                                         <php }else{ ?>
                                                                      <option value="<php echo $post->post_id ?>">
                                                                          <php echo $post->post_name; ?>
                                                                      </option>  
                                                                  <php } } } ?>
                                                                      </select>
                                                                  </div>
                                                              </div>
                                                              <div class="col-md-6">
                                                                  <div class="form-group">
                                                                     <label for="withdrawinput1">Business Category</label>
                                                                     <select class="form-control form-control-sm select2" name="business_category[]" multiple id="business_category">
                                                                  <php $already_business = explode(',', $business_category['setting_value']);

                                                                  if(!empty($postList)){
                                                                  foreach($postList as $key => $post){
                                                                      if(in_array($post->post_id,$already_business)){ ?>
                                                                      <option value="<php echo $post->post_id ?>" selected="selected">
                                                                          <php echo $post->post_name; ?>
                                                                      </option>
                                                                         ?php }else{ ?>
                                                                      <option value="<php echo $post->post_id ?>">
                                                                          <php echo $post->post_name; ?>
                                                                      </option>  
                                                                  <php } } } ?>
                                                                      </select>
                                                                  </div>
                                                              </div>
                                                             
                                                              <div class="col-md-6">
                                                                  <div class="form-group">
                                                                      <label for="withdrawinput1">Just Launched Products</label>
                                                                     <select class="form-control form-control-sm select2"
                                                                      name="launched_products[]" multiple id="launched_products">
                                                                  <php $already_top = explode(',', $launched_products['setting_value']);
                                                                  if(!empty($productList)){
                                                                  foreach($productList as $key => $product){
                                                                      if(in_array($product->product_id,$already_top)){ ?>
                                                                      <option value="<php echo $product->product_id ?>" selected="selected">
                                                                          <php echo $product->product_title; ?>
                                                                      </option>
                                                                         <php }else{ ?>
                                                                      <option value="<php echo $product->product_id ?>">
                                                                          <php echo $product->product_title; ?>
                                                                      </option>  
                                                                  <php } } } ?>    
                                                                      </select>
                                                                  </div>
                                                              </div>
                                                              <div class="col-md-5">
                                                                  <div class="form-group">    
                                                                      <div class="custom-file">
                                                                          <input type="file" name="product_brochure" class="custom-file-input" id="product_brochure"
                                                                          title="Products Brocher" value="{{$product_brochure['setting_value']}}"
                                                                          accept="application/pdf">
                                                                          <label class="custom-file-label">Products Brocher Choose File</label>
                                                                      </div>
                                                                      <div class="form-text text-muted">Only .pdf files allowed</div>
                                                                  </div>
                                                              </div>
                                                              <div class="col-md-1">
                                                                  @if(!empty($product_brochure['setting_value']))
                                                                  <a href="{{url('assets/upload/brochure/'.$product_brochure['setting_value'])}}" target="_blank" class="btn btn-outline-info" >
                                                                      <i class="far fa-file-pdf" style="font-size: 30px;"></i> 
                                                                  </a>
                                                                  @endif
                                                              </div>
                                                              <div class="col-md-5">
                                                                  <div class="form-group">    
                                                                      <div class="custom-file">
                                                                          <input type="file" name="general_brochure" class="custom-file-input" id="general_brochure"
                                                                          title="General Brocher" value="{{$general_brochure['setting_value']}}" 
                                                                          accept="application/pdf">
                                                                          <label class="custom-file-label">General  Brocher Choose File</label>
                                                                      </div>
                                                                      <div class="form-text text-muted">Only .pdf files allowed</div>
                                                                  </div>
                                                              </div>
                                                              <div class="col-md-1">
                                                                  @if(!empty($general_brochure['setting_value']))
                                                                  <a href="{{url('assets/upload/brochure/'.$general_brochure['setting_value'])}}" target="_blank" class="btn btn-outline-info" >
                                                                      <i class="far fa-file-pdf" style="font-size: 30px;"></i> 
                                                                  </a>
                                                                  @endif
                                                              </div>
                                                              <div class="col-md-12">
                                                                  <div class="form-group">
                                                                      <label for="withdrawinput1">Analytics Area</label>
                                                                      <textarea name="analytics" id="analytics" class="form-control" > 
                                                                          {{$analytics['setting_value']}}
                                                                      </textarea>             
                                                                  </div>
                                                              </div>
                                                          </div>
                                                      </div>
                                                  </div>
                                                </div> -->
                                                <!-- <div class="accordion-header collapsed" role="button" data-toggle="collapse" data-target="#home-panel-body-1" aria-expanded="false">
                                                    <h4>Logo Settings</h4>
                                                </div>
                                                <div class="accordion-body collapse " id="home-panel-body-1" data-parent="#accordion" style="">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="row">
                                                                <div class="col-md-4">
                                                                    <label for="withdrawinput1">Sticky Logo </label>
                                                                    <div id="i1" class="drop-area" data-throwback="storePerformance" onclick="document.getElementById('fileElem1').click()"> 
                                                                        <div class="text-center" id="imgPreviewi1">
                                                                          <i class="bi bi-cloud-arrow-up mb-3" style="color:#445dbe;font-size: 60px;"></i>
                                                                          <h3 style="color: #445dbe;font-size: 18px;line-height: 80px;">Drop your Sticky Logo here</h3>
                                                                        </div>
                                                                        <input class="d-none" type="file" id="fileElem1" accept="" onchange="handleFiles(this.files,'i1','storePerformance')">   
                                                                    </div>
                                                                    <progress id="progress-bar" class="d-none" max=100 value=0></progress>
                                                                    <input type="hidden" id="sticky_logo" name="sticky_logo" data-image="i1" value="{{$sticky_logo['setting_value']}}">
                                                                </div>
                                                                <div class="col-md-4">
                                                                    <label for="withdrawinput1">Header Logo </label>
                                                                    <div id="i2" class="drop-area" data-throwback="storePerformance" onclick="document.getElementById('fileElem2').click()"> 
                                                                        <div class="text-center" id="imgPreviewi2">
                                                                          <i class="bi bi-cloud-arrow-up mb-3" style="color:#445dbe;font-size: 60px;"></i>
                                                                          <h3 style="color: #445dbe;font-size: 18px;line-height: 80px;">Drop your Header Logo here</h3>
                                                                        </div>
                                                                        <input class="d-none" type="file" id="fileElem2" accept="" onchange="handleFiles(this.files,'i2','storePerformance')">   
                                                                    </div>
                                                                    <progress id="progress-bar" class="d-none" max=100 value=0></progress>
                                                                    <input type="hidden" id="header_logo" name="header_logo" data-image="i2" value="{{$header_logo['setting_value']}}">
                                                                </div>
                                                                <div class="col-md-4">
                                                                    <label for="withdrawinput1">Footer Logo </label>
                                                                    <div id="i3" class="drop-area" data-throwback="storePerformance" onclick="document.getElementById('fileElem3').click()"> 
                                                                        <div class="text-center" id="imgPreviewi3">
                                                                          <i class="bi bi-cloud-arrow-up mb-3" style="color:#445dbe;font-size: 60px;"></i>
                                                                          <h3 style="color: #445dbe;font-size: 18px;line-height: 80px;">Drop your Footer Logo here</h3>
                                                                        </div>
                                                                        <input class="d-none" type="file" id="fileElem3" accept="" onchange="handleFiles(this.files,'i3','storePerformance')">   
                                                                    </div>
                                                                    <progress id="progress-bar" class="d-none" max=100 value=0></progress>
                                                                    <input type="hidden" id="footer_logo" name="footer_logo" data-image="i3" value="{{$footer_logo['setting_value']}}">
                                                                </div>
                                                                
                                                            </div>
                                                            
                                                        </div>
                                                    </div>
                                                </div> -->
                                                <div class="accordion-header collapsed" role="button" data-toggle="collapse" data-target="#about-panel-body-2" aria-expanded="true">
                                                    <h4>Help & Support Settings</h4>
                                                </div>
                                                <div class="accordion-body collapse show" id="about-panel-body-2" data-parent="#accordion" style="">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="row">
                                                                <div class="col-md-4">
                                                                    <div class="form-group">
                                                                        <label for="withdrawinput1">Contact Number</label>
                                                                        <input type="text" name="phone_no1" id="phone_no1" class="form-control" value="{{$phone_no1['setting_value']}}">
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-4">
                                                                    <div class="form-group">
                                                                        <label for="withdrawinput1">Whatsapp Contact</label>
                                                                        <input type="text" name="whatsapp_contact" id="whatsapp_contact" class="form-control" value="{{$whatsapp_contact['setting_value']}}">
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-4">
                                                                    <div class="form-group">
                                                                        <label for="withdrawinput1">Email Address</label>
                                                                        <input type="text" name="email1" id="email1" class="form-control" value="{{$email1['setting_value']}}">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- WaterMark -->
                                                <div class="accordion-header collapsed" role="button" data-toggle="collapse" data-target="#watermark-panel-body-1" aria-expanded="false">
                                                    <h4>Watermark Settings</h4>
                                                </div>
                                                <div class="accordion-body collapse" id="watermark-panel-body-1" data-parent="#accordion" style="">
                                                  <div class="row">
                                                    <div class="col-md-12">
                                                      <div class="row">
                                                        <div class="col-md-4">
                                                          <label for="withdrawinput1">WaterMark</label>
                                                          <div id="i1" class="drop-area" data-throwback="storePerformance" onclick="document.getElementById('fileElem1').click()"> 
                                                              <div class="text-center" id="imgPreviewi1">
                                                                <i class="bi bi-cloud-arrow-up mb-3" style="color:#445dbe;font-size: 60px;"></i>
                                                                <h3 style="color: #445dbe;font-size: 18px;line-height: 80px;">Drop your Sticky Logo here</h3>
                                                              </div>
                                                              <input class="d-none" type="file" id="fileElem1" accept="" onchange="handleFiles(this.files,'i1','storePerformance')">   
                                                          </div>
                                                          <progress id="progress-bar" class="d-none" max=100 value=0></progress>
                                                          <input type="hidden" id="watermark" name="watermark" data-image="i1" value="{{$watermark['setting_value']}}">
                                                        </div>
                                                      </div>
                                                    </div>
                                                  </div>
                                                </div>
                                                <!--End  WaterMark -->
                                                <!-- Genral Setting -->
                                                <div class="accordion-header collapsed" role="button" data-toggle="collapse" data-target="#general-panel-body-g" aria-expanded="false">
                                                    <h4>General Settings</h4>
                                                </div>
                                                <div class="accordion-body collapse" id="general-panel-body-g" data-parent="#accordion" style="">
                                                  <!-- <div class="row">
                                                    <div class="col-md-12">
                                                      <div class="row">
                                                        <div class="col-md-4">
                                                          <label for="withdrawinput1">WaterMark</label>
                                                          
                                                        </div>
                                                      </div>
                                                    </div>
                                                  </div> -->
                                                  <div class="row">
                                                      <div class="col-md-12">
                                                          <div class="row">
                                                              <div class="col-md-4">
                                                                  <div class="form-group">
                                                                      <label for="withdrawinput1">how it work ( Youtube Link )</label>
                                                                      <input type="text" name="how_it_work" id="how_it_work" class="form-control" value="{{$how_it_work['setting_value']}}">
                                                                  </div>
                                                              </div>
                                                          </div>
                                                      </div>
                                                  </div>
                                                </div>
                                                <!--End  WaterMark -->
                                                
                                            </div>
                                        </div>
                                        <div class="pb-5">
                                            <button type="submit" class="btn btn-success waves-effect waves-light float-right"><i class="fa fa-spinner fa-spin d-none" tabindex="20"></i> Save</button>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </form>
                </div>
            </div> 
             
        </div>
        <br><br>

    </section>
</div>

<script src="{{ asset('assets/indrop/inlancer_drop.js')}}"></script>
<script type="text/javascript">
jQuery(document).ready(function(){
    jQuery('#upcoming_festival').select2();
});
/*Logo */
@if(!empty($watermark_image['image_name'])){
  var watermark = '{{$watermark_image['image_name']}}';
}
@endif
  var file_id1 = 'i1';  
  if(typeof watermark!=='undefined' && watermark!=='') {
    var isrc = APPLICATION_URL+'/assets/upload/images/original/'+watermark;
    console.log(isrc);
    $('#imgPreview'+file_id1).empty();   
    var img = '<img onerror="setImage(this);"  class="previewImage" src="'+isrc+'">';  
    var div = '<div class="col-12"><div style="padding: 5px;margin-bottom: 8px;">'+img+'</div></div>';
    $('#imgPreview'+file_id1).append(div);    
  }
  /*for Header Logo*/
/*@if(!empty($header_logo_image['image_name'])){
  var header_logo = '{{$header_logo_image['image_name']}}';
}
@endif  
  var file_id2 = 'i2';  
  if (typeof header_logo!=='undefined' && header_logo!=='') {
    var isrc2 = APPLICATION_URL+'/assets/upload/images/original/'+header_logo;
    $('#imgPreview'+file_id2).empty();   
    var img2 = '<img onerror="setImage(this);"  class="previewImage" src="'+isrc2+'">';  
    var div2 = '<div class="col-12"><div style="padding: 5px;margin-bottom: 8px;">'+img2+'</div></div>';
    $('#imgPreview'+file_id2).append(div2);    
  }*/
  /*End Header Logo
  /*for footer Logo*/
/*@if(!empty($footer_logo_image['image_name'])){
  var footer_logo = '{{$footer_logo_image['image_name']}}';
}
@endif  
  var file_id3 = 'i3';  
  if (typeof footer_logo!=='undefined' && footer_logo!=='') {
    var isrc3 = APPLICATION_URL+'/assets/upload/images/original/'+footer_logo;
    $('#imgPreview'+file_id3).empty();   
    var img3 = '<img onerror="setImage(this);"  class="previewImage" src="'+isrc3+'">';  
    var div3 = '<div class="col-12"><div style="padding: 5px;margin-bottom: 8px;">'+img3+'</div></div>';
    $('#imgPreview'+file_id3).append(div3);    
  }*/
  /*End Footer Logo*/

function setImage(img){
console.log(img);
    img.src="{{url('/assets/img/placeholder.png');}}";  
    img.style.height="80px";
}
function storePerformance(file_id,returnData) {    
    $("[data-image='"+file_id+"']").val(returnData.image_id);  
}    

</script>
@endsection