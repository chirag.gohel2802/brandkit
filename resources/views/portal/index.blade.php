@extends('portal.template.app')
@section('content')

    <style type="text/css">
        .upgrade-btn {
            float: right;
            font-size: 16px;
            font-weight: 600;
            background: #848c36;
            color: white;
            padding: 2px 6px 2px 6px;
            border-radius: 6px;
            cursor: pointer;
        }

    </style>

    <div class="main-content">
        <section class="section">
            @if ($logged_user_role != 'admin' && $is_master == 1 && $subscription_status != 1 && $subscription_req_status != 1)
                <div class="alert alert-dismissible show fade" style="background: aliceblue;color: black">
                    <div class="alert-body">
                        <button class="close" data-dismiss="alert">
                            <span>×</span>
                        </button>
                        <b style="font-size: 15px;">Access Denied</b>
                    </div>
                </div>
            @endif


            @include('messages.flash-message')
            @if ($is_master == 1 or session()->get('my_chacha')['user_role'] == 'admin')
                <div class="row">
                    <div class="col-lg-4 col-md-4 col-sm-12">
                        <div class="card card-statistic-2">
                            <div class="card-stats">
                                <div class="card-stats-title">Users Overview</div>
                                <div class="card-stats-items">
                                    <div class="card-stats-item">
                                        <div class="card-stats-item-count"> {{ $counts['active_users'] }}</div>
                                        <div class="card-stats-item-label">Active</div>
                                    </div>
                                    <div class="card-stats-item">
                                        <div class="card-stats-item-count">{{ $counts['inactive_users'] }}</div>
                                        <div class="card-stats-item-label">Inactive</div>
                                    </div>
                                    <div class="card-stats-item">
                                        <div class="card-stats-item-count">{{ $counts['pending_users'] }}</div>
                                        <div class="card-stats-item-label">Blocked</div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-icon shadow-primary bg-primary">
                                <svg style=" " width="30" height="30" viewBox="0 0 18 22" fill="none"
                                    xmlns="http://www.w3.org/2000/svg">
                                    <path
                                        d="M8.92939 10.3397C9.60031 10.349 10.2661 10.2213 10.886 9.96449C11.5059 9.70768 12.0668 9.32713 12.5346 8.8461C13.0156 8.37828 13.3962 7.81728 13.653 7.1974C13.9098 6.57751 14.0375 5.91177 14.0282 5.24086C14.0375 4.56994 13.9098 3.9042 13.653 3.28431C13.3962 2.66443 13.0156 2.10343 12.5346 1.63561C12.0612 1.16211 11.4992 0.786509 10.8806 0.530248C10.262 0.273987 9.59897 0.14209 8.92939 0.14209C8.25982 0.14209 7.59681 0.273987 6.97822 0.530248C6.35962 0.786509 5.79757 1.16211 5.32415 1.63561C4.84312 2.1034 4.46257 2.6644 4.20575 3.28429C3.94894 3.90418 3.82127 4.56994 3.83055 5.24086C3.82131 5.91177 3.949 6.57751 4.20581 7.1974C4.46262 7.81728 4.84316 8.37828 5.32415 8.8461C5.79212 9.32689 6.35314 9.70729 6.97299 9.96409C7.59284 10.2209 8.25852 10.3487 8.92939 10.3397ZM6.20183 2.51258C6.56001 2.15426 6.98528 1.87001 7.45334 1.67608C7.92141 1.48215 8.4231 1.38233 8.92974 1.38233C9.43639 1.38233 9.93808 1.48215 10.4061 1.67608C10.8742 1.87001 11.2995 2.15426 11.6577 2.51258C12.0236 2.86491 12.3128 3.28907 12.507 3.75844C12.7013 4.22781 12.7964 4.73227 12.7865 5.24015C12.7965 5.74815 12.7014 6.25274 12.5072 6.72224C12.3129 7.19174 12.0237 7.61601 11.6577 7.96842C11.2995 8.32683 10.8743 8.61115 10.4062 8.80513C9.93813 8.99912 9.43642 9.09896 8.92974 9.09896C8.42307 9.09896 7.92136 8.99912 7.45329 8.80513C6.98522 8.61115 6.55996 8.32683 6.20183 7.96842C5.83577 7.61604 5.54653 7.19177 5.35227 6.72226C5.15801 6.25276 5.06292 5.74816 5.07298 5.24015C5.06302 4.73226 5.15815 4.22779 5.35241 3.75842C5.54667 3.28904 5.83586 2.86489 6.20183 2.51258Z"
                                        fill="white" />
                                    <path
                                        d="M17.8515 16.4235C17.8184 15.9739 17.7611 15.5264 17.68 15.083C17.6 14.628 17.4902 14.1788 17.3513 13.7382C17.2106 13.3021 17.0253 12.8818 16.7981 12.4838C16.5744 12.0831 16.2934 11.7171 15.9642 11.3973C15.617 11.071 15.2096 10.8153 14.7648 10.6445C14.2772 10.4566 13.7584 10.3628 13.2359 10.3679C12.9308 10.401 12.6423 10.5238 12.4069 10.7207C12.1586 10.883 11.8679 11.0735 11.5434 11.2774C11.1929 11.4875 10.8178 11.6534 10.4265 11.7712C9.53947 12.0656 8.58106 12.0656 7.69401 11.7712C7.30397 11.6519 6.93024 11.4846 6.5814 11.2731C6.26038 11.0678 5.96971 10.8802 5.71713 10.7165C5.48195 10.5196 5.19372 10.3969 4.88884 10.3637C4.36627 10.3587 3.84748 10.4527 3.35996 10.641C2.91512 10.8117 2.50775 11.0674 2.16057 11.3938C1.83157 11.7136 1.55088 12.0796 1.32734 12.4803C1.10028 12.8783 0.914916 13.2987 0.774208 13.7347C0.634469 14.1764 0.523933 14.6268 0.443316 15.083C0.362054 15.5264 0.304823 15.9739 0.271873 16.4235C0.243652 16.8277 0.229541 17.2482 0.229541 17.6737C0.207687 18.1654 0.289338 18.6563 0.469211 19.1145C0.649083 19.5727 0.923167 19.988 1.27372 20.3335C2.0058 21.0028 2.9728 21.3556 3.96389 21.3149H14.1595C15.1506 21.3556 16.1176 21.0028 16.8496 20.3335C17.2003 19.988 17.4744 19.5727 17.6543 19.1145C17.8341 18.6563 17.9158 18.1654 17.8938 17.6737C17.8938 17.2504 17.879 16.827 17.8508 16.4235H17.8515ZM15.9945 19.4347C15.746 19.6568 15.4559 19.8276 15.1411 19.9373C14.8263 20.047 14.493 20.0934 14.1602 20.0739H3.96601C3.63322 20.0934 3.29985 20.047 2.98505 19.9373C2.67026 19.8276 2.38023 19.6568 2.13164 19.4347C1.90337 19.204 1.72661 18.9275 1.61302 18.6234C1.49943 18.3194 1.4516 17.9947 1.47268 17.6709C1.47268 17.2743 1.48608 16.8828 1.51219 16.5067C1.54225 16.1026 1.59406 15.7003 1.6674 15.3017C1.7378 14.9005 1.83443 14.5043 1.95667 14.1157C2.07123 13.7611 2.22207 13.4192 2.4068 13.0955C2.57111 12.7997 2.77739 12.5293 3.01919 12.2926C3.25027 12.078 3.52078 11.9103 3.81573 11.7988C4.12791 11.6793 4.45865 11.6155 4.79289 11.6104C4.83663 11.6337 4.91353 11.6809 5.03912 11.7592C5.29381 11.9258 5.58802 12.112 5.91326 12.3237C6.35253 12.5903 6.8235 12.8008 7.31515 12.9502C8.45029 13.3251 9.67589 13.3251 10.811 12.9502C11.3032 12.8006 11.7746 12.5899 12.2143 12.323C12.5466 12.1113 12.8324 11.9265 13.0878 11.7585C13.2133 11.6767 13.2902 11.633 13.334 11.6097C13.6682 11.6148 13.999 11.6786 14.3111 11.798C14.6061 11.9097 14.8766 12.0774 15.1077 12.2919C15.3496 12.5285 15.5559 12.799 15.7201 13.0948C15.9049 13.4182 16.0557 13.7599 16.1702 14.1143C16.2923 14.5033 16.3887 14.9 16.4588 15.3017C16.5319 15.7003 16.5837 16.1026 16.614 16.5067C16.6401 16.8814 16.6535 17.2729 16.6535 17.6709C16.6746 17.9947 16.6268 18.3194 16.5132 18.6234C16.3996 18.9275 16.2228 19.204 15.9945 19.4347Z"
                                        fill="white" />
                                </svg>
                            </div>
                            <div class="card-wrap">
                                <div class="card-header">
                                    <h4>Total users</h4>
                                </div>
                                <div class="card-body">
                                    {{ $counts['total_users'] }}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-12">
                        <div class="card card-statistic-2">
                            <div class="card-stats">
                                <div class="card-stats-title">Post Overview</div>
                                <div class="card-stats-items">
                                    <div class="card-stats-item">
                                        <div class="card-stats-item-count"> {{ $counts['active_post'] }}</div>
                                        <div class="card-stats-item-label">Active</div>
                                    </div>
                                    <div class="card-stats-item">
                                        <div class="card-stats-item-count">{{ $counts['inactive_post'] }}</div>
                                        <div class="card-stats-item-label">Inactive</div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-icon shadow-primary bg-primary">
                                <i class="m-0 fas fa-th"></i>
                            </div>
                            <div class="card-wrap">
                                <div class="card-header">
                                    <h4>Total Post</h4>
                                </div>
                                <div class="card-body">
                                    {{ $counts['total_post'] }}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-12">
                        <div class="card card-statistic-2">
                            <div class="card-stats">
                                <div class="card-stats-title">Category Overview</div>
                                <div class="card-stats-items">
                                    <div class="card-stats-item">
                                        <div class="card-stats-item-count"> {{ $counts['active_category'] }}</div>
                                        <div class="card-stats-item-label">Active</div>
                                    </div>
                                    <div class="card-stats-item">
                                        <div class="card-stats-item-count">{{ $counts['inactive_category'] }}</div>
                                        <div class="card-stats-item-label">Inactive</div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-icon shadow-primary bg-primary">
                                <i class="m-0 fas fa-th-large"></i>
                            </div>
                            <div class="card-wrap">
                                <div class="card-header">
                                    <h4>Total Category</h4>
                                </div>
                                <div class="card-body">
                                    {{ $counts['total_category'] }}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-12">
                        <div class="card card-statistic-2">
                            <div class="card-stats">
                                <div class="card-stats-title">Video Overview</div>
                                <div class="card-stats-items">
                                    <div class="card-stats-item">
                                        <div class="card-stats-item-count"> {{ $counts['active_video'] }}</div>
                                        <div class="card-stats-item-label">Active</div>
                                    </div>
                                    <div class="card-stats-item">
                                        <div class="card-stats-item-count">{{ $counts['inactive_video'] }}</div>
                                        <div class="card-stats-item-label">Inactive</div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-icon shadow-primary bg-primary">
                                <i class="m-0 fas fa-th"></i>
                            </div>
                            <div class="card-wrap">
                                <div class="card-header">
                                    <h4>Total Video</h4>
                                </div>
                                <div class="card-body">
                                    {{ $counts['total_video'] }}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-12">
                        <div class="card card-statistic-2">
                            <div class="card-stats">
                                <div class="card-stats-title">Custom Post Overview</div>
                                <div class="card-stats-items">
                                    <div class="card-stats-item">
                                        <div class="card-stats-item-count"> {{ $counts['active_custom_post'] }}</div>
                                        <div class="card-stats-item-label">Active</div>
                                    </div>
                                    <div class="card-stats-item">
                                        <div class="card-stats-item-count">{{ $counts['inactive_custom_post'] }}</div>
                                        <div class="card-stats-item-label">Inactive</div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-icon shadow-primary bg-primary">
                                <i class="m-0 fas fa-th"></i>
                            </div>
                            <div class="card-wrap">
                                <div class="card-header">
                                    <h4>Total Custom Post</h4>
                                </div>
                                <div class="card-body">
                                    {{ $counts['total_custom_post'] }}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-12">
                        <div class="card card-statistic-2">
                            <div class="card-stats">
                                <div class="card-stats-title">Frame Overview</div>
                                <div class="card-stats-items">
                                    <div class="card-stats-item">
                                        <div class="card-stats-item-count"> {{ $counts['active_frame'] }}</div>
                                        <div class="card-stats-item-label">Active</div>
                                    </div>
                                    <div class="card-stats-item">
                                        <div class="card-stats-item-count">{{ $counts['inactive_frame'] }}</div>
                                        <div class="card-stats-item-label">Inactive</div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-icon shadow-primary bg-primary">
                                <i class="m-0 fas fa-clipboard-list"></i>
                            </div>
                            <div class="card-wrap">
                                <div class="card-header">
                                    <h4>Total Frame</h4>
                                </div>
                                <div class="card-body">
                                    {{ $counts['total_frame'] }}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endif


        </section>
    </div>

    <script type="text/javascript">

    </script>

@endsection
