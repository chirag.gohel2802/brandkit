<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<meta http-equiv="content-type" content="text/html;charset=utf-8" /> 

<head>   
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport"> 
    <meta name="author" content="Inlancer.in"> 
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <link rel="shortcut icon" href="{{asset('festival_inlancer/favicon/favicon.png')}}">
    <title>@if(isset($title) && $title!='') {{$title}} @else Portal @endif | {{ config('app.name') }}</title>
    <!-- General CSS Files -->
    <!-- Styles --> 
    <link rel="stylesheet" href="{{ asset('festival_inlancer/modules/bootstrap/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{ asset('festival_inlancer/modules/fontawesome/css/all.min.css')}}">
    
    <!-- Template CSS -->
    <link rel="stylesheet" href="{{ asset('festival_inlancer/css/style.css')}}">
    <link rel="stylesheet" href="{{ asset('festival_inlancer/css/components.css')}}">  
    <!-- Start GA -->
    <!-- /END GA --> 
    <link rel="stylesheet" href="{{ asset('festival_inlancer/modules/izitoast/css/iziToast.min.css')}}">

    <script language="javascript">csrf_token="{{ csrf_token() }}"</script>    
    <script language="javascript">APPLICATION_URL="{{url('/')}}"</script>    
 
    
    <script src="{{ asset('festival_inlancer/modules/jquery.min.js')}}"></script>
    <script src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
    <script src="{{ asset('js/common.js')}}"></script>
    
    <link rel="stylesheet" type="text/css" href="{{asset('festival_inlancer/modules/select2/dist/css/select2.min.css')}}">
    <script src="{{ asset('festival_inlancer/modules/select2/dist/js/select2.full.min.js')}}"></script>

</head>
<body>
<style type="text/css">
    .modal.left .modal-dialog,
    .modal.right .modal-dialog {
        position: fixed;
        margin: auto;
        width: 100%;
        height: 100%;
        -webkit-transform: translate3d(0%, 0, 0);
            -ms-transform: translate3d(0%, 0, 0);
             -o-transform: translate3d(0%, 0, 0);
                transform: translate3d(0%, 0, 0);
    }

    .modal.left .modal-content,
    .modal.right .modal-content {
        height: 100%;
    }
    
    .modal.left .modal-body,
    .modal.right .modal-body {
        padding: 15px 15px 80px;
    }
    .modal.left.fade .modal-dialog{
        left: -320px;
        -webkit-transition: opacity 0.3s linear, left 0.3s ease-out;
           -moz-transition: opacity 0.3s linear, left 0.3s ease-out;
             -o-transition: opacity 0.3s linear, left 0.3s ease-out;
                transition: opacity 0.3s linear, left 0.3s ease-out;
    }
    .modal.left.fade.in .modal-dialog{
        left: 0;
    }
    .modal.right.fade .modal-dialog {
        right: 0;
        -webkit-transition: opacity 0.3s linear, right 0.3s ease-out;
           -moz-transition: opacity 0.3s linear, right 0.3s ease-out;
             -o-transition: opacity 0.3s linear, right 0.3s ease-out;
                transition: opacity 0.3s linear, right 0.3s ease-out;
    }
    .modal.right.fade.in .modal-dialog {
        right: 0;
    }
    .modal-content {
        border-radius: 0;
        border: none;
    }

    .modal-header {
        border-bottom-color: #EEEEEE;
        background-color: #FAFAFA;
    }
    label{
        text-transform: capitalize;
    }
    a.maja_drop {
        padding: 10px 20px;
        font-weight: 500;
        line-height: 1.2;
        color: white;
    }
    .img_remove{
        position: absolute;/*inheri*/
        /*height: 26px; 
        margin-right: -25px;*/
        z-index: 10;
    }
    </style>
    @php
        if(!empty(session()->get('my_chacha')['user_permission_ids'])){
            $permissions =  explode(',',session()->get('my_chacha')['user_permission_ids']);  
        }else{
            $permissions =  [];  
        } 
        $role = session()->get('my_chacha')['user_role']; 
    @endphp 
    <script>
        base_url = "{{asset('/')}}";
    </script>


    <div id="app">
        <div class="main-wrapper main-wrapper-1">
            <div class="navbar-bg"></div>
            <nav class="navbar navbar-expand-lg main-navbar">
                <form class="form-inline mr-auto">
                    <ul class="navbar-nav mr-3">
                        <li><a href="#" data-toggle="sidebar" class="nav-link nav-link-lg"><i class="fa fa-bars"></i></a></li>
                    </ul>
                </form>
                <ul class="navbar-nav navbar-right">
                    <!-- <li class="dropdown dropdown-list-toggle">
                        <a href="javascript:getNotification();" onclick="getNotification();" data-toggle="dropdown" class="nav-link notification-toggle nav-link-lg"><i class="fa fa-bell"></i></a>
                        <div class="dropdown-menu dropdown-list dropdown-menu-right ">
                            <div class="dropdown-header"> Notifications  <span style="" id="noti_count_number"></span>
                            </div>
                            <div class="notification-list-content dropdown-list-icons noti-list" id="unread_notis"> 
                            </div>
                            <div class="dropdown-footer text-center">
                                <a class="noti_btns" href="{{url('notifications')}}"> View All<i class="fa fa-chevron-right"></i></a>
                            </div>
                        </div>
                    </li> -->
                    <li class="dropdown"><a href="#" data-toggle="dropdown" class="nav-link dropdown-toggle nav-link-lg nav-link-user">
                            <img alt="My Profile" src="{{asset('festival_inlancer/img/avatar/avatar-4.png')}}" class="rounded-circle mr-1">
                            <div class="d-sm-none d-lg-inline-block">Hi, {{session()->get('my_chacha')['user_name']}}</div>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right">
                            <a  style="@if(isset($active) && $active=='profile') {{'background:#18bf81;color:white'}} @endif" href="
                                {{asset('my-profile')}}" class="dropdown-item has-icon">
                                <i class="fa fa-user"></i>Profile
                            </a>
                            <a style="@if(isset($active) && $active=='sendMessageAlert') {{'background:#18bf81;color:white'}} @endif" href="
                                {{asset('send-message-alert')}}" class="dropdown-item has-icon">
                                <i class="fa fa-bell"></i>Notification
                            </a>
                            <a style="@if(isset($active) && $active=='sendMessageAlert') {{'background:#18bf81;color:white'}} @endif" href="
                                {{asset('test-notification')}}" class="dropdown-item has-icon">
                                <i class="fa fa-text-width"></i>Test Notification
                            </a>
                            <a  href="
                                {{asset('export')}}" class="dropdown-item has-icon">
                                <i class="fa fa-file-export"></i>Export Business
                            </a>
                            <div class="dropdown-divider"></div>
                            <a href="{{url('portal-logout')}}" class="dropdown-item has-icon text-danger">
                                <i class="fa fa-sign-out-alt"></i>  Log Out 
                            </a>
                        </div>
                    </li>
                </ul>
            </nav>
            <div class="main-sidebar sidebar-style-2">
                <aside id="sidebar-wrapper">
                    <div class="sidebar-brand">
                        <a href="{{url('portal')}}"> 
                            <img style="height: 50px;" src="{{asset('festival_inlancer/img/logo.svg')}} ">
                        </a>
                    </div>
                    <div class="sidebar-brand sidebar-brand-sm">
                        <a href="{{url('portal')}}"><img style="height: 40px;" src="{{asset('festival_inlancer/favicon/favicon.png')}}"></a>
                    </div>
 

                    <ul class="sidebar-menu">

                        <li class="menu-header">Dashboard</li>
                        <li class="@if(isset($active) && $active=='home') {{'active'}} @endif">
                            <a href="{{url('portal')}}" class="nav-link">
                                <i class="fa fa-home"></i>
                                <span>Dashboard</span>
                            </a>
                        </li>

                        <li class="menu-header">User Master</li>
                        <li class="@if(isset($active) && $active=='user') {{'active'}} @endif">
                            <a href="{{asset('user-master')}}" class="nav-link"> 
                                <i class="fa fa-users"></i>
                                <span>Users</span>
                            </a>
                        </li>
                        <li class="@if(isset($active) && $active=='business') {{'active'}} @endif">
                            <a href="{{asset('business-master')}}" class="nav-link"> 
                                <i class="fa fa-business-time"></i>
                                <span>Users Business List</span>
                            </a>
                        </li>
                        <li class="@if(isset($active) && $active=='business-category') {{'active'}} @endif">
                            <a href="{{asset('business-category-master')}}" class="nav-link">
                                <i class="m-0 fas fa-th-large"></i>
                                <span>Business Category List</span>
                            </a>
                        </li>

                        <li class="menu-header">App data</li>
                        <li class="@if(isset($active) && $active=='banner') {{'active'}} @endif">
                            <a href="{{asset('banner-master')}}" class="nav-link">
                                <i class="fa fa-images"></i>
                                <span>Banner</span>
                            </a>
                        </li>
                        <li class="@if(isset($active) && $active=='client') {{'active'}} @endif">
                            <a href="{{asset('client-master')}}" class="nav-link">
                                <i class="fa fa-images"></i>
                                <span>Clients</span>
                            </a>
                        </li>
                        <li class="@if(isset($active) && $active=='category') {{'active'}} @endif">
                            <a href="{{asset('category-master')}}" class="nav-link">
                                <i class="m-0 fas fa-th-large"></i>
                                <span>Category</span>
                            </a>
                        </li>
                        <li class="@if(isset($active) && $active=='post') {{'active'}} @endif">
                            <a href="{{asset('post-master')}}" class="nav-link">
                                <i class="m-0 fas fa-th"></i>
                                <span>Post</span>
                            </a>
                        </li>
                        <li class="@if(isset($active) && $active=='video') {{'active'}} @endif">
                            <a href="{{asset('video-master')}}" class="nav-link">
                                <i class="m-0 fas fa-th"></i>
                                <span>Video</span>
                            </a>
                        </li>
                        <li class="@if(isset($active) && $active=='custom-post') {{'active'}} @endif">
                            <a href="{{asset('custom-post-master')}}" class="nav-link">
                                <i class="m-0 fas fa-th"></i>
                                <span>Custom post</span>
                            </a>
                        </li>
                        <li class="@if(isset($active) && $active=='frame') {{'active'}} @endif">
                            <a href="{{asset('frame-master')}}" class="nav-link">
                                <i class="fa fa-clipboard-list"></i>
                                <span>Frame</span>
                            </a>
                        </li>

                        
                        <li class="menu-header">App View Setting</li>
                        <li class="@if(isset($active) && $active=='dashboard') {{'active'}} @endif">
                            <a href="{{asset('dashboard-master')}}" class="nav-link"> 
                                <i class="fa fa-mobile-alt"></i>
                                <span>App Dashboard</span>
                            </a>
                        </li>
                        <li class="@if(isset($active) && $active=='custom-dashboard') {{'active'}} @endif">
                            <a href="{{asset('custom-dashboard-master')}}" class="nav-link"> 
                                <i class="fa fa-mobile-alt"></i>
                                <span>Custom Dashboard</span>
                            </a>
                        </li>
                        <li class="@if(isset($active) && $active=='setting') {{'active'}} @endif">
                            <a href="{{asset('portal-settings')}}" class="nav-link"> 
                                <i class="fa fa-cogs"></i>
                                <span>Portal Settings</span>
                            </a>
                        </li> 
                        </li>
                        <li class="menu-header">general Report</li>
                        <li class="@if(isset($active) && $active=='reports') {{'active'}} @endif">
                            <a href="{{asset('report-master')}}" class="nav-link"> 
                                <i class="fa fa-sticky-note"></i>
                                <span>Reports</span>
                            </a>
                        </li>
                        <li class="@if(isset($active) && $active=='report') {{'active'}} @endif">
                            <a href="{{asset('report-user')}}" class="nav-link"> 
                                <i class="fa fa-sticky-note"></i>
                                <span>User Reports</span>
                            </a>
                        </li>
                        
                        
                         
                    </ul>
                    <!-- <div class="mt-4 mb-4 p-3 hide-sidebar-mini">
                        <a href="reichbath.com/contact-us" target="_blank" class="btn btn-primary btn-lg btn-block btn-icon-split">
                            <i class="fa fa-rocket"></i>
                            Contact Us
                        </a>
                    </div> -->
                </aside>
            </div>


            @yield('content')   
            <footer class="main-footer">
                <div class="footer-right">
                    Crafted By <a href="https://inlancer.in/" target="_blank">Inlancer.in</a> 
                </div>
            </footer>
        </div> 
         
    </div>
    
    <!-- General JS Scripts -->
    <script src="{{ asset('festival_inlancer/modules/popper.js')}}"></script>
    <script src="{{ asset('festival_inlancer/modules/tooltip.js')}}"></script>
    <script src="{{ asset('festival_inlancer/modules/bootstrap/js/bootstrap.min.js')}}"></script>
    <script src="{{ asset('festival_inlancer/modules/nicescroll/jquery.nicescroll.min.js')}}"></script>
    <script src="{{ asset('festival_inlancer/modules/moment.min.js')}}"></script>
    <script src="{{ asset('festival_inlancer/js/stisla.js')}}"></script>
    
    <!-- Toaster -->
    <script src="{{ asset('festival_inlancer/modules/izitoast/js/iziToast.min.js')}}"></script>

    <!-- SweetAlert JS File -->
    <script src="{{asset('festival_inlancer/modules/sweetalert/sweetalert2.min.js')}}"></script> 
    <script src="{{ asset('festival_inlancer/js/scripts.js')}}"></script>
    <script src="{{ asset('festival_inlancer/js/custom.js')}}"></script>
    <script>
        jQuery(document).ready(function($) {
            if($('aside').find('li.active').length > 0){
                $($('aside').find('li.active'), window.parent.document).get(0).scrollIntoView();
            }

        });
    </script>
    <script type="text/javascript">
        /* for filter */
        $("[data-collapse]").each(function() {
            var me = $(this),
                target = me.data('collapse');

            me.click(function() {
              $(target).collapse('toggle');
              $(target).on('shown.bs.collapse', function() {
                me.html('Hide');
              });
              $(target).on('hidden.bs.collapse', function() {
                me.html('Show');
              });
              return false;
            });
        });
        /* for filter */
    </script>

</body>
 
</html> 