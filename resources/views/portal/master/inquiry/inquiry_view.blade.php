<?php 
$page_title = 'View Inquiry';
$route_name = 'inquiry'; 
$mode = 'view';
// $save_url = url($route_name.'-save');
$modal_size = 'modal-lg';

?>
<style type="text/css"> 
    textarea{ 
        height: unset;
    }
</style>
<div class="modal fade {{$mode}}_{{$route_name}}_modal " id="{{$mode}}_{{$route_name}}_modal" tabindex="-1" role="dialog" aria-modal="true" style="display: none; padding-left: 17px;" aria-hidden="true" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog {{$modal_size}}" role="document">
        <div class="modal-content" style="border-radius: 15px;">
            <div class="modal-header bg-primary" style="background-color: ;border-color: #dbe0ec;">
              <h5 class="modal-title mt-0 text-white" id="myModalLabel">{{$page_title}}</h5>
              <button type="button" class="close text-white" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body" style=" background-color:#f3f6f7 ;">
                <div class="form-body">  
                    <div class="row">
                        <div class="col-md-12 col-lg-12 col-xl-12">
                            <div class="card">
                                <div class="card-body">
                                    <div class="row">     
                                        <div class="col-md-6 col-12">
                                          <table class="table table-bordered">
                                            <tbody>
                                              <tr>
                                                <td width="25%">Date</td>
                                                <td>{{date('M d, Y',strtotime($created_at))}}</td>
                                              </tr>
                                            </tbody>
                                          </table> 
                                        </div>
                                        <div class="col-md-6 col-12">
                                          <table class="table table-bordered">
                                            <tbody>
                                              <tr>
                                                <td width="25%">UserName</td>
                                                <td>{{$user_name}}</td>
                                              </tr>
                                            </tbody>
                                          </table>
                                        </div>
                                        <div class="col-md-6 col-12">
                                          <table class="table table-bordered">
                                            <tbody>
                                            <tr>
                                              <td width="25%">Email</td>
                                              <td>{{$user_email}}</td>
                                            </tr> 
                                            </tbody>
                                          </table>
                                        </div>
                                        <div class="col-md-6 col-12">
                                          <table class="table table-bordered">
                                            <tbody>
                                              <tr>
                                                <td width="25%">Contact</td>
                                                <td>{{$user_phone_no}}</td>
                                              </tr>
                                            </tbody>
                                          </table>
                                        </div>
                                        <div class="col-md-6 col-12">
                                          <table class="table table-bordered">
                                            <tbody>
                                              <tr>
                                                <td width="25%">Subject</td>
                                                <td>{{$user_subject}}</td>
                                              </tr>
                                            </tbody>
                                          </table>
                                        </div>
                                        <div class="col-md-6 col-12">
                                          <table class="table table-bordered">
                                            <tbody>
                                              <tr>
                                                <td width="25%">Message</td>
                                                <td>{{$user_messsage}}</td>
                                              </tr>
                                            </tbody>
                                          </table>
                                        </div>
                                    </div>
                                </div> 
                            </div>  
                        </div>  
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



<!-- <div class="modal  fade {{$mode}}_{{$route_name}}_modal" id="{{$mode}}_{{$route_name}}_modal" tabindex="-1" role="dialog" style="display: none;" aria-hidden="true" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog {{$modal_size}}" role="document">
        <div class="modal-content" style="overflow-y: auto;">
            <div class="modal-header">
                <h5 class="modal-title mb-0" id="myModalLabel">{{$page_title}}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
             <div class="modal-body">   
                    <div class="form-body">  
                        <div class="row">
                            <div class="col-md-12 col-lg-12 col-xl-12">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="row">     
                                        <div class="col-md-4 col-12">
                                          <table class="table table-bordered">
                                            <tbody>
                                              <tr>
                                                <td width="25%">Date</td>
                                                <td>{{date('M d, Y',strtotime($created_at))}}</td>
                                              </tr>
                                            </tbody>
                                          </table> 
                                        </div>
                                        <div class="col-md-4 col-12">
                                          <table class="table table-bordered">
                                            <tbody>
                                              <tr>
                                                <td width="25%">User Name</td>
                                                <td>{{$user_name}}</td>
                                              </tr>
                                            </tbody>
                                          </table>
                                        </div>
                                        <div class="col-md-4 col-12">
                                          <table class="table table-bordered">
                                            <tbody>
                                            <tr>
                                              <td width="25%">Email Id</td>
                                              <td>{{$user_email}}}</td>
                                            </tr> 
                                            </tbody>
                                          </table>
                                        </div>
                                        <div class="col-md-4 col-12">
                                          <table class="table table-bordered">
                                            <tbody>
                                              <tr>
                                                <td width="25%">Contact No</td>
                                                <td>{{$user_phone_no}}</td>
                                              </tr>
                                            </tbody>
                                          </table>
                                        </div>
                                        <div class="col-md-4 col-12">
                                          <table class="table table-bordered">
                                            <tbody>
                                              <tr>
                                                <td width="25%">Inquiry Subject</td>
                                                <td>{{$user_subject}}</td>
                                              </tr>
                                            </tbody>
                                          </table>
                                        </div>
                                        <div class="col-md-4 col-12">
                                          <table class="table table-bordered">
                                            <tbody>
                                              <tr>
                                                <td width="25%">Inquiry Message</td>
                                                <td>{{$user_messsage}}</td>
                                              </tr>
                                            </tbody>
                                          </table>
                                        </div>
                                        
                                         
                                      </div>
                                    </div> 
                                </div>  
                            </div>  
                        </div>
                    </div>   
                </div> 
            
        </div>
    </div>
</div> -->
<script type="text/javascript">

jQuery(document).ready(function() { 
});

</script>