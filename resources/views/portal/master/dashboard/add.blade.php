@extends('portal.template.app') 
@section('content')  

<?php 
$page_title = 'Add Dashboard';
$route_name = 'dashboard'; 
$mode = 'add'; 
$save_url = url($route_name.'-save');
$title = "Dashboard";
 
?> 
<style type="text/css"> 
    textarea{
        height: unset;
    }
    body.sidebar-mini .main-sidebar .sidebar-menu > li.menu-header {
        padding: 0;
        font-size: 0;
        height: 2px;
        margin: 0;
    }
</style> 
<div class="main-content">
<section class="section">
    <div class="section-header">
        <h1><?php echo $page_title; ?></h1>
        <div class="section-header-breadcrumb d-none">
            <div class="breadcrumb-item active"><a href="<?php echo url('portal');?>">Dashboard</a></div> 
            <div class="breadcrumb-item"><?php echo ucfirst($route_name); ?></div>
        </div>
    </div>
    <div class="section-body">
        <div class="row">
            <div class="col-12"> 
                <div class="card">
                    <form class="form add_form" method="post" id="add_form" action="{{$save_url}}" enctype="multipart/form-data">
                        <input type="hidden" name="mode" value="{{$mode}}">
                        @csrf
                        <div id="message"></div>
                        <div class="form-body">
                            <div class="row">
                                <div class="col-md-12 col-lg-12 col-xl-12">
                                    <div>
                                        <div class="card-body">
                                            <div id="accordion">
                                                <div class="accordion">
                                                    <div class="accordion-header collapsed" role="button" data-toggle="collapse" data-target="#edit-panel-body-1" aria-expanded="true">
                                                        <h4>Step-1 : Dashboards Details</h4>
                                                    </div>
                                                    <div class="accordion-body collapse show" id="edit-panel-body-1" data-parent="#accordion" style="">
                                                        <div class="row">
                                                            <div class="col-md-4">
                                                                <div class="row">
                                                                    <div class="col-md-12">
                                                                        <div class="form-group">
                                                                            <label for="withdrawinput1">Dashboard Name: <span class="text-danger">(Shown in App)</span> </label>
                                                                            <input type="text" name="dashboard_name" class="form-control" required>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <div class="row">
                                                                    <div class="col-md-12">
                                                                        <div class="form-group">
                                                                            <label for="withdrawinput1">Dashboard Position: </label>
                                                                            <input type="number" name="dashboard_position" step="1" min="1" max="99" class="form-control" placeholder="Ex. 10 " required>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <div class="row">
                                                                    <div class="col-md-12">
                                                                        <div class="form-group">
                                                                            <label for="withdrawinput1">Scroll: </label>
                                                                            <select name="is_horizontal" class="form-control "  required>
                                                                                <option value="1" selected>Horizontal Scroll ( આડું )</option>
                                                                                <option value="0">Vertical Scroll ( ઊભું )</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            
                                                        </div>
                                                    </div>
                                                    <div class="accordion-header collapsed" role="button" data-toggle="collapse" data-target="#panel2" aria-expanded="false">
                                                        <h4>Step-2 : Dashboard Category</h4>
                                                    </div>
                                                    <div class="accordion-body collapse show" id="panel2" data-parent="#accordion" style="">
                                                        <div class="row">
                                                            <div class="table-responsive  col-sm-12">
                                                    <table id="dashboard-sub-category-table" class="table table-bordered" style="text-align: center;">
                                                        <thead>
                                                            <tr> 
                                                                <th width="1%">#</th>
                                                                <th width="35%">Category<br><span class="text-danger">(Shown in App)</span></th>
                                                                <th width="15%" class="text-right"><center>Show Date<br><span class="text-danger">(Shown in App)</span></center></th>
                                                                <th width="15%" class="text-right"><center>Start Date</center></th>
                                                                <th width="15%" class="text-right"><center>End Date</center></th>
                                                                <th width="15%" class="text-right"><center>Position</center></th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <tr>
                                                                <td width="1%"><span id="sr_no">#</span></td>
                                                                <td width="35%" style="width: 200px !important;">
                                                                    <select class="select2 category-select-unique form-control saveData" id="sub_category1" name="sub_category[]" onchange="setShowDate(1)" required>
                                                                    <option selected="selected" disabled value="">Select Category</option>
                                                                    @if(!empty($category_list))
                                                                    @foreach($category_list as $key=>$category) 
                                                                        <option value="{{$category->category_id}}"  
                                                                            data-type="{{$category->category_type}}"
                                                                            data-date="{{$category->category_date}}"
                                                                            >
                                                                            {{$category->category_date}} > {{$category->parent_category_name }} > {{$category->category_name}}
                                                                        </option>
                                                                    @endforeach
                                                                    @endif    
                                                                    </select>
                                                                </td>
                                                                <td width="15%"><input type="date" max="2999-12-31"name="show_date[]" id="show_date1" class="form-control"></td>
                                                                <td width="15%"><input type="date" max="2999-12-31"name="start_date[]" value="2021-08-15" id="start_date1" class="form-control" required></td>
                                                                <td width="15%"><input type="date" max="2999-12-31"name="end_date[]" value="2021-08-15" id="end_date1" class="form-control" required></td>
                                                                <td width="10%"><input type="number" value="1" min="1" max="99" step="1" name="postion[]" id="postion1" class="form-control" required></td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                    <input type="hidden" name="total_item" id="total_item" value="1">
                                                    <input type="hidden" name="total_list" id="total_list" value="1">
                                                    <div align="right">
                                                        <button type="button" name="add_row" id="add_row" class="btn btn-success btn-xs">+</button>
                                                    </div> 
                                                            </div>  
                                                        </div>
                                                    </div>    
                                                </div>
                                            </div>
                                            <br>
                                            <button type="submit" class="btn btn-success waves-effect waves-light float-right"><i class="fa fa-spinner fa-spin d-none" tabindex="20"></i> Save</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>        
    </div>
</section>
</div>
<script src="{{ asset('js/jquery.form.js')}}"></script>

<script type="text/javascript">
function setShowDate(setdate){
    var selected_days_arr = [];
    var show_date = $('select#sub_category'+setdate).find(':selected').data('date');
    $('#show_date'+setdate).val(show_date); 
    $('.category-select-unique').each(function(index,val){
        selected_days_arr.push($(val).find(':selected').val());
    })
    for (let index = 0; index < 15; index++) {
      if(index != setdate){
        $('select#sub_category'+index).children('option').attr('disabled', false);
        $.each(selected_days_arr,function(i,val){
          if($('select#sub_category'+index).length > 0){
            $('select#sub_category'+index).children('option[value="'+val+'"]').not(':selected').attr('disabled', true);
          }
        });
      }
    }
    $(".select2").select2({
        width: "100%"
    });
}

jQuery(document).ready(function() { 
    setTimeout(function(){
        jQuery('i.fa.fa-bars').trigger('click');
    },1000);

    var count  = 1;
    $(document).on('click', '#add_row', function(){
          var count= $('#total_item').val();
          var list= $('#total_list').val();
          
        if(list < 15){
            count++;
            list++;
            
            $('#total_item').val(count);
            $('#total_list').val(list);
            var html_code = '';

            html_code += '<tr id="row_id_'+count+'">';
            html_code += '<td width="1%" style="padding:0px;"><button type="button" name="remove_row" id="'+count+'" class="btn btn-danger btn-xs remove_row">X</button></td>';
            /*Category List*/
            html_code += '<td width="35%" style="width: 200px !important;"><select onchange="setShowDate('+count+')" required class="select2 category-select-unique form-control saveData" id="sub_category'+count+'" data-srno="'+count+'" name="sub_category[]" ><option selected="selected" disabled="">Select Category</option>@foreach($category_list as $key=>$category)<option value="{{$category->category_id}}"   data-type="{{$category->category_type}}" data-date="{{$category->category_date}}" > {{$category->category_date.' > '.$category->parent_category_name.' > '.$category->category_name }}</option>@endforeach</select>';
            /*show_date*/
            html_code += '<td width="15%"><input type="date" max="2999-12-31"name="show_date[]" id="show_date'+count+'" data-srno="'+count+'" class="form-control saveData input-sm"/></td>';
             
            /*start_date*/
            html_code += '<td width="15%"><input  type="date" max="2999-12-31"name="start_date[]" value="2021-08-15" id="start_date'+count+'" data-srno="'+count+'" class="form-control saveData input-sm" required/></td>';
            /*end_date*/
            html_code += '<td width="15%"><input  type="date" max="2999-12-31"name="end_date[]" value="2021-08-15" id="end_date'+count+'" data-srno="'+count+'" class="form-control saveData input-sm" required/></td>';
            /*postion*/
            html_code += '<td width="10%"><input type="number" min="1" max="99" step="1" value="'+count+'" name="postion[]" id="postion'+count+'" data-srno="'+count+'" class="form-control saveData input-sm" required/></td>';

            html_code += '</tr>';
            

            $('#dashboard-sub-category-table').append(html_code);

            $('.category-select-unique').each(function(index,val){
                $('#sub_category'+count).children('option[value="'+$(val).find(':selected').val()+'"]').attr('disabled', true);
            });
            
            $('#sub_category'+count).focus();
            
            /*PD Init Select2 Starts*/
            ! function(e) {
                "use strict";
                var t = function() {};
                t.prototype.init = function() {
                    e(".select2").select2({
                        width: "100%"
                    }) 
                }, e.AdvancedForm = new t, e.AdvancedForm.Constructor = t
              }(window.jQuery),
              function(t) {
                  "use strict";
                  window.jQuery.AdvancedForm.init()
              }();
            /*PD Init Select2 Ends*/
        }else{
            Swal.fire({type: 'warning',title: 'Only 15 items are allowed'});
        }
    });
    /*Remove Row Start*/
    $(document).on('click', '.remove_row', function(){
        var count = $('#total_item').val();
        var list = $('#total_list').val(); 
        list--;
        $('#total_item').val(count);
        $('#total_list').val(list);
       
        var row_id = $(this).attr("id"); 
  
        $('#row_id_'+row_id).remove(); 
        $('#total_item').val(count);

    });
    /*Remove Row End*/
    /*Save Data Start*/
    var dd = {
        beforeSend: function() { 
            $('.fa-spinner').removeClass('d-none');
        },
        uploadProgress: function(event, position, total, percentComplete) { 
        },
        success: function() {},
        complete: function(response) {
            var result = jQuery.parseJSON(response.responseText);
            $('.fa-spinner').removeClass('d-none');
            $('.fa-spinner').addClass('d-none');
            if (result.status == 200) {
                Swal.fire({
                    type: 'success',
                    title: result.message,
                    showConfirmButton: false,
                    timer: 1500
                });
                $('#add_form').trigger("reset");
                $(".select2").val('').trigger('change');
                window.location.reload();
                setTimeout(function(){
                    window.location.href= '{{url($route_name."-master")}}';
                },2000); 
            } else {
                Swal.fire({
                    type: 'warning',
                    title: 'Oops',
                    text: result.message,
                    showConfirmButton: false,
                    timer: 2000
                });
            }
        },
        error: function() { 
        }
    };
    jQuery("#add_form").ajaxForm(dd);
});

</script>

@endsection