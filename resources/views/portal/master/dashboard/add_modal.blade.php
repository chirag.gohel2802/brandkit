<link rel="stylesheet" type="text/css" href="{{asset('assets/indrop/inlancer_drop.css')}}">

<script src="{{ asset('festival_inlancer/vendors/select2/js/select2.min.js')}}"></script> 
<link rel="stylesheet" type="text/css" href="{{asset('festival_inlancer/vendors/select2/css/select2.min.css')}}"> 

<?php 
$page_title = 'Add Dashboard';
$route_name = 'dashboard';
$mode = 'add';
$save_url = url($route_name.'-save');
$model_size = 'modal-lg';
$title = "Dashboard";
 
?>
<style type="text/css"> 
label{
    cursor: pointer; 
}
</style>

<div class="modal right fade {{$mode}}_{{$route_name}}_modal" id="{{$mode}}_{{$route_name}}_modal" tabindex="-1" role="dialog" style="display: none;" aria-hidden="true" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog {{$model_size}}" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title mb-1" id="myModalLabel">{{$page_title}}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body" style="overflow-y:auto;">
                <form class="form add_form" method="post" id="add_form" action="{{$save_url}}" enctype="multipart/form-data">
                    <input type="hidden" name="mode" value="{{$mode}}">
                    @csrf
                    <div id="message"></div>
                    <div class="form-body">
                        <div class="row">
                            <div class="col-md-12 col-lg-12 col-xl-12">
                                <div>
                                    <div class="card-body">
                                        <div id="accordion">
                                            <div class="accordion">
                                                <div class="accordion-header collapsed" role="button" data-toggle="collapse" data-target="#edit-panel-body-1" aria-expanded="true">
                                                    <h4>Step-1 : Dashboards Details</h4>
                                                </div>
                                                <div class="accordion-body collapse show" id="edit-panel-body-1" data-parent="#accordion" style="">
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="row">
                                                                <div class="col-md-12">
                                                                    <div class="form-group">
                                                                        <label for="withdrawinput1">Dashboard Name: <span class="text-danger">(Shown in App)</span> </label>
                                                                        <input type="text" name="dashboard_name" class="form-control" required>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="row">
                                                                <div class="col-md-12">
                                                                    <div class="form-group">
                                                                        <label for="withdrawinput1">Dashboard Postion: </label>
                                                                        <input type="number" name="dashboard_position"  class="form-control" placeholder="Ex. 10 " required>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="row">
                                                                <div class="col-md-12">
                                                                    <div class="form-group">
                                                                        <label for="withdrawinput1">Main Category: </label>
                                                                        <input type="hidden" name="dashboard_parent_category" id="dashboard_parent_category">
                                                                        <select id="parent_category" class="form-control select2" onchange="getSubCategory(this);" required>
                                                                            <option selected disabled>Select Category</option>
                                                                            <option value="all">All Categories</option>
                                                                        @if($category_list) @foreach ($category_list as $key => $value)
                                                                            <option data-type="{{$value->category_type}}" value="{{$value->category_id}}">{{$value->category_name }} - @if($value->category_type==1) Post @else Video @endif</option>
                                                                            
                                                                        @endforeach
                                                                        @endif
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="row">
                                                                <div class="col-md-12">
                                                                    <div class="form-group">
                                                                        <label for="withdrawinput1">Sub Category: (Main category name > sub category name)</label>
                                                                        <select id="sub_category" name="sub_category[]" class="form-control form-control-sm select2" multiple required>
                                                                            <option disabled>Select Sub Category</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="col-md-12  d-none">
                                                            <div class="row">
                                                                <div class="col-md-12">
                                                                    <div class="form-group">
                                                                        <label for="withdrawinput1">Dashboard Link: </label>
                                                                        <input type="text" name="dashboard_url" value=" " class="form-control">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                        <br>
                                        <button type="button" class="btn btn-outline-danger waves-effect waves-light float-left" data-dismiss="modal" tabindex="99">Close</button>
                                        <button type="submit" class="btn btn-success waves-effect waves-light float-right"><i class="fa fa-spinner fa-spin d-none" tabindex="20"></i> Save</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>


<script src="{{ asset('js/jquery.form.js')}}"></script>
<script src="{{ asset('assets/indrop/inlancer_drop.js')}}"></script>
<script type="text/javascript">
function getSubCategory(parent){
    $('#dashboard_parent_category').val(parent.value);
    var type = parent.options[parent.selectedIndex].dataset.type;
    if (parent.value=='all') {
        var form_data={_token:'{{csrf_token()}}',parent_id:'',type:''}; 
    }else{
        var form_data={_token:'{{csrf_token()}}',parent_id:parent.value,type:type}; 
    }
    jQuery.ajax({
        type: "POST",
        data: form_data,
        url: '{{url("/get-category")}}',
        cache: false,
        success: function(response) {  
          $('#sub_category').empty();
          $('#sub_category').append(response);
            console.log(response);
        }
    });
}
jQuery(document).ready(function() { 
    jQuery('#sub_category').select2();
    var dd = {
        beforeSend: function() { 
            $('.fa-spinner').removeClass('d-none');
        },
        uploadProgress: function(event, position, total, percentComplete) { 
        },
        success: function() {},
        complete: function(response) {
            var result = jQuery.parseJSON(response.responseText);
            $('.fa-spinner').removeClass('d-none');
            $('.fa-spinner').addClass('d-none');
            if (result.status == 200) {
                Swal.fire({
                    type: 'success',
                    title: result.message,
                    showConfirmButton: false,
                    timer: 1500
                });
                jQuery('.modal').modal('hide');
                jQuery('#add_form').trigger("reset");
                {{$route_name}}_tbl._fnAjaxUpdate();
                window.location.reload();
            } else {
                Swal.fire({
                    type: 'warning',
                    title: 'Oops',
                    text: result.message,
                    showConfirmButton: false,
                    timer: 2000
                });
            }
        },
        error: function() { 
        }
    };
    jQuery("#add_form").ajaxForm(dd);
});

/*END NEW*/

</script>