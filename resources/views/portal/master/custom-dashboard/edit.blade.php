@extends('portal.template.app') 
@section('content')  

<?php 
$page_title = 'Edit Custom Dashboard';
$route_name = 'custom-dashboard'; 
$mode = 'edit'; 
$save_url = url($route_name.'-save');
$title = "Custom Dashboard";
 
?> 
<style type="text/css"> 
    textarea{
        height: unset;
    } 
    body.sidebar-mini .main-sidebar .sidebar-menu > li.menu-header {
        padding: 0;
        font-size: 0;
        height: 2px;
        margin: 0;
    }
</style> 
<div class="main-content">
<section class="section">
    <div class="section-header">
        <h1><?php echo $page_title; ?></h1>
        <div class="section-header-breadcrumb d-none">
            <div class="breadcrumb-item active"><a href="<?php echo url('portal');?>">Custom Dashboard</a></div> 
            <div class="breadcrumb-item"><?php echo ucfirst($route_name); ?></div>
        </div>
    </div>
    <div class="section-body">
        <div class="row">
            <div class="col-12"> 
                <div class="card">
                    <form class="form edit_form" method="post" id="edit_form" action="{{$save_url}}" enctype="multipart/form-data">
                        <input type="hidden" name="mode" value="{{$mode}}">
                        <input type="hidden" name="id" value="{{$custom_dashboard_id}}">
                        @csrf
                        <div id="message"></div>
                        <div class="form-body">
                            <div class="row">
                                <div class="col-md-12 col-lg-12 col-xl-12">
                                    <div>
                                        <div class="card-body">
                                            <div id="accordion">
                                                <div class="accordion">
                                                    <div class="accordion-header collapsed" role="button" data-toggle="collapse" data-target="#edit-panel-body-1" aria-expanded="true">
                                                        <h4>Step-1 : Custom Dashboards Details</h4>
                                                    </div>
                                                    <div class="accordion-body collapse show" id="edit-panel-body-1" data-parent="#accordion" style="">
                                                        <div class="row">
                                                            <div class="col-md-4">
                                                                <div class="row">
                                                                    <div class="col-md-12">
                                                                        <div class="form-group">
                                                                            <label for="withdrawinput1">Custom Dashboard Name: <span class="text-danger">(Shown in App)</span> </label>
                                                                            <input type="text" name="custom_dashboard_name" class="form-control" required value="{{$custom_dashboard_name}}">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <div class="row">
                                                                    <div class="col-md-12">
                                                                        <div class="form-group">
                                                                            <label for="withdrawinput1">Custom Dashboard Position: </label>
                                                                            <input type="number" step="1" min="1" max="99" name="custom_dashboard_position"  class="form-control" placeholder="Ex. 10 " required value="{{$custom_dashboard_position}}">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <div class="row">
                                                                    <div class="col-md-12">
                                                                        <div class="form-group">
                                                                            <label for="withdrawinput1">Scroll: </label>
                                                                            <select name="is_horizontal" class="form-control "  required>
                                                                                <?php if($is_horizontal == 1){ ?>
                                                                                <option value="1" selected>Horizontal Scroll ( આડું )</option>
                                                                                <option value="0">Vertical Scroll ( ઊભું )</option>
                                                                                <?php }else{ ?>
                                                                                <option value="1" >Horizontal Scroll ( આડું )</option>
                                                                                <option value="0" selected>Vertical Scroll ( ઊભું )</option>
                                                                                <?php } ?>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            
                                                        </div>
                                                    </div>
                                                    <div class="accordion-header collapsed" role="button" data-toggle="collapse" data-target="#panel2" aria-expanded="false">
                                                        <h4>Step-2 : Custom Dashboard Category</h4>
                                                    </div>
                                                    <div class="accordion-body collapse show" id="panel2" data-parent="#accordion" style="">
                                                        <div class="row">
                                                            <div class="table-responsive  col-sm-12">
                                                    <table id="custom-dashboard-sub-category-table" class="table table-bordered" style="text-align: center;">
                                                        <thead>
                                                            <tr> 
                                                                <th width="1%">#</th>
                                                                <th width="35%">Category<br><span class="text-danger">(Shown in App)</span></th>
                                                                <th width="15%" class="text-right"><center>Show Date<br><span class="text-danger">(Shown in App)</span></center></th>
                                                                <th width="15%" class="text-right"><center>Start Date</center></th>
                                                                <th width="15%" class="text-right"><center>End Date</center></th>
                                                                <th width="15%" class="text-right"><center>Position</center></th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            @if(!empty($custom_upcoming))
                                                            @foreach ($custom_upcoming as $key => $value)
                                                                
                                                            <tr id="row_id_{{$key+1}}">
                                                                <td width="1%" style="padding:0px;">
                                                                    <button type="button" name="remove_row" id="{{$key+1}}" class="btn btn-danger btn-xs remove_row">X</button>  
                                                                </td>
                                                                <td width="35%" style="width: 200px !important;">
                                                                    <select class="select2 form-control saveData" id="sub_category{{$key+1}}" name="sub_category[]" onchange="setShowDate({{$key+1}})" required>
                                                                        <option selected="selected" disabled="">Select Category</option>
                                                                        @if(!empty($category_list))
                                                                        @foreach($category_list as $k=>$category) 
                                                                            <option  
                                                                            @if($value->custom_upcoming_category_id == $category->category_id) selected="selected" @endif 
                                                                                value="{{$category->category_id}}"  
                                                                                data-type="{{$category->category_type}}"
                                                                                data-date="{{$category->category_date}}" >
                                                                                {{$category->category_date}} > {{$category->parent_category_name }} > {{$category->category_name}}
                                                                            </option>
                                                                        @endforeach
                                                                        @endif    
                                                                    </select>
                                                                </td>
                                                                <td width="15%"><input type="date" max="2999-12-31"name="show_date[]" id="show_date{{$key+1}}" class="form-control" value="{{$value->custom_upcoming_show_date}}" ></td>
                                                                <td width="15%"><input type="date" max="2999-12-31"name="start_date[]" id="start_date{{$key+1}}" class="form-control" value="{{$value->custom_upcoming_start_date}}" required></td>
                                                                <td width="15%"><input type="date" max="2999-12-31"name="end_date[]" id="end_date{{$key+1}}" class="form-control" value="{{$value->custom_upcoming_end_date}}" required></td>
                                                                <td width="10%"><input type="number" min="1" max="99" step="1"  name="postion[]" id="postion{{$key+1}}" class="form-control" value="{{$value->custom_upcoming_postion}}" required></td>
                                                            </tr>
                                                            @endforeach 
                                                                <input type="hidden" name="total_item" id="total_item" value="{{count($custom_upcoming)}}">
                                                                <input type="hidden" name="total_list" id="total_list" value="{{count($custom_upcoming)}}">
                                                            @else
                                                                <input type="hidden" name="total_item" id="total_item" value="0">
                                                                <input type="hidden" name="total_list" id="total_list" value="0">
                                                            @endif
                                                        </tbody>
                                                    </table>
                                                    <div align="right">
                                                        <button type="button" name="add_row" id="add_row" class="btn btn-success btn-xs">+</button>
                                                    </div> 
                                                            </div>  
                                                        </div>
                                                    </div>    
                                                </div>
                                            </div>
                                            <br>
                                            <button type="submit" class="btn btn-success waves-effect waves-light float-right"><i class="fa fa-spinner fa-spin d-none" tabindex="20"></i> Save</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>        
    </div>
</section>
</div>
<script src="{{ asset('js/jquery.form.js')}}"></script>

<script type="text/javascript">

function setShowDate(setdate){
    var show_date = $('select#sub_category'+setdate).find(':selected').data('date');
    $('#show_date'+setdate).val(show_date); 
}

jQuery(document).ready(function() { 
    setTimeout(function(){
        jQuery('i.fa.fa-bars').trigger('click');
    },500);
    
    var count  = 1;
    $(document).on('click', '#add_row', function(){
          var count= $('#total_item').val();
          var list= $('#total_list').val();
          
        if(list < 15){
            count++;
            list++;
            
            console.log(count);

            $('#total_item').val(count);
            $('#total_list').val(list);
            var html_code = '';

            html_code += '<tr id="row_id_'+count+'">';
            html_code += '<td width="1%" style="padding:0px;"><button type="button" name="remove_row" id="'+count+'" class="btn btn-danger btn-xs remove_row">X</button></td>';
            /*Category List*/
            html_code += '<td width="35%" style="width: 200px !important;"><select onchange="setShowDate('+count+')" required class="select2 form-control saveData" id="sub_category'+count+'" data-srno="'+count+'" name="sub_category[]" ><option selected="selected" disabled="">Select Category</option>@foreach($category_list as $key=>$category)<option value="{{$category->category_id}}"   data-type="{{$category->category_type}}" data-date="{{$category->category_date}}">{{$category->category_date.' > '.$category->parent_category_name.' > '.$category->category_name }}</option>@endforeach</select>';
             
            /*show_date*/
            html_code += '<td width="15%"><input type="date" max="2999-12-31"name="show_date[]" id="show_date'+count+'" data-srno="'+count+'" class="form-control saveData input-sm" /></td>';
            /*start_date*/
            html_code += '<td width="15%"><input  type="date" max="2999-12-31"name="start_date[]" value="2021-08-15" id="start_date'+count+'" data-srno="'+count+'" class="form-control saveData input-sm" required/></td>';
            /*end_date*/
            html_code += '<td width="15%"><input  type="date" max="2999-12-31"name="end_date[]" value="2021-08-15" id="end_date'+count+'" data-srno="'+count+'" class="form-control saveData input-sm" required/></td>';
            /*postion*/
            html_code += '<td width="10%"><input type="number" min="1" max="99" step="1" value="'+count+'" name="postion[]" id="postion'+count+'" data-srno="'+count+'" class="form-control saveData input-sm" required/></td>';

            html_code += '</tr>';
            

            $('#custom-dashboard-sub-category-table').append(html_code);
            
            $('#sub_category'+count).focus();
            
            /*PD Init Select2 Starts*/
            ! function(e) {
                "use strict";
                var t = function() {};
                t.prototype.init = function() {
                    e(".select2").select2({
                        width: "100%"
                    }) 
                }, e.AdvancedForm = new t, e.AdvancedForm.Constructor = t
              }(window.jQuery),
              function(t) {
                  "use strict";
                  window.jQuery.AdvancedForm.init()
              }();
            /*PD Init Select2 Ends*/
        }else{
            Swal.fire({type: 'warning',title: 'Only 15 items are allowed'});
        }
    });
    /*Remove Row Start*/
    $(document).on('click', '.remove_row', function(){
        var count = $('#total_item').val();
        var list = $('#total_list').val(); 
        list--;
        $('#total_item').val(count);
        $('#total_list').val(list);
       
        var row_id = $(this).attr("id"); 
  
        $('#row_id_'+row_id).remove(); 
        $('#total_item').val(count);

    });
    /*Remove Row End*/
    /*Save Data Start*/
    var dd = {
        beforeSend: function() { 
            $('.fa-spinner').removeClass('d-none');
        },
        uploadProgress: function(event, position, total, percentComplete) { 
        },
        success: function() {},
        complete: function(response) {
            var result = jQuery.parseJSON(response.responseText);
            $('.fa-spinner').removeClass('d-none');
            $('.fa-spinner').addClass('d-none');
            if (result.status == 200) {
                Swal.fire({
                    type: 'success',
                    title: result.message,
                    showConfirmButton: false,
                    timer: 1500
                });
                $('#edit_form').trigger("reset");
                $(".select2").val('').trigger('change');
                window.location.reload();
                setTimeout(function(){
                    window.location.href= '{{url($route_name."-master")}}';
                },2000); 
            } else {
                Swal.fire({
                    type: 'warning',
                    title: 'Oops',
                    text: result.message,
                    showConfirmButton: false,
                    timer: 2000
                });
            }
        },
        error: function() { 
        }
    };
    jQuery("#edit_form").ajaxForm(dd);
});

</script>

@endsection