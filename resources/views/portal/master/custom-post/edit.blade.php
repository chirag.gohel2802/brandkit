@extends('portal.template.app') 
@section('content')

<link rel="stylesheet" type="text/css" href="{{asset('assets/indrop/inlancer_drop.css')}}">
<script src="{{asset('festival_inlancer/js/wz_dragdrop.js')}}"></script>

<?php 
$page_title = 'Edit Custom Post';
$route_name = 'custom-post'; 
$mode = 'edit';
$save_url = url($route_name.'-save');
$model_size = 'modal-lg';

?> 
<style type="text/css">  
    textarea{
        height: unset;
    } 
    #setup_image{
        pointer-events: none;
    }
    [width-100]{
        width: max-content !important;
        cursor: move !important;
    }
    .width-60-60{
        width: 60px !important;
        height: 60px !important;
    }
    .section .section-title:before {
        content: ' ';
        border-radius: 5px;
        height: 8px; 
        width: 30px;
        background-color: #70b8ee;
        display: inline-block;
        float: left;
        margin-top: 6px;
        margin-right: 15px;
    }
</style>
<div class="main-content">
    <section class="section">
        <div class="section-header">
            <h1><?php echo $page_title; ?></h1>
            <div class="section-header-breadcrumb">
                <div class="breadcrumb-item active"><a href="<?php echo url('portal');?>">Dashboard</a></div> 
                <div class="breadcrumb-item"><?php echo ucfirst($route_name); ?></div>
            </div>
        </div>
        <div class="section-body">
            <div class="row">
                <div class="col-12"> 
                    <div class="card">
                        <form class="form edit_form" method="post" id="edit_form" action="{{$save_url}}" enctype="multipart/form-data">
                            <input type="hidden" name="mode" value="{{$mode}}">
                            <input type="hidden" name="id" value="{{$post_id}}">
                            @csrf
                            <div id="message"></div> 
                            <div class="form-body">
                                <div class="row">
                                    <div class="col-md-12 col-lg-12 col-xl-12 pb-5">
                                        <div class="card-body">
                                            <div id="accordion">
                                                <div class="accordion">
                                                    <div class="accordion-header collapsed" role="button" data-toggle="collapse" data-target="#edit-panel-body-1" aria-expanded="true">
                                                        <h4>Step-1 : Post Basic Info</h4>
                                                    </div>
                                                    <div class="accordion-body collapse show" id="edit-panel-body-1" data-parent="#accordion" style="">
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="row">
                                                                    <div class="col-md-12">
                                                                        <div class="form-group">
                                                                            <label for="withdrawinput1">Post Sub Category: </label>
                                                                            <select name="post_category" required class="form-control select2">
                                                                                <option value="" selected disabled>Select Sub Category</option>
                                                                                <?php 
                                                                                if(!empty($category_list)){
                                                                                    foreach ($category_list as $category) { 
                                                                                        $selected = '';
                                                                                        if($category->category_id == $post_category){
                                                                                            $selected = 'selected';
                                                                                        } ?>
                                                                                        <option value="{{$category->category_id}}" {{$selected}}>
                                                                                            {{$category->category_date}} > {{$category->parent_category_name }} > {{$category->category_name}}
                                                                                        </option>
                                                                                <?php } } ?>
                                                                            </select> 

                                                                        </div> 
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="row">
                                                                    <div class="col-md-12">
                                                                        <div class="form-group">
                                                                            <label for="withdrawinput1">Post Name: </label>
                                                                            <input type="text" name="post_name" id="post_name" class="form-control" value="{{$post_name}}">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <div class="row">  
                                                                    <div class="col-md-12">
                                                                        <div class="form-group">
                                                                            <label for="withdrawinput1">Post Date: </label>
                                                                            <input type="date" name="post_date" id="post_date" class="form-control" value="{{date('Y-m-d'),strtotime($post_date)}}">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <div class="row">  
                                                                    <div class="col-md-12">
                                                                        <div class="form-group">
                                                                            <label for="withdrawinput1">Post Package: </label>
                                                                             <select name="post_package" class="form-control select2">
                                                                                <?php if(!empty($post_package)  && $post_package == 'free'){ ?>
                                                                                    <option value="free" selected >Free</option>
                                                                                    <option value="premium">Premium</option>
                                                                                <?php }else{ ?>
                                                                                    <option value="premium" selected>Premium</option>
                                                                                    <option value="free"  >Free</option>

                                                                                <?php } ?>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <div class="row">  
                                                                    <div class="col-md-12">
                                                                        <div class="form-group">
                                                                            <label for="withdrawinput1">Post Type: </label>
                                                                             <select name="post_type" class="form-control select2">
                                                                                <?php if(!empty($post_type)  && $post_type == 'festival'){ ?>
                                                                                    <option value="festival" selected >Festival</option>
                                                                                    <option value="incident">Incident</option>
                                                                                <?php }else{ ?>
                                                                                    <option value="incident" selected>Festival</option>
                                                                                    <option value="festival">Incident</option>

                                                                                <?php } ?>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="row">
                                                                    <div class="col-md-12">
                                                                        <div class="form-group">
                                                                            <label for="withdrawinput1">Post Search Keyword: </label>
                                                                            <textarea name="post_search_keyword" class="form-control">{{$post_search_keyword}}</textarea>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6 d-none">
                                                                <div class="row">
                                                                    <div class="col-md-12"> 
                                                                        <div class="form-group">
                                                                            <label for="withdrawinput1">Post Image Alt Tag: </label>
                                                                            <input type="text" name="iati1" class="form-control" value="{{$image_alt_tag}}">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <!-- post_image -->
                                                            <div class="col-md-6">
                                                                <div id="i1" class="drop-area" data-throwback="storePerformance" onclick="document.getElementById('fileElem1').click()"> 
                                                                    <div class="text-center" id="imgPreviewi1">
                                                                      <i class="bi bi-cloud-arrow-up mb-3" style="color:#445dbe;font-size: 60px;"></i>
                                                                      <h3 style="color: #445dbe;font-size: 18px;line-height: 100px;">Click "Here" or drop your image here<span style="color:red">*</span></h3>
                                                                    </div>
                                                                    <input class="d-none" type="file" id="fileElem1" accept="" onchange="handleFiles(this.files,'i1','storePerformance')">   
                                                                </div>
                                                              <progress id="progress-bar" class="d-none" max=100 value=0></progress>
                                                            </div>
                                                            <input type="hidden" id="post_image" name="post_image" data-image="i1" value="{{$post_image}}">
                                                            <!-- post_image -->
                                                        </div>
                                                        <input type="hidden" name="custom_post_json" id="json_aya_muko" value="">
                                                        <div class="row mb-4">
                                                                <div class="col-4">
                                                                    <input id="check_all" type="checkbox" name="all_check" onchange="toggle_check(this)">
                                                                    <label for="check_all">Check all </label>
                                                                </div>
                                                                <div class="col-4">
                                                                </div>
                                                                <div class="col-3 d-flex justify-content-between">
                                                                    <button type="button" onclick="copy_logo()" class="btn btn-primary">Add New Logo</button>
                                                                    <button type="button" onclick="copy_text()" class="btn btn-primary">Add New Text</button>
                                                                </div>
                                                        </div>

                                                        <div class="row">
                                                            <div class="col-md-6 main-sec">
                                                            <?php 
                                                                $temp_arr =json_decode($custom_post_json);
                                                                $logo_count = 0;
                                                                $text_count = 0;
                                                                foreach($temp_arr as $val){
                                                                    $inc_counter = '';
                                                                    if($val->type == 'logo'){
                                                                        $lable_text = 'logo';
                                                                        if($logo_count >= 1){
                                                                            $lable_text .= ' '.$logo_count;
                                                                            $inc_counter = $logo_count;
                                                                        }
                                                                        $logo_count++;
                                                                    }
                                                                    if($val->type == 'text'){
                                                                        $lable_text = 'text';
                                                                        if($text_count >= 1){
                                                                            $lable_text .= ' '.$text_count;
                                                                            $inc_counter = $text_count;
                                                                        }
                                                                        $text_count++;
                                                                    }
                                                            ?>
                                                                <h4 class="section-title text-capitalize">{{$lable_text}} 
                                                                </h4>
                                                                <div class="row" data-frame="{{$val->type}}" style=" margin-top: -37px; ">
                                                                    <div class="col-md-3 pr-0">
                                                                        <?php 
                                                                        $class_name = 'visible';
                                                                        $checked = 'checked';
                                                                        if(empty($val->value[0]->value)){
                                                                            $class_name = 'invisible';
                                                                            $checked = '';
                                                                        }

                                                                        ?>
                                                                        <div class="form-group d-flex align-items-center float-right m-0">
                                                                            <input type="checkbox" id="{{$val->type}}" onchange="checkbox_check(this)" <?php echo $checked; ?> class="form-check-input" > 
                                                                            <label for="{{$val->type}}" class="m-0 pt-1">Show</label>
                                                                            <input type="number" min="0" max="1" 
                                                                            data-name="show" value="<?php echo $val->value[0]->value ?>" class="d-none" 
                                                                            onchange="move_this_feild('show',$(this).val(),'img_{{$val->type.$inc_counter}}')" >
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-3 pr-0 <?php echo $class_name ?>" data-show="{{$val->type.$inc_counter}}">
                                                                        <div class="form-group d-flex align-items-center m-0">
                                                                            <label class="m-0 ml-2" for="withdrawinput1">Top</label>
                                                                            <input type="number" 
                                                                            min="0" max="504" data-name="top" 
                                                                            value="<?php echo $val->value[1]->value ?>" class="form-control mx-2 p-2" 
                                                                            onchange="move_this_feild('top',$(this).val(),'img_{{$val->type.$inc_counter}}')" 
                                                                            onkeyup="move_this_feild('top',$(this).val(),'img_{{$val->type.$inc_counter}}')" 
                                                                            >
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-3 pr-0 <?php echo $class_name ?>" data-show="{{$val->type.$inc_counter}}">
                                                                        <div class="form-group d-flex align-items-center m-0">
                                                                            <label class="m-0 ml-2" for="withdrawinput1">Left</label>
                                                                            <input type="number" min="0" max="504" data-name="left" value="<?php echo $val->value[2]->value ?>" class="form-control mx-2 p-2"
                                                                            onchange="move_this_feild('left',$(this).val(),'img_{{$val->type.$inc_counter}}')" 
                                                                            onkeyup="move_this_feild('left',$(this).val(),'img_{{$val->type.$inc_counter}}')" 
                                                                            >
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-3 pr-0 <?php echo $class_name ?>" data-show="{{$val->type.$inc_counter}}">
                                                                        <div class="form-group d-flex align-items-center m-0">
                                                                            <label class="m-0 ml-2" for="withdrawinput1">Rotate</label>
                                                                            <input type="number" min="0" max="360" data-name="rotate" value="<?php echo $val->value[3]->value ?>" class="form-control mx-2 p-2" 
                                                                            onchange="move_this_feild('rotate',$(this).val(),'img_{{$val->type.$inc_counter}}')" 
                                                                            onkeyup="move_this_feild('rotate',$(this).val(),'img_{{$val->type.$inc_counter}}')" 
                                                                            >
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-5 pr-0 mt-3 px-0 <?php echo $class_name ?>" data-show="{{$val->type.$inc_counter}}">
                                                                        <div class="form-group d-flex align-items-center m-0">
                                                                            <label class="m-0 ml-2" for="withdrawinput1">Change label</label>
                                                                            <?php 
                                                                                if($val->type == 'text'){
                                                                                    $lable_text = 'Add text';
                                                                                }
                                                                                if($val->type == 'logo'){
                                                                                    $lable_text = 'logo';
                                                                                }
                                                                            

                                                                             ?>
                                                                            <input type="text" value="{{$lable_text.' '.$inc_counter}}" class="form-control mx-2 p-2" 
                                                                            onchange="move_this_feild('font-text',$(this).val(),'img_{{$val->type.$inc_counter}}')" 
                                                                            onkeyup="move_this_feild('font-text',$(this).val(),'img_{{$val->type.$inc_counter}}')" 
                                                                            >
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-3 pr-0 mt-3 ml-auto <?php echo $class_name ?>" data-show="{{$val->type.$inc_counter}}">
                                                                        <div class="form-group d-flex align-items-center m-0">
                                                                            <label class="m-0 ml-2" for="withdrawinput1">Size</label>
                                                                            <input type="number" min="0" max="360" data-name="font-size" value="<?php echo $val->value[4]->value ?>" class="form-control mx-2 p-2" 
                                                                            onchange="move_this_feild('font-size',$(this).val(),'img_{{$val->type.$inc_counter}}')" 
                                                                            onkeyup="move_this_feild('font-size',$(this).val(),'img_{{$val->type.$inc_counter}}')" 
                                                                            >
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-3 pr-0 mt-3 ml-auto <?php echo $class_name ?>" data-show="{{$val->type.$inc_counter}}">
                                                                        <div class="form-group d-flex align-items-center m-0">
                                                                            <label class="m-0 ml-2" for="withdrawinput1">Color</label>
                                                                            <input type="color"
                                                                            data-name="color" 
                                                                            value="<?php echo $val->value[5]->value ?>" 
                                                                            class="form-control mx-2 p-2" 
                                                                            onchange="move_this_feild('color',$(this).val(),'img_{{$val->type.$inc_counter}}')" 
                                                                            oninput="move_this_feild('color',$(this).val(),'img_{{$val->type.$inc_counter}}')" 
                                                                            >
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-12">
                                                                        <hr>
                                                                    </div>
                                                                </div>
                                                            <?php } ?>
                                                            </div>
                                                                
                                                            <div class="col-md-6 m-0 p-0" id="edit_image_preview" style="max-height: 504px;max-width: 504px;min-height: 504px;min-width: 504px;">
                                                                
                                                                <span style="position: absolute;" class="d-none1" id="image_setup_section">
                                                                <?php 
                                                                $temp_arr =json_decode($custom_post_json);
                                                                $logo_count = 0;
                                                                $text_count = 0;

                                                                foreach($temp_arr as $val){
                                                                    $css_property = 'position: absolute;';
                                                                    $font_size = 'font-size:12px;';
                                                                    $border_size = 'border: 2px solid;line-height: 70%;';
                                                                    $class = '';
                                                                    
                                                                    if($val->type == 'text'){
                                                                        $font_size = 'font-size:12px;width: max-content;';
                                                                    }elseif($val->type == 'logo'){
                                                                        $font_size = 'width: 60px; height:60px;'; //logo - 60x60dp
                                                                        $class .= "width-60-60";
                                                                    }
                                                                    if(!empty($val->value)){
                                                                        foreach($val->value as $sub_val){
                                                                            if($sub_val->name == 'show' && $sub_val->value == 0){
                                                                                $css_property .= 'display:none';
                                                                            }
                                                                            if($sub_val->name == 'top'){
                                                                                $css_property .= 'top:'.$sub_val->value.'px;';
                                                                            }
                                                                            if($sub_val->name == 'left'){
                                                                                $css_property .= 'left:'.$sub_val->value.'px;';
                                                                            }
                                                                            if($sub_val->name == 'rotate'){
                                                                                $css_property .= ' transform: rotate('.$sub_val->value.'deg);';
                                                                            }
                                                                            if($sub_val->name == 'color'){
                                                                                $css_property .= ' color: '.$sub_val->value.';';
                                                                            }
                                                                            if($sub_val->name == 'font-size'){
                                                                                $css_property .= ' font-size: '.$sub_val->value.'px;';
                                                                            }
                                                                        }
                                                                    }
                                                                    $css_property = $css_property.$font_size.$border_size;
                                                                ?>
                                                                <?php 
                                                                    $inc_counter = '';
                                                                    if($val->type == 'logo'){
                                                                        $lable_text = 'logo';
                                                                        if($logo_count >= 1){
                                                                            $lable_text .= ' '.$logo_count;
                                                                            $inc_counter = $logo_count;
                                                                        }
                                                                        $logo_count++;
                                                                    }
                                                                    if($val->type == 'text'){
                                                                        $lable_text = 'Add text';
                                                                        if (str_contains($css_property, 'display:none')){
                                                                            $css_property .= ' padding-bottom: 8px';
                                                                        }

                                                                        if($text_count >= 1){
                                                                            $lable_text .= ' '.$text_count;
                                                                            $inc_counter = $text_count;
                                                                        }
                                                                        $text_count++;
                                                                    }
                                                                 ?>
                                                                    <span 
                                                                    width-100 
                                                                    onselectstart="return false;"
                                                                    onmouseout="set_drag_values(this,'{{$val->type.$inc_counter}}')" 
                                                                    id="img_{{$val->type.$inc_counter}}"
                                                                    data-type="{{$val->type}}"
                                                                    class="text-capitalize <?php echo $class ?>"
                                                                    style="<?php echo $css_property; ?>"  
                                                                    >{{$lable_text}}</span>

                                                                <?php } ?>

                                                                </span>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                            <br>
                                            <button type="submit" class="btn btn-success waves-effect waves-light float-right"><i class="fa fa-spinner fa-spin d-none" tabindex="20"></i>Save</button>
                                        </div>
                                    </div>
                                </div> 
                            </div>
                        </form>
                    </div>
                </div>
            </div>        
        </div>
    </section>
</div>
<script src="{{ asset('js/jquery.form.js')}}"></script>
<script src="{{ asset('assets/indrop/inlancer_drop.js')}}"></script>

<script type="text/javascript">

$(function() {
    $('input[type="number"]').on('keyup', function() {
        var el = $(this),
            val = Math.max((0, el.val())),
            max = parseInt(el.attr('max'));
        el.val(isNaN(max) ? val : Math.min(max, val));
        el.trigger('change');
  });
});

var image = '{{$image_name}}';
var file_id1 = 'i1';  
if(typeof image!=='undefined' && image!=='') {
    var isrc = APPLICATION_URL+'/assets/upload/images/thumb/'+image;
    $('#imgPreview'+file_id1).empty();
    var img = '<img onerror="setImage(this);"  class="previewImage" src="'+isrc+'">';  
    var div = '<div class="col-12"><div style="padding: 5px;margin-bottom: 8px;">'+img+'</div></div>';
    $('#imgPreview'+file_id1).append(div);

    var isrc = APPLICATION_URL+'/assets/upload/images/original/'+image;
    var img = '<img onerror="setImage(this);" id="setup_image" src="'+isrc+'" style="object-fit:contain; width: 504px; height: 504px; border: 2px solid black; ">';  
    $('#edit_image_preview').append(img);

    $('input[type="number"]').trigger('change');
}

function setImage(img){
    img.src="{{url('/assets/img/placeholder.png');}}";  
    img.style.height="80px";
}
function storePerformance(file_id,returnData) {
    $("[data-image='"+file_id+"']").val(returnData.image_id);

    if($("#setup_image").length > 0){
        $("#setup_image").attr('src',$(".previewImage").attr("src"));
    }else{
        var $img = $(".previewImage").clone();
        $("#edit_image_preview").append($img);
        $("#edit_image_preview").find('img').attr('id', 'setup_image');
    }
    $("#setup_image").css({
        "width": '504px',
        "height": '504px',
        "border": '2px solid black',
    });
}

jQuery(document).ready(function() { 


    var dd = {
        beforeSend: function() { 
            $('.fa-spinner').removeClass('d-none');
            save_json_data();
        },
        uploadProgress: function(event, position, total, percentComplete) { 
        },
        success: function() {},
        complete: function(response) {
            var result = jQuery.parseJSON(response.responseText);
            $('.fa-spinner').removeClass('d-none');
            $('.fa-spinner').addClass('d-none');
            if (result.status == 200) {
                Swal.fire({
                    type: 'success',
                    title: result.message,
                    showConfirmButton: false,
                    timer: 1500
                });
                setTimeout(function(){
                    // window.location.href= '{{url($route_name."-master")}}';
                    window.location.reload();
                },2000);
            } else {
                Swal.fire({
                    type: 'warning',
                    title: 'Oops',
                    text: result.message,
                    showConfirmButton: false,
                    timer: 2000
                });
            }
        },
        error: function() { 
        }
    };

    setTimeout(function(){
        jQuery("#edit_form").ajaxForm(dd);
    },1000);
});

async function save_json_data(){
    var json_data = [];
    $('[data-frame]').each(function(index, el) {
        json_temp_arr = [];
        $(el).find('input').each(function(sub_index, sub_el) {
            if($(sub_el).attr('type') == 'number' || $(sub_el).attr('type') == 'color'){
                temp_arr = {
                    name : $(sub_el).attr('data-name'),
                    value : $(sub_el).val()
                };
                json_temp_arr.push(temp_arr);
            }
        });
        parent_json = {
            type:  $(el).attr('data-frame'),
            value: json_temp_arr
        }
        json_data.push(parent_json);
    });
    final_data = JSON.stringify(json_data);
    $('#json_aya_muko').val(final_data);
}

function checkbox_check(passed_this){
    var div_id = $(passed_this).attr('id');
    var input_hidden = $(passed_this).parent().find('[data-name="show"]');
    if($(passed_this).is(':checked')){
        input_hidden.val('1');
        $('[data-show="'+div_id+'"]').removeClass('invisible');
    }else{
        input_hidden.val('0');
        $('[data-show="'+div_id+'"]').addClass('invisible');
    }
    input_hidden.trigger('change');
    save_json_data();
    $('#image_setup_section').find('span').each(function(index, el) {        
        id = $(el).attr('id');
        ADD_DHTML(id);
    });
}

function move_this_feild(property = '', passed_value = '', passed_id = ''){
    if(property == '' || passed_value == '' || passed_id == ''){
        return false;
    }
    if(property == 'show'){
        if(passed_value == 0){
            $('#'+passed_id).css('display','none');
        }
        if(passed_value == 1){
            $('#'+passed_id).css('display','unset');
        }
    }
    if(property == 'top'){
        $('#'+passed_id).css('top',passed_value+'px');
    }
    if(property == 'left'){
        $('#'+passed_id).css('left',passed_value+'px');
    }
    if(property == 'rotate'){
        $('#'+passed_id).css('transform','rotate('+passed_value+'deg)');
    }
    if(property == 'font-size'){
        $('#'+passed_id).css('font-size',passed_value+'px');
    }
    if(property == 'color'){
        $('#'+passed_id).css('color',passed_value);
    }
    if(property == 'font-text'){
        $('#'+passed_id).text(passed_value);
    }
    save_json_data();
}

function set_drag_values(passed_this,target_element){
    var top  =  $(passed_this).css("top");
    var left =  $(passed_this).css("left");
    $('[data-show="'+target_element+'"]').find('[data-name="top"]').val(parseFloat(top));
    $('[data-show="'+target_element+'"]').find('[data-name="left"]').val(parseFloat(left));
    
    setTimeout(function(){save_json_data();},200);

}

function toggle_check(passed_this){
    checked_value = $(passed_this).prop('checked');
    $('.main-sec').find('input[type=checkbox]').each(function(index, el) {
        $(el).prop('checked',checked_value);
        checkbox_check(el);
    });
    $('#image_setup_section').find('span').each(function(index, el) {        
        id = $(el).attr('id');
        ADD_DHTML(id);
    });
}

function copy_logo(){
    var count = $('[data-type="logo"]').length;

    var $new_logo = $('#img_logo').clone()
                                .removeAttr("id")
                                .removeAttr("onmouseout");

    $new_logo.attr("onmouseout","set_drag_values(this,'logo"+count+"')")
            .attr("id","img_logo"+count)
            .text('logo '+count)
            .css('top','40px').css('left','40px');

    $('#image_setup_section').append($new_logo);
    ADD_DHTML("img_logo"+count);


    var $new_logo_data_title = $($('[data-frame="logo"]')[0]).prev().clone().text('logo '+count);
    $('.main-sec').append($new_logo_data_title);
    var $new_logo_data = $($('[data-frame="logo"]')[0]).clone()
    $new_logo_data.find('[data-show]').attr("data-show","logo"+count);
    $new_logo_data.find('[value="Logo"]').attr("value","Logo "+count);
    $new_logo_data.find('[type="checkbox"]').attr("id","logo"+count);
    $new_logo_data.find('[type="checkbox"]').next().attr("for","logo"+count);
    $new_logo_data.find('input').each(function(index, el) {
        var onchange_data = $(el).attr('onchange');
        var img_data = onchange_data.replace('img_logo', "img_logo"+count);
        $(el).attr('onchange',img_data);
        $(el).attr('onkeyup',img_data);
    });
    
    $('.main-sec').append($new_logo_data);
    save_json_data();
}

function copy_text(){
    var count = $('[data-type="text"]').length;

    var $new_text = $('#img_text').clone()
                                .removeAttr("id")
                                .removeAttr("onmouseout");

    $new_text.attr("onmouseout","set_drag_values(this,'text"+count+"')")
            .attr("id","img_text"+count)
            .text('Add text '+count)
            .css('top','230px').css('left','40px');

    $('#image_setup_section').append($new_text);
    ADD_DHTML("img_text"+count);


    var $new_text_data_title = $($('[data-frame="text"]')[0]).prev().clone().text('text '+count);
    $('.main-sec').append($new_text_data_title);
    var $new_text_data = $($('[data-frame="text"]')[0]).clone()
    $new_text_data.find('[data-show]').attr("data-show","text"+count);
    $new_text_data.find('[value="Add text"]').attr("value","Add text "+count);
    $new_text_data.find('[type="checkbox"]').attr("id","text"+count);
    $new_text_data.find('[type="checkbox"]').next().attr("for","text"+count);
    $new_text_data.find('input').each(function(index, el) {
        var onchange_data = $(el).attr('onchange');
        var img_data = onchange_data.replace('img_text', "img_text"+count);
        $(el).attr('onchange',img_data);
        $(el).attr('onkeyup',img_data);
    });
    
    $('.main-sec').append($new_text_data);
    save_json_data();
}

SET_DHTML("img_logo","img_text");
jQuery(document).ready(function($) {
    setTimeout(function(){
        $('#image_setup_section').find('span').each(function(index, el) {        
            id = $(el).attr('id');
            ADD_DHTML(id);
        });
    },1000);
});
</script>

@endsection