<?php 

$page_title = 'Add Role';
$route_name = 'role';
$mode = 'add';
$save_url = url($route_name.'-save');
$model_size = 'modal-md';

?>
<style type="text/css">
label{
    cursor: pointer;
}
</style>
<div class="modal right fade {{$mode}}_{{$route_name}}_modal" id="{{$mode}}_{{$route_name}}_modal" tabindex="-1" role="dialog" style="display: none;" aria-hidden="true" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog {{$model_size}}" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title mb-3" id="myModalLabel">{{$page_title}}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body" style="overflow-y:auto;">
                <form class="form add_form" method="post" id="add_form" action="{{$save_url}}" enctype="multipart/form-data">
                    <input type="hidden" name="mode" value="{{$mode}}">
                    @csrf
                    <div id="message"></div>
                    <div class="form-body">
                        <div class="row">
                            <div class="col-md-12 col-lg-12 col-xl-12">
                                <div >
                                    <div class="card-body">
                                        <div id="accordion">
                                            <div class="accordion">
                                                <div class="accordion-header collapsed" role="button" data-toggle="collapse" data-target="#panel-body-1" aria-expanded="true">
                                                    <h4>Step-1 : Role Details</h4>
                                                </div>
                                                <div class="accordion-body collapse show" id="panel-body-1" data-parent="#accordion" style="">
                                                    <div class="row">

                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <label for="withdrawinput1">Role Name <span style="color: red">*</span></label>
                                                                <input type="text" name="role_name" class="form-control" required>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <label for="withdrawinput1">Role Description </label>
                                                                <textarea class="form-control" name="role_details" rows="10"></textarea>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>

                                                <div class="accordion-header" role="button" data-toggle="collapse" data-target="#panel-body-2" aria-expanded="false">
                                                    <h4> Step- 2 : Role Permissions</h4>
                                                </div>
                                                <div class="accordion-body collapse" id="panel-body-2" data-parent="#accordion" style="">
                                                    <div class="row">
                                                        <?php 
                                                        $i = 0;
                                                        foreach ($module_data as $key => $val) { $i++; ?>
                                                            <?php if($i > 1 && !empty($val->permissions)){ ?>
                                                                <div class="col-md-12"> <hr> </div>
                                                            <?php } ?>

                                                            <div class="col-md-12">
                                                                <?php if(!empty($val->permissions)){ ?>
                                                                    <div class="form-group m-2">
                                                                        <label style="font-size: 16px;"><?php echo $val->module_name; ?> Module</label>
                                                                    </div>
                                                                <?php } ?>

                                                                <div class="form-group ml-4 row">
                                                                    <?php if(!empty($val->permissions)){ ?>

                                                                        <div class="col-12 form-check form-check-inline mb-1">
                                                                            <input class="form-check-input" type="checkbox" id="check_all_{{$i}}" onclick="check_all({{$i}})">
                                                                            
                                                                            <input class="d-none" name="role_module_ids[]" type="checkbox" id="main_check_{{$i}}" value="{{$val->module_id}}">
                                                                            
                                                                            <label class="form-check-label font-weight-bold" for="check_all_{{$i}}" onclick="check_all({{$i}})">Select all</label>
                                                                        </div>

                                                                    <?php $j=0;
                                                                        foreach ($val->permissions as $sub_val) { $j++; ?>
                                                                        <div class="col-3 form-check form-check-inline">
                                                                            <input class="form-check-input" type="checkbox" id="sub_{{$i}}_{{$j}}" value="{{$sub_val->permission_id}}" data-check-group="{{$i}}" name="role_permission_ids[]">
                                                                            <label class="form-check-label" for="sub_{{$i}}_{{$j}}">{{$sub_val->permission_name}}</label>
                                                                        </div>
                                                                    <?php } } ?>

                                                                </div>
                                                            </div>

                                                        <?php } ?>

                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                        <br>
                                        <button type="button" class="btn btn-outline-danger waves-effect waves-light float-left" data-dismiss="modal" tabindex="99">Close</button>
                                        <button type="submit" class="btn btn-success waves-effect waves-light float-right"><i class="fa fa-spinner fa-spin d-none" tabindex="20"></i> Save</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script src="{{ asset('js/jquery.form.js')}}"></script>
<script type="text/javascript">

jQuery(document).ready(function() { 
    var dd = {
        beforeSend: function() { 
            $('.fa-spinner').removeClass('d-none');
        },
        uploadProgress: function(event, position, total, percentComplete) { 
        },
        success: function() {},
        complete: function(response) {
            var result = jQuery.parseJSON(response.responseText);
            $('.fa-spinner').removeClass('d-none');
            $('.fa-spinner').addClass('d-none');
            if (result.status == 200) {
                Swal.fire({
                    type: 'success',
                    title: result.message,
                    showConfirmButton: false,
                    timer: 1500
                });
                jQuery('.modal').modal('hide');
                jQuery('#add_form').trigger("reset");
                {{$route_name}}_tbl._fnAjaxUpdate();
            } else {
                Swal.fire({
                    type: 'warning',
                    title: 'Oops',
                    text: result.message,
                    showConfirmButton: false,
                    timer: 2000
                });
            }
        },
        error: function() { 
        }
    };
    jQuery("#add_form").ajaxForm(dd);
});

function check_all(passed_this){
    setTimeout(function(){
        var is_checked = $('#check_all_'+passed_this).prop('checked');
        console.log(is_checked);
        $('[data-check-group="'+passed_this+'"]').prop('checked', is_checked);
    },50);
}
$('[data-check-group]').change(function(event) {

    main_module_id = $($(this)[0]).attr('data-check-group');
    is_checked = $('[data-check-group="'+main_module_id+'"]:checked').length;

    if(is_checked){
        $('#main_check_'+main_module_id).prop('checked',true);
    }else{
        $('#main_check_'+main_module_id).prop('checked',false);
    }

});

</script>