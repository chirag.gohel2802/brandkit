@extends('portal.template.app') 
@section('content')  

<link rel="stylesheet" type="text/css" href="{{asset('festival_inlancer/modules/select2/dist/css/select2.min.css')}}">

<!-- <div class="modal fade add_user_modal" id="add_user_modal"   role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content" style="border-radius: 15px;">
            <div class="modal-header">
                <h5 class="modal-title mt-0" id="myModalLabel">
                    @if($user_add==1)
                    {{__('label.New User')}}
                    @else 
                    {{__('message.User limit exceeded')}}
                    @endif
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                @if($user_add==1)
                <form class="form add_form" method="post" name="add_form" id="add_user" action="{{url('/save-user')}}" enctype="multipart/form-data">
                    <input type="hidden" name="mode" value="add"> 
                    @csrf
                    <div id="message"></div>
                    <div class="form-body">
                        <div class="row">
                            <div class="col-md-12 col-lg-12 col-xl-12">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="withdrawinput1">{{__('label.Full Name')}} <span style="color: red">*</span></label>
                                                    <input type='text' name="user_name" class="form-control" required>
                                                </div>
                                            </div> 
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="withdrawinput1">{{__('label.Email ID')}}</label>
                                                    <input type='email' name="user_email" class="form-control">
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="withdrawinput1">{{__('label.Phone No')}}</label>
                                                    <input type='tel' name="user_phone_no" class="form-control">
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="withdrawinput1">{{__('label.Password')}}</label>
                                                    <input type='password' autocomplete="new-password" name="user_password" class="form-control">
                                                </div>
                                            </div>
                                            @if(session()->get('my_chacha')['user_role']=='admin')
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="withdrawinput1">{{__('label.Access Roles')}}</label>
                                                    <select onchange="getParents(this.value);" class="form-control" name="user_role_id">
                                                    	<option selected disabled>{{__('label.Select Role')}}</option>
                                                    	<option value="1">{{__('label.Agency')}}</option>
                                                    	<option value="2">{{__('label.Regular')}}</option>  
                                                    </select>
                                                </div>
                                            </div> 
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="withdrawinput1">{{__('label.Assign Templates')}}</label>
                                                    <select multiple onchange="setTemplate();" class="form-control select2" name="user_template_ids[]">
                                                        <option disabled>{{__('label.Select Template')}}</option> 
                                                        @if(!empty($data['templates']))
                                                        @foreach($data['templates'] as $t_key=>$t_value))
                                                            <option value="{{$t_value->template_id}}">{{$t_value->template_name}}</option>
                                                        @endforeach
                                                        @endif
                                                    </select>
                                                </div>
                                            </div> 
                                            @endif
                                            @if($logged_user_role!='admin' && !empty($data['roles']))
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="withdrawinput1">{{__('label.User Group')}}</label>
                                                        <select class="form-control" name="user_role_id">
                                                            <option value="" selected>{{__('label.No Group')}}</option> 
                                                            @foreach($data['roles'] as $key=>$value))
                                                                <option value="{{$value->role_id}}">{{$value->role_name}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>  
                                            @endif


                                            <div  class="col-md-4 d-none" id="parent_users">
                                                <div class="form-group">
                                                    <label for="withdrawinput1">{{__('label.Assign Parent User')}}</label>
                                                    <select class="select2 form-control" name="user_master_id">
                                                        <option selected disabled>{{__('label.Select User')}}</option> 
                                                        @if(!empty($data['parent_users']))
                                                        @foreach($data['parent_users'] as $key=>$value)
                                                        <option value="{{$value->user_id}}">{{$value->user_name}}</option>
                                                        @endforeach
                                                        @endif
                                                    </select>
                                                </div>
                                            </div> 
                                        </div>
                                        <hr>
                                        <div class="row 
                                        @if(session()->get('my_chacha')['user_role']=='admin')
                                        d-none
                                        @endif 
                                        " id="permission_section">
                                            <div class="col-12 col-md-12 col-lg-12">
                                                <div class="card">
                                                  <div class="card-header">
                                                    <h4>{{__('label.Permissions')}}</h4>
                                                  </div>
                                                  <div class="card-body">
                                                    <div class="row">
                                                        @if(!empty($data['access_modules'])) 
                                                        @foreach($data['access_modules'] as $key=>$value)
                                                            <div class="col-md-6" id="{{$value->module_id}}_module">
                                                                <div class="accordion">
                                                                    <div class="accordion-header"  >
                                                                        <h4>{{ucfirst(__('label.'.$value->module_name))}}</h4>
                                                                    </div>
                                                                    <div class="row accordion-body" style="">  
                                                                        <div class="col-12">
                                                                            <select class="select2" multiple="multiple" name="{{$value->module_id}}_module[]">
                                                                                
                                                                                @if(!empty($value->permissions))
                                                                                @foreach($value->permissions as $p_key=>$p_value) 
                                                                                <option value="{{$p_value->permission_id}}">
                                                                                {{__('permission.'.$p_value->permission_name)}}   
                                                                                </option>
                                                                                @endforeach
                                                                                @endif 
                                                                            </select>
                                                                        </div> 

                                                                    </div> 
                                                                </div>
                                                            </div> 
                                                        @endforeach
                                                        @endif

                                                    </div>
                                                  </div>
                                                </div>
                                            </div> 
                                        </div>
                                        <br>
                                        <div class="row" style="float: right;">
                                            <button style="font-size: 15px !important;" type="submit" class="btn btn-success waves-effect waves-light"><i class="fa-pulse fa fa-spinner d-none"></i> {{__('label.Save')}}</button>&nbsp;
                                            <button style="font-size: 15px !important;" type="button" class="btn btn-outline-danger waves-effect waves-light" data-dismiss="modal">{{__('label.Close')}}</button> 
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form> 
                @endif
            </div>
        </div>
    </div>
</div> -->




<script src="{{ asset('festival_inlancer/modules/select2/dist/js/select2.full.min.js')}}"></script>
<script type="text/javascript">
    $('.select2').select2();

    /*function setTemplate()
        {
            console.log('YES');
        } 
    function getParents(type)
        {
            $('#permission_section').removeClass('d-none');
            if (type==2) {
                $('#1_module').addClass('d-none');
                $('#6_module').addClass('d-none'); 
            }else{
                $('#1_module').removeClass('d-none');
                $('#6_module').removeClass('d-none'); 
            } 
        } 
    jQuery(document).ready(function() { 
        var dd = {
            beforeSend: function() { 
                $('.fa-spinner').removeClass('d-none');
            },
            uploadProgress: function(event, position, total, percentComplete) { 
            },
            success: function() {},
            complete: function(response) {
                console.log(response.responseText);
                var result = jQuery.parseJSON(response.responseText);
                $('.fa-spinner').removeClass('d-none');
                $('.fa-spinner').addClass('d-none');
                if (result.status == 200) {
                    Swal.fire({
                        type: 'success',
                        title: 'Successfully saved',
                        showConfirmButton: false,
                        timer: 1500
                    });
                    user_tbl._fnAjaxUpdate();
                    jQuery('#add_user').trigger("reset");
                    jQuery('.modal').modal('hide');
                } else {
                    Swal.fire({
                        type: 'warning',
                        title: 'Oops',
                        text: result.message,
                        showConfirmButton: false,
                        timer: 2000
                    });
                }
            },
            error: function() { 
            }
        }; 
        jQuery("#add_user").ajaxForm(dd); 
    });*/
</script>

@endsection