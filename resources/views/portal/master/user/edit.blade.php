@extends('portal.template.app') 
@section('content')   

<!-- <div class="modal fade edit_user_modal" id="edit_user_modal"   role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content" style="border-radius: 15px;">
            <div class="modal-header">
                <h5 class="modal-title mt-0" id="myModalLabel">{{__('label.Edit User')}}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <form class="form add_form" method="POST" name="edit_form" id="userEdit" action="{{url('/save-user')}}">
                    <input type="hidden" name="mode" value="edit">
                    <input type="hidden" name="id" value="{{$user_id}}">
                    @csrf
                    <div id="message"></div>
                    <div class="form-body">
                        <div class="row">
                            <div class="col-md-12 col-lg-12 col-xl-12">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="withdrawinput1">Full Name<span style="color: red">*</span></label>
                                                    <input type='text' value="{{$user_name}}" name="user_name" class="form-control" required>
                                                </div>
                                            </div> 
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="withdrawinput1">Email ID</label>
                                                    <input type='email' value="{{$user_email}}" name="user_email" class="form-control">
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="withdrawinput1">Phone No</label>
                                                    <input type='tel' value="{{$user_phone_no}}" name="user_phone_no" class="form-control">
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="withdrawinput1">Password</label>
                                                    <input type='password'  autocomplete="new-password"  name="user_password" class="form-control">
                                                </div>
                                            </div> 
                                            @if(session()->get('my_chacha')['user_role']=='admin')
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="withdrawinput1">Access Roles</label>
                                                    <select onchange="getParents(this.value);" class="form-control" name="user_role_id">
                                                      <option disabled <?php if($user_role_id==NULL OR $user_role_id=='' ){ echo 'selected';} ?>>Select Roles</option>
                                                      <option <?php if($user_role_id==1){ echo 'selected';} ?> value="1">Agency</option>
                                                      <option <?php if($user_role_id==2){ echo 'selected';} ?> value="2">Regular</option> 
                                                    </select>
                                                </div>
                                            </div>  
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="withdrawinput1">Assign Templates</label>
                                                    <select multiple onchange="setTemplate(this.value);" class="form-control select2" name="user_template_ids[]">
                                                        <option disabled>Select Template</option> 
                                                        @if(!empty($data['templates']))
                                                        @php
                                                            if(!empty($data['user_template_ids'])){
                                                                $templates = explode(',',$data['user_template_ids']); 
                                                            }else{
                                                                $templates = []; 
                                                            }
                                                        @endphp
                                                        @foreach($data['templates'] as $t_key=>$t_value))
                                                            <option 
                                                            @if(in_array($t_value->template_id,$templates)) 
                                                                selected="selected"
                                                            @endif
                                                            value="{{$t_value->template_id}}">{{$t_value->template_name}}</option>
                                                        @endforeach
                                                        @endif
                                                    </select>
                                                </div>
                                            </div>
                                            @endif 
                                            
                                            @if(session()->get('my_chacha')['user_role']!='admin' && !empty($data['roles']))
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="withdrawinput1">{{__('label.User Group')}}</label>
                                                        <select class="form-control" name="user_role_id">
                                                            <option value="" selected>No Group</option> 
                                                            @foreach($data['roles'] as $key=>$value))
                                                                <option 
                                                                @if($value->role_id == $data['user_role_id'])
                                                                selected="selected" 
                                                                @endif
                                                                value="{{$value->role_id}}">{{$value->role_name}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>  
                                            @endif

                                            
                                        </div>
                                        <hr>
                                        <div class="row" style="float: right;">
                                            <button style="font-size: 15px !important;" type="submit" class="btn btn-success waves-effect waves-light"><i class="fa-pulse fa fa-spinner d-none"></i> Save</button>&nbsp;
                                            <button style="font-size: 15px !important;" type="button" class="btn btn-outline-danger waves-effect waves-light" data-dismiss="modal">Close</button> 
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div> -->
<script type="text/javascript"> 
/*function setTemplate(ids)
    {
        if (typeof ids !='Undefined' && ids!='') {
            $("#7_module_input").val([38]);
        }else{
            console.log ($("#7_module_input").val());              
        }
    } 

function getParents(type)
    {
        $('#permission_section').removeClass('d-none');
        if (type==2) {
            $('#1_module').addClass('d-none');
            $('#6_module').addClass('d-none'); 

            $("#1_module_input").val([]);
            $("#6_module_input").val([]);
        }else{
            $('#1_module').removeClass('d-none');
            $('#6_module').removeClass('d-none'); 
        } 
    } 
*/

/*jQuery(document).ready(function() {
    var user_role_id = $('select[name="user_role_id"]').val();
    if (user_role_id==2) {
        $('#1_module').addClass('d-none');
        $('#6_module').addClass('d-none'); 
    } 

    var dd = {
        beforeSend: function() {
          $('.fa-spinner').removeClass('d-none')
        },
        uploadProgress: function(event, position, total, percentComplete) {

        },
        success: function() {},
        complete: function(response) {
            console.log(response.responseText);
            var result = jQuery.parseJSON(response.responseText); 
            $('.fa-spinner').removeClass('d-none');
            $('.fa-spinner').addClass('d-none');
            if (result.status == 200) {
                Swal.fire({
                    type: 'success',
                    title: 'Successfully saved',
                    showConfirmButton: false,
                    timer: 1500
                });
                user_tbl._fnAjaxUpdate();
                jQuery('#userEdit').trigger("reset");
                jQuery('.modal').modal('hide');
            } else {
                Swal.fire({
                    type: 'warning',
                    title: 'Oops',
                    text: result.message,
                    showConfirmButton: false,
                    timer: 2000
                });
            }
        },
        error: function() {

        }
    };

    jQuery("#userEdit").ajaxForm(dd);

});*/

/* function showImage(src,target) {
   var fr=new FileReader();
   
   fr.onload = function(e) { target.src = this.result; };
   src.addEventListener("change",function() {
      
     fr.readAsDataURL(src.files[0]);
   });
 }

 var src = document.getElementById("src");
 var target = document.getElementById("target");
 showImage(src,target);



 var _URL = window.URL || window.webkitURL; 
 jQuery("#src").change(function(e) {
     var file, img; 

     if ((file = this.files[0])) {
         img = new Image();
         img.onload = function() {
           if(this.width>500 || this.height>500){
             document.getElementById("src").value = "";
             document.getElementById('target').src = "./assets/images/another.jpg"; 
             alert('Select Another Image');
           }
             alert(this.width + " " + this.height);
         };
         img.onerror = function() {
             alert( "not a valid file: " + file.type);
         };
         img.src = _URL.createObjectURL(file);


     }

 });*/
</script>

@endsection