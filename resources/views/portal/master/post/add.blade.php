@extends('portal.template.app') 
@section('content')

<link rel="stylesheet" type="text/css" href="{{asset('assets/indrop/inlancer_drop.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('festival_inlancer/vendors/summernote/summernote.css')}}">  

<?php 
$page_title = 'Add Multiple Posts';
$route_name = 'post'; 
$mode = 'add'; 
$save_url = url($route_name.'-save');
$model_size = 'modal-lg';
 
?> 
<style type="text/css"> 
    textarea{
        height: unset;
    } 
</style> 
<div class="main-content">
    <section class="section">
        <div class="section-header">
            <h1><?php echo $page_title; ?></h1>
            <div class="section-header-breadcrumb">
                <div class="breadcrumb-item active"><a href="<?php echo url('portal');?>">Dashboard</a></div> 
                <div class="breadcrumb-item"><?php echo ucfirst($route_name); ?></div>
            </div>
        </div>
        <div class="section-body">
            <div class="row">
                <div class="col-12"> 
                    <div class="card">
                        <form class="form add_form" method="post" id="add_form" action="{{$save_url}}" enctype="multipart/form-data">
                            <input type="hidden" name="mode" value="{{$mode}}">
                            @csrf
                            <div id="message"></div> 
                            <div class="form-body">
                                <div class="row">
                                    <div class="col-md-12 col-lg-12 col-xl-12 pb-5">
                                        <div class="card-body">
                                            <div id="accordion">
                                                <div class="accordion">
                                                    <div class="mb-2 p-2" style="box-shadow: 0 2px 6px #acb5f6;background-color: #c60b008a;color: #fff;border-radius: 8px;">
                                                        <p>Illegal Drugs, Horrifying & Scary Elements, Nazi Symbols - swastika symbol 卐, Violence Towards Vulnerable or Defenseless Characters, Sexual Material, Offensive Language, Tobacco, Creatures Behave Like Humans (Aliens), Violence, Age-Restricted Physical Goods, Lottery</p>
                                                    </div>
                                                    <div class="accordion-header collapsed" role="button" data-toggle="collapse" data-target="#add-panel-body-1" aria-expanded="true">
                                                        <h4>Step-1 : Post Basic Info</h4>
                                                    </div>
                                                    <div class="accordion-body collapse show" id="add-panel-body-1" data-parent="#accordion" style="">
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="row">
                                                                    <div class="col-md-12">
                                                                        <div class="form-group">
                                                                            <label for="withdrawinput1">Post Sub Category: </label>
                                                                            <select name="post_category" class="form-control select2"  
                                                                            >
                                                                            <!-- onchange="getParent(this)" -->
                                                                                <option value="0" selected disabled>Select Sub Category</option>
                                                                                <?php 
                                                                                if(!empty($category_list)){
                                                                                    foreach ($category_list as $category) { ?>
                                                                                        <option data-parent="{{$category->category_parent_id}}" value="{{$category->category_id}}">
                                                                                            {{$category->category_date}} > {{$category->category_name}}
                                                                                        </option>
                                                                                <?php } } ?>
                                                                            </select>
                                                                            <!-- <input type="hidden" name="post_parent_category" id="post_parent_category">  -->
                                                                        </div> 
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="row">
                                                                    <div class="col-md-12">
                                                                        <div class="form-group">
                                                                            <label for="withdrawinput1">Post Name: </label>
                                                                            <input type="text" name="post_name" id="post_name" class="form-control" >
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <div class="row">  
                                                                    <div class="col-md-12">
                                                                        <div class="form-group">
                                                                            <label for="withdrawinput1">Post Date: </label>
                                                                            <input type="date" name="post_date" 
                                                                            id="post_date" 
                                                                            class="form-control" 
                                                                            value="<?php echo date('Y-m-d') ?>" 
                                                                            >
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <div class="row">  
                                                                    <div class="col-md-12">
                                                                        <div class="form-group">
                                                                            <label for="withdrawinput1">Post Package: </label>
                                                                             <select name="post_package" class="form-control select2">
                                                                                <option value="free" selected >Free</option>
                                                                                <option value="premium">Premium</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <div class="row">  
                                                                    <div class="col-md-12">
                                                                        <div class="form-group">
                                                                            <label for="withdrawinput1">Post Type: </label>
                                                                             <select name="post_type" class="form-control select2">
                                                                                <option value="festival" selected >Festival</option>
                                                                                <option value="incident">Incident</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-12">
                                                                <div class="row">
                                                                    <div class="col-md-12">
                                                                        <div class="form-group">
                                                                            <label for="withdrawinput1">Post Search Keyword: </label>
                                                                            <textarea name="post_search_keyword" class="form-control"></textarea>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6 invisible">
                                                                <div class="row">
                                                                    <div class="col-md-12"> 
                                                                        <div class="form-group">
                                                                            <label for="withdrawinput1">Post Image Alt Tag: </label>
                                                                            <input type="text" name="iati1" class="form-control">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <!-- post_image -->
                                                            <div class="col-md-6">
                                                                <div id="i1" class="drop-area" data-throwback="storePerformance" onclick="document.getElementById('fileElem1').click()"> 
                                                                    <div class="row justify-content-center" id="imgPreviewi1">
                                                                      <i class="bi bi-cloud-arrow-up mb-3" style="color:#445dbe;font-size: 60px;"></i>
                                                                      <h3 style="color: #445dbe;font-size: 18px;line-height: 100px;">Click "Here" or drop your image here<span style="color:red">*</span></h3>
                                                                    </div>
                                                                    <input class="d-none" type="file" id="fileElem1" accept="" multiple="multiple" onchange="handleFiles(this.files,'i1','storePerformance')">   
                                                                </div>
                                                              <progress id="progress-bar" class="d-none" max=100 value=0></progress>
                                                            </div>
                                                            <input type="hidden" id="post_image" name="post_image" data-image="i1">
                                                            <!-- post_image -->
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <br>
                                            <button type="submit" class="btn btn-success waves-effect waves-light float-right"><i class="fa fa-spinner fa-spin d-none" tabindex="20"></i>Save</button>
                                        </div>
                                    </div>
                                </div> 
                            </div>
                        </form>
                    </div>
                </div>
            </div>        
        </div>
    </section>
</div>
<script src="{{ asset('js/jquery.form.js')}}"></script>
<script src="{{ asset('assets/indrop/inlancer_drop.js')}}"></script>
<script src="{{ asset('festival_inlancer/vendors/summernote/summernote.min.js')}}"></script>

<script type="text/javascript">

/*function getParent(parent){
    var parentId = parent.options[parent.selectedIndex].dataset.parent;
    $('#post_parent_category').val(parentId);
}*/
function storePerformance(file_id,returnData) {    
    $("[data-image='"+file_id+"']").val($("[data-image='"+file_id+"']").val()+','+returnData.image_id);  
}

jQuery(document).ready(function() { 
    /*$("#post_date").datepicker();*/
    var dd = {
        beforeSend: function() { 
            $('.fa-spinner').removeClass('d-none');
        },
        uploadProgress: function(event, position, total, percentComplete) { 
        },
        success: function() {},
        complete: function(response) {
            var result = jQuery.parseJSON(response.responseText);
            $('.fa-spinner').removeClass('d-none');
            $('.fa-spinner').addClass('d-none');
            if (result.status == 200) {
                Swal.fire({
                    type: 'success',
                    title: result.message,
                    showConfirmButton: false,
                    timer: 1500
                });
                $('#add_form').trigger("reset");
                $(".select2").val('').trigger('change');
                window.location.reload();
                // setTimeout(function(){
                    // window.location.href= '{{url($route_name."-master")}}';
                // },2000); 
            } else {
                Swal.fire({
                    type: 'warning',
                    title: 'Oops',
                    text: result.message,
                    showConfirmButton: false,
                    timer: 2000
                });
            }
        },
        error: function() { 
        }
    };
    jQuery("#add_form").ajaxForm(dd);
});
$(document).on('change','select[name="post_category"]',function () {
    let selectedVal = $('select[name="post_category"] option:selected').text();
    let searchKeyWord = selectedVal.split('>')[1];
    $('textarea[name="post_search_keyword"]').val(searchKeyWord.trim());
    $('input[name="post_name"]').val(searchKeyWord.trim());
});
</script>

@endsection