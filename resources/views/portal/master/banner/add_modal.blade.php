<link rel="stylesheet" type="text/css" href="{{asset('assets/indrop/inlancer_drop.css')}}">
<?php 
$parent_banner_id = 0; 
$page_title = 'Add Banner';
$route_name = 'banner';
$mode = 'add';
$save_url = url($route_name.'-save');
$model_size = 'modal-lg';

$title = "Banner";
 
?>
<style type="text/css">
label{
    cursor: pointer;
}
</style>

<div class="modal right fade {{$mode}}_{{$route_name}}_modal" id="{{$mode}}_{{$route_name}}_modal" tabindex="-1" role="dialog" style="display: none;" aria-hidden="true" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog {{$model_size}}" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title mb-1" id="myModalLabel">{{$page_title}}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body" style="overflow-y:auto;">
                <form class="form add_form" method="post" id="add_form" action="{{$save_url}}" enctype="multipart/form-data">
                    <input type="hidden" name="mode" value="{{$mode}}">
                    @csrf
                    <div id="message"></div>
                    <div class="form-body">
                        <div class="row">
                            <div class="col-md-12 col-lg-12 col-xl-12">
                                <div>
                                    <div class="card-body">
                                        <div id="accordion">
                                            <div class="accordion">
                                                <div class="accordion-header collapsed" role="button" data-toggle="collapse" data-target="#edit-panel-body-1" aria-expanded="true">
                                                    <h4>Step-1 : Banners Details (Banner size - 1000x400 px )</h4>
                                                </div>
                                                <div class="accordion-body collapse show" id="edit-panel-body-1" data-parent="#accordion" style="">
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="row">
                                                                <div class="col-md-12">
                                                                    <div class="form-group">
                                                                        <label for="withdrawinput1">Banner Name: </label>
                                                                        <input type="text" name="banner_name"  class="form-control">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6 d-none">
                                                            <div class="row">
                                                                <div class="col-md-12">
                                                                    <div class="form-group">
                                                                        <label for="withdrawinput1">Banner Title: </label>
                                                                        <input type="text" name="banner_title" class="form-control">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="row">
                                                                <div class="col-md-12">
                                                                    <div class="form-group">
                                                                        <label for="withdrawinput1">Banner Link: </label>
                                                                        <input type="text" name="banner_link" class="form-control">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12 d-none">
                                                            <div class="row">
                                                                <div class="col-md-12">
                                                                    <div class="form-group">
                                                                        <label for="withdrawinput1">Banner Description: </label>
                                                                        <textarea name="banner_text" class="form-control"></textarea>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        
                                                        <div class="col-md-6 invisible">
                                                            <div class="row">
                                                                <div class="col-md-12"> 
                                                                    <div class="form-group">
                                                                        <label for="withdrawinput1">Banner Image Alt Tag: </label>
                                                                        <input type="text" name="iati1" class="form-control">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!-- banner_image -->
                                                        <div class="col-md-6">
                                                          <div id="i1" class="drop-area" data-throwback="storePerformance" onclick="document.getElementById('fileElem1').click()"> 
                                                            <div class="text-center" id="imgPreviewi1">
                                                              <i class="bi bi-cloud-arrow-up mb-3" style="color:#445dbe;font-size: 60px;"></i>
                                                              <h3 style="color: #445dbe;font-size: 18px;line-height: 60px;">Click "Here" or drop your Image here ( 1000x400 px )</h3>
                                                            </div>
                                                            <input class="d-none" type="file" id="fileElem1" accept="" onchange="handleFiles(this.files,'i1','storePerformance')">   
                                                          </div>
                                                          <progress id="progress-bar" class="d-none" max=100 value=0></progress>
                                                        </div>
                                                        <input type="hidden" id="banner_image" name="banner_image" data-image="i1">
                                                        <!-- banner_image -->
                                                        
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                        <br>
                                        <button type="button" class="btn btn-outline-danger waves-effect waves-light float-left" data-dismiss="modal" tabindex="99">Close</button>
                                        <button type="submit" class="btn btn-success waves-effect waves-light float-right"><i class="fa fa-spinner fa-spin d-none" tabindex="20"></i> Save</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>


<script src="{{ asset('js/jquery.form.js')}}"></script>
<script src="{{ asset('assets/indrop/inlancer_drop.js')}}"></script>
<script type="text/javascript">
/*Start NEW*/
function storePerformance(file_id,returnData) {    
    $("[data-image='"+file_id+"']").val(returnData.image_id);  
}
jQuery(document).ready(function() { 
    var dd = {
        beforeSend: function() { 
            $('.fa-spinner').removeClass('d-none');
        },
        uploadProgress: function(event, position, total, percentComplete) { 
        },
        success: function() {},
        complete: function(response) {
            var result = jQuery.parseJSON(response.responseText);
            $('.fa-spinner').removeClass('d-none');
            $('.fa-spinner').addClass('d-none');
            if (result.status == 200) {
                Swal.fire({
                    type: 'success',
                    title: result.message,
                    showConfirmButton: false,
                    timer: 1500
                });
                jQuery('.modal').modal('hide');
                jQuery('#add_form').trigger("reset");
                {{$route_name}}_tbl._fnAjaxUpdate();
                window.location.reload();
            } else {
                Swal.fire({
                    type: 'warning',
                    title: 'Oops',
                    text: result.message,
                    showConfirmButton: false,
                    timer: 2000
                });
            }
        },
        error: function() { 
        }
    };
    jQuery("#add_form").ajaxForm(dd);
});

/*END NEW*/

</script>