@extends('portal.template.app') 
@section('content')  
<?php 
$user_title = 'Report';
$route_name = 'report';
$save_url = url('user-update');

$mode = 'view';
$model_size = 'modal-lg';
$business_title = "Business";
?>
<style type="text/css">
  body > table{
    display: none;
  }
</style>
<div class="main-content">
  <section class="section">
    <div class="section-header">
        <h1><?php echo $user_title.' & '.$business_title; ?></h1>
        <div class="section-header-breadcrumb">
            <div class="breadcrumb-item "><a href="<?php echo url('portal');?>">Dashboard</a></div> 
            <div class="breadcrumb-item "><a href="<?php echo url('user-master');?>">Users</a></div> 
            <div class="breadcrumb-item active"><?php echo ucfirst($route_name); ?></div>
        </div>
    </div>
    <div class="section-body"> 
      <div class="row mt-sm-4"> 
        <div class="col-12 col-md-6 col-lg-7">
          <div class="card">
              <div class="card-header">
                 <h4 class="card-title"> User - Info</h4> 
              </div>
              <div class="card-body">
                <div class="row">
                  <div class="col-md-9 col-12">
                    <div class="row ml-3">
                      <label>@if(!empty($user_detail['user_name'])) User Name : {{$user_detail['user_name']}} @endif</label>
                    </div>
                    <div class="row ml-3">
                      <label>@if(!empty($user_detail['user_email'])) Email : {{$user_detail['user_email']}} @endif</label>
                    </div>
                    <div class="row ml-3"><label>User Contact : {{$user_detail['user_phone_no']}}</label></div>
                    <div class="row ml-3"><label> Status : 
                                      @if($user_detail['user_status'] == 1) Active 
                                      @elseif($user_detail['user_status'] == 2) Block 
                                      @else InActive @endif </label>
                    </div>                  
                  </div> 
                  <div class="col-md-3 col-12">
                    <div class="col-md-6 col-12">
                     <?php if(!empty($user_detail['user_profile_image'])){?>
                      <img src="{{asset('assets/upload/users/thumbnail/'.$user_detail['user_profile_image'])}}" style="height: 80px; width: ; object-fit: cover;">
                     <?php }else{ ?>
                      <img src="{{asset('festival_inlancer/img/avatar/avatar-1.png')}}" style="height: 80px; width: ; object-fit: cover;">
                     <?php } ?>
                    </div> 
                  </div>
                </div>
              </div>
          </div>
        </div>
        <div class="col-12 col-md-6 col-lg-5">

          <div class="card">
              <div class="card-header">
                 <h4 class="card-title">Assigned Frame</h4> 
              </div>
              <div class="card-body">
                <div class="row">
                  @if(!empty($frame_data) )
                  @foreach($frame_data as $key=>$value)
                  <div class="col-md-4 col-12">
                    <div>
                      <div class="text-center">
                      </div>
                      <div class="card-body p-1">
                        <img src="<?php echo url($value->frame_image);?>" style="height: 100px; object-fit: contain; width: 100%;">
                        <h6 class="text-center">{{$value->frame_name}}</h6>
                      </div>
                    </div>
                  </div>
                  @endforeach
                  @endif 
                </div>
              </div>
          </div>

        </div>
      </div>
      <div class="row">
        <div class="col-12 col-md-12 col-lg-12">
          <div class="card">
          	<div class="card-header">
          	   <h4 class="card-title">{{$business_title}}</h4>
      	    </div>
            <div class="card-body">
            	  <form  name="business_form" id="business_form" method="post"> 
                    <div class="table-responsive">
                        <table id='business_tbl' class="table table-striped table-loading" >
                            <thead>
                              <tr>
                            	  <?php foreach ($grid['grid_business_columns'] as $key => $value) {  ?>
                                  <th <?php echo $value['width'] ?> <?php echo $value['style'] ?> <?php echo $value['class'] ?> > 
                                    <?php echo $value['name'];?> 
                                  </th> 
                                <?php  } ?> 
                              </tr>
                            </thead>
                        </table>
                    </div>
                </form>
            </div>
          </div>
        </div>
      </div>
      <div class="row mt-sm-4"> 
        <div class="col-12 col-md-6 col-lg-6">
          <div class="card">
              <div class="card-header">
                 <h4 class="card-title">User Post: </h4> 
              </div>
              <div class="card-body">
                <div class="row">
                  @if(!empty($track_data['savePostData']) )
                  @foreach($track_data['savePostData'] as $key=>$value)
                  <div class="col-md-3 col-12">
                    <div class=" card" style="; background: ;">
                      <div class="text-center">
                      </div>
                      <div class="card-body p-1">
                        <img src="<?php echo url($value->post_image);?>" style="height: 130px; object-fit: contain; width: 100%;" onclick="post({{$value->post_id}})">
                      </div>
                    </div>
                  </div>
                  @endforeach
                  @else
                  <div class="col-md-12 col-12">
                    <div class=" card" style="; background: ;">
                      <div class="text-center">
                        <h6>The user does not have  post available !</h6>
                      </div>
                    </div>
                  </div>
                  @endif 
                </div>
              </div>
          </div>
        </div>
        <div class="col-12 col-md-6 col-lg-6">
          <div class="card">
              <div class="card-header">
                 <h4 class="card-title">User Frame: </h4> 
              </div>
              <div class="card-body">
                <div class="row">
                  @if(!empty($track_data['saveFrameData']) )
                  @foreach($track_data['saveFrameData'] as $key=>$value)
                  <div class=" col-md-3 col-12">
                    <div class="card" style="border: ; background: ;">
                      <div class="text-center">
                      </div>
                      <div class="card-body p-1">
                        <img src="<?php echo url($value->frame_image);?>" style="height: 130px; object-fit: contain; width: 100%;" onclick="frame({{$value->frame_id}})">
                      </div>
                    </div>
                  </div>
                  @endforeach
                  @else
                  <div class="col-md-12 col-12">
                    <div class=" card" style="; background: ;">
                      <div class="text-center">
                        <h6>User have no Frame available !</h6>
                      </div>
                    </div>
                  </div>
                  @endif 
                </div>
              </div>
          </div>
        </div>
      </div>
    </div>    
  </section>
</div>
<script type="text/javascript">
  function post(id){
    window.open("<?php echo url('post-edit/'); ?>/"+id,'_blank');
  }function frame(id){
    window.open("<?php echo url('frame-edit/'); ?>/"+id,'_blank');
  }
/*User Update */  
jQuery(document).ready(function() {  
    var dd = {
        beforeSend: function() { 
            $('.fa-spinner').removeClass('d-none');
        },
        uploadProgress: function(event, position, total, percentComplete) { 
        },
        success: function() {},
        complete: function(response) {
            console.log(response.responseText);
            var result = jQuery.parseJSON(response.responseText);
            $('.fa-spinner').removeClass('d-none');
            $('.fa-spinner').addClass('d-none');
            if (result.status == 200) {
                Swal.fire({
                    type: 'success',
                    title: 'Successfully saved',
                    showConfirmButton: false,
                    timer: 1500
                });  
                setTimeout(function(){ location.reload(); }, 1500);
            } else {
                Swal.fire({
                    type: 'warning',
                    title: 'Oops',
                    text: result.message,
                    showConfirmButton: false,
                    timer: 2000
                });
            }
        },
        error: function() { 
        }
    }; 
    jQuery("#user_edit").ajaxForm(dd);
  });
/*End User Update */  
var is_first_time_load = true;
jQuery(document).ready(function(){ 
  <?php echo $grid['grid_business_tbl_name']; ?>_tbl = jQuery('#<?php echo $grid['grid_business_tbl_name']; ?>_tbl').dataTable({
      "oLanguage": {
          "sSearch": "Search",
          "sLengthMenu": "Show _MENU_ enteries",
          "sInfo":  " Showing  _START_  to  _END_  of  _TOTAL_  entries ", 
      },
      "language": {
        "paginate": {
          "previous": "Previous",
          "next": "Next",
        },
       "emptyTable": "No data available in table"
      },
      "processing": true,
      "fixedHeader": true,
      "serverSide": true, 
      "bAutoWidth": true, 
      /*"scrollY":300,   */
      "iDisplayLength": <?php echo $grid['grid_business_tbl_length']; ?>,
      "ajaxSource": "<?php echo $grid['grid_business_dt_url']?>",
      "aoColumns": [<?php foreach($grid['grid_business_columns'] as $key=>$value) { if($value['sortable']=='true'){ echo "{ 'bSortable' : true}," ;}  else {echo "{ 'bSortable' : false}," ;} }?>                     
      ],
      "order":[['{{$grid["grid_business_order_by"]}}','{{$grid["grid_business_order_by_type"]}}']],
      "sDom": "<'row'<'col-sm-9 col-xs-9'l><'col-sm-3 col-xs-3'f>r>t<'row'<'col-sm-5 hidden-xs paging-class'i><'col-sm-7 col-xs-12 clearfix'p>>",
      'fnDrawCallback' : function(){
        jQuery('th:first-child').removeClass('sorting_desc');
        jQuery('th:first-child').removeClass('sorting');
        if(is_first_time_load){
          jQuery('#<?php echo $grid['grid_business_tbl_name']; ?>_tbl_length select').addClass("form-control");
          jQuery('#<?php echo $grid['grid_business_tbl_name']; ?>_tbl_filter input').addClass("form-control"); 
          is_first_time_load = false;
        } 
      },
  });  
  jQuery('#<?php echo $grid['grid_business_tbl_name']; ?>_tbl').delay(100).css("width","100%");  
});
jQuery(document).ready(function() {
  jQuery('#{{$grid["grid_business_tbl_name"]}}_tbl').on('init.dt',function() {
        jQuery("#{{$grid["grid_business_tbl_name"]}}_tbl").removeClass('table-loading').show();
      });
  setTimeout(function(){
    jQuery('#{{$grid["grid_business_tbl_name"]}}_tbl').dataTable();
  }, 3000);
});
</script>

<script src="{{ asset('festival_inlancer/js/jquery.form.js')}}"></script>  
<script src="{{ asset('festival_inlancer/vendors/dataTable/datatables.min.js')}}"></script>
@endsection

