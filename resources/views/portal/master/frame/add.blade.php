@extends('portal.template.app') 
@section('content')

<?php 
$page_title = 'Add Frame';
$route_name = 'frame'; 
$mode = 'add'; 
$save_url = url($route_name.'-save');
$model_size = 'modal-lg';

?> 

<link rel="stylesheet" type="text/css" href="{{asset('assets/indrop/inlancer_drop.css')}}">
<script src="{{asset('festival_inlancer/js/wz_dragdrop.js')}}"></script>
<style type="text/css"> 
    textarea{
        height: unset; 
    }
    #setup_image{
        pointer-events: none;
    }
    [width-100]{
        width: max-content !important;
        cursor: move !important;
    }
    .width-60-60{
        width: 60px !important;
        height: 60px !important;
    }
    .section .section-title:before {
        content: ' ';
        border-radius: 5px;
        height: 8px;
        width: 30px;
        background-color: #70b8ee;
        display: inline-block;
        float: left;
        margin-top: 6px;
        margin-right: 15px;
    }
</style>
<div class="main-content">
    <section class="section">
        <div class="section-header">
            <h1><?php echo $page_title; ?></h1>
            <div class="section-header-breadcrumb">
                <div class="breadcrumb-item active"><a href="<?php echo url('portal');?>">Dashboard</a></div> 
                <div class="breadcrumb-item"><?php echo ucfirst($route_name); ?></div>
            </div>
        </div>
        <div class="section-body">
            <div class="row">
                <div class="col-12"> 
                    <div class="card">
                        <form class="form add_form" method="post" id="add_form" action="{{$save_url}}" enctype="multipart/form-data">
                            <input type="hidden" name="mode" value="{{$mode}}">
                            @csrf
                            <div id="message"></div> 
                            <div class="form-body">
                                <div class="row">
                                    <div class="col-md-12 col-lg-12 col-xl-12 pb-5">
                                        <div class="card-body">
                                            <div id="accordion">
                                                <div class="accordion">
                                                    <div class="accordion-header collapsed" role="button" data-toggle="collapse" data-target="#add-panel-body-1" aria-expanded="true">
                                                        <h4>Step-1 : Frame Basic Info</h4>
                                                    </div>
                                                    <div class="accordion-body collapse show" id="add-panel-body-1" data-parent="#accordion" style="">
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="row">
                                                                    <div class="col-md-12">
                                                                        <div class="form-group">
                                                                            <label for="withdrawinput1">Frame Name: </label>
                                                                            <input type="text" name="frame_name" class="form-control" >
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <div class="row">  
                                                                    <div class="col-md-12">
                                                                        <div class="form-group">
                                                                            <label for="withdrawinput1">Frame Package: </label>
                                                                             <select name="frame_package" class="form-control select2">
                                                                                <option value="free" selected >Free</option>
                                                                                <option value="premium">Premium</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6 d-none">
                                                                <div class="row">
                                                                    <div class="col-md-12"> 
                                                                        <div class="form-group">
                                                                            <label for="withdrawinput1">Frame Image Alt Tag: </label>
                                                                            <input type="text" value=" " name="iati1" class="form-control">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="row">
                                                                    <div class="col-md-12"> 
                                                                        <div class="form-group">
                                                                            <label>User list: </label>
                                                                             <select name="assigned_user[]" multiple class="form-control select2" placeholder="select user">
                                                                                <option disabled >select user</option>
                                                                                <?php 
                                                                                if(!empty($user_list)){
                                                                                    foreach ($user_list as $key => $value) { ?>
                                                                                    <option value="<?php echo $value->user_id ?>">
                                                                                        {{$value->user_phone_no}} {{$value->user_name ?? ""}} {{$value->user_email ?? ""}}
                                                                                    </option>
                                                                                <?php 
                                                                                    }
                                                                                }
                                                                                 ?>

                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <!-- frame_image -->
                                                            <div class="col-md-6">
                                                                <div id="i1" class="drop-area" data-throwback="storePerformance" onclick="document.getElementById('fileElem1').click()"> 
                                                                    <div class="row justify-content-center" id="imgPreviewi1">
                                                                      <i class="bi bi-cloud-arrow-up mb-3" style="color:#445dbe;font-size: 60px;"></i>
                                                                      <h3 style="color: #445dbe;font-size: 18px;line-height: 100px;">Click "Here" or drop your image here<span style="color:red">*</span></h3>
                                                                    </div>
                                                                    <input class="d-none" type="file" id="fileElem1" accept="" onchange="handleFiles(this.files,'i1','storePerformance')">   
                                                                </div>
                                                              <progress id="progress-bar" class="d-none" max=100 value=0></progress>
                                                            </div>
                                                            <input type="hidden" id="frame_image" name="frame_image" data-image="i1">
                                                            <!-- frame_image -->
                                                        </div>
                                                        <input type="hidden" name="frame_data" id="json_aya_muko" value="">
                                                        <div>
                                                            <input id="check_all" type="checkbox" name="all_check" onchange="toggle_check(this)">
                                                            <label for="check_all">Check all </label>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-6 main-sec">
                                                            <?php 
                                                            // $temp_arr = ['name','logo','phone1','phone2','email','website','address'];

                                                            $frame_data = '[{"type":"name","value":[{"name":"show","value":"1"},{"name":"top","value":"439"},{"name":"left","value":"10"},{"name":"rotate","value":"0"},{"name":"font-size","value":"12"},{"name":"color","value":"#000000"}]},{"type":"logo","value":[{"name":"show","value":"1"},{"name":"top","value":"20"},{"name":"left","value":"20"},{"name":"rotate","value":"0"},{"name":"font-size","value":"12"},{"name":"color","value":"#000000"}]},{"type":"phone1","value":[{"name":"show","value":"1"},{"name":"top","value":"408"},{"name":"left","value":"10"},{"name":"rotate","value":"0"},{"name":"font-size","value":"12"},{"name":"color","value":"#000000"}]},{"type":"phone2","value":[{"name":"show","value":"1"},{"name":"top","value":"375"},{"name":"left","value":"10"},{"name":"rotate","value":"0"},{"name":"font-size","value":"12"},{"name":"color","value":"#000000"}]},{"type":"email","value":[{"name":"show","value":"1"},{"name":"top","value":"408"},{"name":"left","value":"167"},{"name":"rotate","value":"0"},{"name":"font-size","value":"12"},{"name":"color","value":"#000000"}]},{"type":"website","value":[{"name":"show","value":"1"},{"name":"top","value":"440"},{"name":"left","value":"167"},{"name":"rotate","value":"0"},{"name":"font-size","value":"12"},{"name":"color","value":"#000000"}]},{"type":"address","value":[{"name":"show","value":"1"},{"name":"top","value":"472"},{"name":"left","value":"10"},{"name":"rotate","value":"0"},{"name":"font-size","value":"12"},{"name":"color","value":"#000000"}]}]';
                                                            $temp_arr =json_decode($frame_data);

                                                            foreach($temp_arr as $val){
                                                            ?>
                                                                <h4 class="section-title text-capitalize">{{$val->type}} 
                                                                </h4>
                                                                <div class="row" data-frame="{{$val->type}}" style=" margin-top: -37px; ">
                                                                    <div class="col-md-3 pr-0">
                                                                        <?php 
                                                                        $class_name = 'visible';
                                                                        if(empty($val->value[0]->value)){
                                                                            $class_name = 'invisible';
                                                                        } ?>
                                                                        <div class="form-group d-flex align-items-center float-right m-0">
                                                                            <input type="checkbox" id="{{$val->type}}" onchange="checkbox_check(this)" checked class="form-check-input" > 
                                                                            <label for="{{$val->type}}" class="m-0 pt-1">Show</label>
                                                                            <input type="number" min="0" max="1" 
                                                                            data-name="show" value="<?php echo $val->value[0]->value ?>" class="d-none" 
                                                                            onchange="move_this_feild('show',$(this).val(),'img_{{$val->type}}')" >
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-3 pr-0 <?php echo $class_name ?>" data-show="{{$val->type}}">
                                                                        <div class="form-group d-flex align-items-center m-0">
                                                                            <label class="m-0 ml-2" for="withdrawinput1">Top</label>
                                                                            <input type="number" 
                                                                            min="0" max="504" data-name="top" 
                                                                            value="<?php echo $val->value[1]->value ?>" class="form-control mx-2 p-2" 
                                                                            onchange="move_this_feild('top',$(this).val(),'img_{{$val->type}}')" 
                                                                            onkeyup="move_this_feild('top',$(this).val(),'img_{{$val->type}}')" 
                                                                            >
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-3 pr-0 <?php echo $class_name ?>" data-show="{{$val->type}}">
                                                                        <div class="form-group d-flex align-items-center m-0">
                                                                            <label class="m-0 ml-2" for="withdrawinput1">Left</label>
                                                                            <input type="number" min="0" max="504" data-name="left" value="<?php echo $val->value[2]->value ?>" class="form-control mx-2 p-2"
                                                                            onchange="move_this_feild('left',$(this).val(),'img_{{$val->type}}')" 
                                                                            onkeyup="move_this_feild('left',$(this).val(),'img_{{$val->type}}')" 
                                                                            >
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-3 pr-0 <?php echo $class_name ?>" data-show="{{$val->type}}">
                                                                        <div class="form-group d-flex align-items-center m-0">
                                                                            <label class="m-0 ml-2" for="withdrawinput1">Rotate</label>
                                                                            <input type="number" min="0" max="360" data-name="rotate" value="<?php echo $val->value[3]->value ?>" class="form-control mx-2 p-2" 
                                                                            onchange="move_this_feild('rotate',$(this).val(),'img_{{$val->type}}')" 
                                                                            onkeyup="move_this_feild('rotate',$(this).val(),'img_{{$val->type}}')" 
                                                                            >
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-5 pr-0 mt-3 px-0 <?php echo $class_name ?>" data-show="{{$val->type}}">
                                                                        <div class="form-group d-flex align-items-center m-0">
                                                                            <label class="m-0 ml-2" for="withdrawinput1">Change label</label>
                                                                            <?php 
                                                                            if($val->type == 'name'){
                                                                                $lable_text = 'Business Name';
                                                                            }
                                                                            if($val->type == 'logo'){
                                                                                $lable_text = 'logo';
                                                                            }
                                                                            if($val->type == 'phone1'){
                                                                                $lable_text = '+91-7737397070';
                                                                            }
                                                                            if($val->type == 'phone2'){
                                                                                $lable_text = '+91-7737397070';
                                                                            }
                                                                            if($val->type == 'email'){
                                                                                $lable_text = 'wearebrandkit@gmail.com';
                                                                            }
                                                                            if($val->type == 'website'){
                                                                                $lable_text = 'www.wearebrandkit.in';
                                                                            }
                                                                            if($val->type == 'address'){
                                                                                $lable_text = 'Street address, City, State, Country.';
                                                                            }

                                                                             ?>
                                                                            <input type="text" value="{{$lable_text}}" class="form-control mx-2 p-2" 
                                                                            onchange="move_this_feild('font-text',$(this).val(),'img_{{$val->type}}')" 
                                                                            onkeyup="move_this_feild('font-text',$(this).val(),'img_{{$val->type}}')" 
                                                                            >
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-3 pr-0 mt-3 ml-auto <?php echo $class_name ?>" data-show="{{$val->type}}">
                                                                        <div class="form-group d-flex align-items-center m-0">
                                                                            <label class="m-0 ml-2" for="withdrawinput1">Size</label>
                                                                            <input type="number" min="0" max="360" data-name="font-size" value="<?php echo $val->value[4]->value ?>" class="form-control mx-2 p-2" 
                                                                            onchange="move_this_feild('font-size',$(this).val(),'img_{{$val->type}}')" 
                                                                            onkeyup="move_this_feild('font-size',$(this).val(),'img_{{$val->type}}')" 
                                                                            >
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-3 pr-0 mt-3 ml-auto <?php echo $class_name ?>" data-show="{{$val->type}}">
                                                                        <div class="form-group d-flex align-items-center m-0">
                                                                            <label class="m-0 ml-2" for="withdrawinput1">Color</label>
                                                                            <input type="color"
                                                                            data-name="color" 
                                                                            value="<?php echo $val->value[5]->value ?>" 
                                                                            class="form-control mx-2 p-2" 
                                                                            onchange="move_this_feild('color',$(this).val(),'img_{{$val->type}}')" 
                                                                            oninput="move_this_feild('color',$(this).val(),'img_{{$val->type}}')" 
                                                                            >
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-12">
                                                                        <hr>
                                                                    </div>
                                                                </div>
                                                            <?php } ?>
                                                            </div>
                                                            <div class="col-md-6" id="edit_image_preview">
                                                                <span style="position: absolute;" class="d-none1" id="image_setup_section">

                                                                <?php 
                                                                $temp_arr =json_decode($frame_data);
                                                                foreach($temp_arr as $val){
                                                                    $css_property = 'position: absolute;';
                                                                    $font_size = 'font-size:12px;';
                                                                    $border_size = 'border: 2px solid;line-height: 70%;';
                                                                    $class = '';
                                                                    
                                                                    if($val->type == 'name'){
                                                                        $font_size = 'font-size:12px;width: max-content;';
                                                                    }elseif($val->type == 'logo'){
                                                                        $font_size = 'width: 60px; height:60px;'; //logo - 60x60dp
                                                                        $class .= "width-60-60";
                                                                    }elseif($val->type == 'phone1'){
                                                                        $font_size = 'font-size:12px;width: max-content;';
                                                                    }elseif($val->type == 'phone2'){
                                                                        $font_size = 'font-size:12px;width: max-content;';
                                                                    }elseif($val->type == 'email'){
                                                                        $font_size = 'font-size:12px;width: max-content;';
                                                                    }elseif($val->type == 'website'){
                                                                        $font_size = 'font-size:12px;width: max-content;';
                                                                    }elseif($val->type == 'address'){
                                                                        $font_size = 'font-size:12px;width: max-content;';
                                                                    }
                                                                    if(!empty($val->value)){
                                                                        foreach($val->value as $sub_val){
                                                                            if($sub_val->name == 'show' && $sub_val->value == 0){
                                                                                $css_property .= 'display:none';
                                                                            }
                                                                            if($sub_val->name == 'top'){
                                                                                $css_property .= 'top:'.$sub_val->value.'px;';
                                                                            }
                                                                            if($sub_val->name == 'left'){
                                                                                $css_property .= 'left:'.$sub_val->value.'px;';
                                                                            }
                                                                            if($sub_val->name == 'rotate'){
                                                                                $css_property .= ' transform: rotate('.$sub_val->value.'deg);';
                                                                            }
                                                                            if($sub_val->name == 'color'){
                                                                                $css_property .= ' color: '.$sub_val->value.';';
                                                                            }
                                                                            if($sub_val->name == 'font-size'){
                                                                                $css_property .= ' font-size: '.$sub_val->value.'px;';
                                                                            }
                                                                        }
                                                                    }
                                                                    $css_property = $css_property.$font_size.$border_size;
                                                                ?>
                                                                <?php 
                                                                    if($val->type == 'name'){
                                                                        $lable_text = 'Business Name';
                                                                    }
                                                                    if($val->type == 'logo'){
                                                                        $lable_text = 'logo';
                                                                    }
                                                                    if($val->type == 'phone1'){
                                                                        $lable_text = '7737397070';
                                                                    }
                                                                    if($val->type == 'phone2'){
                                                                        $lable_text = '+91-7737397070';
                                                                    }
                                                                    if($val->type == 'email'){
                                                                        $lable_text = 'wearebrandkit@gmail.com';
                                                                    }
                                                                    if($val->type == 'website'){
                                                                        $lable_text = 'www.wearebrandkit.in';
                                                                    }
                                                                    if($val->type == 'address'){
                                                                        $lable_text = 'Street address, City, State, Country.';
                                                                    }
                                                                    
                                                                 ?>
                                                                    <span 
                                                                    width-100 
                                                                    onmouseout="set_drag_values(this,'{{$val->type}}')" 
                                                                    id="img_{{$val->type}}"
                                                                    class="text-capitalize <?php echo $class ?>"
                                                                    style="<?php echo $css_property; ?>" 
                                                                    >{{$lable_text}}</span>

                                                                <?php } ?>

                                                                </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="accordion-header collapsed" role="button" data-toggle="collapse" data-target="#add-panel-body-2" aria-expanded="false">
                                                        <h4>Step-2 : Frame Icon Settings</h4>
                                                    </div>
                                                    <div class="accordion-body collapse " id="add-panel-body-2" data-parent="#accordion" style="">
                                                        <div class="row">
                                                            <div class="col-md-6"> 
                                                                <div class="form-group">
                                                                    <label for="withdrawinput1">Address Icon: </label>
                                                                    <input type="file" name="address_icon" class="form-control" accept="image/png, image/jpg, image/jpeg">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6"> 
                                                               <div class="form-group">
                                                                    <label for="withdrawinput1">Website Icon: </label>
                                                                    <input type="file" name="website_icon" class="form-control"  accept="image/png, image/jpg, image/jpeg">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label for="withdrawinput1">Phone Icon: </label>
                                                                    <input type="file" name="phone_icon" class="form-control"  accept="image/png, image/jpg, image/jpeg">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label for="withdrawinput1">Email Icon: </label>
                                                                    <input type="file" name="email_icon" class="form-control"  accept="image/png, image/jpg, image/jpeg">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>    
                                                </div>
                                            </div>
                                            <br>
                                            <button type="submit" class="btn btn-success waves-effect waves-light float-right"><i class="fa fa-spinner fa-spin d-none" tabindex="20"></i>Save</button>
                                        </div>
                                    </div>
                                </div> 
                            </div>
                        </form>
                    </div>
                </div>
            </div>        
        </div>
    </section>
</div>

<script src="{{ asset('js/jquery.form.js')}}"></script>
<script src="{{ asset('assets/indrop/inlancer_drop.js')}}"></script>

<script type="text/javascript">

function storePerformance(file_id,returnData) {    
    $("[data-image='"+file_id+"']").val(returnData.image_id);  
    $('#image_setup_section').removeClass('d-none');

    if($("#setup_image").length > 0){
        $("#setup_image").attr('src',$(".previewImage").attr("src"));
    }else{
        var $img = $(".previewImage").clone();
        $("#edit_image_preview").append($img);
        $("#edit_image_preview").find('img').attr('id', 'setup_image');
    }
    $("#setup_image").css({
        "width": '504px',
        "height": '504px',
        "border": '2px solid black',
    });
}

jQuery(document).ready(function() { 
    var dd = {
        beforeSend: function() { 
            $('.fa-spinner').removeClass('d-none');
            save_json_data();
        },
        uploadProgress: function(event, position, total, percentComplete) { 
        },
        success: function() {},
        complete: function(response) {
            var result = jQuery.parseJSON(response.responseText);
            $('.fa-spinner').removeClass('d-none');
            $('.fa-spinner').addClass('d-none');
            if (result.status == 200) {
                Swal.fire({
                    type: 'success',
                    title: result.message,
                    showConfirmButton: false,
                    timer: 1500
                });
                window.location.reload(true); 
            } else {
                Swal.fire({
                    type: 'warning',
                    title: 'Oops',
                    text: result.message,
                    showConfirmButton: false,
                    timer: 2000
                });
            }
        },
        error: function() { 
        }
    };

    save_json_data();
    setTimeout(function(){
        jQuery("#add_form").ajaxForm(dd);
    },1000);

});

async function save_json_data(){
    var json_data = [];
    $('[data-frame]').each(function(index, el) {
        json_temp_arr = [];
        $(el).find('input').each(function(sub_index, sub_el) {
            if($(sub_el).attr('type') == 'number' || $(sub_el).attr('type') == 'color'){
                temp_arr = {
                    name : $(sub_el).attr('data-name'),
                    value : $(sub_el).val()
                };
                json_temp_arr.push(temp_arr);
            }
        });
        parent_json = {
            type:  $(el).attr('data-frame'),
            value: json_temp_arr
        }
        json_data.push(parent_json);
    });
    final_data = JSON.stringify(json_data);
    $('#json_aya_muko').val(final_data);
}

function checkbox_check(passed_this){
    var div_id = $(passed_this).attr('id');
    var input_hidden = $(passed_this).parent().find('[data-name="show"]');
    if($(passed_this).is(':checked')){
        input_hidden.val('1');
        $('[data-show="'+div_id+'"]').removeClass('invisible');
    }else{
        input_hidden.val('0');
        $('[data-show="'+div_id+'"]').addClass('invisible');
    }
    input_hidden.trigger('change');
    save_json_data();
}

function move_this_feild(property = '', passed_value = '', passed_id = ''){
    if(property == '' || passed_value == '' || passed_id == ''){
        return false;
    }
    if(property == 'show'){
        if(passed_value == 0){
            $('#'+passed_id).css('display','none');
        }
        if(passed_value == 1){
            $('#'+passed_id).css('display','unset');
        }
    }
    if(property == 'top'){
        $('#'+passed_id).css('top',passed_value+'px');
    }
    if(property == 'left'){
        $('#'+passed_id).css('left',passed_value+'px');
    }
    if(property == 'rotate'){
        $('#'+passed_id).css('transform','rotate('+passed_value+'deg)');
    }
    if(property == 'font-size'){
        $('#'+passed_id).css('font-size',passed_value+'px');
    }
    if(property == 'color'){
        $('#'+passed_id).css('color',passed_value);
    }
    if(property == 'font-text'){
        $('#'+passed_id).text(passed_value);
    }
    save_json_data();
}

function set_drag_values(passed_this,target_element){
    var top  =  $(passed_this).css("top");
    var left =  $(passed_this).css("left");
    $('[data-show="'+target_element+'"]').find('[data-name="top"]').val(parseFloat(top));
    $('[data-show="'+target_element+'"]').find('[data-name="left"]').val(parseFloat(left));
    
    setTimeout(function(){save_json_data();},200);
}

function toggle_check(passed_this){
    checked_value = $(passed_this).prop('checked');
    $('.main-sec').find('input[type=checkbox]').each(function(index, el) {
        $(el).prop('checked',checked_value);
        checkbox_check(el);
    });
    
}

$(function() {
    $('input[type="number"]').on('keyup', function() {
        var el = $(this),
            val = Math.max((0, el.val())),
            max = parseInt(el.attr('max'));
        el.val(isNaN(max) ? val : Math.min(max, val));
        el.trigger('change');
  });
});

SET_DHTML("img_name", "img_logo", "img_phone1", "img_phone2","img_email" ,"img_website" ,"img_address");

</script>


@endsection