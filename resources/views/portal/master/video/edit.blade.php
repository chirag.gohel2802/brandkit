@extends('portal.template.app') 
@section('content')

<link rel="stylesheet" type="text/css" href="{{asset('assets/indrop/inlancer_drop.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('festival_inlancer/vendors/summernote/summernote.css')}}">  

<?php 
$page_title = 'Edit Video';
$route_name = 'video'; 
$mode = 'edit';
$save_url = url($route_name.'-save');
$model_size = 'modal-lg';

?> 
<style type="text/css"> 
    textarea{
        height: unset;
    } 
</style>
<div class="main-content">
    <section class="section">
        <div class="section-header">
            <h1><?php echo $page_title; ?></h1>
            <div class="section-header-breadcrumb">
                <div class="breadcrumb-item active"><a href="<?php echo url('portal');?>">Dashboard</a></div> 
                <div class="breadcrumb-item"><?php echo ucfirst($route_name); ?></div>
            </div>
        </div>
        <div class="section-body">
            <div class="row">
                <div class="col-12"> 
                    <div class="card">
                        <form class="form edit_form" method="POST" id="edit_form" action="{{$save_url}}" enctype="multipart/form-data">
                            <input type="hidden" name="mode" value="{{$mode}}">
                            <input type="hidden" name="id" value="{{$video_id}}">
                            @csrf
                            <div id="message"></div> 
                            <div class="form-body">
                                <div class="row">
                                    <div class="col-md-12 col-lg-12 col-xl-12 pb-5">
                                        <div class="card-body">
                                            <div id="accordion">
                                                <div class="accordion">
                                                    <div class="mb-2 p-2" style="box-shadow: 0 2px 6px #acb5f6;background-color: #c60b008a;color: #fff;border-radius: 8px;">
                                                        <p>Illegal Drugs, Horrifying & Scary Elements, Nazi Symbols - swastika symbol 卐, Violence Towards Vulnerable or Defenseless Characters, Sexual Material, Offensive Language, Tobacco, Creatures Behave Like Humans (Aliens), Violence, Age-Restricted Physical Goods, Lottery</p>
                                                    </div>
                                                    <div class="accordion-header collapsed" role="button" data-toggle="collapse" data-target="#edit-panel-body-1" aria-expanded="true">
                                                        <h4>Step-1 : Video Basic Info</h4>
                                                    </div>
                                                    <div class="accordion-body collapse show" id="edit-panel-body-1" data-parent="#accordion" style="">
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="row">
                                                                    <div class="col-md-12">
                                                                        <div class="form-group">
                                                                            <label for="withdrawinput1">Video Category: </label>
                                                                            <select name="video_category" class="form-control select2">
                                                                                <option value="0" selected disabled>Select Category</option>
                                                                                <?php 
                                                                                if(!empty($category_list)){
                                                                                    foreach ($category_list as $category) { 
                                                                                        $selected = '';
                                                                                        if($category->category_id == $video_category){
                                                                                            $selected = 'selected';
                                                                                        }?>
                                                                                        <option value="{{$category->category_id}}" {{$selected}}>{{$category->category_date}} > {{$category->category_name}}</option>
                                                                                <?php } } ?>
                                                                            </select> 

                                                                        </div> 
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="row">
                                                                    <div class="col-md-12">
                                                                        <div class="form-group">
                                                                            <label for="withdrawinput1">Video Name: </label>
                                                                            <input type="text" name="video_name" id="video_name" class="form-control" value="{{$video_name}}">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="row">  
                                                                    <div class="col-md-12">
                                                                        <div class="form-group">
                                                                            <label for="withdrawinput1">Video Date: </label>
                                                                            <input type="date" name="video_date" id="video_date" class="form-control" value="{{$video_date}}">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="row">  
                                                                    <div class="col-md-12">
                                                                        <div class="form-group">
                                                                           <label for="withdrawinput1">Video Package: </label>
                                                                             <select name="video_package" class="form-control select2">
                                                                            <?php if($video_package == 'free'){ ?>
                                                                            <option value="free" selected>Free</option>
                                                                            <option value="premium">Premium</option>
                                                                            <?php }else{ ?> 
                                                                            <option value="free" >Free</option>
                                                                            <option value="premium" selected>Premium</option>
                                                                            <?php } ?>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="row">  
                                                                    <div class="col-md-12">
                                                                        <div class="form-group">
                                                                           <label for="withdrawinput1">Video Type: </label>
                                                                             <select name="video_type" class="form-control select2">
                                                                            <?php if($video_type == 'festival'){ ?>
                                                                            <option value="festival" selected>Festival</option>
                                                                            <option value="incident">Incident</option>
                                                                            <?php }else{ ?> 
                                                                            <option value="festival" >Festival</option>
                                                                            <option value="incident" selected>Incident</option>
                                                                            <?php } ?>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="row">
                                                                    <div class="col-md-12">
                                                                        <div class="form-group">
                                                                            <label for="withdrawinput1">Video Search Keyword: </label>
                                                                            <textarea name="video_search_keyword" class="form-control" >{{$video_search_keyword}}</textarea>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="row">  
                                                                    <div class="col-md-10">
                                                                        <div class="form-group">
                                                                            <label for="withdrawinput1">Video File </label>
                                                                            <input type="file" name="video_file" id="video_file" accept="video/mp4,video/x-m4v,video/*" class="form-control" >
                                                                        </div>
                                                                    </div>
                                                                    <input type="hidden" name="old_video_file" value="{{$video_file}}">
                                                                    <div class="col-md-2">
                                                                        <div class="form-group">
                                                                            <label for="withdrawinput1"></label>
                                                                            @if(!empty($video_file))
                                                                            <a target="_blank" href="<?php echo url('public/assets/upload/videos/'.$video_file);?>">
                                                                            <i class="fa fa-file-video" style="font-size: 70px;"></i>
                                                                            </a>
                                                                            @endif
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <!-- video_image -->
                                                            <div class="col-md-6">
                                                                <div id="i1" class="drop-area" data-throwback="storePerformance" onclick="document.getElementById('fileElem1').click()"> 
                                                                    <div class="text-center" id="imgPreviewi1">
                                                                      <i class="bi bi-cloud-arrow-up mb-3" style="color:#445dbe;font-size: 60px;"></i>
                                                                      <h3 style="color: #445dbe;font-size: 18px;line-height: 100px;">Click "Here" or drop your image here<span style="color:red">*</span></h3>
                                                                    </div>
                                                                    <input class="d-none" type="file" id="fileElem1" accept="" onchange="handleFiles(this.files,'i1','storePerformance')">   
                                                                </div>
                                                              <progress id="progress-bar" class="d-none" max=100 value=0></progress>
                                                            </div>
                                                            <input type="hidden" id="video_image" name="video_image" data-image="i1" value="{{$video_image}}">
                                                            <!-- video_image -->
                                                            
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <br>
                                            <button type="submit" class="btn btn-success waves-effect waves-light float-right"><i class="fa fa-spinner fa-spin d-none" tabindex="20"></i>Save</button>
                                        </div>
                                    </div>
                                </div> 
                            </div>
                        </form>
                    </div>
                </div>
            </div>        
        </div>
    </section>
</div>
<script src="{{ asset('js/jquery.form.js')}}"></script>
<script src="{{ asset('assets/indrop/inlancer_drop.js')}}"></script>
<script src="{{ asset('festival_inlancer/vendors/summernote/summernote.min.js')}}"></script>

<script type="text/javascript">
$(document).on('change','select[name="video_category"]',function () {
    let selectedVal = $('select[name="video_category"] option:selected').text();
    let searchKeyWord = selectedVal.split('>')[1];
    $('textarea[name="video_search_keyword"]').val(searchKeyWord.trim());
    $('input[name="video_name"]').val(searchKeyWord.trim());
});
/*image Show */
  var image = '{{$image_name}}';
  var file_id1 = 'i1';  
  if(typeof image!=='undefined' && image!=='') {
    var isrc = APPLICATION_URL+'/assets/upload/images/thumb/'+image;
    $('#imgPreview'+file_id1).empty();   
    var img = '<img onerror="setImage(this);"  class="previewImage" src="'+isrc+'">';  
    var div = '<div class="col-12"><div style="padding: 5px;margin-bottom: 8px;">'+img+'</div></div>';
    $('#imgPreview'+file_id1).append(div);    
  }
function setImage(img){
console.log(img);
    img.src="{{url('/assets/img/placeholder.png');}}";  
    img.style.height="80px";
}
function storePerformance(file_id,returnData) {
    $("[data-image='"+file_id+"']").val(returnData.image_id);
}
/*End */

jQuery(document).ready(function() { 
    var dd = {
        beforeSend: function() { 
            $('.fa-spinner').removeClass('d-none');
        },
        uploadProgress: function(event, position, total, percentComplete) { 
        },
        success: function() {},
        complete: function(response) {
            var result = jQuery.parseJSON(response.responseText);
            $('.fa-spinner').removeClass('d-none');
            $('.fa-spinner').addClass('d-none');
            if (result.status == 200) {
                Swal.fire({
                    type: 'success',
                    title: result.message,
                    showConfirmButton: false,
                    timer: 1500
                });
                setTimeout(function(){
                    window.location.href= '{{url($route_name."-master")}}';
                },2000); 
            } else {
                Swal.fire({
                    type: 'warning',
                    title: 'Oops',
                    text: result.message,
                    showConfirmButton: false,
                    timer: 2000
                });
            }
        },
        error: function() { 
        }
    };
    jQuery("#edit_form").ajaxForm(dd);
});

</script>

@endsection