@extends('portal.template.blank') 
@section('content') 
    <section class="section">
        <div class="container mt-5">
            <div class="row"> 
                <div class="col-12 col-sm-8 offset-sm-2 col-md-6 offset-md-3 col-lg-6 offset-lg-3 col-xl-4 offset-xl-4"> 
                    <div class="login-brand text-center"> 
                        <img style="height: 60px;" src="{{asset('festival_inlancer/img/logo.svg')}}"> 
                    </div>
                    <div class="card card-primary">
                        <div class="card-header"> 
                            <h4>Login</h4>  
                        </div> 
                        <div class="card-body">
                            @include('messages.flash-message')
                            @php
                                $mail_verified = 1;
                                $default_data=[];
                                if(Session::get('data')){
                                    $default_data = Session::get('data'); 
                                }

                                if(!empty($default_data['mail_message'])){
                                    $result = json_decode($default_data['mail_message'],true);
                                    if(isset($result['status']) && $result['status']==0){ 
                                        $mail_verified = 0;
                                    } 
                                }
                            @endphp
                             
                            <form method="POST" action="{{url('portal-do-login')}}" class="needs-validation" novalidate="">
                                @csrf
                                <div class="form-group">
                                    <label for="email">Email</label>
                                    <input id="email" type="email" value="{{$default_data['user_email'] ?? '' }}" class="form-control" name="email"  required autofocus>
                                    <div class="invalid-feedback">
                                        Please fill in your Email ID
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="d-block">
                                        <label for="password" class="control-label">Password</label>
                                        <!-- <div class="float-right">
                                            <a href="{{url('portal-reset-password')}}" class="text-small">
                                                Forgot Password?
                                            </a>
                                        </div> -->
                                    </div>
                                    <input autocomplete="false" readonly onfocus="this.removeAttribute('readonly');" id="password" autocomplete="new-password" type="password" class="form-control" name="password"   required>
                                    <div class="invalid-feedback">
                                        Please fill in your Password
                                    </div>
                                </div>
                                <!-- <div class="form-group">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" name="remember" class="custom-control-input" tabindex="3" id="remember-me">
                                        <label class="custom-control-label" for="remember-me">Remember Me</label>
                                    </div>
                                </div> -->
                                <div class="form-group">
                                    <button type="submit" class="btn btn-primary btn-lg btn-block" tabindex="4">
                                        Login
                                    </button>
                                </div>
                            </form> 
                        </div>
                    </div> 
                     
                </div>
            </div>
        </div>
    </section> 
@endsection