@extends('portal.template.blank') 
@section('content') 
    <section class="section">
        <div class="container mt-5">
            <div class="row">
                <div class="col-12 col-sm-8 offset-sm-2 col-md-6 offset-md-3 col-lg-6 offset-lg-3 col-xl-4 offset-xl-4"> 
                    <div class="login-brand text-center"> 
                        <img style="height: 60px;" src="{{url('public/manager_template/img/reichFrontLogo.png')}}"> 
                    </div>
                    <div class="card card-primary">
                        <div class="card-header">
                            <h4>Change Your Password</h4>
                        </div> 
                        <div class="card-body">
                            @include('messages.flash-message')
                            @php
                                $mail_verified = 1;
                                $default_data=[];
                                if(Session::get('data')){
                                    $default_data = Session::get('data'); 
                                }

                                if(!empty($default_data['mail_message'])){
                                    $result = json_decode($default_data['mail_message'],true);
                                    if(isset($result['status']) && $result['status']==0){ 
                                        $mail_verified = 0;
                                    } 
                                }
                            @endphp
                             
                            <form method="POST" action="{{url('portal-do-change-token-password')}}" class="needs-validation" novalidate="">
                                @csrf
                                <div class="form-group">
                                    <label for="password">New Password</label>
                                    <input id="password" type="password"  class="form-control" name="password"  required autofocus>
                                    <input type="hidden" name="token" value="{{$token}}">
                                    <div class="invalid-feedback">
                                        Please fill in your Password
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="d-block">
                                        <label for="password" class="control-label">Confirm Password</label> 
                                    </div>
                                    <input autocomplete="false" readonly onfocus="this.removeAttribute('readonly');" id="confirm_password" autocomplete="new-password" type="password" class="form-control" name="confirm_password"   required>
                                    <div class="invalid-feedback">
                                        Please fill in your confirm Password
                                    </div>
                                </div> 
                                <div class="form-group">
                                    <button type="submit" class="btn btn-primary btn-lg btn-block" tabindex="4">
                                        Update Now
                                    </button>
                                </div>
                            </form> 
                        </div>
                    </div> 
                    <div class="  text-muted text-center">
                      Want to Login ? 
                      <a href="{{url('portal-login')}}">Login</a>
                    </div>
                    
                </div>
            </div>
        </div>
    </section> 
@endsection