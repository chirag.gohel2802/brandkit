@extends('portal.template.blank') 
@section('content') 
    <section class="section">
        <div class="container mt-5">
            <div class="row">
                <div class="col-12 col-sm-8 offset-sm-2 col-md-6 offset-md-3 col-lg-6 offset-lg-3 col-xl-4 offset-xl-4"> 
                    <div class="login-brand text-center"> 
                        <img style="height: 60px;" src="{{url('public/manager_template/img/reichFrontLogo.png')}}"> 
                    </div>
                    <div class="card card-primary">
                        <div class="card-header">
                            <h4   style="color: #3f5236 !important">Verify Email</h4>
                        </div> 
                        <div class="card-body">
                            @include('messages.flash-message')
                            @php
                                $mail_otp = 0;
                                $default_data=[];
                                if(Session::get('data')){
                                    $default_data = Session::get('data'); 
                                }

                                if(!empty($default_data['mail_message'])){
                                    $result = json_decode($default_data['mail_message'],true);
                                    if(isset($result['status']) && $result['status']==1){ 
                                        $mail_otp = 1;
                                    } 
                                }
                            @endphp
                             
                            <form method="POST" action="{{url('portal-send-otp')}}" class="needs-validation" novalidate="">
                                @csrf
                                @php
                                $email_show =   'd-none'; 
                                $otp_show   =   ''; 
                                if($mail_otp==0){ 
                                    $email_show =   ''; 
                                    $otp_show   =   'd-none'; 
                                } 
                                @endphp
                                <div class="form-group {{$email_show}}">
                                    <label for="email">Email</label>
                                    <input id="email" type="email" value="{{$default_data['user_email'] ?? '' }}" class="form-control" name="user_email"  required autofocus>
                                    <div class="invalid-feedback">
                                        Please fill in your Email ID
                                    </div>
                                </div>
                                <div class="form-group {{$otp_show}}">
                                    <div class="d-block">
                                        <label for="password" class="control-label">Enter Your OTP</label> 
                                    </div>
                                    <input autocomplete="false" readonly onfocus="this.removeAttribute('readonly');" id="password" autocomplete="new-password" type="text" class="form-control" name="user_otp"   required>
                                    <div class="invalid-feedback">
                                        Please fill in your OTP
                                    </div>
                                </div> 
                                <div class="form-group">
                                    <button type="submit" class="btn btn-primary btn-lg btn-block" tabindex="4">
                                        Verify
                                    </button>
                                </div>
                            </form> 
                        </div>
                    </div> 
                    <div class="  text-muted text-center"> 
                      <a href="{{url('portal')}}">GO BACK</a>
                    </div>
                     
                </div>
            </div>
        </div>
    </section> 
@endsection