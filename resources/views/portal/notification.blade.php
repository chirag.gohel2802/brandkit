@extends('portal.template.app') 
@section('content')
<style type="text/css">
    .dropdown-list-content::after {
        content: none!important;
    }
    .alice {
        background: aliceblue;
    }
    .p-cursor{
        cursor: pointer
    }
    .up-green{
       color:#ec673b
    }
     
</style>
 <div class="main-content">
    <section class="section"> 
        <div class="section-header">
            <h1>Notifications</h1>
            <div class="section-header-breadcrumb">
                <div class="breadcrumb-item active"><a href="{{url('')}}">Dashboard</a></div> 
                <div class="breadcrumb-item">Notifications</div>
            </div>
        </div>
        

        @if(!empty($notifications))
            @php
                $no_noti_class='d-none';
            @endphp
            @foreach($notifications as $key=>$value)
                @php
                    if($value->noti_type_id==1){
                        $pre    =  __('label.Task');
                        $post   =  __('label.Assigned');
                    }
                    elseif($value->noti_type_id==3){
                        $pre    =  __('label.Form');
                        $post   =  __('label.Assigned');
                    }else{
                        $pre    =  '';
                        $post   =  '';
                    }
                @endphp



            <div class="card noti-list noti_id_{{$value->noti_id}}" style="margin-bottom: 8px;font-size: 1rem;">
                <div class="row" style="padding:10px" > 
                    <div class="col-10" onclick="openNoti();">
                        <span class="p-cursor ">
                            {{$pre}} "{{$value->noti_title}}" {{$post}}
                        </span>
                        <br><span class="" style="color: grey">{{__('label.'.date('F',strtotime($value->created_at)))}} {{date('d',strtotime($value->created_at))}}</span>
                    </div>
                    <div class="col-2 text-right" onclick="removeNoti(this,'{{$value->noti_id}}');" style="justify-content: right; display: flex;">
                        <svg class="p-cursor"  style="height: 0.7rem;margin:auto" fill="#37a000" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 14 14" aria-hidden="true" role="img"><polygon points="12.524 0 7 5.524 1.476 0 0 1.476 5.524 7 0 12.524 1.476 14 7 8.476 12.524 14 14 12.524 8.476 7 14 1.476"></polygon></svg>
                    </div>
                </div>  
            </div> 
            @endforeach 
        @else
            @php
                $no_noti_class='';
            @endphp
        @endif
        <div class="{{$no_noti_class}}" id="no_noti" style="margin-bottom: 8px;font-size: 1rem;">
            <div class="row" style="padding:10px" > 
                <div class="col-12 text-center">
                    <h5 class="up-green"> No Notifications Yet  !</h5><br>
                    <img style="height: 200px;" src="{{asset('assets/images/notification.svg')}}">
                    <br><br>
                    <p class="up-green"> We'll notify you when something arrives   !</p>
                </div>
            </div>
        </div>

         
 
    </section>
</div>


<script type="text/javascript">  
    jQuery(document).ready(function() {   
        var all_words = ["update", "task","Covid Survey In Maputo"];
        all_words.forEach(function(word) { 
            var replacement = '<span class="up-green">' + word + '</span>';   
            var re = new RegExp(word, 'ig'); 
             
            $('.noti-list').each(function() {
                var text = $(this).html();
                $(this).html(text.replace(re, replacement)); 
            });  

        }); 
    });

    

    function openNoti() { 
        // alert('Opened');
    }

    function removeNoti(item,noti_id) {  
        $('.noti_id_'+noti_id).css('opacity','0.5');  
        $.ajax({
            headers: {
              'X-CSRF-TOKEN': '{{csrf_token()}}'
            },
            type: "POST",
            data: {noti_id:noti_id},
            url: "{{url('remove-noti')}}",
            cache: false,
            success: function(response) {
                var json_data = jQuery.parseJSON(response);       
                $('#noti_count_number').text(' ('+json_data.count+')');
                if (json_data.status=="200") {   
                    if (json_data.count==0 || json_data.count<1) {
                        $('#no_noti').removeClass('d-none');
                        $('.notification-toggle').removeClass('beep');
                    }else{
                        $('.notification-toggle').addClass('beep');
                    }
                    iziToast.warning({
                        title: 'Removed!',
                        message: json_data.message,
                        position: 'topRight',
                        timeout: 1400,
                    }); 
                     $('.noti_id_'+noti_id).remove(); 
                }else{
                    alert('failed');
                }
            }
        }); 
    }


</script> 
@endsection