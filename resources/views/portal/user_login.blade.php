<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<meta http-equiv="content-type" content="text/html;charset=utf-8" /> 

<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport"> 

    <link rel="apple-touch-icon" sizes="57x57" href="{{url('/public/manager_template/favicon/favicon1.png')}}">
    <!-- <link rel="apple-touch-icon" sizes="60x60" href="{{url('/public/manager_template/favicon/apple-icon-60x60.png')}}">
    <link rel="apple-touch-icon" sizes="72x72" href="{{url('/public/manager_template/favicon/apple-icon-72x72.png')}}">
    <link rel="apple-touch-icon" sizes="76x76" href="{{url('/public/manager_template/favicon/apple-icon-76x76.png')}}">
    <link rel="apple-touch-icon" sizes="114x114" href="{{url('/public/manager_template/favicon/apple-icon-114x114.png')}}">
    <link rel="apple-touch-icon" sizes="120x120" href="{{url('/public/manager_template/favicon/apple-icon-120x120.png')}}">
    <link rel="apple-touch-icon" sizes="144x144" href="{{url('/public/manager_template/favicon/apple-icon-144x144.png')}}">
    <link rel="apple-touch-icon" sizes="152x152" href="{{url('/public/manager_template/favicon/apple-icon-152x152.png')}}">
    <link rel="apple-touch-icon" sizes="180x180" href="{{url('/public/manager_template/favicon/apple-icon-180x180.png')}}">
    <link rel="icon" type="image/png" sizes="192x192" href="{{url('/public/manager_template/favicon/android-icon-192x192.png')}}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{url('/public/manager_template/favicon/favicon-32x32.png')}}">
    <link rel="icon" type="image/png" sizes="96x96" href="{{url('/public/manager_template/favicon/favicon-96x96.png')}}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{url('/public/manager_template/favicon/favicon-16x16.png')}}">
    <link rel="manifest" href="{{url('/public/manager_template/favicon/manifest.json')}}">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="{{url('/public/manager_template/favicon/ms-icon-144x144.png')}}">
    <meta name="theme-color" content="#ffffff"> -->

    <title>@if(isset($data['title']) && $data['title']!='') {{$data['title']}} @else Portal @endif | {{ config('app.name') }}</title>
        
    <!-- General CSS Files -->
    <!-- Styles --> 
    <link rel="stylesheet" href="{{ asset('public/manager_template/modules/bootstrap/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{('public/manager_template//modules/fontawesome/css/all.min.css')}}">
     
 
    
    <!-- Template CSS -->
    <link rel="stylesheet" href="{{ asset('public/manager_template/css/style.css')}}">
    <link rel="stylesheet" href="{{ asset('public/manager_template/css/components.css')}}">
    <!-- Start GA -->
    <!-- /END GA -->

    <script src="{{ asset('public/manager_template/modules/jquery.min.js')}}"></script>
 
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
</head>

<body>
    <div id="app">
        <div class="main-wrapper main-wrapper-1">
            <div class="navbar-bg"></div>
            <nav class="navbar navbar-expand-lg main-navbar">
                <form class="form-inline mr-auto">
                    <ul class="navbar-nav mr-3">
                        <li><a href="#" data-toggle="sidebar" class="nav-link nav-link-lg"><i class="fas fa-bars"></i></a></li>
                    </ul>
                </form>
                <ul class="navbar-nav navbar-right">
                    <li class="dropdown dropdown-list-toggle"><a href="#" data-toggle="dropdown" class="nav-link notification-toggle nav-link-lg beep"><i class="far fa-bell"></i></a>
                        <div class="dropdown-menu dropdown-list dropdown-menu-right">
                            <div class="dropdown-header">Notifications
                                <div class="float-right">
                                    <a href="#" onclick="read_all_notifications()">Mark All As Read</a>
                                </div>
                            </div>
                            <div class="notification-list-content dropdown-list-icons">
                                <div id="unread_notis"></div>
                            </div>
                            <div class="dropdown-footer text-center">
                                <a href="{{url('/grocery-manager-notifications')}}">View All <i class="fas fa-chevron-right"></i></a>
                            </div>
                        </div>
                    </li>
                    <li class="dropdown"><a href="#" data-toggle="dropdown" class="nav-link dropdown-toggle nav-link-lg nav-link-user">
                            <img alt="My Profile" src="{{url('/public/manager_template/img/avatar/avatar-4.png')}}" class="rounded-circle mr-1">
                            <div class="d-sm-none d-lg-inline-block">Hi,
                                 
                            </div>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right">
                            <div class="dropdown-title">Logged in
                                20 Min Ago
                            </div>
                            <a  style="@if(isset($data['active']) && $data['active']=='profile') {{'background:#18bf81;color:white'}} @endif" href="
                                {{url('my-profile')}}" class="dropdown-item has-icon">
                                <i class="far fa-user"></i> Profile
                            </a>
                            <a   href="#" class="dropdown-item has-icon">
                                <i class="fas fa-bolt"></i> Notifications
                            </a>
                            <a  href="#" class="dropdown-item has-icon">
                                <i class="fas fa-cog"></i> Settings
                            </a>
                            <div class="dropdown-divider"></div>
                            <a href="{{url('portal-logout')}}" class="dropdown-item has-icon text-danger">
                                <i class="fas fa-sign-out-alt"></i> Logout
                            </a>
                        </div>
                    </li>
                </ul>
            </nav>
            <div class="main-sidebar sidebar-style-2">
                <aside id="sidebar-wrapper">
                    <div class="sidebar-brand">
                        <a href="{{url('portal')}}"> 
                            <img style="height: 65px;" src="{{url('/public/manager_template/img/logo.png')}}">
                        </a>
                    </div>
                    <div class="sidebar-brand sidebar-brand-sm">
                        <a href="{{url('portal')}}">GS</a>
                    </div>
                    <ul class="sidebar-menu">
                        <li class="menu-header">Dashboard</li>
                        <li class="@if(isset($data['active']) && $data['active']=='home') {{'active'}} @endif">
                            <a href="{{url('portal')}}" class="nav-link"><i class="fas fa-fire"></i><span>Home</span></a>
                        </li>
                        <li class="menu-header">User Management</li>
                        <li class="@if(isset($data['active']) && $data['active']=='user') {{'active'}} @endif">
                            <a href="{{url('user-master')}}" class="nav-link"><i class="fas fa-user"></i><span>User Master</span></a>
                        </li>
                        <li class="menu-header">Site Management</li>
                        <li class="@if(isset($data['active']) && $data['active']=='site') {{'active'}} @endif">
                            <a href="{{url('site-master')}}" class="nav-link"><i class="fas fa-shopping-cart"></i><span>Site Master</span></a>
                        </li>
                        <li class="@if(isset($data['active']) && $data['active']=='category') {{'active'}} @endif">
                            <a href="#" class="nav-link"><i class="fas fa-th"></i> <span>Category Master</span></a> 
                        </li>
                        <li class="">
                            <a href="{{url('brand')}}" class="nav-link"><i class="fas fa-university"></i><span>Brand Master</span></a>
                        </li>
                        <li class="menu-header">Order Management</li>
                        <li class="">
                            <a href="#" class="nav-link"><i class="fa fa-cart-plus"></i><span>Order Master</span></a>
                        </li>
                        <li class="">
                            <a href="#" class="nav-link"><i class="fa fa-cart-arrow-down"></i><span>Return Order Master</span></a>
                        </li>
                        <li class="menu-header">Shipping & Are Management</li>
                        <li class="">
                            <a href="{{url('area-master')}}" class="nav-link"><i class="fas fa-map-pin"></i><span>Service Areas</span></a>
                        </li>
                        <li class="">
                            <a href="#" class="nav-link"><i class="fas fa-bicycle"></i><span>Shipping Master</span></a>
                        </li>
                        <li class="menu-header">Finance & Reports</li>
                        <li class="">
                            <a href="#" class="nav-link"><i class="fa fa-book"></i><span>Sales Transactions</span></a>
                        </li>
                        <li class="">
                            <a href="#" class="nav-link"><i class="fa fa-credit-card"></i><span>Payments & Refunds</span></a>
                        </li>
                        <li class="dropdown">
                            <a href="#" class="nav-link has-dropdown"><i class="fa fa-book"></i> <span>Reports</span></a>
                            <ul class="dropdown-menu">
                                <li><a href="#">Wallet</a></li>
                                <li><a href="#">Sales Reports</a></li>
                            </ul>
                        </li>
                    </ul>
                    <div class="mt-4 mb-4 p-3 hide-sidebar-mini">
                        <a href="https://inlancer.in" target="_blank" class="btn btn-primary btn-lg btn-block btn-icon-split">
                            <i class="fas fa-rocket"></i> Contact Us
                        </a>
                    </div>
                </aside>
            </div>
            <div class="main-content">
    <section class="section">
        <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-12">
                <div class="card card-statistic-2">
                    <div class="card-stats">
                        <div class="card-stats-title">Order Statistics -
                            <div class="dropdown d-inline">
                                <a class="font-weight-600 dropdown-toggle" data-toggle="dropdown" href="#" id="orders-month">August</a>
                                <ul class="dropdown-menu dropdown-menu-sm">
                                    <li class="dropdown-title">Select Month</li>
                                    <li><a href="#" class="dropdown-item">January</a></li>
                                    <li><a href="#" class="dropdown-item">February</a></li>
                                    <li><a href="#" class="dropdown-item">March</a></li>
                                    <li><a href="#" class="dropdown-item">April</a></li>
                                    <li><a href="#" class="dropdown-item">May</a></li>
                                    <li><a href="#" class="dropdown-item">June</a></li>
                                    <li><a href="#" class="dropdown-item">July</a></li>
                                    <li><a href="#" class="dropdown-item active">August</a></li>
                                    <li><a href="#" class="dropdown-item">September</a></li>
                                    <li><a href="#" class="dropdown-item">October</a></li>
                                    <li><a href="#" class="dropdown-item">November</a></li>
                                    <li><a href="#" class="dropdown-item">December</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="card-stats-items">
                            <div class="card-stats-item">
                                <div class="card-stats-item-count">24</div>
                                <div class="card-stats-item-label">Pending</div>
                            </div>
                            <div class="card-stats-item">
                                <div class="card-stats-item-count">12</div>
                                <div class="card-stats-item-label">Shipping</div>
                            </div>
                            <div class="card-stats-item">
                                <div class="card-stats-item-count">23</div>
                                <div class="card-stats-item-label">Completed</div>
                            </div>
                        </div>
                    </div>
                    <div class="card-icon shadow-primary bg-primary">
                        <i class="fas fa-archive"></i>
                    </div>
                    <div class="card-wrap">
                        <div class="card-header">
                            <h4>Total Orders</h4>
                        </div>
                        <div class="card-body">
                            59
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-12">
                <div class="card card-statistic-2">
                    <div class="card-stats">
                        <div class="card-stats-title">Order Statistics -
                            <div class="dropdown d-inline">
                                <a class="font-weight-600 dropdown-toggle" data-toggle="dropdown" href="#" id="orders-month">August</a>
                                <ul class="dropdown-menu dropdown-menu-sm">
                                    <li class="dropdown-title">Select Month</li>
                                    <li><a href="#" class="dropdown-item">January</a></li>
                                    <li><a href="#" class="dropdown-item">February</a></li>
                                    <li><a href="#" class="dropdown-item">March</a></li>
                                    <li><a href="#" class="dropdown-item">April</a></li>
                                    <li><a href="#" class="dropdown-item">May</a></li>
                                    <li><a href="#" class="dropdown-item">June</a></li>
                                    <li><a href="#" class="dropdown-item">July</a></li>
                                    <li><a href="#" class="dropdown-item active">August</a></li>
                                    <li><a href="#" class="dropdown-item">September</a></li>
                                    <li><a href="#" class="dropdown-item">October</a></li>
                                    <li><a href="#" class="dropdown-item">November</a></li>
                                    <li><a href="#" class="dropdown-item">December</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="card-stats-items">
                            <div class="card-stats-item">
                                <div class="card-stats-item-count">24</div>
                                <div class="card-stats-item-label">Pending</div>
                            </div>
                            <div class="card-stats-item">
                                <div class="card-stats-item-count">12</div>
                                <div class="card-stats-item-label">Shipping</div>
                            </div>
                            <div class="card-stats-item">
                                <div class="card-stats-item-count">23</div>
                                <div class="card-stats-item-label">Completed</div>
                            </div>
                        </div>
                    </div>
                    <div class="card-icon shadow-primary bg-primary">
                        <i class="fas fa-dollar-sign"></i>
                    </div>
                    <div class="card-wrap">
                        <div class="card-header">
                            <h4>Balance</h4>
                        </div>
                        <div class="card-body">
                            $187,13
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-12">
                <div class="card card-statistic-2">
                    <div class="card-stats">
                        <div class="card-stats-title">Order Statistics -
                            <div class="dropdown d-inline">
                                <a class="font-weight-600 dropdown-toggle" data-toggle="dropdown" href="#" id="orders-month">August</a>
                                <ul class="dropdown-menu dropdown-menu-sm">
                                    <li class="dropdown-title">Select Month</li>
                                    <li><a href="#" class="dropdown-item">January</a></li>
                                    <li><a href="#" class="dropdown-item">February</a></li>
                                    <li><a href="#" class="dropdown-item">March</a></li>
                                    <li><a href="#" class="dropdown-item">April</a></li>
                                    <li><a href="#" class="dropdown-item">May</a></li>
                                    <li><a href="#" class="dropdown-item">June</a></li>
                                    <li><a href="#" class="dropdown-item">July</a></li>
                                    <li><a href="#" class="dropdown-item active">August</a></li>
                                    <li><a href="#" class="dropdown-item">September</a></li>
                                    <li><a href="#" class="dropdown-item">October</a></li>
                                    <li><a href="#" class="dropdown-item">November</a></li>
                                    <li><a href="#" class="dropdown-item">December</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="card-stats-items">
                            <div class="card-stats-item">
                                <div class="card-stats-item-count">24</div>
                                <div class="card-stats-item-label">Pending</div>
                            </div>
                            <div class="card-stats-item">
                                <div class="card-stats-item-count">12</div>
                                <div class="card-stats-item-label">Shipping</div>
                            </div>
                            <div class="card-stats-item">
                                <div class="card-stats-item-count">23</div>
                                <div class="card-stats-item-label">Completed</div>
                            </div>
                        </div>
                    </div>
                    <div class="card-icon shadow-primary bg-primary">
                        <i class="fas fa-shopping-bag"></i>
                    </div>
                    <div class="card-wrap">
                        <div class="card-header">
                            <h4>Sales</h4>
                        </div>
                        <div class="card-body">
                            4,732
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row"> 
            <div class="col-lg-6">
                <div class="card gradient-bottom">
                    <div class="card-header">
                        <h4>All Users</h4> 
                    </div>
                    <div class="card-body" id="top-5-scroll">
                        <ul class="list-unstyled list-unstyled-border">  
                            <?php foreach($data['users'] as $key=>$value) { ?>
                            <li class="media">
                                <img class="mr-3 rounded" width="55" src="{{url('public/manager_template/img/products/product-3-50.png')}}" alt="product">
                                <div class="media-body">
                                    <div class="float-right">
                                        <div class="font-weight-600 text-muted text-small">{{$value->user_email_id}}</div>
                                    </div>
                                    <div class="media-title">{{$value->user_full_name}}</div>
                                    <div class="mt-1">
                                        <div class="budget-price"> 
                                            <div class="budget-price-label">{{$value->user_phone_no}}</div>
                                        </div>
                                         
                                    </div>
                                </div>
                            </li>
                            <?php } ?>   
                        </ul>
                    </div> 
                </div>
            </div>
            <div class="col-md-6">
                <div class="card">
                    <div class="card-header">
                        <h4>Top Countries</h4>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="text-title mb-2">July</div>
                                <ul class="list-unstyled list-unstyled-border list-unstyled-noborder mb-0">
                                    <li class="media">
                                        <img class="img-fluid mt-1 img-shadow" src="{{url('public/manager_template/modules/flag-icon-css/flags/4x3/id.svg')}}" alt="image" width="40">
                                        <div class="media-body ml-3">
                                            <div class="media-title">Indonesia</div>
                                            <div class="text-small text-muted">3,282 <i class="fas fa-caret-down text-danger"></i></div>
                                        </div>
                                    </li>
                                    <li class="media">
                                        <img class="img-fluid mt-1 img-shadow" src="{{url('public/manager_template/modules/flag-icon-css/flags/4x3/my.svg')}}" alt="image" width="40">
                                        <div class="media-body ml-3">
                                            <div class="media-title">Malaysia</div>
                                            <div class="text-small text-muted">2,976 <i class="fas fa-caret-down text-danger"></i></div>
                                        </div>
                                    </li>
                                    <li class="media">
                                        <img class="img-fluid mt-1 img-shadow" src="{{url('public/manager_template/modules/flag-icon-css/flags/4x3/us.svg')}}" alt="image" width="40">
                                        <div class="media-body ml-3">
                                            <div class="media-title">United States</div>
                                            <div class="text-small text-muted">1,576 <i class="fas fa-caret-up text-success"></i></div>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                            <div class="col-sm-6 mt-sm-0 mt-4">
                                <div class="text-title mb-2">August</div>
                                <ul class="list-unstyled list-unstyled-border list-unstyled-noborder mb-0">
                                    <li class="media">
                                        <img class="img-fluid mt-1 img-shadow" src="{{url('public/manager_template/modules/flag-icon-css/flags/4x3/id.svg')}}" alt="image" width="40">
                                        <div class="media-body ml-3">
                                            <div class="media-title">Indonesia</div>
                                            <div class="text-small text-muted">3,486 <i class="fas fa-caret-up text-success"></i></div>
                                        </div>
                                    </li>
                                    <li class="media">
                                        <img class="img-fluid mt-1 img-shadow" src="{{url('public/manager_template/modules/flag-icon-css/flags/4x3/ps.svg')}}" alt="image" width="40">
                                        <div class="media-body ml-3">
                                            <div class="media-title">Palestine</div>
                                            <div class="text-small text-muted">3,182 <i class="fas fa-caret-up text-success"></i></div>
                                        </div>
                                    </li>
                                    <li class="media">
                                        <img class="img-fluid mt-1 img-shadow" src="{{url('public/manager_template/modules/flag-icon-css/flags/4x3/de.svg')}}" alt="image" width="40">
                                        <div class="media-body ml-3">
                                            <div class="media-title">Germany</div>
                                            <div class="text-small text-muted">2,317 <i class="fas fa-caret-down text-danger"></i></div>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div> 
        <div class="row">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">
                        <h4>Invoices</h4>
                        <div class="card-header-action">
                            <a href="#" class="btn btn-danger">View More <i class="fas fa-chevron-right"></i></a>
                        </div>
                    </div>
                    <div class="card-body p-0">
                        <div class="table-responsive table-invoice">
                            <table class="table table-striped">
                                <tr>
                                    <th>Invoice ID</th>
                                    <th>Customer</th>
                                    <th>Status</th>
                                    <th>Due Date</th>
                                    <th>Action</th>
                                </tr>
                                <tr>
                                    <td><a href="#">INV-87239</a></td>
                                    <td class="font-weight-600">Kusnadi</td>
                                    <td>
                                        <div class="badge badge-warning">Unpaid</div>
                                    </td>
                                    <td>July 19, 2018</td>
                                    <td>
                                        <a href="#" class="btn btn-primary">Detail</a>
                                    </td>
                                </tr>
                                <tr>
                                    <td><a href="#">INV-48574</a></td>
                                    <td class="font-weight-600">Hasan Basri</td>
                                    <td>
                                        <div class="badge badge-success">Paid</div>
                                    </td>
                                    <td>July 21, 2018</td>
                                    <td>
                                        <a href="#" class="btn btn-primary">Detail</a>
                                    </td>
                                </tr>
                                <tr>
                                    <td><a href="#">INV-76824</a></td>
                                    <td class="font-weight-600">Muhamad Nuruzzaki</td>
                                    <td>
                                        <div class="badge badge-warning">Unpaid</div>
                                    </td>
                                    <td>July 22, 2018</td>
                                    <td>
                                        <a href="#" class="btn btn-primary">Detail</a>
                                    </td>
                                </tr>
                                <tr>
                                    <td><a href="#">INV-84990</a></td>
                                    <td class="font-weight-600">Agung Ardiansyah</td>
                                    <td>
                                        <div class="badge badge-warning">Unpaid</div>
                                    </td>
                                    <td>July 22, 2018</td>
                                    <td>
                                        <a href="#" class="btn btn-primary">Detail</a>
                                    </td>
                                </tr>
                                <tr>
                                    <td><a href="#">INV-87320</a></td>
                                    <td class="font-weight-600">Ardian Rahardiansyah</td>
                                    <td>
                                        <div class="badge badge-success">Paid</div>
                                    </td>
                                    <td>July 28, 2018</td>
                                    <td>
                                        <a href="#" class="btn btn-primary">Detail</a>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div> 
        </div>
    </section>
</div>
            <footer class="main-footer">
                <div class="footer-right">
                    System Crafted By <a href="https://inlancer.in" target="_blank">Inlancer Tech Company</a>
                </div>
                <div class="footer-left">
                    Copyright &copy; {{ date('Y')}} inlancer
                </div>
            </footer>
        </div> 
         
    </div>
     
    
    <script language="javascript">APPLICATION_URL="{{url('/')}}"</script>  












    <!-- General JS Scripts -->
    <script src="{{ asset('public/manager_template/modules/popper.js')}}"></script>
    <script src="{{ asset('public/manager_template/modules/tooltip.js')}}"></script>
    <script src="{{ asset('public/manager_template/modules/bootstrap/js/bootstrap.min.js')}}"></script>
    <script src="{{ asset('public/manager_template/modules/nicescroll/jquery.nicescroll.min.js')}}"></script>
    <script src="{{ asset('public/manager_template/modules/moment.min.js')}}"></script>
    <script src="{{ asset('public/manager_template/js/stisla.js')}}"></script>
    
 
    <script src="{{ asset('public/manager_template/js/scripts.js')}}"></script>
    <script src="{{ asset('public/manager_template/js/custom.js')}}"></script>
     
    
</body>
 
</html> 
 